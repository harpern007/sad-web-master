import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Prioridad } from '../models/Prioridad';

@Injectable({
  providedIn: 'root'
})
export class PrioridadService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  ListarPrioridad() {

    return this.http.get<Prioridad[]>(`${this.url}/prioridad/ListarPrioridadPorTipo`);
    
  }

  ListarPrioridadPorFicha(codTipo:string)
  {

    return this.http.get<Prioridad[]>(`${this.url}/prioridad/listarPrioridadPorFicha/${codTipo}`);

  }

}
