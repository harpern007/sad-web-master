import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Distrito } from 'src/app/models/Distrito';
import { Departamento } from 'src/app/models/Departamento';
import { Provincia } from 'src/app/models/Provincia';
import { Host1 } from 'src/app/general/variables';
@Injectable({
  providedIn: 'root'
})
export class UbigeoService {

  url: String = `${Host1}`;
  constructor(protected http: HttpClient) { }
  obtenerDistritosXProv(idProvincia: string) {
    return this.http.get<Distrito[]>(`${this.url}/distrito/listarDistritoPorProvincia/${idProvincia}`);
  }

  obtenerDepartamentosAll() {
    return this.http.get<Departamento[]>(`${this.url}/departamento/listarDepartamento`);
  }

  obtenerProvinciasXDep(idDepart: string) {
    return this.http.get<Provincia[]>(`${this.url}/provincia/listarProvinciaPorDepartamento/${idDepart}`);
  }
  obtenerProvinciasXUbigeo(idUbigeo: string) {
    return this.http.get<Provincia[]>(`${this.url}/provincia/obtenerProvinciaPorUbigeo/${idUbigeo}`);
  }
}
