import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Departamento } from '../models/Departamento';

@Injectable({
  providedIn: 'root'
})
export class DepartamentoService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  obtenerDepartamento() {
    return this.http.get<Departamento[]>(`${this.url}/ubigeo/listarDepartamento`);
  }
  
}
