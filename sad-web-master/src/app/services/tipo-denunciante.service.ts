import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { TipoDenunciante } from '../models/TipoDenunciante';

@Injectable({
  providedIn: 'root'
})
export class TipoDenuncianteService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;
  listarTipoDenunciante() {
    return this.http.get<TipoDenunciante[]>(`${this.url}/tipoDenunciante/listarTipoDenunciante`);
  }
}
