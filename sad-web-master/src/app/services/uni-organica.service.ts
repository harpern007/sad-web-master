import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HostInfobras, Host1 } from '../general/variables';
import { UniOrganica } from '../models/UniOrganica';

@Injectable({
  providedIn: 'root'
})

export class UniOrganicaService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  ListarUnidadOrganicaActiva() {
    return this.http.get<UniOrganica[]>(`${this.url}/admParam/listarUnidadOrganicaActiva`);
  }

  
}