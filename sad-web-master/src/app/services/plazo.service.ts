import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Plazo } from '../models/Plazo';

@Injectable({
  providedIn: 'root'
})
export class PlazoService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  ListarPlazo() {

    return this.http.get<Plazo[]>(`${this.url}/plazo/listarPlazo`);
    
  }
  
}