import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { DocumentoDet } from '../models/DocumentoDet';
import { ActualizarDet } from '../models/DocumentoCab';

@Injectable({
  providedIn: 'root'
})
export class DocumentoDetService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  RegistrarListaEvidencia(actualizarDet:ActualizarDet) {

    //return this.http.get<DocumentoDet[]>(`${this.url}/documentoDetalle/registrarListaEvidencia/${idDepartamento}/${idProvincia}`);

    return this.http.post<ActualizarDet>(`${this.url}/documentoDetalle/registrarListaEvidencia`,actualizarDet);

    
  }
  
}