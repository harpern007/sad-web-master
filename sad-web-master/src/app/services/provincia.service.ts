import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Provincia } from '../models/Provincia';

@Injectable({
  providedIn: 'root'
})
export class ProvinciaService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  obtenerProvincia(idDepartamento: string ) {

    return this.http.get<Provincia[]>(`${this.url}/ubigeo/listarProvincia/${idDepartamento}`);
    
  }
  
}