import { Injectable } from '@angular/core';
import { HostPayaraInspira } from 'src/app/general/variables';
import { EvaluacionPreliminar } from 'src/app/models/BPM/evaluacionPreliminar';
import { HttpClient } from '@angular/common/http';
import { Oficio } from 'src/app/models/BPM/oficio';
@Injectable({
  providedIn: 'root'
})
export class EvaluacionPreliminarService {

  constructor(protected http: HttpClient) { }

  url: String = `${HostPayaraInspira}`;

  inciarProcesoEvaluacionPreliminar(evaluacion : EvaluacionPreliminar) {

    return this.http.post<EvaluacionPreliminar>(`${this.url}/restBPMCGR/BpmInstance/generarNuevaInstancia`,evaluacion);

    //return this.http.post<Fur>(`http://10.23.2.52:8080/restSAD/fur/registrarFur`,fur);

    //http://11.162.109.173:8080/restSAD/fur/registrarFur/{fur}

  }

  generarOficio(oficio : Oficio){

    //CHEKAR URL DE RESPUESTA

    return this.http.post<EvaluacionPreliminar>(`${this.url}/restBPMCGR/oficioCGR/generarOficio`,oficio);
   // http://10.23.3.2:8080/restBPMCGR/oficioCGR/generarOficio
  }
}
