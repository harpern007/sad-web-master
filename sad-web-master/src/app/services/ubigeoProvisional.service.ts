import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HostInfobras } from '../general/variables';
import { Departamento } from '../models/Departamento';
import { Provincia } from '../models/Provincia';
import { Distrito } from '../models/Distrito';

@Injectable({
  providedIn: 'root'
})

export class UbigeoProvisionalService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${HostInfobras}`;

  obtenerDepartamento() {
    return this.http.get<Departamento[]>(`${this.url}/INFOBRAS/getDepartamento`);
  }

  obtenerProvincia() {
    return this.http.get<Provincia[]>(`${this.url}/INFOBRAS/getProvincia`);
  }

  obtenerDistrito() {
    return this.http.get<Distrito[]>(`${this.url}/INFOBRAS/getDistrito`);
  }
  
}