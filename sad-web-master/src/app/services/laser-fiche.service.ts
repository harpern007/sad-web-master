import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HostLaserFiche } from '../general/variables';
import { LaserFiche } from '../models/LaserFiche';

@Injectable({
  providedIn: 'root'
})
export class LaserFicheService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${HostLaserFiche}`;

  UploadLaserFiche(laserfiche:LaserFiche) {

    return this.http.post<LaserFiche>(`${this.url}/UploadLaserfiche`,laserfiche);

    //return this.http.post<LaserFiche>(`${this.url}/UploadLaserficheJVF`,laserfiche);

  }

}
