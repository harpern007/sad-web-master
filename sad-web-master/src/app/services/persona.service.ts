import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Persona } from '../models/Persona';

@Injectable({
  providedIn: 'root'
})

export class PersonaService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  obtenerPersona( tipoDocumento : string,dscDNI: string) {

    return this.http.get<Persona>(`${this.url}/persona/obtenerPersonaPorDocIdent/${tipoDocumento}/${dscDNI}`);

  }
}
