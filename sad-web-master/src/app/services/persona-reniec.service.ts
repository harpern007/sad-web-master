import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HostReniec } from '../general/variables';
import { PersonaReniec } from '../models/PersonaReniec';

@Injectable({
  providedIn: 'root'
})

export class PersonaReniecService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${HostReniec}`;

  obtenerPersonaReniec(dscDNI: string) {
    return this.http.get<PersonaReniec>(`${this.url}/Persona/listarDocument/${dscDNI}`);
  }
}
