import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { PersonalCgr } from '../models/PersonalCgr';

@Injectable({
  providedIn: 'root'
})
export class PersonalCgrService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  ListarPersonalCgrActivoPorSuperv(codSuperv:string) {

    return this.http.get<PersonalCgr[]>(`${this.url}/admParam/listarPersonalCgrActivoPorSuperv/${codSuperv}`);
    
  }

  ListarPersonalCgrActivo() {

    return this.http.get<PersonalCgr[]>(`${this.url}/admParam/listarPersonalCgrActivo/`);
    
  }

  ListarSupervisorCgrActivo() {

    return this.http.get<PersonalCgr[]>(`${this.url}/admParam/listarSupervisorCgrActivo/`);
    
  }

  ObtenerPersonalCgrPorCodigo(codPersCgr:string) {

    return this.http.get<PersonalCgr>(`${this.url}/admParam/obtenerPersonalCgrPorCodigo/${codPersCgr}`);
    
  }
}
