import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HostSeguridad, wcfClaveAUT, wcfTipoCLAVE, wcfUsuarioAUT, HostSeguridad2 } from 'src/app/general/variables';
import { DatosToken } from 'src/app/models/personalizados/seguridad/DatosToken';
import { SeguridadVerificar } from 'src/app/models/personalizados/seguridad/SeguridadVerificar';

@Injectable({
  providedIn: 'root'
})

export class SeguridadService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${HostSeguridad}`;
  url2:string =`${HostSeguridad2}`;
  datosToken:DatosToken ;//= new DatosToken();

  generarToken() {

    // const headers: HttpHeaders = new HttpHeaders({
    //     "content-type" : "xml"
    // });

    // let params = new HttpParams().set('WcfClaveAUT',"+4bwPY3J+anV04YGmtuI9FOyDtOnqMgXYwwQCshAgnWH5tlwoqrJYFUPOovoiash").set('WcfTipoCLAVE',"T").set("WcfUsuarioAUT","SVIAW00001");

    this.datosToken = new DatosToken();//= new DatosToken();

    this.datosToken.wcfClaveAUT = wcfClaveAUT;
    this.datosToken.wcfTipoCLAVE = wcfTipoCLAVE;
    this.datosToken.wcfUsuarioAUT = wcfUsuarioAUT;

    return this.http.post<DatosToken>(`${this.url}/Seguridad/GenerarToken`,this.datosToken);
  }

  accesoVerificar(seguridadVerificar:SeguridadVerificar) {

    // console.log(`${this.url2}/Seguridad/AccesoVerificar`);
    // console.log(seguridadVerificar);
    // console.log("============0ejecutar");

    return this.http.post<DatosToken>(`${this.url2}/Seguridad/AccesoVerificar`,seguridadVerificar);
  }

}