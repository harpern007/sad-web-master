import { Injectable } from '@angular/core';
import { HostReniec, HostSunat, HostInfobras } from 'src/app/general/variables';
import { HttpClient } from '@angular/common/http';
import { EventoInfobra } from 'src/app/models/EventoInfobra';
import { EjecucionInfobra } from 'src/app/models/EjecucionInfobra';

@Injectable({
  providedIn: 'root'
})
export class ExternosService {


  url: String = `${HostReniec}`;
  urlSunat : String = `${HostSunat}`;
  urlInfobras : String = `${HostInfobras}`;
  constructor(protected http: HttpClient) { }

  obtenerPersonaXDniReniec(dni : string){
    return this.http.get<any[]>(`${this.url}/Persona/listarDocument/${dni}`);
  }

  obtenerEmpresaXRUC(ruc : string){
    return this.http.get<any>(`${this.urlSunat}/Ruc/listarRuc/${ruc}`)
  }

  obtenerEmpresaXRazonSocial(razonSocial : string){
    return this.http.get<any>(`${this.urlSunat}/Ruc/listarRazonSocial/${razonSocial}`)
  }


  //Infoobras
  //http://10.23.3.2:8080/servicesINFOBRA/INFOBRAS/getEjecucion
  //http://10.23.3.2:8080/servicesINFOBRA/INFOBRAS/getEvento
}
