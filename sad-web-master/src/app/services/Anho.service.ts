import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Anho } from '../models/Anho';

@Injectable({
  providedIn: 'root'
})

export class AnhoService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  listarAnho() {

    return this.http.get<Anho[]>(`${this.url}/anho/listarAnho`);

  }
  
}
