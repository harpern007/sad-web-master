import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Canal } from '../models/Canal';

@Injectable({
  providedIn: 'root'
})
export class CanalService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  listarCanalDocumento() {
    return this.http.get<Canal[]>(`${this.url}/canal/listarCanalDocumento`);
  }

  listarCanalServicioAtencion() {
    return this.http.get<Canal[]>(`${this.url}/canal/listarCanalServicioAtencion`);
  }

  listarCanal() {
    return this.http.get<Canal[]>(`${this.url}/canal/listarCanal`);
  }


  
}
