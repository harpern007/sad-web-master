import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Distrito } from '../models/Distrito';

@Injectable({
  providedIn: 'root'
})
export class DistritoService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  obtenerDistrito(idProvincia: string, idDepartamento: string) {
    return this.http.get<Distrito[]>(`${this.url}/ubigeo/listarDistrito/${idDepartamento}/${idProvincia}`);
  }
  
}