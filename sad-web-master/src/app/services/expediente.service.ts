import { Injectable } from '@angular/core';
import { ExpedienteSGD } from '../models/ExpedienteSGD';
import { HttpClient } from '@angular/common/http';
import { Host1, HostExpediente } from '../general/variables';
import { FiltroExpediente } from '../models/personalizados/FiltroExpediente';
import { Fur } from '../models/Fur';

@Injectable({
  providedIn: 'root'
})
export class ExpedienteService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${HostExpediente}`;

  listarExpedientes(filtroExpediente : FiltroExpediente) {
    return this.http.post<ExpedienteSGD[]>(`${this.url}/getNumeroXAnioExpediente/`,filtroExpediente);
  }

  listarExpedientesSAD(filtroExpediente : FiltroExpediente){
    return this.http.get<Fur[]>(`${this.url}/getNumeroXAnioExpediente/${filtroExpediente.nu_expediente}/${filtroExpediente.anio_expediente}`);
  }

}
