import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { HojaEvaluacion } from '../models/HojaEvaluacion';
import { ObservacionDetalle } from '../models/observacionDetalle';

@Injectable({
  providedIn: 'root'
})

export class HojaEvaluacionService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  RegistrarHojaEvaluacion(hojaEvaluacion:HojaEvaluacion) {

    return this.http.post<HojaEvaluacion>(`${this.url}/hojaEvaluacion/registrarHojaEvaluacion`,hojaEvaluacion);

  }

  ObtenerHojaEvaluacionPorId(numIdHe:number) {

    return this.http.get<HojaEvaluacion>(`${this.url}/hojaEvaluacion/obtenerHojaEvaluacionPorId/${numIdHe}`);

  }

  ActualizarHojaEvaluacion(hojaEvaluacion:HojaEvaluacion) {

    return this.http.post<HojaEvaluacion>(`${this.url}/hojaEvaluacion/actualizarHojaEvaluacion`,hojaEvaluacion);

  }

  registrarObservaciones(listaObservacion : ObservacionDetalle[]){
    return this.http.post<ObservacionDetalle[]>(`${this.url}/obs/registrarObservacion`,listaObservacion);
    //http://10.23.2.52:8080/restSAD/obs/registrarObservacion
  }

  

  
}
