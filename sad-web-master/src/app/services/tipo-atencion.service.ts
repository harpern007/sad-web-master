import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { TipoDenunciante } from '../models/TipoDenunciante';
import { TipoAtencion } from '../models/TipoAtencion';

@Injectable({
  providedIn: 'root'
})
export class TipoAtencionService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;
  listarTipoAtencion() {
    return this.http.get<TipoAtencion[]>(`${this.url}/tipoAtencion/listarTipoAtencion`);
  }
}