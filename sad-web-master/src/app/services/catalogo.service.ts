import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Catalogo } from '../models/Catalogo';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  listarCatalogo(idTipoCatalogo: Number) {
    return this.http.get<Catalogo[]>(`${this.url}/catalogo/listarCatalogo/${idTipoCatalogo}`);
  }
  
}
