import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { TipoDocExp } from '../models/TipoDocExp';

@Injectable({
  providedIn: 'root'
})
export class TipoDocExpService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  ListarTipoDocumentoExpediente() {

    return this.http.get<TipoDocExp[]>(`${this.url}/tipoDocumentoExpediente/listarTipoDocumentoExpediente`);

  }
}
