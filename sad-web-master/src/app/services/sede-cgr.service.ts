import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { SedeCgr } from '../models/SedeCgr';

@Injectable({
  providedIn: 'root'
})
export class SedeCgrService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  listarSedeCgr() {
    return this.http.get<SedeCgr[]>(`${this.url}/admParam/listarSedeCgr`);
  }
}
