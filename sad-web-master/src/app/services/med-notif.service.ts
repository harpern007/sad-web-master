import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { MedioNotif } from '../models/MedioNotif';

@Injectable({
  providedIn: 'root'
})
export class MedioNotifService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  listarMedioNotif() {
    return this.http.get<MedioNotif[]>(`${this.url}//medioNotificacion/listarMedioNotificacion`);
  }
}
