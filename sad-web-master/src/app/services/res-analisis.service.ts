import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { ResAnalisis } from '../models/ResAnalisis';

@Injectable({
  providedIn: 'root'
})
export class ResAnalisisService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  ListarResultadoAnalisis() {

    return this.http.get<ResAnalisis[]>(`${this.url}/resultadoAnalisis/listarResultadoAnalisis`);
    
  }
  
}