import { Injectable } from '@angular/core';
import { Host1 } from 'src/app/general/variables';
import { HttpClient } from '@angular/common/http';
import { TipoHecho } from 'src/app/models/TipoHecho';

import { Cargo } from 'src/app/models/Cargo';
import { Departamento } from 'src/app/models/Departamento';
import { Distrito } from 'src/app/models/Distrito';
import { Provincia } from 'src/app/models/Provincia';
import { Mes } from 'src/app/models/mes';
import { Anho } from 'src/app/models/Anho';
import { Estado } from 'src/app/models/Estado';
import { TipoDocumento } from 'src/app/models/TipoDocumento';

@Injectable({
  providedIn: 'root'
})
export class MaestrasService {

  
  url: String = `${Host1}`;
  constructor(protected http: HttpClient) { }




  obtenerTipoDocumento(){
    return this.http.get<TipoDocumento[]>(`${this.url}/tipoDocumento/listarTipoDocumento`);
  }

  obtenerCargo(){
    return this.http.get<Cargo[]>(`${this.url}/cargo/listarCargo`);
  }

  obtenerDepartamentos(){
    return this.http.get<Departamento[]>(`${this.url}/ubigeo/listarDepartamento`);
  }

  obtenerProvincias(codDep : string ){
    return this.http.get<Provincia[]>(`${this.url}/ubigeo/listarProvincia/${codDep}`);
  }

  obtenerDistritos(codDep : string, codProv : string){
    console.log(codProv);
    return this.http.get<Distrito[]>(`${this.url}/ubigeo/listarDistrito/${codDep}/${codProv}`);
  }

  obtenerTipoHecho(){
    return this.http.get<TipoHecho[]>(`${this.url}/tipoHecho/listarTipoHecho/`);
  }

  obtenerMes(){
    return this.http.get<Mes[]>(`${this.url}/mes/listarMes`);
  }

  obtenerAnho(){
    return this.http.get<Anho[]>(`${this.url}/anho/listarAnho`);
  }

  obtenerEstado(){
    return this.http.get<Estado[]>(`${this.url}/estado/listarEstado`)
  }

  listarEstadoPorFicha(strCodTipFicha:string){
    return this.http.get<Estado[]>(`${this.url}/estado/listarEstadoPorFicha/${strCodTipFicha}`)
  }


}
