import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Parametro } from '../models/Parametro';


@Injectable({
  providedIn: 'root'
})
export class ObservacionService {

  constructor(protected http: HttpClient) { }


  url: String = `${Host1}`;

  registrarObservacion(idParametro: Number) {
    return this.http.get<Parametro>(`${this.url}/parametro/obtenerParametroPorID/${idParametro}`);
  }

  
}
