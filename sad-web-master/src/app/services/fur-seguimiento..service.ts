import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { FurSeguimiento } from '../models/FurSeguimiento';
import { FurCabSeguimiento } from '../models/FurCabSeguimiento';
import { FurSeguimientoFiltro } from '../models/FurSeguimientoFiltro';
import { Fur } from '../models/Fur';

@Injectable({
  providedIn: 'root'
})

export class FurSeguimientoService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  //ListarGCSeguimiento(idCanal:number, codFur:string, fchIni:string, fchFin:string,flgAsignado:boolean,codDep:string) {
  ListarGCSeguimiento(furSeguimientoFiltro:FurSeguimientoFiltro){
    

    return this.http.post<Fur[]>(`${this.url}/fur/listarFurSeguimiento`,furSeguimientoFiltro);

  }

  ObtenerCantidadFurDashboard(codDep:string) {

    return this.http.get<FurCabSeguimiento>(`${this.url}/fur/obtenerFurDashBoardPorDepart/${codDep}`);

  }

  obtenerCantidadFurDashboard(codDep : string ){
    return this.http.get<number>(`${this.url}/fur/obtenerCantidadFUR/${codDep}`);
  }
  
  obtenerFurDashboard(furSeguimientoFiltro : FurSeguimientoFiltro){
    return this.http.post<Fur[]>(`${this.url}/fur/listarFurSeguimientoCodTipo`,furSeguimientoFiltro);
  }
}