import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from 'src/app/general/variables';
import { Entidad } from 'src/app/models/Entidad';

@Injectable({
  providedIn: 'root'
})
export class EntidadService {

  url: String = `${Host1}`;
  constructor(protected http: HttpClient) { }

  obtenerEntidades() {
    return this.http.get<Entidad[]>(`${this.url}/entidad/listar`);
  }

  obtenerEntidadesxDistrito(idDistrito: string) {
    return this.http.get<Entidad[]>(`${this.url}/entidad/listarEntidadPorDistrito/${idDistrito}`);
  }

  obtenerEntidadesxProvincia(idProvincia: string) {
    return this.http.get<Entidad[]>(`${this.url}/entidad/listarEntidadPorProvincia/${idProvincia}`);
  }

  obtenerEntidadesGeneral() {
    return this.http.get<Entidad[]>(`${this.url}/entidad/listarEntidadPorDistrito/150101`);
  }

  // TO DO. se va a cambiar por el servicio de SUNAT.
  obtenerEmpresaDenunciante(idRUC: string) {
    return this.http.get<Entidad[]>(`${this.url}/entidad/listarEntidadPorRUC/${idRUC}`);
  }

  obtenerEntidadesXNombre(razonSocial: string) {
    return this.http.get<Entidad[]>(`${this.url}/entidad/listarEntidadPorNombre/${razonSocial}`);
  }

  obtenerEntidadParaCombo() {
    return this.http.get<Entidad[]>(`${this.url}/entidad/listarEntidadAcortado`);
  }

  obtenerEntidadXCodigo(idEntidad: string) {
    return this.http.get<Entidad>(`${this.url}/entidad/obtenerEntidadPorCodigo/${idEntidad}`);
  }
}
