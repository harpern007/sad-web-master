import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Fur } from '../models/Fur';

@Injectable({
  providedIn: 'root'
})

export class FurService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  RegistrarFur(fur:Fur) {

    return this.http.post<Fur>(`${this.url}/fur/registrarFur`,fur);

    //return this.http.post<Fur>(`http://10.23.2.52:8080/restSAD/fur/registrarFur`,fur);

    //http://11.162.109.173:8080/restSAD/fur/registrarFur/{fur}

  }

  ActualizarFur(fur:Fur) {

    console.log(fur);
    
    return this.http.post<Fur>(`${this.url}/fur/actualizarFur`,fur);

    //return this.http.post<Fur>(`http://10.23.2.52:8080/restSAD/fur/registrarFur`,fur);

  }

  ObtenerFurPorCodigo(codFur:string) {

    return this.http.get<Fur>(`${this.url}/fur/obtenerFurPorCodigo/${codFur}`);

    //http://11.162.109.173:8080/restSAD/FUR/obtenerFurPorCodigo/{codigo}

  }

  ObtenerFurPorId(idFur:number) {

    return this.http.get<Fur>(`${this.url}/fur/obtenerFurPorId/${idFur}`);

  }
  
}