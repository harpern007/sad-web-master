import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Estado } from '../models/Estado';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  listarEstadoPorFicha(codigo: string) {
    return this.http.get<Estado[]>(`${this.url}/estado/listarEstadoPorFicha/${codigo}`);
  }

  listarEstado() {
    return this.http.get<Estado[]>(`${this.url}/estado/listarEstado`);
  }

}
