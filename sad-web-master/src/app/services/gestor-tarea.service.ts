import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Gestortarea } from '../models/GestorTarea';

@Injectable({
  providedIn: 'root'
})

export class GestorTareaService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  ListarCargaTareaGestorPorSuperv(codSuperv:string) {

    return this.http.get<Gestortarea[]>(`${this.url}/admParam/listarCargaTareaGestorPorSuperv/${codSuperv}`);

  }
  
}