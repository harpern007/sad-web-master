import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { TipoDocumento } from '../models/TipoDocumento';

@Injectable({
  providedIn: 'root'
})
export class TipoDocumentoService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;
  obtenerTipoDocumento() {
    return this.http.get<TipoDocumento[]>(`${this.url}/tipoDocumento/listarTipoDocumento`);
  }
}
