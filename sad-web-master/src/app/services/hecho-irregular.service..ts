import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { ObraHechoIrregular } from '../models/ObraHechoIrregular';

@Injectable({
  providedIn: 'root'
})

export class HechoIrregularService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  ListarHechoIrregular(codInfobras:string) {

    return this.http.get<ObraHechoIrregular[]>(`${this.url}/hechoirregular/ListarHechoIrregularObr/${codInfobras}`);

  }
  
}