import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host1 } from '../general/variables';
import { Parametro } from '../models/Parametro';


@Injectable({
  providedIn: 'root'
})
export class ParametroService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${Host1}`;

  obtenerParametro(idParametro: Number) {
    return this.http.get<Parametro>(`${this.url}/parametro/obtenerParametroPorID/${idParametro}`);
  }
}
