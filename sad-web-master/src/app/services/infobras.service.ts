import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {HostInfobras, Host1 } from '../general/variables';
import { Infobras } from '../models/Infobras';
import { InfobraConsulta } from '../models/personalizados/InfobraConsulta';
import { EjecucionInfobra } from '../models/EjecucionInfobra';
import { EventoInfobra } from '../models/EventoInfobra';

@Injectable({
  providedIn: 'root'
})

export class InfobrasService {

  constructor(protected http: HttpClient) { }

  // tslint:disable-next-line: ban-types
  url: String = `${HostInfobras}`;
  urlInfobras : String = `${HostInfobras}`;
  ListarInfobras(infobras : Infobras) {

    return this.http.get<Infobras[]>(`${this.urlInfobras}/INFOBRAS/getListado/${infobras.departamento}/${infobras.provincia}/${infobras.distrito}`);

  }

  listarInfobras(infobra : InfobraConsulta){
    return this.http.post<any[]>(`${this.urlInfobras}/INFOBRAS/getListado/`,infobra);
    // http://11.162.109.173:8080/servicesINFOBRA/INFOBRAS/getListado/
  }
  obtenerEjecucionObras(){
    return this.http.get<EjecucionInfobra[]>(`${this.urlInfobras}/INFOBRAS/getEjecucion`);//getEstado
  }

  //getEjecucion

  obtenerEventos(){
    return this.http.get<EventoInfobra[]>(`${this.urlInfobras}/INFOBRAS/getEvento`);
  }
}