export class MailDetalle{
    nbpmmaildetId : number;
    nbpmmailIdmail : number;
    nbpmmailIntento : number;
    cbpmmailIdcodtip : string;
    nbpmmailIdtipo : number;
    cbpmmaildetRemitente : string;
    cbpmmaildetEstado : string;
    cbpmmaildetDestinatario : string;
    cbpmmaildetContenido : string;
    cbpmmaildetAsunto : string; 
}