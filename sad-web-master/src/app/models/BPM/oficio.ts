export class Oficio{
    
        rutaArchivoPlantilla: string;//"C:\\\\archivos\\\\",
        nombreArchivoPlantilla: string; //"OFICIO.DOCX",
        rutaArchivoGenerado :string; //"C:/archivos/",
        nombreArchivoGenerado : string;//"oficioGeneradoCGR.docx",
        asunto : string;// "DENUNCIA",
        cargo : string; //"SUPERVVISOR",
        cargo_EMP_ENC : string;// "ANALISTA",
        contenido : string; //"En ese sentido, en virtud a lo dispuesto en el artículo 8° de la Ley N° 29542 “Ley de Protección al Denunciante en el Ámbito Administrativo y de Colaboración Eficaz en el Ámbito Penal” y en el artículo 10° de su reglamento, aprobado mediante Decreto Supremo N° 038-2011-PCM, los cuales establecen la competencia del (Ministerio del Trabajo y Promoción del Empleo / las Direcciones Regionales de Trabajo y Promoción del Empleo) para realizar inspecciones laborales cuando el trabajador denunciante es objeto de represalias que se materializan en actos de hostildad, independientemente del régimen laboral en el que se encuentre ; se remite a su Despacho, fotocopia de la documentación alcanzada por el mencionado por el mencionado denunciante, en los folios, para las acciones correspondientes y posterior remisión a este Órgano Superiror de Control de la actas o informes conteniendo el resultado de la actuación de inspección, en cumplimiento de lo señalado en el literal e) del artículo 10° del mencinado reglamento.",
        datos_EMISION_CGR : string;// "CGR-123456",
        des_ENTIDAD : string;// "SUNAT",
        direccion_DESTINATARIO : string;// "MZ. C13 LT. 34 Bocanegra-Callao",
        empleado_EMITE : string; //"Congresita",
        iniciales_EMP : string;// "USIL",
        nombre_ANIO : string; //"DAVC-1993",
        nombre_DESTINATARIO : string;//"Alexander Valderrama",
        pie_PAGINA : string ;// "Contraloria General",
        post_FIRMA : string;// "Firmado",
        referencia : string; //"http://192.168.0.1:9000/Hola Mundo",
        sigla_DOC : string;//"ASDQWEZXC-123456",
        ubigeo_DESTINATARIO : string; //"Lima-San Isidro-Navarrete",
        uuoo_DESTINO : string //"DASDASDASDASDASD-Callao";
      
}