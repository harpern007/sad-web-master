import { Persona } from './Persona';

export class FUDXEmpresaInvolucrada{
    cfudxempinvDscempnoreg	:string;
    nfudxempinvId	:number;
    nfudxempinvIdempinv	:number;
    nfudxempinvIdfud	:number;
    persona	:Persona;
}