import { Persona } from './Persona';

export class FURXEmpresaInvolucrada {
    nfurxempinvId:number;
	nfurxempinvIdfur:number;
	nfurxempinvIdempinv:number;
    cfurxempinvDscempnoreg:string;
    persona:Persona;
}  