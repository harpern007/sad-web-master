import { DenunciadoXFicha } from './DenunciadoXFicha';
import { DocumentoDet } from './DocumentoDet';
import { HojaEvaluacion } from './HojaEvaluacion';
import { Denunciante } from './Denunciante';
import { Anexo } from './Anexo';
import { FurXObraPublica } from './FurXObraPublica';
import { FichaXInstancia } from './FichaXInstancia';
import { FURXEmpresaInvolucrada } from './FURXEmpresaInvolucrada';
import { Canal } from './Canal';
import { Estado } from './Estado';
import { ExpedienteDet } from './ExpedienteDet';
import { RecupPartesDocExp, RecupPartesFchInicio } from '../general/variables';
import { FichaExpediente } from './FichaExpediente';
import { MailDetalle } from './MailDetalle';

export class Fur{
    nfurId: number;
    cfurCodigo:string;
    nfurIdcanal:number;
    cfurCdepent:string;
    cfurCprovent:string;
    cfurCdistent:string;
    cfurCodent:string;
    nfurIdtipaten:number = 1; //SIEMPRE denuncia por defecto
    nfurIdtipinfcompl:number;
    nfurIdtipdenun:number;
    cfurIdusuasig:string;
    cfurIdususuperv:string;//CAMBIO POR DATOS CGR
    dfurFecregistro:Date;//PENDIENTE

    strDfurFecregistro : string;

    strDfurFecinicioreg:string;//PENDIENTE
    dfurFecasigna:string;
    cfurIdusureg:string;//CAMBIO POR DATOS CGR
    dfurFecinicioreg:Date;//PENDIENTE
    cfurFecinicioreg:string;//PENDIENTE
    nfurIdestadoformulario:number;
    cfurFlgdirocuhecho:string;
    nfurIdtipohecho:number;
    cfurDschecho:string;
    cfurFlgexismonto:string;
    cfurFlgotrentinst:string;
    cfurFlghdenconocu:string;
    cfurDscrefentidad:string;
    nfurIdbase:number;
    nfurPeriodoohini:number;
    nfurPeriodoohfin:number;
    cfurFlgdatfuninv:string;
    cfurFlgexispruh:string;
    cfurFlgreqidentif:string;
    nfurImpmontoperj:number;
    cfurFlgemontodesc:string;
    cfurFlgpruebahechos:string;
    cfurNroexpediente:string;
    nfurIdestado:number = 4;
    nfurIdprioridad:number = 9;//Provisional por defecto, veronica definirá las el criterio
    cfurFlgencobrapub:string;
    cfurDirentidad:string;
    listaDenunciadoPorFicha:DenunciadoXFicha[];//OK
    listaFurPorEmpresaInvolucrada: FURXEmpresaInvolucrada[]; //OK
    listaDocumentoAdjuntoDet:DocumentoDet[];//OK
    hojaEvaluacion: HojaEvaluacion;//PENDIENTE, OK PERO NO DEBE MATRICULAR SI NO SE ENVIA EL OBJETO
    denunciante: Denunciante;//OK 
    listaAnexo:Anexo[];//OK
    listaFurPorObraPublica: FurXObraPublica[];//OK
    listaFichaPorInstancia:FichaXInstancia[];//OK
    canal:Canal;
    estado:Estado;
    
    //CUS 01 - NUEVO
    //hora_inicio_reg:string = (this.strDfurFecinicioreg == null? "":this.strDfurFecinicioreg.substring(11));
    //fch_inicio_reg:string = (this.strDfurFecinicioreg == null? "":this.strDfurFecinicioreg.substring(0,10));

    get hora_inicio_reg(): string {
        // return (this.strDfurFecinicioreg == null? "":this.strDfurFecinicioreg.substring(11));
        return (this.strDfurFecinicioreg == null? "":RecupPartesFchInicio(this.strDfurFecinicioreg,1));
    }

    get fch_inicio_reg(): string {
        // return (this.strDfurFecinicioreg == null? "":this.strDfurFecinicioreg.substring(0,10));
        return (this.strDfurFecinicioreg == null? "":RecupPartesFchInicio(this.strDfurFecinicioreg,2));
    }

    get hora_fin_reg(): string {
        // return (this.strDfurFecregistro == null? "":this.strDfurFecregistro.substring(11));
        return (this.strDfurFecregistro == null? "":RecupPartesFchInicio(this.strDfurFecinicioreg,1));
    }

    get fch_fin_reg(): string {
        // return (this.strDfurFecregistro == null? "":this.strDfurFecregistro.substring(0,10));
        return (this.strDfurFecregistro == null? "":RecupPartesFchInicio(this.strDfurFecinicioreg,2));
    }

    cfurCodtipo:string = "REG"; //Por defecto es u n Registro, caso contrario FUR
    nfurIdplazo:number;
    nfurNumplazo:number;
    cfurCoduno:string;
    cfurCodperscgr1:string;
    cfurCodperscgr2:string;
    nfurIdtipodocexp:number;
    listaExpedienteDet:ExpedienteDet[];
    
    cfurDscregotr:string;
    cfurDscregotr2:string;
    dfurFchrecep:Date;
    strDfurFchrecep:string;
    cfurDscregotr3:string;
    cfurDscindica:string;
    nfurNumplazoasig:number;
    nfurIdplazoasig:number;

    //Para fur - agregar expediente
    //Correos notificación
    correoDetalle : MailDetalle;
    //CUS 04 - CUS 06
    listaFichaExpediente : FichaExpediente[];

    get num_doc_exp(): string {
        // return (this.cfurNroexpediente == null? "": this.cfurNroexpediente.substring(0,this.cfurNroexpediente.indexOf("-")));
        return (this.cfurNroexpediente == null? "": RecupPartesDocExp(this.cfurNroexpediente,1));
        
    }

    get num_doc_exp2(): string {
        // return (this.cfurNroexpediente == null? "": this.cfurNroexpediente.substring(this.cfurNroexpediente.indexOf("-") + 1,this.cfurNroexpediente.lastIndexOf("-")));
        return (this.cfurNroexpediente == null? "": RecupPartesDocExp(this.cfurNroexpediente,2));
    }

    get num_doc_exp3(): string {
        // return (this.cfurNroexpediente == null? "": this.cfurNroexpediente.substring(this.cfurNroexpediente.lastIndexOf("-") + 1));
        return (this.cfurNroexpediente == null? "": RecupPartesDocExp(this.cfurNroexpediente,3));
    }

    flgDeshabilitaEditar: boolean;
    numItem:number;
    //flgDeshabilitaAtender: boolean;

}