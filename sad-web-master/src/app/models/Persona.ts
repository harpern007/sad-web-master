export class Persona {
  npersonaId: number;
  cpersonaNrodoc: string;
  // tslint:disable-next-line: ban-types
  npersonaIdtipdoc: number;
  cpersonaApepaterno: string;
  cpersonaApematerno: string;
  cpersonaNombres: string;
  dpersonaFnacimiento: Date;
  cpersonaRazonsocial: string;
  cpersonaDscReferencia : string;
  //cpersonaNroDoc : String;
  cpersonaDscdireccion : string;
  cpersonaDscreprelegal : string;
  cpersonaCodProvincia : string;
  cpersonaCodDepartamento : string;
  cpersonaCodDistrito: string;
  // Ggsolis
  // cpersonaDscTelefono : string;
  // cpersonaDscMovil :string;
  // cpersonaDscCorreo: string;
  cpersonaDsctelef : string;
  cpersonaDscmovil : string;
  cpersonaDsccorreo : string;
  cpersonaPrinommater : string;
  dpersonaFemidoc : Date;
  strDpersonaFemidoc : string;
  strDpersonaFnacimiento : string;
  // FIN GSOLIS
    // npersona_id: Number;
    // npersona_idtipdoc: Number;
    // cpersona_nrodoc: string;
    // cpersona_apepaterno:string;
    // cpersona_apematerno:string;
    // cpersona_nombres:string;
    // dpersona_fnacimiento:Date;
    // cpersona_razonsocial:string;
    // cpersona_dscreferencia:string;
    // cpersona_dscdireccion:string;
    // cpersona_dscreprelegal:string;
    // cpersona_cdepartdomic:string;
    // cpersona_cprovdomic:string;
    // cpersona_cdistdomic:string;
    // cpersona_dsctelef:string;
    // cpersona_dscmovil:string;
    // cpersona_dsccorreo:string;
     //cpersona_prinommater:string;
    // dpersona_femidoc:Date;

}
  