import { DocumentoDet } from './DocumentoDet';
import { NumIdEstadoHePendiente, CodDEM } from '../general/variables';
import { ObservacionDetalle } from './observacionDetalle';

export class HojaEvaluacion{
    nhojaevalId:number;
    dhojaevalFregistro:Date;
    dhojaevalFregini:Date;

    strDhojaevalFregistro:string;
    strDhojaevalFregini:string;

    chojaevalNroexpediente:string;
    nhojaevalIdfur:number;
    nhojaevalIdresanalisis:number;
    chojaevalDscconclusion1:string;
    chojaevalDscconclusion2:string;
    chojaevalFlgcompsnc:string;
    nhojaevalIdtipinfo:number;
    nhojaevalIdtipodenuncia:number;
    chojaevalFlginfcompl:string;
    chojaevalFlginfprep:string;
    chojaevalCodosnad:string;
    chojaevalCodresevalden:string;
    nhojaevalIdreseval:number;
    chojaevalComentarios:string;
    nhojaevalIdusuasig:number;
    chojaevalDscindicaciones:string;
    nhojaevalNumplazo:number;
    nhojaevalIdplazo:number;
    chojaevalFlgconfentpub:string;
    chojaevalFlgconfper:string;
    chojaevalFlgconfhip:string;
    chojaevalFlgconfdpci:string;
    nhojaevalIdcalfur:number;
    nhojaevalIdnexocaus:number;
    nhojaevalIdtippedesp:number;
    nhojaevalIdconclu:number;
    nhojaevalIdrecomen:number;
    chojaevalFlgisnfoval:string;
    chojaevalFlginfosuf:string;
    nhojaevalIdfud:number;
    chojaevalDscobjgen:string;
    nhojaevalIdestado:number = NumIdEstadoHePendiente;
    chojaevalCodtipo:string;
    listaDocumentoDetalle:DocumentoDet[];//OK
    chojaevalCodsnc:string;
    chojaevalCoddem:string = CodDEM;
    chojaevalCodformatocgr:string;
    chojaevalCodusureg:string;
    chojaevalCodsupervder:string;
    
    /*CUS07*/
    chojaevalFlgtipoantec : string;
    chojaevalFlgexisantec : string;
    chojaevalTipodenun : string;
    chojaevalTipinform : string;
    listaObservacionDetalle : ObservacionDetalle[];
    
}