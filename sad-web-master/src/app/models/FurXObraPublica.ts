import { ObraPublica } from './ObraPublica';
import { FurXObraPublicaXHi } from './FurXObraPublicaXHi';

export class FurXObraPublica{
    nfurxobrpubId:number;
    nfurxobrpubIdfur:number;
    nfurxobrpubIdobrpub:number;
    obraPublica: ObraPublica;
    listaFurPorObraPubPorHechoIrr: FurXObraPublicaXHi[];
}