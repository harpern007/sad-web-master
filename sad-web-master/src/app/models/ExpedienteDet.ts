//import { ConvertStrDateToFormatBD } from '../general/variables';

export class ExpedienteDet{
    nexpdetId:number;
    nexpdetIdfur:number;
    cexpdetCodtipo:string;
    cexpdetCodanho:string;
    nexpdetIdtipdoc:number;
    cexpdetNrodoc:string;
    cexpdetNroexp:string;
    dexpdetFecha:Date;
    strDexpdetFecha:string;
    cexpdetCoduno:string;
    cexpdetCodlocal:string;
    cexpdetCodpersonal:string;
    cexpdetCodresp:string;
    cexpdetDscindica:string;
    nexpdetIdprioridad:number;

    // get fch_registro(): string { // se comenta por que hay problemas de prevención al setear datos
    //     return (this.strDexpdetFecha == null? "" : ConvertStrDateToFormatBD(this.strDexpdetFecha));
    // }

    // get fch_registro2(): string {
    //     return (this.strDexpdetFecha == null? "" : ConvertDateToStr(this.dexpdetFecha));
    // }

    // get strDexpdetFecha2(): string {
    //     return (this.dexpdetFecha == null? "" : ConvertDateTimeToStr(this.dexpdetFecha));
    // }

    

}