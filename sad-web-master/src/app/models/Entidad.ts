export class Entidad {
  centCodigo: string;
  centNombre: string;
  centRuc: string;
  centDirecc: string;
  centDirdep: string;
  centDirpro: string;
  centDirdis: string;
  centDirRef:string;
}
