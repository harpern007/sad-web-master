export class ExpedienteSGD{
    nu_expediente:string;
    fecha_expediente:string;
    fecha_documento:string;
    anio_expediente:string;
    codigo_uuoo_emisor : string;
    de_uuoo_emisor : string;
    co_cargo_publico : string;
    de_cargo_publico : string;
    co_tipo_remitente : string;
    de_tipo_remitente : string;
    co_remitente : string;
    de_remitente : string;
    ciudadano_emisor_doc_externo : string;
    de_tipo_remitente_res : string;
    co_remitente_res : string;
    de_remitente_res : string;
    co_congresista_comision: string;
    de_congresista_comision : string;
    co_asunto : string;
    de_tipo_asunto : string;
    de_asunto : string; 
    co_entidad_sujeta : string;
    entidad_sujeta : string;
    uo_emp_resp : string;
    de_estado_doc_externo : string;
    uo_co_responsable : string;
    uo_responsable : string;
    uo_co_emp_responsable : string;
    uo_emp_responsable : string;
    nu_folios_total : string;
    fe_conl_exp : string;
    co_tipo_expediente: string;
    de_tipo_expediente : string;
}

