export class Infobras {

    num_item:Number;
    codigo_entidad:string;
    entidad:string;
    codigo_infobras:string;
    codigo_snip:string;
    descripcion_obra:string;
    estado_de_ejecucion:string;
    departamento:string;
    provincia:string;
    distrito:string;
    fecha_de_inicio:string;
    plazo_de_ejecucion:Number;
    fecha_de_fin:string;
    catalogo_nivel_1:string;
    fecha_registro:Date;
    costo:number;

    //tipoEjecucion : string;
    // Departamento: string;
    // Provincia: string;
    // Distrito: string;
    //EntidadPublica: string;
    //NombreObra: string;
    //CodSnip: string;
    //CodInfobras: string;
    Evento: number;
    Anho: number;
    DetalleEvento:string;
    Fase: string;
    MonitoreoCiu: string;
    MontoDesde: Number;
    MontoHasta: Number;
    
    //////////////////////
    flgSeleccionar: boolean;
    num_index:number;


}