export class FurSeguimientoFiltro {
    cfurCodigo:string;
    nfurIdcanal:number;
    flgAsignado:boolean;
    cfurCdepent:string;
    strFechaIni:string;
    strFechaFin:string;
    nfurId:number;
    nfurIdestado:number;
    cfurIdususuperv:string;
    cfurIdusuasig:string;
    cfurIdusureg:string;
    //CUS05
    cfurCodtipo:string;
    
}  