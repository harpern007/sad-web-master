import { DenunciadoXFicha } from './DenunciadoXFicha';
import { FichaXInstancia } from './FichaXInstancia';
import { FUDXEmpresaInvolucrada } from './FUDXEmpresaInvolucrada';
import { FUDXObraPublica } from './FUDXObraPublica';

export class Fud{
cfudCdepent	:string;
cfudCdistent:	string;
cfudCodent	:string;
cfudCodigo	:string;
cfudCodperscgr1	:string;
cfudCodperscgr2	:string;
cfudCodtipo	:string;
cfudCoduno	:string;
cfudCprovent	:string;
cfudDirentidad	:string;
cfudDschecho	:string;
cfudDscindica	:string;
cfudDscrefentidad	:string;
cfudDscregotr	:string;
cfudDscregotr2	:string;
cfudDscregotr3	:string;
cfudEtapa	:string;
cfudFlgantec	:string;
cfudFlgdatfuninv	:string;
cfudFlgdirocuhecho	:string;
cfudFlgemontodesc	:string;
cfudFlgencobrapub	:string;
cfudFlgexismonto	:string;
cfudFlgexispruh	:string;
cfudFlghdenconocu	:string;
cfudFlgotrentinst	:string;
cfudFlgpruebahechos	:string;
cfudFlgreqidentif	:string;
cfudIdusuasig	:string;
cfudIdusureg	:string;
cfudIdususuperv	:string;
cfudNroexpediente	:string;

nfudId	:number;
nfudIdbase	:number;
nfudIdestado	:number;
nfudIdfur	:number;
nfudIdplazo	:number;
nfudIdprioridad	:number;
nfudIdtipodocexp	:number;
nfudImpmontoperj	:number;
nfudNumplazo	:number;
nfudNumplazoasig	:number;
nfudPeriodoohfin	:number;
nfudPeriodoohini	:number;
strDfudFecasigna	:string;
strDfudFecregistro	:string;

listaDenunciadoPorFicha : DenunciadoXFicha[];
listaFichaPorInstancia : FichaXInstancia[];
listaFudPorEmpresaInvolucrada :FUDXEmpresaInvolucrada[];
listaFudPorObraPublica : FUDXObraPublica[];

}