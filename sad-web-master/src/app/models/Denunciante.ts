import { DenuncianteXFamiliar } from './DenuncianteXFamiliar';
import { Persona } from './Persona';

export class Denunciante {
    ndenuncianteId: Number;
    ndenuncianteIdtipden : Number;
    ndenuncianteIdpersona : Number;
    cdenuncianteFlganonimo : string;
    cdenuncianteFlgnotelec : string;
    cdenuncianteFlgreprelegal : string;
    ndenuncianteIdempragrup : Number;
    ndenuncianteIdfur: Number;
    cdenuncianteFlgtrabentden : string;
    cdenuncianteFlgmedidaprotec: string;
    cdenuncianteDscdirecc : string;
    cdenuncianteCdepartdomic: string;
    cdenuncianteCprovdomic: string;
    cdenuncianteCdistdomic: string;
    cdenuncianteCreferenciadomic: string;
    cdenuncianteDsccorreo: string;
    ndenuncianteIdmedionotif: Number;
    cdenuncianteDsctelef: string;
    cdenuncianteDscmovil: string;
    cdenuncianteFlgacepmedpyb: string;
    cdenuncianteFlgconfleyprot: string;
    cdenuncianteDscagrup: string;
    listaDenunciantePorFamiliar:DenuncianteXFamiliar[];
    persona:Persona;

    //nUEVOS MOCJUP
    ndenuncianteIdrangoedad : number; //rangoEdad
    cdenuncianteCodsexo : string; //flgEsServidorPublico
    cdenuncianteFlgservpub : string; //flgEsTestigoHecho
    cdenuncianteFlgtestigoh : string; //flgAccesoInformacion
    cdenuncianteFlgaccdirinf : string; //flgAccesoInformacion
    cdenuncianteDscenterohecho : string; //dscEnteroHecho;

}  