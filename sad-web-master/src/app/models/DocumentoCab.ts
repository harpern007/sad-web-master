import { DocumentoDet } from './DocumentoDet';

export class ActualizarDet{
    
    idTipoDoc : number;
    codTipo : string;
    codSubtipo : string;
    listaDocumentoDetalle: DocumentoDet[];

}