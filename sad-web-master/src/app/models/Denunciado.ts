
import { Cargo } from './Cargo';
import { Persona } from './Persona';
import { TipoDocumento } from './TipoDocumento';

export class Denunciado {
  ndenunciadoId: number;
  persona : Persona;
  tipoDocumento : TipoDocumento;
  cdenunciadoNrodoc: string;
  cdenunciadoApepaterno: string;
  cdenunciadoApematerno: string;
  cdenunciadoNombres: string;
  cdenunciadoFlgpermentidad:string;
  ndenunciadoIdcargo:number;
  ndenunciadoIdtipdoc:number;
  cargo : Cargo ;

  constructor(){
    this.cargo = new Cargo();
    this.tipoDocumento = new TipoDocumento();
    this.persona = new Persona();
  }

  numIndex:number;
  //fichaXdenunciado: FichaXdenunciado = new FichaXdenunciado(); // uno a uno
}