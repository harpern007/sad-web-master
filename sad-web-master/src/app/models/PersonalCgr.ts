import { SedeCgr } from './SedeCgr';

export class PersonalCgr {
    cpPerApPater: string;
    cpPerApMater:string;
    cpPerNombre:string;
    cpPerCodigo:string;
    cpPerSupervisor:string;
    sedeCGR:SedeCgr;
    cpUnoCodigo:string;
    cperSede:string;
    strDscNombreCompleto:string;
    
  }
  