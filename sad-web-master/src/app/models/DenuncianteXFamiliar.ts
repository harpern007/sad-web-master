export class DenuncianteXFamiliar {
    ndciantexfamId:number;
    ndciantexfamIddenunciante:number;
    ndciantexfamIdtipodocfam:number;
    cdciantexfamNrodocfam:string;
    cdciantexfamDscapepaterf:string;
    cdciantexfamDscapematerf:string;
    cdciantexfamDscnombres:string;
}