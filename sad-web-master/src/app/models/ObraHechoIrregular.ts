export class ObraHechoIrregular {
    nobrphiId : number;
    nobrphiIdobrpub: string;
    cobrphiDsc : string;
    cobrphiFlgactivo: string;
    cobrphiCodinfobras: string;
    flgSeleccionar: boolean;
    num_item:number;
    num_index:number;
    
}
