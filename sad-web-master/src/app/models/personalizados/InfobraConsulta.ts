export class InfobraConsulta{
    //Datos para consultar al servicio infoobras
    entidad : string;
	cod_departamento : string;
	cod_provincia : string;
	cod_distrito : string;
	descripcion_obra:string;
	codigo_snip:string;
	codigo_infobras:string;
	evento:string;
	estado_de_ejecucion:string;
        
}
    