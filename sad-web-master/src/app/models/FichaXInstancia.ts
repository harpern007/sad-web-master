import { Entidad } from './Entidad';
import { Estado } from './Estado';
import { Cargo } from './Cargo';
import { Catalogo } from './Catalogo';
import { CodTipoInstEnt } from '../general/variables';

export class FichaXInstancia{
    nfichaxinstanIdtipd:number;
    cfichaxinstanCodtipo:string;
    cfichaxinstanCodent:string;
    entidad : Entidad;
    nfichaxinstanIdestado : number;
    /*
    *GSOLIS
    NUEVA TABLA o nuevos campos */

    cfichaxinstanMotivo: string;
    dfichaxinstanFchPresent :Date;
    //cfichaxinstanstrFchPrest : string;
    cfichaxinstanNrodoc : string;
    estado : Catalogo;
    strDfichaxinstanFechaprest:string;
    
    cfichaxinstanCodtipoinst: string = CodTipoInstEnt;

    constructor() {
        this.estado = new Catalogo();
        this.entidad = new Entidad();
     }
}


	