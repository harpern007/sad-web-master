//import { ObraHechoIrregular } from './ObraHechoIrregular';

export class ObraPublica {
    nobrapublicaId:Number;
    nobrapublicaIdejec:Number;
    cobrapublicaCdepent: string;
    cobrapublicaCprovent: string;
    cobrapublicaCdistent: string;
    cobrapublicaDscentidad: string;
    cobrapublicaDscnombre: string;
    cobrapublicaSnip: string;
    cobrapublicaCodinfob: string;
    nobrapublicaIdevento:number;
    nobrapublicaAnho:number;
    nobrapublicaIddeteven:number;
    nobrapublicaIdfase:number;
    nobrapublicaIdmonitorc:number;
    nobrapublicaCosto:number;
    cobrapublicaCodent: string;

    //Revisar
    //cobrapublicaEstado : string;
    cobrapublicaDscestado : string;
    nobrapublicaIdestado : number;
    dobrapublicaFechaIni : Date;
    dobrapublicaFechaFin : Date;

    // cobrapulicaDirec : string;
    // cobrapublicaRef : string;

    cobrapublicaDscdireccion:string;
    cobrapublicaDscreferencia:string;

    //strDobrapublicaFechaIni:string;
    strDobrapublicaFechaini:string;
    strDobrapublicaFechafin:string;

    //strDobrapublicaFechaFin:string;

    strNobrapublicaCosto:string;
}