export class PersonaReniec {
    dcAddDepUbigCode: string;
    dcAddDistUbigCode: string;
    dcAddProvUbigCode: string;
    dcAddress: string;
    dcBornDate: string;
    dcDocument: string;
    dcExpeditionDate: string;
    dcFirstName: string;
    dcLastName1: string;
    dcLastName2: string;
    dcMotherName: string;

}
  