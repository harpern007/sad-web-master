import { CodTipoFichaFUR } from '../general/variables';

export class DocumentoDet{
    ndocumentoId : number;
    ndocumentoIdtipd : number;
    cdocumentoCodtipo : string = CodTipoFichaFUR;
    cdocumentoCodsubtipo : string = CodTipoFichaFUR;
    cdocumentoDscurl : string;
    cdocumentoDscnombre: string;

    ddocumentoFchreg:Date;
    strDdocumentoFchreg:string;
    strDscRutaLocal:string;
    fileBase64:string;

}