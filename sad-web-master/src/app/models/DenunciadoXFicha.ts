import { Denunciado } from './Denunciado';

export class DenunciadoXFicha{
    ndciadoxfichaId:number;
    ndciadoxfichaIdficha:number;
    ndciadoxfichaIddciado:number;
    cdciadoxfichaCodtipo:string;
    denunciado: Denunciado;
}