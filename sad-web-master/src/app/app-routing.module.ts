import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormularioWebComponent } from './pages/formulario-web/formulario-web.component';
import { FurComponent } from './pages/fur/fur.component';

const routes: Routes = [{path:"formulario",component:FormularioWebComponent},{path:"fur",component:FurComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
