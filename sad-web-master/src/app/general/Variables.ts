import { ModalMensajeComponent } from '../pages/General/modal-mensaje/modal-mensaje.component';
import { MatDialog } from '@angular/material/dialog';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons/faTimesCircle';
import {faEye} from '@fortawesome/free-solid-svg-icons/faEye';
import {faEdit} from '@fortawesome/free-solid-svg-icons/faEdit';

//import {faTimesCircle} from "@fortawesome/free-solid-svg-icons/faTimesCircle"
import { ErrorValidator } from '../models/personalizados/ErrorValidator';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import * as moment from 'moment';

export const ipPrueba = `http://11.162.109.175`;


/////////////////////SERVIDOR CALIDAD
// export const Host1 =`http://11.162.109.175:8080/restSAD`; //Desarrollo Calidad
// export const HostContraloria = `http://11.162.109.175/:8080/SGD`; //calidad
// export const HostSunat =`http://11.162.109.175:8080/ServicioSUNAT` //calidad;
// export  const HostExpediente = `http://11.162.109.175:8080/serviceExpediente/Expediente` //calidad
// export const HostReniec = `http://11.162.109.175:8080/servicesReniec`; //calidad
// export const HostInfobras = `http://11.162.109.175:8080/servicesINFOBRA`; //calidad
// export const HostSeguridad = `http://11.162.109.175:8080/servicesSeguridad`; //contraloria CALIDAD
// export const HostSeguridad2 = `http://11.162.109.175:8080/servicioSeguridadGx`; //contraloria CALIDAD
// export const HostLaserFiche = `http://11.162.109.175:8080/serviceLaserfiche`; //CALIDAD
// export const constobraPublica = 41; //desarrollo : 1
// export const constcontrataciones = 43; //desarrollo : 21
/////////////////////SERVIDOR CALIDAD


/////////////////////SERVIDOR CONTRALORIA
export const Host1 =`http://192.168.192.3:8080/restSAD`; //Desarrollo Casa Hector
//export const Host1 =`http://11.162.109.173:8080/restSAD`; //Desarrollo Contraloria
//export const Host1 =`http://10.23.3.2:8080/restSAD`; //Desarrollo Contraloria
//export const Host1 = `http://10.23.2.161:9999/restSAD`; //Desarrollo Hector

export const HostContraloria = `http://11.162.109.173/:8080/SGD`; //contraloria
export const HostSunat =`http://11.162.109.173:8080/ServicioSUNAT`;
export  const HostExpediente = `http://10.23.3.2:8080/serviceExpediente/Expediente` //Inspira
export const HostReniec = `http://11.162.109.173:8080/servicesReniec`;//contraloria

export const HostInfobras = `http://11.162.109.173:8080/servicesINFOBRA`; //calidad
//export const HostInfobras = `http://10.23.3.2:8080/servicesINFOBRA`; //inspira

export const HostSeguridad = `http://11.162.109.173:8080/servicesSeguridad`; //contraloria
export const HostSeguridad2 = `http://11.162.109.173:8080/servicioSeguridadGx`; //contraloria
export const HostLaserFiche = `http://10.23.3.2:8080/serviceLaserfiche`; //CONTRALORIA

export const constobraPublica = 1;
export const constcontrataciones = 21;
/////////////////////SERVIDOR CONTRALORIA


/////////////////////SERVIDOR DESARROLLO INSPIRA
//export const Host1 =`http://10.23.3.2:8080/restSAD`; //Desarrollo Inspira
//export const Host1 = `http://192.168.191.237:8080/restSAD`; //Desarrollo bruno

// export const HostSunat =`http://10.23.3.2:8080/ServicioSUNAT`;
// export  const HostExpediente = `http://10.23.3.2:8080/serviceExpediente/Expediente` //Inspira
// export const HostReniec = `http://10.23.3.2:8080/servicesReniec`;//contraloria

// export const HostInfobras = `http://10.23.3.2:8080/servicesINFOBRA`; //calidad
// //export const HostInfobras = `http://10.23.3.2:8080/servicesINFOBRA`; //inspira

// export const HostSeguridad = `http://10.23.3.2:8080/servicesSeguridad`; //contraloria
// export const HostSeguridad2 = `http://10.23.3.2:8080/servicioSeguridadGx`; //contraloria
// export const HostLaserFiche = `http://10.23.3.2:8080/serviceLaserfiche`; //CONTRALORIA

// export const constobraPublica = 1;
// export const constcontrataciones = 21;
/////////////////////SERVIDOR DESARROLLO INSPIRA


////////////////////////////////////////OLD
// //export const Host1 = `http://11.162.109.173:8080/restSAD`; //Desarrollo contraloria
// //export const Host1 = `http://10.23.2.52:8080/restSAD`; //Desarrollo bruno
// //export const Host1 = `http://10.23.3.2:8080/restSAD`; //Desarrollo inspira
// //export const Host1 = `http://10.23.2.161:9999/restSAD`; //Desarrollo Hector
// //export const Host1 =`http://11.162.109.175:8080/restSAD`; //Desarrollo Contraloria
// export const Host1 =`http://11.162.109.175:8080/restSAD`; //Desarrollo Calidad


// export const ipPrueba = `http://11.162.109.175`;

// //export const Host1 = `http://10.23.2.161:9999/restSAD` //Desarrollo hector
// //export const HostContraloria = `http://11.162.109.173/:8080/SGD`; //contraloria
// export const HostContraloria = `http://11.162.109.175/:8080/SGD`; //calidad

// //export const HostSunat =`http://11.162.109.173:8080/servicesSunat`;
// //export const HostSunat =`http://10.23.3.2:8080/ServicioSUNAT` //Inspira;
// //export  const HostExpediente = `http://10.23.3.2:8080/serviceExpediente/Expediente` //Inspira

// export const HostSunat =`http://11.162.109.175:8080/ServicioSUNAT` //calidad;
// export  const HostExpediente = `http://11.162.109.175:8080/serviceExpediente/Expediente` //calidad

// //export const HostReniec = `http://11.162.109.173:8080/servicesReniec`;//contraloria
// export const HostReniec = `http://11.162.109.175:8080/servicesReniec`; //calidad
// // export const HostInfobras = `http://11.162.109.173:8080/integracionSAD`;

// //export const HostInfobras = `http://11.162.109.173:8080/servicesINFOBRA`; // contraloria
// export const HostInfobras = `http://11.162.109.175:8080/servicesINFOBRA`; //calidad

// //export const HostSeguridad = `http://11.162.108.89:8019/SrvTarea`;
// // export const HostSeguridad = `http://11.162.109.173:8080/servicesSeguridad`; //contraloria
// // export const HostSeguridad2 = `http://11.162.109.173:8080/servicioSeguridadGx`; //contraloria

// export const HostSeguridad = `http://11.162.109.175:8080/servicesSeguridad`; //contraloria CALIDAD
// export const HostSeguridad2 = `http://11.162.109.175:8080/servicioSeguridadGx`; //contraloria CALIDAD

// //export const HostSeguridad = `http://11.162.109.175:8080/servicesSeguridad`; //calidad
// //export const HostSeguridad2 = `http://11.162.109.175:8080/servicioSeguridadGx`; //calidad

// //export const HostLaserFiche = `http://localhost:8095/serviceLaserfiche`;
// //export const HostLaserFiche = `http://10.23.3.2:8080/serviceLaserfiche`; //CONTRALORIA
// export const HostLaserFiche = `http://11.162.109.175:8080/serviceLaserfiche`; //CALIDAD
////////////////////////////////////////OLD

//
export const HostPayaraInspira = `http://10.23.3.2:8080`;
export const NumCanalFormWeb = 1;

export const PERSONA_MAESTRA = 2;
export const PERSONA_EXTERNA = 1;
export const iconError = faTimesCircle;
export const wcfClaveAUT = "beV5cAWMg/uVlpxz8Q1zlrh76+Yy8nNABk03T9C4UGw6zm3se/vIQL2r+QDKQF6n";
export const wcfTipoCLAVE = "T";
export const wcfUsuarioAUT = "DENUN00001";
export const codAccesoVerificarOk = "C";
export const TipoNroExpdiente = "DEN";
export const NumIdCanalExp = 3;
export const NumIdCanalCorreo = 4;
export const NumIdCanalCarta = 6;
export const NumIdCanalReporte = 7;

export const CodTipoExpDetREF = "REF";
export const CodTipoExpDetDES = "DES";

export const CodTipoFichaFUR = "FUR";
export const CodSubTipoFichaFUR = "FUR";
export const CodTipoFichaEXP = "EXP";
export const CodUnoGestDenuncias = "L530";
export const CodUnoSubGerCMP = "L334";

export const CodTipoTramiteInstancia = 21;

export const NumIdCargoGestorCan = 3;
export const NumIdEstadoAsignado = 5;
export const NumIdEstadoAtendido = 9;
export const NumIdEstadoRegistrado = 4;
export const NumIdEstadoHePendiente = 13;
export const NumIdEstadoHeEvaluac = 14;
export const NumIdEstadoHeSuperv = 15;

export const NumIdTipDenAnonimo = 5;
//
export const NumResultadoAnalisDenuncia =1;

/*CUS07 */
export const NumIdEstadoHeEvaluacionSuperv = 86;
export const NumIdEstadoHeEvaluacionObservado = 87;
export const NumIdEstadoFudPendiente = 1;
/** */
export const CodDEM = "DEM";
export const CodSNCCGR = "CGR";
export const CodSNCOCI = "OCI";

export const CodTipoInstEnt = "ENT";

//export const CodTipoFichaFur = "FUR";
export const CodTipoFichaHe = "HEV";

export const DscEtiquetaBtnVal = "Validar";
export const DscEtiquetaBtnVal2 = "Validando";




//export const NumIdCargoGestorCan = 3;
//export var GlobalCodDep:string;

//Correo
export const correoEmisor = 'gsolisve@gmail.com';
//

//Año
export const anioActualGlobal = 2020;
export const stringFechaMaxima = anioActualGlobal + "-12-31";

//codigo
export const codigoIncial = "000000000000";
//
export const iconoMatAgregar = "note_add";
export const DscTituloPaginator = "Registros por Página";
export const DscHoraDefault = " 00:00:01";
export const listTypesUpload = ["application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword", "application/pdf"];




export class GlobalVars {
  //public static CodDep: string;
  public static CodUsuario: string;
  public static DscContrasenha: string;
  public static CodPersonal: string;
  public static DscNombreComplPers: string;
  public static CodPersSuperv: string;
  public static FlgSupervisor: boolean = false;
  public static CodPersSedeDpto: string;
  public static FlgGestor: boolean = false;
  public static FlgOtroUsuario: boolean = false;
  public static CodUNOPers: string;
  public static CodSNC: string;

}

export function iconPanelContraer(flgContraer:boolean)
{

  return (flgContraer?"keyboard_arrow_left":"keyboard_arrow_down");

}

export function cookieGlobalVars(numCaso:number,cookieService:CookieService)
{

  
  var strCampo1 = "cod-usuario";
  var strCampo2 = "dsc-contrasenha";
  var strCampo3 = "cod-personal";
  var strCampo4 = "cod-perssuperv";
  var strCampo5 = "flg-supervisor";
  var strCampo6 = "cod-perssededpto";
  var strCampo7 = "flg-gestor";
  var strCampo8 = "flg-otrousuario";
  var strCampo9 = "dsc-nombrecomplpers";
  var strCampo10 = "cod-unopers";
  var strCampo11 = "cod-snc";
  

  switch (numCaso) {
    case 1:

    // console.log("===========0CASO 1");

      cookieService.set(strCampo1,GlobalVars.CodUsuario);
      cookieService.set(strCampo2,GlobalVars.DscContrasenha);
      cookieService.set(strCampo3,GlobalVars.CodPersonal);
      cookieService.set(strCampo4,GlobalVars.CodPersSuperv);
      cookieService.set(strCampo5,GlobalVars.FlgSupervisor.toString());
      cookieService.set(strCampo6,GlobalVars.CodPersSedeDpto);
      cookieService.set(strCampo7,GlobalVars.FlgGestor.toString());
      cookieService.set(strCampo8,GlobalVars.FlgOtroUsuario.toString());
      cookieService.set(strCampo9,GlobalVars.DscNombreComplPers);
      cookieService.set(strCampo10,GlobalVars.CodUNOPers);
      cookieService.set(strCampo11,GlobalVars.CodSNC);
      
      
      break;

    case 2:

      // console.log("===========0CASO 2");
      // console.log(GlobalVars.FlgSupervisor);
      GlobalVars.CodUsuario = cookieService.get(strCampo1);
      GlobalVars.DscContrasenha = cookieService.get(strCampo2);
      GlobalVars.CodPersonal = cookieService.get(strCampo3);
      GlobalVars.CodPersSuperv = cookieService.get(strCampo4);

      // console.log("===========JSON");
      // console.log(cookieService.get(strCampo1));

      GlobalVars.FlgSupervisor = JSON.parse(cookieService.get(strCampo5));

      //console.log("===========JSON1");

      GlobalVars.CodPersSedeDpto = cookieService.get(strCampo6);
      GlobalVars.FlgGestor = JSON.parse(cookieService.get(strCampo7));
      GlobalVars.FlgOtroUsuario = JSON.parse(cookieService.get(strCampo8));
      GlobalVars.DscNombreComplPers = cookieService.get(strCampo9);
      GlobalVars.CodUNOPers = cookieService.get(strCampo10);
      GlobalVars.CodSNC = cookieService.get(strCampo11);
      
      break;
  
    default:
      break;
  }

  

}

export function saveContent(fileContents, fileName)
{
    var link = document.createElement('a');
    link.download = fileName;
    link.href = 'data:application/octet-stream;base64,' + fileContents;
    link.click();
}


//export const Host2 = `http://10.23.2.52:8080/SGD`;

export function openModalMensaje(dscTitulo: string, dscMensaje: string,dialog: MatDialog) 
{

    
    const dialogRef = dialog.open(ModalMensajeComponent,{ width: '50%',height:'35%', data: {titulo : dscTitulo,mensaje: dscMensaje}});
    
    // dialogRef.afterClosed().subscribe(result => {
    //   //console.log('The dialog was closed');
    // });

    return dialogRef;
}

export function openModalMensajePrg(dscTitulo: string, dscMensaje: string,dialog: MatDialog) 
{

    const dialogRef = new Observable(observer => {

      dialog.open(ModalMensajeComponent,{ width: '50%',height:'35%', data: {titulo : dscTitulo,mensaje: dscMensaje,flgRegresar:true}}).afterClosed().subscribe(result => {

        observer.next(result);
        
      });
    

    });

    return dialogRef;

}


export function ValidarEmail(dscEmail:string) {

    var emailRegex = /^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/i;

    return emailRegex.test(dscEmail);
}

export function ConvertirFlag(flag:boolean) {

    
    return (flag) ? "SI":"NO";
}

export function getBooleanFlag(flag : string){
  return (flag == "SI") ? true : false;
}

export function GetDate() {
    
  let fecha = new Date();
  let dia = fecha.getDate();
  let diaString = ((dia<10) ? ("0"+dia) :  dia) ;
  let mes = Number(fecha.getMonth())+1;
  let mesString = (mes < 10) ? ("0"+String(mes)) : String(mes);
  //let fechaString = fecha.getFullYear() +"-"+ Number(fecha.getMonth()+1)+  "-" + ((dia<10) ? ("0"+dia) :  dia);
  //let fechaString = ((dia<10) ? ("0"+dia) :  dia) + "-" +Number(fecha.getMonth()+1) + "-" +  fecha.getFullYear();
  let fechaString = diaString+ "/" + mesString + "/" +  fecha.getFullYear();

  return fechaString;
}

export function GetDateTime(){
  let fecha = new Date();

  console.log("==============fecha");
  console.log(fecha);

  let dia = fecha.getDate();

  console.log("==============dia");
  console.log(dia);

  let diaString = ((dia<10) ? ("0"+dia) :  dia) ;
  let mes = Number(fecha.getMonth())+1;

  console.log("==============mes");
  console.log(mes);
  
  let mesString = (mes < 10) ? ("0"+String(mes)) : String(mes);
 
  //let fechaString = fecha.getFullYear() +"-"+ Number(fecha.getMonth()+1)+  "-" + ((dia<10) ? ("0"+dia) :  dia);
  //let fechaString = ((dia<10) ? ("0"+dia) :  dia) + "-" +Number(fecha.getMonth()+1) + "-" +  fecha.getFullYear();
  let fechaString = diaString+ "/" + mesString + "/" +  fecha.getFullYear() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();

  return fechaString;
  
  // let fechaString = ((dia<10) ? ("0"+dia) :  dia) + "-" +Number(fecha.getMonth()+1) + "-" +  fecha.getFullYear();
  
  // return fechaString;
}

export function GetAnho() {
    
  let fecha = new Date();

  return fecha.getFullYear();
}

export function GetDateHour() {
    
  let fecha = new Date();
  let dia = fecha.getDate();
  //let fechaString = fecha.getFullYear() +"-"+ Number(fecha.getMonth()+1)+  "-" + ((dia<10) ? ("0"+dia) :  dia);
  // let fechaString = ((dia<10) ? ("0"+dia) :  dia) + "/" + Number(fecha.getMonth()+1) + "/" +  fecha.getFullYear();
  let fechaString = ((dia<10) ? ("0"+dia) :  dia) + "/" + ("0" + Number(fecha.getMonth()+1)).slice(-2) + "/" +  fecha.getFullYear();

  // let horaString2 = fecha.getHours() + ":" + fecha.getMinutes();
  let horaString2 = ("0" + fecha.getHours()).slice(-2) + ":" + ("0" + fecha.getMinutes()).slice(-2);
 

  // let horaString = horaString2 + ":" + fecha.getSeconds();
  let horaString = horaString2 + ":" + ("0" + fecha.getSeconds()).slice(-2);
  

  return [fechaString, horaString, horaString2];
}

export function GetDateTimeString(fechaParam : string){
  let fecha = new Date(fechaParam);
  let dia = fecha.getDate();
  let diaString = ((dia<10) ? ("0"+dia) :  dia) ;
  let mes = Number(fecha.getMonth())+1;
  let mesString = (mes < 10) ? ("0"+String(mes)) : String(mes);
  let fechaString = diaString+ "/" + mesString + "/" +  fecha.getFullYear() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();

  return fechaString;
}

export function ConvertStrDateToFormatBD(strFecha : string){

  //se espera strFecha = "yyyy-mm-dd"

  console.log("==========strFecha");
  console.log(strFecha);

  var strReturn = "";

  if(strFecha != null)
  {
    strReturn = strFecha.substring(8) + "/" + strFecha.substring(5,7)  + "/" +  strFecha.substring(0,4)   + DscHoraDefault;//" 00:00:01";

  }
  console.log("==========strReturn");
  console.log(strReturn);
  //let fecha = fechaParam.toString();

  return strReturn;
}

export function ageFromDateOfBirth(dateOfBirth:Date){

  return moment().diff(dateOfBirth,"years");

}

export function ConvertDateTimeToStr3(fechaParam : string){

  //let fecha = fechaParam.toString();
  //var fecha = "";
  var dscReturn = "";

  if(fechaParam != null)
  {

    //fecha = fechaParam.toString();

    dscReturn = fechaParam.substring(0,4) + "-" + fechaParam.substring(4,6)  + "-" + fechaParam.substring(6,8);

  }

  return dscReturn;
}



export function ConvertDateTimeToStr(fechaParam : Date){

  //let fecha = fechaParam.toString();
  var fecha = "";
  var dscReturn = "";

  if(fechaParam != null)
  {

    fecha = fechaParam.toString();

    dscReturn = fecha.substring(0,4) + "-" + fecha.substring(5,7)  + "-" + fecha.substring(8,10);

  }

  return dscReturn;
}

export function ConvertDateTimeToStr2(fechaParam : Date){

  //let fecha = fechaParam.toString();
  var fecha = "";
  var dscReturn = "";

  if(fechaParam != null)
  {

    fecha = fechaParam.toString();

    dscReturn = fecha.substring(0,4) + "-" + fecha.substring(5,7)  + "-" + fecha.substring(8,10) + " " + fecha.substring(11);

    

  }

  return dscReturn;
}
/**
 * GSOLIS
 */
export function ConvertDateTimeToStrForBD(fechaParam : Date){

  //let fecha = fechaParam.toString();
  var fecha = "";
  var dscReturn = "";

  if(fechaParam != null)
  {

    fecha = fechaParam.toString();

    dscReturn =  fecha.substring(8,10) + "/" +  fecha.substring(5,7)  + "/" +fecha.substring(0,4) + DscHoraDefault;//" 00:00:01";
  }

  return dscReturn;
}
 //
export function ConvertStringDateStr(fechaParam : string){
  let fecha : string = "";
  console.log(fechaParam);
  if(fechaParam != null){

      fecha = fechaParam.substring(6,10) + "-" + fechaParam.substring(3,5)  + "-" + fechaParam.substring(0,2);
      console.log(fecha);
  }
  return fecha;
}

export function RecupPartesDocExp(strNumDocExp:string,idCaso:number){

  var strReturn = "";

  if(strNumDocExp != null)
  {

    switch (idCaso) {
      case 1:
  
        strReturn = strNumDocExp.substring(0,strNumDocExp.indexOf("-"));
        
        break;
    
      case 2:
  
        strReturn = strNumDocExp.substring(strNumDocExp.indexOf("-") + 1,strNumDocExp.lastIndexOf("-"));
        
        break;
  
      case 3:
  
        strReturn = strNumDocExp.substring(strNumDocExp.lastIndexOf("-") + 1);
        
        break;
    }


  }

  return strReturn;

}

export function RecupPartesFchInicio(strFecha:string,idCaso:number){

  var strReturn = "";

  if(strFecha != null)
  {

    switch (idCaso) {
      case 1:
  
        strReturn = strFecha.substring(11); // HORA
        
        break;
    
      case 2:
  
        strReturn = strFecha.substring(0,10); // FECHA
        
        break;

      case 3:
  
        strReturn = strFecha.substr(11,5); // hora sin seg
          
        break;
  
    }
    
  }

  return strReturn;

}

// export function _cargarTableMaterial(){


//   setTimeout(() => {

//     this.dataSource = new MatTableDataSource(this.instanciaList);

//     if(this.paginator != null){this.paginator._intl.itemsPerPageLabel = DscTituloPaginator}; //this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;

//     this.dataSource.paginator = this.paginator;
//     this.dataSource.sort = this.sort;
    
//   }, 1);

  
// }

export class MfuncErrorVal{

  iconError = faTimesCircle;

  LeerErrorValidator(index:number,errorValidator: ErrorValidator[]) {

    var lbFlgReturn = false;
    /* console.log(index);
    console.log(errorValidator); */
    if(errorValidator.length > 0)
    {
  
      lbFlgReturn = errorValidator[index].flgInvalid;
  
    }
  
    return lbFlgReturn;
  }

  AgregarErrorValidator(flgInvalid:boolean, dscMensaje:string,errorValidator: ErrorValidator[],flgReset:boolean) {

    //console.log("==========en la funcion 1");

    if(flgReset)
    {errorValidator = [];}
  
    var index = errorValidator.length;
  
    //console.log("==========en la funcion 13");
  
    // errorValidator[index] = new ErrorValidator();
      
    // errorValidator[index].flgInvalid = flgInvalid;
    // errorValidator[index].dscMensaje = (flgInvalid ? dscMensaje : "");

    errorValidator = this.GenerarErrorValidator(flgInvalid, dscMensaje,errorValidator,index);
  
    //console.log("==========en la funcion 12");
  
    return errorValidator;
  }

  AgregarErrorValIndex(flgInvalid:boolean, dscMensaje:string,errorValidator: ErrorValidator[],index:number) {

    if(index == 0)
    {errorValidator = [];}
  
    errorValidator = this.GenerarErrorValidator(flgInvalid, dscMensaje,errorValidator,index);
  
    return errorValidator;
  }

  GenerarErrorValidator(flgInvalid:boolean, dscMensaje:string,errorValidator: ErrorValidator[],index:number) {

  
    errorValidator[index] = new ErrorValidator();
      
    errorValidator[index].flgInvalid = flgInvalid;
    errorValidator[index].dscMensaje = (flgInvalid ? dscMensaje : "");
  
    return errorValidator;
  }



  RevisarErrorValidator(errorValidator: ErrorValidator[]) {

    return (errorValidator.filter(x => x.flgInvalid).length > 0 ? true : false); //SI HAY ERRORES ES TRUE
  }

  //this.mFuncErrorVal.AgregarErrorValidator
  
}


/*Icons */
export const iconEliminar = faTrash;
export const iconAgregar = faPlus;
export const iconVer = faEye;
export const iconEditar = faEdit;
//export const iconError = faTimesCircle;

export function iniciarFirma(){
  window.open("http://localhost:8080/SAD_WEB/firma.html", '_blank');
  //window.location.href ="http://localhost:8092/SAD_WEB/firma.html";
}