import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HechoIrregularComponent } from './pages/denuncia/hecho-irregular/hecho-irregular.component';
import { DatosGeneralComponent } from './pages/datos-general/datos-general.component';

import {MatInputModule} from '@angular/material/input';
import {MatTooltipModule} from '@angular/material/tooltip';
import { DenunciaContenedorComponent } from './pages/denuncia/denuncia-contenedor/denuncia-contenedor.component';

import { EntidadHechoComponent } from './pages/denuncia/entidad-hecho/entidad-hecho.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { FuncionarioInvolucradoComponent } from './pages/denuncia/Funcionario/funcionario-involucrado/funcionario-involucrado.component';
import { EmpresaInvolucradaComponent } from './pages/denuncia/empresa/empresa-involucrada/empresa-involucrada.component';
import { PruebaHechoComponent } from './pages/denuncia/prueba-hecho/prueba-hecho.component';
import { FormularioWebComponent } from './pages/formulario-web/formulario-web.component';

import { ModalMontoComponent } from './pages/denuncia/modal/modal-monto/modal-monto.component';

import { ModalEmpresaComponent } from './pages/denuncia/empresa/modal-involucrada/modal-empresa/modal-empresa.component';

import { ContenedorDenuncianteComponent } from './pages/Denunciante/Contenedor/contenedor-denunciante.component';
import { PersonaJuridicaComponent } from './pages/Denunciante/PersonaJuridica/persona-juridica.component';
import { PersonaNaturalComponent } from './pages/Denunciante/PersonaNatural/persona-natural.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ContenedorMpComponent } from './pages/MedidasProt/contenedor-mp/contenedor-mp.component';

import { ModalNotificacionComponent } from './pages/Denunciante/ModalNotificacion/modal-notificacion.component';
import {MatDialogModule} from '@angular/material/dialog';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { ModalMensajeComponent } from './pages/General/modal-mensaje/modal-mensaje.component';
import { ModalBusqObrpuComponent } from './pages/ObraPublica/modal-busq-obrpu/modal-busq-obrpu.component';
import { ModalIrregInfobrasComponent } from './pages/ObraPublica/modal-irreg-infobras/modal-irreg-infobras.component';
import { LoginGcComponent } from './pages/Login/login-gc/login-gc.component'
export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};
import { RecaptchaModule } from 'ng-recaptcha';
import { PanelPrincipalComponent } from './pages/Dashboard/panel-principal/panel-principal.component';
import { BodyOpcionesComponent } from './pages/Dashboard/body-opciones/body-opciones.component';
import { BodyBusquedaComponent } from './pages/Dashboard/body-busqueda/body-busqueda.component';
import { MatTableModule, MatDateFormats, MAT_DATE_FORMATS, MAT_DATE_LOCALE, DateAdapter } from '@angular/material' ;
import { MatPaginatorModule } from '@angular/material';
import { CookieService } from 'ngx-cookie-service';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatNativeDateModule } from '@angular-material-components/datetime-picker';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule, MomentDateAdapter } from '@angular/material-moment-adapter';
import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';
//import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';
//import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';
// import { ContenedorDenuncianteComponent } from './pages/Denunciante/Contenedor/contenedor-denunciante/contenedor-denunciante.component';
// import { PersonaJuridicaComponent } from './pages/Denunciante/PersonaJuridica/persona-juridica/persona-juridica.component';
// import { PersonaNaturalComponent } from './pages/Denunciante/PersonaNatural/persona-natural/persona-natural.component';


import {MatIconModule} from '@angular/material/icon';

import { PanelPrincipalServicioComponent } from './pages/Dashboard/panel-principal-servicio/panel-principal-servicio.component';

const CUSTOM_DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: "DD/MM/YYYY",
    
  },
  display: {
    dateInput: "DD/MM/YYYY",
    
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY"
  }
};

const router: Routes = [
  { path: 'ContenedorDenunciante',
  component: ContenedorDenuncianteComponent },
  { path: 'PanelPrincipal/:usuario',
  component: PanelPrincipalComponent },
  { path: 'PanelPrincipalServicio/:usuario',
  component: PanelPrincipalServicioComponent },
  { path: 'Login',component: LoginGcComponent },
  { path: '',   redirectTo: '/Login', pathMatch: 'full' },
  { path: 'BodyOpciones',
  component: BodyOpcionesComponent },
  { path: 'AsignarInformacion/:idFur',
  component: AsignarInformacionComponent},
/*   { path: 'EvaluarInformacion/:idFur',
  component: EvalinfRegfurComponent}, */
  {path:'EvaluarInformacion/:idFur',
    component : EvaluacionFudComponent
  },
  { path: 'FormularioWeb',
  component: FormularioWebComponent},
  {path: 'FirmaDigital',component : FirmaDigitalComponent}
  
  
];
import { ModalEntidadComponent } from './pages/denuncia/entidad-hecho/modal-entidad/modal-entidad.component';
import { ModalFuncionarioComponent } from './pages/denuncia/Funcionario/modal-funcionario/modal-funcionario/modal-funcionario.component';
import { ModalMpComponent } from './pages/MedidasProt/modal-mp/modal-mp.component';
import { ModalContratacionComponent } from './pages/denuncia/contratacion/modal-contratacion/modal-contratacion.component';
import { ModalRegistroObraComponent } from './pages/ObraPublica/modal-registro-obra/modal-registro-obra.component';
import { InstanciaComponent } from './pages/denuncia/instancia/instancia.component';
import { ModalHechoIrregularComponent } from './pages/ObraPublica/modal-hecho-irregular/modal-hecho-irregular.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EvalinfRegfurComponent } from './pages/evalinf-regfur/evalinf-regfur.component';

import { DgHojaEvalComponent } from './pages/datos-general/dg-hoja-eval/dg-hoja-eval.component';
import { EvidenciasComponent } from './pages/denuncia/evidencias/evidencias.component';
//import { ModalAntecedenteComponent } from './pages/evalInf-regfur/modal-antecedente/modal-antecedente.component';
import { AntecedentesFurComponent } from './pages/evalinf-regfur/antecedentes-fur/antecedentes-fur.component';
import { ModalAntecedenteComponent } from './pages/evalinf-regfur/modal-antecedente/modal-antecedente.component';
//import { AntecedentesFurComponent } from './pages/antecedentes-fur/antecedentes-fur.component';
import { FurComponent } from './pages/fur/fur.component';
import { ModalInstanciaComponent } from './pages/denuncia/instancia/modal-instancia/modal-instancia.component';
import { BodyRegistroComponent } from './pages/Dashboard/body-registro/body-registro.component';
import { BodyDatosGeneralComponent } from './pages/Dashboard/body-datos-general/body-datos-general.component';
import { BodyDatosExpedienteComponent } from './pages/Dashboard/body-datos-expediente/body-datos-expediente.component';
import { BodyDatosOtrosComponent } from './pages/Dashboard/body-datos-otros/body-datos-otros.component';
import { ContenedorAsigInfoComponent } from './pages/Dashboard/contenedor-asig-info/contenedor-asig-info.component';
import { RegistroDocumentoComponent } from './pages/registro-documento/registro-documento.component';
import { AsignarInformacionComponent } from './pages/asignar-informacion/asignar-informacion.component';
import { DatosRegistroComponent } from './pages/asignar-informacion/datos-registro/datos-registro.component';
import { DgAsigRespComponent } from './pages/General/dg-asig-resp/dg-asig-resp.component';
import { AsignaResponsableComponent } from './pages/General/asigna-responsable/asigna-responsable.component';
import { ContenedorWebComponent } from './pages/formulario-web/contenedor-web/contenedor-web.component';
import { AgrupacionComponent } from './pages/Denunciante/agrupacion/agrupacion.component';
import { ExpedienteComponent } from './pages/MedidasProt/expediente/expediente.component';
import { EvaluacionFudComponent } from './pages/evalinf-regfur/evaluacion-fud/evaluacion-fud.component';
import { DocAGenerarComponent } from './pages/General/doc-a-generar/doc-a-generar.component';
import { DocumentosEmitidosComponent } from './pages/General/documentos-emitidos/documentos-emitidos.component';
import { FirmaDigitalComponent } from './pages/firma-digital/firma-digital.component';
import { NavModalComponent } from './pages/General/nav-modal/nav-modal.component';
import { AtenderSolicitudComponent } from './pages/atender-solicitud/atender-solicitud.component';
import { BodyOpcionesServicioAteComponent } from './pages/Dashboard/body-opciones-servicio-ate/body-opciones-servicio-ate.component';
import { ContenedorServAtenCiudComponent } from './pages/Dashboard/contenedor-serv-aten-ciud/contenedor-serv-aten-ciud.component';
import { BodyOrientarAtenderSolicitudComponent } from './pages/Dashboard/body-orientar-atender-solicitud/body-orientar-atender-solicitud.component';



@NgModule({
  declarations: [
    AppComponent,
 
    HechoIrregularComponent,
    DatosGeneralComponent,
    DenunciaContenedorComponent,
    EntidadHechoComponent,
    FuncionarioInvolucradoComponent,
    EmpresaInvolucradaComponent,
    PruebaHechoComponent,
    FormularioWebComponent,
    ModalMontoComponent,
    ModalEmpresaComponent,
    ModalEntidadComponent,
    ModalFuncionarioComponent,
    ContenedorDenuncianteComponent,
    ModalIrregInfobrasComponent,
    ModalMensajeComponent,
    ModalBusqObrpuComponent,
    LoginGcComponent,
    PanelPrincipalComponent,
    BodyOpcionesComponent,
    BodyBusquedaComponent,
    PersonaJuridicaComponent,
    PersonaNaturalComponent,
    ModalNotificacionComponent,
    ContenedorMpComponent,
    ModalMpComponent,
    ModalContratacionComponent,
    ModalRegistroObraComponent,
    InstanciaComponent,
    ModalHechoIrregularComponent,
    EvidenciasComponent,
    FurComponent,
    ModalInstanciaComponent,
    ModalAntecedenteComponent,
    AntecedentesFurComponent,
    EvalinfRegfurComponent,
    DgHojaEvalComponent,
    
    BodyRegistroComponent,
    
    BodyDatosGeneralComponent,
    
    BodyDatosExpedienteComponent,
    
    BodyDatosOtrosComponent,
    
    ContenedorAsigInfoComponent,
    
    RegistroDocumentoComponent,
    
    AsignarInformacionComponent,
    
    DatosRegistroComponent,
    
    DgAsigRespComponent,
    
    AsignaResponsableComponent,


    ContenedorWebComponent,
    
    AgrupacionComponent,
    
    ExpedienteComponent,
    
    EvaluacionFudComponent,
    DocAGenerarComponent,
    
    DocumentosEmitidosComponent,
    
    FirmaDigitalComponent,
    
    NavModalComponent,
    
    AtenderSolicitudComponent,
    
    BodyOpcionesServicioAteComponent,
    
    ContenedorServAtenCiudComponent,
    
    PanelPrincipalServicioComponent,
    
    BodyOrientarAtenderSolicitudComponent
  


  ],
  imports: [
    
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    HttpClientModule,
    MatSlideToggleModule,
    BrowserModule, 
    RecaptchaModule,
    CommonModule, // modulo para directivas angular ngModel
    FormsModule, // modulo para directivas angular ngModel
    ReactiveFormsModule, // modulo para directivas angular ngModel
    RouterModule.forRoot(router, {useHash: true}),
    MatDialogModule,
    NgxMaskModule.forRoot(options),
    MatIconModule,
    FontAwesomeModule,
    MatTooltipModule,
    MatTableModule,
    MatPaginatorModule
    ,
    NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatNativeDateModule,
    MatDatepickerModule
     ,
     NgxMatMomentModule
  ],
  exports:[
    MatDialogModule
  ],
  entryComponents:[
    ModalMontoComponent,
 
    ContenedorDenuncianteComponent,
    //PersonaJuridicaComponent,
    //PersonaNaturalComponent,
    ModalNotificacionComponent,
    ContenedorMpComponent,
    ModalMpComponent,
    ModalNotificacionComponent,
    ModalEmpresaComponent,
    ModalEntidadComponent,
    ModalFuncionarioComponent,
    ModalMensajeComponent,
    ModalBusqObrpuComponent,
    ModalIrregInfobrasComponent,
  
    ModalContratacionComponent,
    ModalRegistroObraComponent,

    ModalHechoIrregularComponent,
    ModalAntecedenteComponent,
    ModalInstanciaComponent
  ],
  providers: [DatePipe,
            CookieService,
            //{provide: DateAdapter, useClass: MomentDateAdapter},
            //{ provide: MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS },
            {provide: MAT_DATE_LOCALE, useValue: 'es-ES'}
          ],
  bootstrap: [AppComponent]
})
export class AppModule { }
