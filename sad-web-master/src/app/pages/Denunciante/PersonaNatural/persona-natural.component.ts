import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TipoDocumento } from 'src/app/models/TipoDocumento';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { TipoDocumentoService } from 'src/app/services/tipo-documento.service';
import { PersonaReniecService } from 'src/app/services/persona-reniec.service';
import { PersonaService } from 'src/app/services/persona.service';
import { PersonaReniec } from 'src/app/models/PersonaReniec';
import { Persona } from 'src/app/models/Persona';
import { MatDialog } from '@angular/material/dialog';
import { openModalMensaje, ConvertStringDateStr, ConvertStrDateToFormatBD, stringFechaMaxima, anioActualGlobal, ConvertDateTimeToStr3, ageFromDateOfBirth } from 'src/app/general/variables';
import { Fur } from 'src/app/models/Fur';


@Component({
  selector: 'app-persona-natural',
  templateUrl: './persona-natural.component.html',
  styleUrls: ['./persona-natural.component.css']
})
export class PersonaNaturalComponent implements OnInit {

  listTipoDocumento: TipoDocumento[] = [];
  personaReniec: PersonaReniec = new PersonaReniec();
  persona: Persona = new Persona();//new Persona();
  dscMask: string;
  flgPersBD = false;
  flgDeshabilitaValidar = false;
  flgLectura : boolean = false;
  fechaEmisionDni : string;
  nombreMadre : string;

  @Output() datosComplementOut = new EventEmitter();

  formPersonaNatural = new FormGroup({
    select_tipDocumento : new FormControl('', Validators.required),
    input_nroDoc : new FormControl(''),
    input_apepaterno : new FormControl({value: '', disabled: true}),
    input_apematerno : new FormControl({value: '', disabled: true}),
    input_domicilio : new FormControl(''),
    input_telefono : new FormControl(''),
    input_email : new FormControl(''),
    input_movil : new FormControl('')
  });

  fechaMaxima : string = stringFechaMaxima;
  constructor(protected tipoDocumentoService: TipoDocumentoService,
    protected personaReniecService: PersonaReniecService,
    protected personaService: PersonaService,
    public dialog: MatDialog
    ) { }

  ngOnInit() {

    this.tipoDocumentoService.obtenerTipoDocumento().subscribe(
      data => {
        this.listTipoDocumento = data;
        //this.formPersonaNatural.controls.select_tipDocumento.setValue("1");

        
      }
    );

  }

  cambiarTipDocumento(idTipDocumento: number){

    console.log("====================Carga Maskara");
    console.log(this.listTipoDocumento.length);
    console.log(idTipDocumento);
    
    for (let i = 0; i < this.listTipoDocumento.length; i++) 
    {

      if (this.listTipoDocumento[i].ntipdocumentoId == idTipDocumento)//this.listTipoDocumento[i].ntipdocumentoId.toString() == idTipDocumento
      {

          this.dscMask = this.listTipoDocumento[i].ctipdocumentoMask;
          

      }
    
    }

  }

  ValidarReniec(){

    var vFechaEmi = "";
    var vFechaEmiAux = "";
    var vFlgError = false;
    this.flgPersBD = false
    
    var dscNombMadre = this.nombreMadre;//this.formPersonaNatural.controls.input_nommadre.value;
    var fchEmiDni = this.fechaEmisionDni;//this.formPersonaNatural.controls.fecha_emision_dni.value;
    var dscNroDoc = this.persona.cpersonaNrodoc//this.formPersonaNatural.controls.input_nroDoc.value;

    //console.log("===========valida renica 1");


    if(dscNombMadre.length == 0 || fchEmiDni == null || dscNroDoc.length == 0)
    {

      openModalMensaje("Datos Reniec","Ingrese el número de documento, fecha de emisión y nombre de la madre, por favor verifique",this.dialog);

      return;

    }

    this.flgDeshabilitaValidar = true;

    if(!this.flgPersBD)
    {

      //this.persona = new Persona();
      let fechaMaxima = new Date(fchEmiDni);

      if(fechaMaxima.getFullYear()>anioActualGlobal){

        openModalMensaje("Datos Reniec","La fecha de emisión no puede ser mayor al año actual",this.dialog);

      }else{

        this.personaService.obtenerPersona('1',dscNroDoc).subscribe(personaMa =>{

          let resultadoMa = JSON.parse(JSON.stringify(personaMa));

          console.log(personaMa);

            if(resultadoMa == null)
            {
              
              this.personaReniecService.obtenerPersonaReniec(dscNroDoc).subscribe(
                data => {

                  console.log(data);

                  this.flgDeshabilitaValidar = false;

                  if(data != null) 
                  {

                    if(data.dcFirstName != null)
                    {

                      this.personaReniec = data;

                      let nombreMaterno = this.personaReniec.dcMotherName.trim(); 

                      vFechaEmiAux = this.personaReniec.dcExpeditionDate;
                      vFechaEmi = vFechaEmiAux.substr(0,4) + "-" + vFechaEmiAux.substr(4,2) + "-" + vFechaEmiAux.substr(6);
    
                      
                    // console.log(this.persona.cpersona_prinommater);
                      console.log(vFechaEmi);
                      console.log(this.fechaEmisionDni);
                      //this.persona.cpersona_prinommater == this.formPersonaNatural.controls.input_nommadre.value && 
                      //if(this.persona.cpersona_prinommater.includes(this.formPersonaNatural.controls.input_nommadre.value) && this.formPersonaNatural.controls.fecha_emision_dni.value.toString() == vFechaEmi)
                      if(this.personaReniec.dcMotherName.includes(this.nombreMadre) && this.fechaEmisionDni == vFechaEmi)
                      {
                          
                        this._llenarPersona();
    
                        /*
                        this.formPersonaNatural.controls.input_nombres.setValue(this.persona.cpersonaNombres);
                        this.formPersonaNatural.controls.input_apepaterno.setValue(this.persona.cpersonaApepaterno);
                        this.formPersonaNatural.controls.input_apematerno.setValue(this.persona.cpersonaApematerno);
                        this.formPersonaNatural.controls.input_domicilio.setValue(this.persona.cpersonaDscdireccion);
                        this.formPersonaNatural.controls.input_telefono.setValue(this.persona.cpersonaDscMovil);
                        this.formPersonaNatural.controls.input_email.setValue(this.persona.cpersonaDscCorreo);
                        this.formPersonaNatural.controls.input_movil.setValue(this.persona.cpersonaDscMovil);
                        */
          
                      }
                      else
                      {

                        vFlgError = true;
                        
          
                      }

                    }else{
                      vFlgError = true;
                    }

                    if(vFlgError)
                    {

                      openModalMensaje("Datos Reniec","La fecha de emisión y el primer nombre de la madre no coinciden con los datos de la persona consultada, por favor verifique",this.dialog);
                      
                      return;

                    }

                    
                  }
                }
              );
  
  
            }else{

              this.personaReniec = resultadoMa;

              this.flgDeshabilitaValidar = false;

              if(this.personaReniec.dcMotherName.includes(this.nombreMadre) && this.fechaEmisionDni == vFechaEmi)
                    {
                        
                      this._llenarPersona();
  
                      /*
                      this.formPersonaNatural.controls.input_nombres.setValue(this.persona.cpersonaNombres);
                      this.formPersonaNatural.controls.input_apepaterno.setValue(this.persona.cpersonaApepaterno);
                      this.formPersonaNatural.controls.input_apematerno.setValue(this.persona.cpersonaApematerno);
                      this.formPersonaNatural.controls.input_domicilio.setValue(this.persona.cpersonaDscdireccion);
                      this.formPersonaNatural.controls.input_telefono.setValue(this.persona.cpersonaDscMovil);
                      this.formPersonaNatural.controls.input_email.setValue(this.persona.cpersonaDscCorreo);
                      this.formPersonaNatural.controls.input_movil.setValue(this.persona.cpersonaDscMovil);
                      */
        
                    }
                    else
                    {
                      openModalMensaje("Datos Reniec","La fecha de emisión y el primer nombre de la madre no coinciden con los datos de la persona consultada, por favor verifique",this.dialog);
                      
                      return;
        
                    }
            }
        });
        
      }



    }
    
  }


  _llenarPersona()
  {

    this.persona.cpersonaNombres = this.personaReniec.dcFirstName.trim();
    this.persona.cpersonaApepaterno = this.personaReniec.dcLastName1.trim();
    this.persona.cpersonaApematerno = this.personaReniec.dcLastName2.trim();
    this.persona.cpersonaDscdireccion = this.personaReniec.dcAddress.trim();

    this.persona.cpersonaPrinommater = this.personaReniec.dcMotherName.trim();//.trim();
    this.persona.cpersonaCodDepartamento = this.personaReniec.dcAddDepUbigCode.trim();
    this.persona.cpersonaCodProvincia = this.personaReniec.dcAddProvUbigCode.trim();
    this.persona.cpersonaCodDistrito = this.personaReniec.dcAddDistUbigCode.trim();
    this.persona.strDpersonaFemidoc = ConvertStrDateToFormatBD(this.fechaEmisionDni);//ConvertStringDateStr(this.fechaEmisionDni);

    // console.log("==============this.personaReniec.dcBornDate.trim()");
    // console.log(this.personaReniec.dcBornDate.trim());

    var vfchBornDate = ConvertDateTimeToStr3(this.personaReniec.dcBornDate.trim());

    // console.log("==============vfchBornDate");
    // console.log(vfchBornDate);

    var vfchNac = new Date(vfchBornDate);

    // console.log("==============vfchNac");
    // console.log(vfchNac);

    var vNumEdad = ageFromDateOfBirth(vfchNac);

    // console.log("=====================vNumEdad");
    // console.log(vNumEdad);
    
    this.datosComplementOut.emit(vNumEdad);

  }

  mostrarFecha(){
   console.log(this.fechaEmisionDni);
  }

  personaNaturalCReadEnable(){
    this.flgLectura = true;
  }

  personaNaturalCReadDisable(){
    this.flgLectura = false;
  }

  setPersonaNaturalC(fur : Fur){
    this.persona = fur.denunciante.persona;
    this.nombreMadre = this.persona.cpersonaPrinommater;
    this.fechaEmisionDni = ConvertStringDateStr(this.persona.strDpersonaFemidoc);
    console.log("FECHA DE EMISION: " + this.fechaEmisionDni );
  }


  mayusculas(e){
   // console.log(e);
    e.target.value = e.target.value.toUpperCase();
    e.target.value = e.target.value.trim();
    //this.nombreMadre = this.nombreMadre.toUpperCase();
    // e.key = e.key.toUpperCase();
  }

  // openDialog(dscTitulo: string, dscMensaje: string) {
  //   const dialogRef = this.dialog.open(ModalMensajeComponent,{ width: '50%',height:'35%',
  //   data: {titulo : dscTitulo,mensaje: dscMensaje}});
    
  //   dialogRef.afterClosed().subscribe(result => {
  //     //console.log('The dialog was closed');
  //   });
  // }

  
}
