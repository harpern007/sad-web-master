import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Persona } from 'src/app/models/Persona';
import { ExternosService } from 'src/app/services/externos/externos.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { openModalMensaje, iconEliminar, DscTituloPaginator } from 'src/app/general/variables';
import { ModalEmpresaComponent } from '../../denuncia/empresa/modal-involucrada/modal-empresa/modal-empresa.component';
import { Fur } from 'src/app/models/Fur';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-persona-juridica',
  templateUrl: './persona-juridica.component.html',
  styleUrls: ['./persona-juridica.component.css']
})
export class PersonaJuridicaComponent implements OnInit {
  //Icono
  iconoEliminar = iconEliminar;
  //
  flgRepreLegal: boolean;
  persona: Persona = null;
  
  dataSource: MatTableDataSource<Persona>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  matTableColumnsDef = ["ruc","razonSocial","direccion","representante","acciones"];

  /*formPersonaJuridica = new FormGroup({
    radio_reprelegal : new FormControl('', Validators.required),
    radio_validasunat : new FormControl('ruc'),
    input_validasunat : new FormControl(''),
  });*/

  flgLectura : boolean = false;
 
  empresaList: Persona[] = []; 
  cbk_ruc  : boolean = true;
  cbk_empresa : string = "RUC";
  inputRuc :string;
  inputRazonSocial ; string;
  empresa_registrada :boolean  = false;
  flgRucDisabled = false;
  flgRazonSocialDisabled = true;
  flgDeshabilitaValidar = false;



  //flgDeshabilitaValidar = false;//Estaba false

  /*formPersonaJuridica = new FormGroup({
    input_ruc: new FormControl(''),
    input_razon_social : new FormControl(''),
    flg_empresa_registrada : new FormControl(''),
    cbk_empresa : new FormControl('RUC'),
    empresa_registrada  : new FormControl(false)
    
  });*/

  constructor(private externoService : ExternosService,public dialog: MatDialog) { }

  ngOnInit() {

    this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;
  }


  CambioRepreLegal(flgEvent){

    this.flgRepreLegal = flgEvent.checked;

  }

  // RepreLegalNO(){

  //   this.flgRepreLegal = false;

  // }

  // RepreLegalSI(){

  //   this.flgRepreLegal = true;

  // }


  ////

//   obtenerEmpresa(){

//     this.flgDeshabilitaValidar = true;

//     let isRuc = this.formPersonaJuridica.get('cbk_empresa').value;
//     let valorInput;
//     let resultado ;
//     let persona = new Persona();
//     console.log(isRuc);
//     if(isRuc == 'RAZON_SOCIAL' ){ 
//       ///Refactorizar codigo
//         if(this.formPersonaJuridica.get('input_razon_social').value != ''){
          
          
//             valorInput = this.formPersonaJuridica.get('input_razon_social').value;
//             this.externoService.obtenerEmpresaXRazonSocial(valorInput).subscribe(data=>{
//               resultado  = JSON.parse(JSON.stringify(data));
//               if(resultado.listaRetorno != null){
               
//                 persona = this.cargarDatosPersona(resultado.listaRetorno[0]);
//                 this.abrirModal(persona,true,this.empresaList);
//                 /*this.dialog.open(ModalEmpresaComponent,{width:'60%',data:{cpersonaNroDoc:persona.cpersonaNroDoc,
//                   cpersonaRazonsocial:persona.cpersonaRazonsocial,flgValida:true,listaEmpresa: this.empresaList}});*/
//               }else{
//                 console.log(resultado.mensaje);
//               }

//              });
          
          
         
//         }else{
//           this.flgDeshabilitaValidar = false;
//           openModalMensaje('Empresa Involucrada','Debe ingresar una razón social',this.dialog);
//         }
        
//     }else{

//       if(this.formPersonaJuridica.get('input_ruc').value != ''){
//         if(this.formPersonaJuridica.get('input_ruc').value.length < 11){
//           this.flgDeshabilitaValidar = false;
//           openModalMensaje('Empresa Involucrada','Debe ingresar un ruc valido',this.dialog);
//         }else{
//            valorInput = this.formPersonaJuridica.get('input_ruc').value;
//           this.externoService.obtenerEmpresaXRUC(valorInput).subscribe(data=>{
//               resultado = JSON.parse(JSON.stringify(data));
//               persona = this.cargarDatosPersona(resultado.listaRetorno[0]);
//               this.abrirModal(persona,true,this.empresaList);
//               /*this.dialog.open(ModalEmpresaComponent,{width:'60%',data:{cpersonaNroDoc:persona.cpersonaNroDoc,
//                 cpersonaRazonsocial:persona.cpersonaRazonsocial,flgValida:true,listaEmpresa: this.empresaList}});*/
//           });
//         }
      
//       }else{
//         this.flgDeshabilitaValidar = false;
//         openModalMensaje('Empresa Involucrada','Debe ingresar un ruc',this.dialog);
        
//       }
     
//     }
  
// }
obtenerEmpresa(){

  this.flgDeshabilitaValidar = true;
  //this.dscTextoValidar = 
  
  let isRuc = this.cbk_empresa;//this.formEmpresaInvolucrada.get('cbk_empresa').value;
  let valorInput = this.inputRazonSocial;
  let resultado ;
  let persona = new Persona();

  if(isRuc == 'RAZON_SOCIAL' ){ 
    ///Refactorizar codigo
      if(valorInput != ''){//this.formEmpresaInvolucrada.get('input_razon_social').value
        
        
      valorInput = //this.formEmpresaInvolucrada.get('input_razon_social').value;
          this.externoService.obtenerEmpresaXRazonSocial(valorInput).subscribe(data=>{

            resultado  = JSON.parse(JSON.stringify(data));
            //if(resultado.listaRetorno != null){
              if(resultado.ruc != null){
              //persona = this.cargarDatosPersona(resultado.listaRetorno[0]);
              persona = this.cargarDatosPersona(resultado);
              this.abrirModal(persona,true,this.empresaList);
               
            }else{
              openModalMensaje('No se encontró datos de la razón social','Debe ingresar una razón social valida',this.dialog);
            }
            /*this.dialog.open(ModalEmpresaComponent,{width:'60%',data:{cpersonaNroDoc:persona.cpersonaNroDoc,
              cpersonaRazonsocial:persona.cpersonaRazonsocial,flgValida:true,listaEmpresa: this.empresaList}});*/
           });
        
        
       
      }else{
        this.flgDeshabilitaValidar = false;
        openModalMensaje('Empresa Involucrada','Debe ingresar una razón social',this.dialog);
      }
      
  }else{

    if(this.inputRuc != ''){ //this.formEmpresaInvolucrada.get('input_ruc').value
      if(this.inputRuc.length < 11){ //this.formEmpresaInvolucrada.get('input_ruc').value.length
        this.flgDeshabilitaValidar = false;
        openModalMensaje('Empresa Involucrada','Debe ingresar un ruc valido',this.dialog);
      }else{
         valorInput = this.inputRuc;//this.formEmpresaInvolucrada.get('input_ruc').value;
        this.externoService.obtenerEmpresaXRUC(valorInput).subscribe(data=>{
            resultado = JSON.parse(JSON.stringify(data));
           // if(resultado.listaRetorno != null){
            if(resultado.ruc != null){
              persona = this.cargarDatosPersona(resultado);
              this.abrirModal(persona,true,this.empresaList);
            }else{
              openModalMensaje('No se encontró datos del RUC','Debe ingresar un ruc valido',this.dialog);
            }
          
            /*this.dialog.open(ModalEmpresaComponent,{width:'60%',data:{cpersonaNroDoc:persona.cpersonaNroDoc,
              cpersonaRazonsocial:persona.cpersonaRazonsocial,flgValida:true,listaEmpresa: this.empresaList}});*/
        });
      }
    
    }else{
      this.flgDeshabilitaValidar = false;
      openModalMensaje('Empresa Involucrada','Debe ingresar un ruc',this.dialog);
      
    }
   
  }

}

cargarDatosPersona(resultado){

   let persona = new Persona();
   persona.cpersonaNrodoc = resultado.ruc;
   persona.cpersonaRazonsocial = resultado.razon_Social;
    //Faltaría agregar campo dirección y representante legal
    persona.cpersonaDscdireccion = resultado.domicilio_Legal; 
   persona.cpersonaDscreprelegal = resultado.representante;
   persona.cpersonaCodDepartamento = resultado.ubigeo.substring(0,2);
   persona.cpersonaCodProvincia = resultado.ubigeo.substring(2,4);
   persona.cpersonaCodDistrito = resultado.ubigeo.substring(4,6);

   return persona;

}


abrirModal(personaArg:Persona,flgValidaArg : boolean, listaEmpresaArg : Persona[]){
  const configuracionGlobal = new MatDialogConfig();
  configuracionGlobal.disableClose = true;
  configuracionGlobal.autoFocus = true;
  configuracionGlobal.data = {
    cpersonaNrodoc:personaArg.cpersonaNrodoc,
    cpersonaRazonsocial:personaArg.cpersonaRazonsocial,
    cpersonaDscdireccion : personaArg.cpersonaDscdireccion,
    cpersonaDscreprelegal :personaArg.cpersonaDscreprelegal,
    flgValida:flgValidaArg,
    listaEmpresa: listaEmpresaArg,
    departamento : personaArg.cpersonaCodDepartamento,
    provincia : personaArg.cpersonaCodProvincia,
    distrito : personaArg.cpersonaCodDistrito
  }
  configuracionGlobal.width = "60%";
  const  modal=this.dialog.open(ModalEmpresaComponent, configuracionGlobal);

      modal.afterClosed().subscribe( data=>{
        this.persona = new Persona();
        if(data != null){
          this.flgDeshabilitaValidar = false;
          this.empresa_registrada = false;
          this.inputRazonSocial = "";
          this.inputRuc = "";
          //this.formEmpresaInvolucrada.get('empresa_registrada').setValue(false);
          this.persona = data.listaEmpresa[0];
          this._cargarTableMaterial();
        }
     
      }
       
    );  
}


cambiarEmpresaRegistada(evento){
  console.log(evento);
  if(evento.target.checked){
    let persona  = new Persona();
    this.abrirModal(persona,false,this.empresaList);
  }

}

rucSeleccionado(evento){

  this.inputRazonSocial = "";
  this.flgRucDisabled = false;
  this.flgRazonSocialDisabled = true;
  console.log(this.cbk_empresa);
  //this.formEmpresaInvolucrada.get('input_razon_social').setValue('');
}

razonSeleccionado(){
  this.inputRuc = "";
  this.flgRazonSocialDisabled = false;
  this.flgRucDisabled = true;
  //this.formEmpresaInvolucrada.get('input_ruc').setValue('');  
}
  empresaCReadEnable(){
    this.flgLectura = true;
    this.flgRazonSocialDisabled = true;
    this.flgRucDisabled = true;
  }

  empresaCReadDisable(){
    this.flgLectura = false;
    this.flgRazonSocialDisabled = false;
    this.flgRucDisabled = false;
  }

  setPersonaJuridica(fur : Fur){
    this.persona = fur.denunciante.persona;
    this.empresaList.push(fur.denunciante.persona);
    this.flgRepreLegal = fur.denunciante.cdenuncianteFlgreprelegal == "SI" ? true : false;
    this._cargarTableMaterial();
    console.log("Representante legal: " + this.flgRepreLegal);
  }

  _cargarTableMaterial(){
    this.dataSource = new MatTableDataSource(this.empresaList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  eliminar(empresa){
    this.empresaList.splice(this.empresaList.indexOf(empresa),1);
    this._cargarTableMaterial();
  }
}
