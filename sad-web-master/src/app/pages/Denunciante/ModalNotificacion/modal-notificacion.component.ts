import { Component, OnInit, Inject, Output, EventEmitter, ViewChild } from '@angular/core';
import { MedioNotifService } from 'src/app/services/med-notif.service';
import { MedioNotif } from 'src/app/models/MedioNotif';
import { Distrito } from 'src/app/models/Distrito';
import { Provincia } from 'src/app/models/Provincia';
import { Departamento } from 'src/app/models/Departamento';
import { FormGroup, FormControl } from '@angular/forms';
import { DistritoService } from 'src/app/services/distrito.service';
import { ProvinciaService } from 'src/app/services/provincia.service';
import { DepartamentoService } from 'src/app/services/departamento.service';
import { ModalNotificacion } from 'src/app/models/ModalNotificacion';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { General } from 'src/app/models/General';
import { NavModalComponent } from '../../General/nav-modal/nav-modal.component';

@Component({
  selector: 'app-modal-notificacion',
  templateUrl: './modal-notificacion.component.html',
  styleUrls: ['./modal-notificacion.component.css']
})
export class ModalNotificacionComponent implements OnInit {

  listMedioNotif: MedioNotif[] = [];
  listDistrito: Distrito[] = [];
  listProvincia: Provincia[] = [];
  listDepartamento: Departamento[] = [];
  dataReturn: General = new General();

  mostrarCheckCambioEmail = false;
  mostrarCambioEmail = false;
  mostrarCheckDireccion = false;
  mostrarCambioDireccion = false;


  //
  medioNotificacion : string;
  email : string;
  departamento : string;
  distrito : string
  provincia : string;
  direccion : string;
  referencia : string;
  //
  formModalNotificacion = new FormGroup({
    //select_medNotif: new FormControl(''),
   // input_dsc_dir: new FormControl(''),
    //input_dsc_ref_dir: new FormControl(''),
    //select_departamento : new FormControl(''),
    //select_provincia : new FormControl(''),
    //select_distrito : new FormControl(''),
    //input_email : new FormControl(''),
    radio_cambemail : new FormControl(''),
    radio_cambdirecc : new FormControl(''),
  });

  @Output() modalEmitter: EventEmitter<FormGroup> = new EventEmitter();
  @ViewChild(NavModalComponent) navModalC : NavModalComponent;

  constructor( private dialogRef: MatDialogRef<ModalNotificacionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ModalNotificacion,
    protected medioNotifService: MedioNotifService,
    protected distritoService: DistritoService,
    protected provinciaService: ProvinciaService,
    protected departamentoService: DepartamentoService
    ) { }

  ngOnInit() {

      this.navModalC.setDatosNavModal2("Notificaciones");

      this.medioNotifService.listarMedioNotif().subscribe(
      data => {
        this.listMedioNotif = data;
        //console.log(data);
        this.medioNotificacion = null;
        //this.formModalNotificacion.controls.select_medNotif.setValue(null);
      });

      this.departamentoService.obtenerDepartamento().subscribe(
        data => {
          this.listDepartamento = data;
          //console.log(data);
          this.departamento = null;
          //this.formModalNotificacion.controls.select_departamento.setValue(null);
        });

      // console.log("==================this.data");
      // console.log(this.data);

      //this.formModalNotificacion.controls.select_departamento.setValue(this.data.departamento);
        this.departamento = this.data.departamento;
      this.cambiarDepartamento(this.data.departamento,this.data.provincia);
      this.cambiarProvincia(this.data.provincia,this.data.distrito,this.data.departamento);

  }

  // onNoClick(): void {
  //   this.dialogRef.close();
  // }

  cambiarDepartamento(idDepartamento: string,idProvincia:string){

    this.provinciaService.obtenerProvincia(idDepartamento).subscribe(
      data => {
        this.listProvincia = data;
        //console.log(data);
        //this.formModalNotificacion.controls.select_provincia.setValue(idProvincia);
        this.provincia = idProvincia;
        //this.formModalNotificacion.controls.select_distrito.setValue(null);
        this.distrito = null;
      });

  }

  cambiarProvincia(idProvincia: string, idDistrito: string,idDepartamento: string)
  {

    this.distritoService.obtenerDistrito(idProvincia,idDepartamento).subscribe(
      data => {
        this.listDistrito = data;
        //console.log(data);
        //this.formModalNotificacion.controls.select_distrito.setValue(idDistrito);
        this.distrito = idDistrito;
      });

  }

  MostrarMedioNotif(idMedNotif: string){

    this.mostrarCheckCambioEmail = false;
    this.mostrarCheckDireccion = false;
    console.log("Cambio : " + idMedNotif);

    if(idMedNotif == "1")
    {

      this.mostrarCambioEmail = false;
      this.mostrarCheckCambioEmail = true;

    }else if(idMedNotif == "2")
    {

      //this._CambDirNo();
      this.mostrarCambioDireccion = false;

      this.mostrarCheckDireccion = true;

    }

  }

  // _CambMailSi(){

  //   this.mostrarCambioEmail = true;

  // }

  // _CambMailNo(){

  //   this.mostrarCambioEmail = false;

  // }

  CambioMail(flgEvent){
  
    this.mostrarCambioEmail = flgEvent.checked;

    
  }

  CambioDir(flgEvent){
  
    this.mostrarCambioDireccion = flgEvent.checked;
    
    
  }

  // _CambDirSi(){

  //   this.mostrarCambioDireccion = true;

  // }

  // _CambDirNo(){

  //   this.mostrarCambioDireccion = false;

  // }

  AceptarModal(){


    this.dataReturn.numCampo1 = Number(this.medioNotificacion);//this.formModalNotificacion.controls.select_medNotif.value;

    this.dataReturn.flgCampo1 = this.mostrarCambioEmail;
    this.dataReturn.flgCampo2 = this.mostrarCambioDireccion;

    this.dataReturn.dscCampo1 = this.email;//this.formModalNotificacion.controls.input_email.value;

    this.dataReturn.dscCampo2 = this.direccion;//this.formModalNotificacion.controls.input_dsc_dir.value;
    this.dataReturn.dscCampo6 = this.referencia;//this.formModalNotificacion.controls.input_dsc_ref_dir.value;
    this.dataReturn.dscCampo3 = this.departamento;//this.formModalNotificacion.controls.select_departamento.value;
    this.dataReturn.dscCampo4 = this.provincia;//this.formModalNotificacion.controls.select_provincia.value;
    this.dataReturn.dscCampo5 = this.distrito;//this.formModalNotificacion.controls.select_distrito.value;

    //this.modalEmitter.emit(this.formModalNotificacion);
   
    this.dialogRef.close(this.dataReturn);
    //this.dialogRef.close();

  }


  _cerrar(){
    this.dialogRef.close(null);
  }
}
