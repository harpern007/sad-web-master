import { Component, OnInit, Output, EventEmitter, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormGroup } from '@angular/forms';
import { TipoDenuncianteService } from 'src/app/services/tipo-denunciante.service';
import { TipoDenunciante } from 'src/app/models/TipoDenunciante';
import { ModalNotificacionComponent } from '../ModalNotificacion/modal-notificacion.component';
import { PersonaNaturalComponent } from '../PersonaNatural/persona-natural.component';
import { PersonaJuridicaComponent } from '../PersonaJuridica/persona-juridica.component';
import { AgrupacionComponent } from '../agrupacion/agrupacion.component';
import { Denunciante } from 'src/app/models/Denunciante';
import { ModalBusqObrpuComponent } from '../../ObraPublica/modal-busq-obrpu/modal-busq-obrpu.component';
import { General } from 'src/app/models/General';
import { openModalMensaje, ConvertirFlag, getBooleanFlag, NumIdTipDenAnonimo, constobraPublica } from 'src/app/general/variables';
import { Fur } from 'src/app/models/Fur';
import { MailDetalle } from 'src/app/models/MailDetalle';
import { CatalogoService } from 'src/app/services/Catalogo.service';
import { Catalogo } from 'src/app/models/Catalogo';


@Component({
  selector: 'app-contenedor-denunciante',
  templateUrl: './contenedor-denunciante.component.html',
  styleUrls: ['./contenedor-denunciante.component.css']
})

export class ContenedorDenuncianteComponent implements OnInit {

  @Output() opSolicitaMPB = new EventEmitter<FormGroup>() ;

  @ViewChild(PersonaNaturalComponent) personaNaturalComponent : PersonaNaturalComponent;  

  @ViewChild(PersonaNaturalComponent) personaNaturalCAux : PersonaNaturalComponent;  

  @ViewChild(PersonaJuridicaComponent)
  personaJuridicaComponent : PersonaJuridicaComponent;  

  @ViewChild(AgrupacionComponent)
  agrupacionComponent : AgrupacionComponent;  

  //@ViewChild('myInput') campoTipDenun: ElementRef; 

  listTipoDenunciante: TipoDenunciante[] = [];
  mostrarPersonaNatural = false;
  mostrarPersonaJuridica = false;
  mostrarAgrupacion = false;
  flgAnonimo = false;
  flgTrabEntDenun: boolean;
  flgNotificacion: boolean;
  flgSolicMP: boolean;
  dataModalNotif : General = new General();
  //vPersonaNaturalC = null; 
  idMedioNotif:number;
  //Email
  email : MailDetalle = new MailDetalle();
  //CUS04
  flgEsFur : boolean = false;

  //CUS01 - NUEVO 
  rangoEdad : string = "";
  flgEsServidorPublico : boolean = false;
  flgEsTestigoHecho : boolean = false;
  flgAccesoInformacion : boolean = false;
  dscEnteroHecho : string = "";
  flgSexo : string = "";
  listRangoEdad:Catalogo[] = [];
  /**
   * GSOLIS
   */
  flgLectura : boolean = false;
   /**
    * 
    */
  //vPersonaNaturalC: PersonaNaturalComponent;
  
  denunciante : Denunciante = new Denunciante();
  // public fcomp: any;
 
  /*formContenedorDenunciante = new FormGroup({
    select_tipDenunciante : new FormControl(''),
    radio_entidaddenun : new FormControl(''),
    radio_medprotec : new FormControl(''),
    radio_notificacion : new FormControl('')
  });*/

  constructor(protected tipoDenuncianteService: TipoDenuncianteService,
    protected catalogoService: CatalogoService,
    
    public dialog: MatDialog,
    private ref: ChangeDetectorRef
    ) { }

  ngOnInit() {

  
    this.tipoDenuncianteService.listarTipoDenunciante().subscribe(
      data => {

        this.listTipoDenunciante = data;

        //console.log(data);

      }
    );

    this.catalogoService.listarCatalogo(3).subscribe(
      data => {

        this.listRangoEdad = data;
        
        
      });
    
    this.denunciante.ndenuncianteIdtipden = NumIdTipDenAnonimo;

    this.MostrarSeccionPersona(NumIdTipDenAnonimo);

  }


  MostrarSeccionPersona(idTipoDenunciante: Number) {

    this.mostrarPersonaNatural = false;
    this.mostrarPersonaJuridica = false;
    this.mostrarAgrupacion = false;

    this.flgAnonimo = (idTipoDenunciante == NumIdTipDenAnonimo ? true:false);
   
    if(idTipoDenunciante == 2)
    {

      this.mostrarPersonaJuridica = true;

    }else{

      //console.log(idTipoDenunciante);

      if(idTipoDenunciante == 1 || idTipoDenunciante == 4)  //Natural o Congrrsista
      {
  
        this.mostrarPersonaNatural = true;
        
        //vPersonaNaturalC = this.personaNaturalComponent;

        setTimeout(()=> {

          this.CambiarValoresPersonaNat(this.personaNaturalComponent,1);
  
        }, 500);
  
      }

      if(idTipoDenunciante == 3)
      {

        this.mostrarAgrupacion = true;

        //vPersonaNaturalC = this.agrupacionComponent.personaNaturalComponent; 
        setTimeout(()=> {

          this.CambiarValoresPersonaNat(this.agrupacionComponent.personaNaturalComponent,1);
  
        }, 500);
        
      }
     

    }

  }

  CambiarValoresPersonaNat(aPersonaNaturalC : PersonaNaturalComponent, caso:number)
  {

    var dscEmailN = null;
    var dscDirN = null;
    var codDepartamentoN = null;
    var codProvinciaN = null;
    var codDistritoN = null;
    var dscRefDirN = null;
    //setTimeout(()=> {
    
    switch (caso) {
      case 1://DATOS POR DEFECTO CUANDO CAMBIA EL TIPO DE DENUNCIANTE

        //aPersonaNaturalC.formPersonaNatural.controls.select_tipDocumento.setValue("1");
        aPersonaNaturalC.persona.npersonaIdtipdoc =1;
        aPersonaNaturalC.cambiarTipDocumento(1);
       
        
        break;

      case 2://DATOS PARA CAMBIO DE NOTIFICACION POR EMAIL

        dscEmailN = this.dataModalNotif.dscCampo1;

        //aPersonaNaturalC.formPersonaNatural.controls.input_email.setValue(dscEmailN);
        aPersonaNaturalC.persona.cpersonaDsccorreo = dscEmailN;
        this.email.cbpmmaildetDestinatario = dscEmailN;
        this.email.cbpmmaildetRemitente = 'gsolis@inspiratls.com';
       
        break;

      case 3://DATOS PARA CAMBIO DE NOTIFICACION POR DIRECCION

        dscDirN = this.dataModalNotif.dscCampo2;
        codDepartamentoN = this.dataModalNotif.dscCampo3;
        codProvinciaN = this.dataModalNotif.dscCampo4;
        codDistritoN = this.dataModalNotif.dscCampo5;
        dscRefDirN = this.dataModalNotif.dscCampo6;

        //aPersonaNaturalC.formPersonaNatural.controls.input_domicilio.setValue(dscDirN);
        aPersonaNaturalC.persona.cpersonaDscdireccion = dscDirN;
        aPersonaNaturalC.persona.cpersonaDscReferencia =  dscRefDirN;
        aPersonaNaturalC.persona.cpersonaCodDepartamento = codDepartamentoN;
        aPersonaNaturalC.persona.cpersonaCodProvincia = codProvinciaN;
        aPersonaNaturalC.persona.cpersonaCodDistrito = codDistritoN;

        break;
    
      default:
        break;
    }

  }

  openModalNotif() {

    var flgCambioEmail = null;
    var flgCambioDir = null;
    var numCaso = 0;    
    //var vPersonaNaturalC = (this.mostrarPersonaNatural ? this.personaNaturalComponent : this.agrupacionComponent.personaNaturalComponent);
    
    
    const dialogRef = this.dialog.open(ModalNotificacionComponent,{ width: '75%',height:'75%',
    data: {departamento:"1",provincia: "1", distrito:"1", direccion:"av. direccion"}});
    
    dialogRef.afterClosed().subscribe(result => {

      if(result != null)
      {

        this.dataModalNotif = result;

        flgCambioEmail = this.dataModalNotif.flgCampo1;
        flgCambioDir = this.dataModalNotif.flgCampo2;
        //dscEmailN = this.dataModalNotif.dscCampo1;

        this.idMedioNotif = this.dataModalNotif.numCampo1;

        if(!this.mostrarPersonaJuridica) //solo para persona natural
        {

          if(flgCambioEmail)
          {

            numCaso = 2;

          }

          if(flgCambioDir)
          {

            numCaso = 3;

          
          }

          if(this.mostrarPersonaNatural)
          {

            this.CambiarValoresPersonaNat(this.personaNaturalComponent, numCaso);

          }else{

            this.CambiarValoresPersonaNat(this.agrupacionComponent.personaNaturalComponent, numCaso);

          }

        
        }

      }
      else{
        this.flgNotificacion = false;
      }

      


    });

  }

  _datosComplementOut(numEdad : number)
  {

    var vNumRangoIni = 0;
    var vNumRangoFin = 0;

    var vStrRangoIni = "";
    var vStrRangoFin = "";

    console.log("================numEdad");
    console.log(numEdad);

    this.listRangoEdad.forEach(element => {

      vStrRangoIni = element.ccatalogoDsc.substring(0,2);
      vStrRangoFin = element.ccatalogoDsc.substring(5);

      console.log("===========rangos");
      console.log(vStrRangoIni);
      console.log(vStrRangoFin);



      vNumRangoIni =  isNaN(Number(vStrRangoIni)) ? 0 : Number(vStrRangoIni); ;
      vNumRangoFin = isNaN(Number(vStrRangoFin)) ? 999 : Number(vStrRangoFin);

      console.log("===========rangos0");
      console.log(vNumRangoIni);
      console.log(vNumRangoFin);

      if(vNumRangoIni <= numEdad &&  vNumRangoFin >= numEdad) 
      {

        this.denunciante.ndenuncianteIdrangoedad = element.ncatalogoId;

        return;

      }
      
    });

    //this.listRangoEdad.filter(x => );

  }

  CambioNotif(flgEvent)
  {

    var flgChecked = flgEvent.checked;
    var persona = null;

    this.flgNotificacion = flgChecked;

    if(flgChecked)
    {

     
      if (this.mostrarPersonaNatural)
      {

        persona = this.personaNaturalComponent.persona;

      }else if(this.mostrarPersonaJuridica)
      {

        persona = this.personaJuridicaComponent.persona;

      }else if (this.mostrarAgrupacion)
      {

        persona = this.agrupacionComponent.personaNaturalComponent.persona;

      }

      if (this.denunciante.ndenuncianteIdtipden == 0 || persona == null)//this.formContenedorDenunciante.controls.select_tipDenunciante.value.length
      {

        openModalMensaje("Denunciante","Debe ingresar primero los datos de la persona natural o jurídica, por favor verifique.",this.dialog)
        .afterClosed()
        .subscribe(result => {

          this.flgNotificacion = false;

        });

        return;

        // const dialogRef = this.dialog.open(ModalNotificacionComponent,{ width: '75%',height:'75%',
        // data: {departamento:"1",provincia: "1", distrito:"1", direccion:"av. direccion"}});
        
        

      }

      this.openModalNotif();

    }

  }


  cambioTrabaja(flgEvent){
  
    this.flgTrabEntDenun = flgEvent.checked;
  }

  
  CambioSolic(flgEvent)
  {

    //var vFlgEvent = flgEvent.checked;
    var vFlgEvent = flgEvent;

    this.flgSolicMP = vFlgEvent;
  
    this.opSolicitaMPB.emit(vFlgEvent);

  }


  ProvisionalBorrar(){

    const dialogRef = this.dialog.open(ModalBusqObrpuComponent,{ width: '75%',height:'75%'});
    
    
    dialogRef.afterClosed().subscribe(result => {

    });

  }

  contenedorDenuncianteReadEnable(){

    this.flgLectura = true;

    //Persona
    this.MostrarSeccionPersona(this.denunciante.ndenuncianteIdtipden);

    setTimeout(()=>{
      if(this.mostrarPersonaNatural ){
        this.personaNaturalComponent.personaNaturalCReadEnable();
      }
      if(this.mostrarPersonaJuridica){
        this.personaJuridicaComponent.empresaCReadEnable();
      }
      if(this.mostrarAgrupacion){
        this.agrupacionComponent.agrupacionCReadEnable();
      }
    },0);
  }

  contenedorDenuncianteReadDisable(){
    this.flgLectura = false;
    //Persona
    this.MostrarSeccionPersona(this.denunciante.ndenuncianteIdtipden);
    setTimeout(()=>{
      if(this.mostrarPersonaNatural ){
        this.personaNaturalComponent.personaNaturalCReadDisable();
      }
      if(this.mostrarPersonaJuridica){
        this.personaJuridicaComponent.empresaCReadDisable();
      }
      if(this.mostrarAgrupacion){
        this.agrupacionComponent.agrupacionCReadDisable();
      }
    },0);
  }
 
  
  setContenedorDenuncianteC(fur : Fur){

    this.denunciante = fur.denunciante;

    // this.flgTrabEntDenun = fur.denunciante.cdenuncianteFlgtrabentden == "SI" ? true : false;
    // this.flgNotificacion = fur.denunciante.cdenuncianteFlgnotelec == "SI" ? true : false;
    // this.flgSolicMP = fur.denunciante.cdenuncianteFlgmedidaprotec == "SI" ? true : false;

    this.flgTrabEntDenun = getBooleanFlag(fur.denunciante.cdenuncianteFlgtrabentden);
    this.flgNotificacion = getBooleanFlag(fur.denunciante.cdenuncianteFlgnotelec);
    this.flgSolicMP = getBooleanFlag(fur.denunciante.cdenuncianteFlgmedidaprotec);

    this.flgEsServidorPublico = getBooleanFlag(fur.denunciante.cdenuncianteFlgservpub);
    this.flgEsTestigoHecho = getBooleanFlag(fur.denunciante.cdenuncianteFlgtestigoh);
    this.flgAccesoInformacion = getBooleanFlag(fur.denunciante.cdenuncianteFlgaccdirinf);

    if(fur.cfurCodtipo == 'FUR')
    {
      this.flgEsFur = true;
    }

    console.log("Flag es fur: " + this.flgEsFur);
    this.MostrarSeccionPersona(this.denunciante.ndenuncianteIdtipden);
    setTimeout(()=>{
      if(this.mostrarPersonaNatural ){
        this.personaNaturalComponent.setPersonaNaturalC(fur);
      }

      if(this.mostrarPersonaJuridica){
        this.personaJuridicaComponent.setPersonaJuridica(fur);
      }

      if(this.mostrarAgrupacion){
        this.agrupacionComponent.setAgrupacionC(fur);
      }

      if(this.flgSolicMP){
        this.CambioSolic(this.flgSolicMP);
      }
    },0);
   
    
  }

  asignarRango(evento){

    this.rangoEdad = evento.target.value;
  }

  cambioServidor(flgEvent){

    //var vFlgChecked = flgEvent.checked;
  
    this.flgEsServidorPublico = flgEvent;

    this.denunciante.cdenuncianteFlgservpub = ConvertirFlag(flgEvent);
  }

  cambioTestigo(flgEvent){

    //var vFlgChecked = flgEvent.checked;
  
    this.flgEsTestigoHecho = flgEvent;

    this.denunciante.cdenuncianteFlgtestigoh = ConvertirFlag(flgEvent);

  }

  cambioAccesoInfo(flgEvent){

    //var vFlgChecked = flgEvent.checked;
  
    this.flgAccesoInformacion = flgEvent;

    this.denunciante.cdenuncianteFlgaccdirinf = ConvertirFlag(flgEvent);

  }

  asignarSexo(evento)
  {

    //this.flgSexo = flgEvent.checked;
    this.denunciante.cdenuncianteCodsexo = evento.target.value

  }
}

