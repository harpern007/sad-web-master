import { Component, OnInit, ViewChild } from '@angular/core';
import { PersonaNaturalComponent } from '../PersonaNatural/persona-natural.component';
import { Fur } from 'src/app/models/Fur';

@Component({
  selector: 'app-agrupacion',
  templateUrl: './agrupacion.component.html',
  styleUrls: ['./agrupacion.component.css']
})
export class AgrupacionComponent implements OnInit {

  @ViewChild(PersonaNaturalComponent)
  personaNaturalComponent : PersonaNaturalComponent;  
  flgLectura : boolean = false;
  nombreAgrupacion : string ;
  /*
  formAgrupacion = new FormGroup({
    input_nom_agrup : new FormControl('')
  });
  */
  constructor() { }

  ngOnInit() {



  }

  agrupacionCReadEnable(){
    this.flgLectura = true;
    this.personaNaturalComponent.personaNaturalCReadEnable();
  }

  agrupacionCReadDisable(){
    this.flgLectura = false;
    this.personaNaturalComponent.personaNaturalCReadDisable();
  }

  setAgrupacionC(fur : Fur){
    this.personaNaturalComponent.setPersonaNaturalC(fur);
    this.nombreAgrupacion = fur.denunciante.cdenuncianteDscagrup;
  }
  // setTimeout(()=> {

  //   this.personaNaturalComponent.formPersonaNatural.controls.select_tipDocumento.setValue("1");;
  //   this.personaNaturalComponent.cambiarTipDocumento("1");
  //   this.personaNaturalComponent.formPersonaNatural.controls.select_tipDocumento.disable();

  // }, 2);


}
