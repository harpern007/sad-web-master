import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ContenedorMpComponent } from '../../MedidasProt/contenedor-mp/contenedor-mp.component';
import { DenunciaContenedorComponent } from '../../denuncia/denuncia-contenedor/denuncia-contenedor.component';
import { ContenedorDenuncianteComponent } from '../../Denunciante/Contenedor/contenedor-denunciante.component';
import { DatosGeneralComponent } from '../../datos-general/datos-general.component';
import { Fur } from 'src/app/models/Fur';
import { Persona } from 'src/app/models/Persona';
import { MformularioWebC } from 'src/app/models/personalizados/MformularioWebC';
import { FurService } from 'src/app/services/fur.service';
import { MatDialog } from '@angular/material/dialog';
import { DatosGeneral } from 'src/app/models/personalizados/DatosGeneral';
import { MdenunciaContenedorC } from 'src/app/models/personalizados/MdenunciaContenedorC';
import { MentidadHechoC } from 'src/app/models/personalizados/mEntidadHechoC';
import { PersonaNaturalComponent } from '../../Denunciante/PersonaNatural/persona-natural.component';
import { ConvertirFlag, openModalMensaje, ValidarEmail, NumCanalFormWeb, GetDateTimeString, GetDateTime, ConvertDateTimeToStrForBD, correoEmisor, NumIdEstadoHeEvaluacionSuperv, NumIdEstadoHeEvaluacionObservado, codigoIncial, anioActualGlobal } from 'src/app/general/variables';
import { Denunciante } from 'src/app/models/Denunciante';
import { DenuncianteXFamiliar } from 'src/app/models/DenuncianteXFamiliar';
import { FurXObraPublica } from 'src/app/models/FurXObraPublica';
import { DenunciadoXFicha } from 'src/app/models/DenunciadoXFicha';
import { FURXEmpresaInvolucrada } from 'src/app/models/FURXEmpresaInvolucrada';
import { DocumentoDet } from 'src/app/models/DocumentoDet';
import { Anexo } from 'src/app/models/Anexo';
import { EvaluacionPreliminarService } from 'src/app/services/bpm/evaluacion-preliminar.service';
import { Oficio } from 'src/app/models/BPM/oficio';

@Component({
  selector: 'app-contenedor-web',
  templateUrl: './contenedor-web.component.html',
  styleUrls: ['./contenedor-web.component.css']
})
export class ContenedorWebComponent implements OnInit {

  @ViewChild(DenunciaContenedorComponent) denunciaContenedorC : DenunciaContenedorComponent;
  @ViewChild(ContenedorDenuncianteComponent) contenedorDenuncianteC : ContenedorDenuncianteComponent;
  @ViewChild(ContenedorMpComponent) contenedorMpC : ContenedorMpComponent;
  @ViewChild(DatosGeneralComponent) datosGeneralC : DatosGeneralComponent;

  @Output()
  guardarEmitter = new EventEmitter();

  tabDenunciante : boolean = false;
  tabProteccion : boolean = false;
  flgTransicionTab = true;
  fur:Fur;
  persona: Persona = new Persona();
  flgDeshabilitaGuardar = false;
  //datosGeneral : DatosGeneral ;//= new DatosGeneral();
  mFormularioWebC : MformularioWebC;
  //CUS04 - 06 
  flgEsFud : boolean = false; //Para cuando es fud solo se debe mostrar los datos de la primera pestaña
  //furNew : Fur = new Fur();
  oficio : Oficio = new Oficio();
  constructor(protected furService: FurService,
    public dialog: MatDialog, 
    protected bpmService : EvaluacionPreliminarService) { }

  ngOnInit() {
  }

  verFormularioWeb(data:Fur){

    setTimeout(() => {
  
      this.flgTransicionTab = false;

      this.datosGeneralC.setDatosGenerales(data);
  
      this.denunciaContenedorC.entidadHechoC.entidadComponentReadEnable();
      this.denunciaContenedorC.hechoIrregularC.hechoIrregularCReadEnable();
      this.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.funcionarioCReadEnable();
      this.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.empresaCReadEnable();
      this.denunciaContenedorC.denunciaContenedorCReadEnable();
      this.denunciaContenedorC.pruebaHechoC.pruebaHechoCReadEnable();
      this.denunciaContenedorC.denunciaContenedorCReadEnable();
  
      this.denunciaContenedorC.entidadHechoC.setEntidadHecho(data);
      this.denunciaContenedorC.hechoIrregularC.setHechoIrregularComponent(data);
      //this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.setFuncionarioComponent(data);
      this.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.setEmpresaComponent(data);
      this.denunciaContenedorC.pruebaHechoC.setPruebaHechoComponent(data);
      this.denunciaContenedorC.setDenunciaContenedorC(data);

      this.denunciaContenedorC.instanciaC.instanciaCReadEnable();
      
      //if cus 04
      if(!this.flgEsFud){

        setTimeout(() => {
    
          if(this.contenedorDenuncianteC != null)
          {
    
            this.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
            this.contenedorDenuncianteC.setContenedorDenuncianteC(data);
    
          }
    
          setTimeout(() => {
    
            // console.log("Medidas de protección: " + this.contenedorWebC.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec);
    
            // console.log("========this.contenedorWebC.contenedorMpC");
            //   console.log(this.contenedorWebC.contenedorMpC);
    
    
            if(this.contenedorMpC != null)
            {
    
              this.contenedorMpC.medidaProteccionCReadEnable();
              this.contenedorMpC.setMedidaProteccionC(data);
    
              // console.log("========this.contenedorWebC.contenedorMpC.flgLectura");
              // console.log(this.contenedorWebC.contenedorMpC.flgLectura);
    
            }
            
          }, 0);
    
          // console.log("===========this.contenedorWebC");
          // console.log(this.contenedorWebC);
    
         
          
        }, 0);
      }
      
    }, 0);
  
  }

  ConsultarFur(fur: Fur){

    // console.log("===============0FUR");
    // console.log(fur);

    this.mFormularioWebC = new MformularioWebC();

    this.mFormularioWebC.datosGeneral = new DatosGeneral();

    this.mFormularioWebC.datosGeneral.estado_formulario = fur.estado.nestadoId.toString();
    this.mFormularioWebC.datosGeneral.fecha_registro = fur.fch_inicio_reg;
    this.mFormularioWebC.datosGeneral.hora_inicio = fur.hora_inicio_reg;
    this.mFormularioWebC.datosGeneral.nro_formulario = fur.cfurCodigo;

    this.datosGeneralC.ConsultarDatosGenerales(this.mFormularioWebC.datosGeneral);

    this.mFormularioWebC.mDenunciaContenedorC = new MdenunciaContenedorC();
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC = new MentidadHechoC();

    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.flg_deshabilita = true;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.select_entidades = fur.cfurCodent;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.inputDireccion = fur.cfurDirentidad;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.inputReferencia = fur.cfurDscrefentidad;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.select_departamento = fur.cfurCdepent;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.select_provincia = fur.cfurCprovent;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.select_distrito = fur.cfurCdistent;

    console.log("==========this.mFormularioWebC.mDenunciaContenedorC");
    console.log(this.mFormularioWebC.mDenunciaContenedorC);
    console.log(fur);

    this.denunciaContenedorC.ConsultarDenuncia(this.mFormularioWebC.mDenunciaContenedorC);

    //this.denunciaContenedorC.
    
    //this.datosGeneralC.datosGeneral.flg_deshabilita = true;

  }

  validarTab(valor){
    console.log(valor);
    //this.campoTipDenun.nativeElement.focus(); 
    //this.getTrainingName(4);
    //document.getElementById("aTabDenun").onclick();

    this.tabDenunciante = valor;

    if(valor)
    {

      if(this.flgTransicionTab)
      {

        setTimeout(() => {

          document.getElementById('aTabDenun').click();
          //CUS04
          if(this.denunciaContenedorC.flgEsFur){
            this.contenedorMpC.flgEsFur = true;
          }
        }, 1);

      }

    }
    
  }

  ActivaPanelMP(flgSolicitaMPB){

    this.tabProteccion = flgSolicitaMPB;

    if(flgSolicitaMPB)
    {

      if(this.flgTransicionTab)
      {

        setTimeout(() => {

          document.getElementById('aTabMP').click();
        //CUS04
        if(this.contenedorDenuncianteC.flgEsFur){
          this.contenedorMpC.flgEsFur = true;
        }

        }, 1);  

      }

    }

  }

validarGuardar() : boolean{
  var flgPruebas = this.denunciaContenedorC.pruebaHechoC.cuenta_pruebas;
  var vEntidadHechoC = this.denunciaContenedorC.entidadHechoC;
  var vHechoIrregular = this.denunciaContenedorC.hechoIrregularC;
  var cfurCodent = vEntidadHechoC.cfurCodent;
  var cfurCdepent = vEntidadHechoC.cfurCdepent;
  var cfurCdistent = vEntidadHechoC.cfurCdistent;
  var cfurCprovent = vEntidadHechoC.cfurCprovent;
  var cfurDscrefentidad = vEntidadHechoC.cfurDscrefentidad;
  var cfurDirentidad = vEntidadHechoC.cfurDirentidad;
  var nfurIdtipohecho = vEntidadHechoC.nfurIdtipohecho;
  var flgObraPub = vEntidadHechoC.flgObrasPub;
  var listObraPub = vEntidadHechoC.listObrasPublicas;
  var entidadSelec = vEntidadHechoC.entidad;
  var cfurDschecho = vHechoIrregular.cfurDschecho;
  var flgPerjuicioEco = ConvertirFlag(vHechoIrregular.perjuicio_economico);
  var flgMontoDesc = ConvertirFlag(vHechoIrregular.flgMontoDesc);
  var periodoMesIni = vHechoIrregular.select_mes_desde;
  var periodoAnhoIni = vHechoIrregular.select_anio_desde;
  var periodoMesFin = vHechoIrregular.select_mes_hasta;
  var periodoAnhoFin = vHechoIrregular.select_anio_hasta;
  var flgConoceFun = this.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.conoce_funcionario;
  var listDenunciado = this.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.listDenunciado;
  var listEmpresa = this.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.empresaList;
  var listEvidencias = [];
  var nfurIdtipdenun = null;
  var formContenedorDenunciante = null;
  var flgAgrupacion = null;
  var dscAgrupacion = null;
  var flgPersonaNatural = null;
   //PESTAÑA DENUNCIANTE
  formContenedorDenunciante = this.contenedorDenuncianteC;
  var personaNaturalC : PersonaNaturalComponent = null;
  var formPersonaNatural = null;
  var flgPersonaJuridica = null;
  var dscValidaSunat = null;
  var flgRepreLegalPJ = false;
  var flgSolicMP = null;
  var flgAceptoMP = null;
  var flgConfirmaSolMP = null;
  var vIdRangoEdad = null;
  var vCodSexo = null;
  var vDscEnteroH = null;
  

  if(cfurCodent == "" || cfurCodent == null) //this.fur.cfurCodent
  {

      openModalMensaje("Formulario Web","Debe seleccionar la entidad, por favor verifique.",this.dialog);
      return false;
  }

  // if(cfurDscrefentidad == "" || cfurDscrefentidad == null)
  // {

  //     openModalMensaje("Formulario Web","Debe ingresar la referencia de la entidad, por favor verifique.",this.dialog);
  //     return false;
  // }

  if(nfurIdtipohecho <= 0 || nfurIdtipohecho == null){
    openModalMensaje("Formulario Web","Debe ingresar el tipo de hecho irregular, por favor verifique.",this.dialog);
    return false;
  }
  if(listObraPub.length == 0 && flgObraPub)
  {

    openModalMensaje("Formulario Web","Debe seleccionar la obra pública y sus hechos irregulares, por favor verifique.",this.dialog);
    return   false;

  }

  if(cfurDschecho == "" || cfurDschecho == null)
  {

      openModalMensaje("Formulario Web","Debe ingresar la descripción del hecho irregular, por favor verifique.",this.dialog);
      return false;
  }

  if(flgPerjuicioEco)
  {


    if((periodoMesIni == "" || periodoMesIni == null) && (periodoAnhoIni == "" || periodoAnhoIni == null))
    {

        openModalMensaje("Formulario Web","Debe ingresar el periodo inicial del hecho irregular, por favor verifique.",this.dialog);
        return false;

    }

    if((periodoMesFin == "" || periodoMesFin == null) && (periodoAnhoFin == "" || periodoAnhoFin == null))
    {

      openModalMensaje("Formulario Web","Debe ingresar el periodo final del hecho irregular, por favor verifique.",this.dialog);
      return false;

    }

  }

  if(listDenunciado.length == 0)
  {

    openModalMensaje("Formulario Web","Debe ingresar a los funcionarios involucrados, por favor verifique.",this.dialog);
    return false;

  }

  if(listEmpresa.length == 0)
  {

    openModalMensaje("Formulario Web","Debe ingresar las empresas involucradas, por favor verifique.",this.dialog);
    return false;

  }

  if(flgPruebas){
    listEvidencias = this.denunciaContenedorC.pruebaHechoC.evidenciaC.listEvidencias;
  }

  
  if(flgPruebas && listEvidencias.length == 0)
  {

    openModalMensaje("Formulario Web","Debe ingresar las pruebas de hechos, por favor verifique.",this.dialog);
    return false;

  }
  

      //validaciones de denunciante
      var flgReqIdent = this.denunciaContenedorC.requireIdentificarse;
    
      if(flgReqIdent)
      {
        nfurIdtipdenun = formContenedorDenunciante.denunciante.ndenuncianteIdtipden;
        flgPersonaNatural = this.contenedorDenuncianteC.mostrarPersonaNatural;
        flgPersonaJuridica = this.contenedorDenuncianteC.mostrarPersonaJuridica;
        flgAgrupacion = this.contenedorDenuncianteC.mostrarAgrupacion;

        vIdRangoEdad = this.contenedorDenuncianteC.denunciante.ndenuncianteIdrangoedad;
        vCodSexo = this.contenedorDenuncianteC.denunciante.cdenuncianteCodsexo;
        vDscEnteroH = this.contenedorDenuncianteC.denunciante.cdenuncianteDscenterohecho;
        
        


        if(nfurIdtipdenun == 0 || nfurIdtipdenun == null)
        {
    
          openModalMensaje("Formulario Web","Debe seleccionar el tipo de denunciante, por favor verifique.",this.dialog);
          return false;
    
        }

        if(vIdRangoEdad == 0 || vIdRangoEdad == null)
        {
    
          openModalMensaje("Formulario Web","Debe seleccionar el rango de edad del denunciante, por favor verifique.",this.dialog);
          return false;
    
        }

        if(vCodSexo == "" || vCodSexo == null)
        {
    
          openModalMensaje("Formulario Web","Debe seleccionar el sexo del denunciante, por favor verifique.",this.dialog);
          return false;
    
        }

        if(vDscEnteroH == "" || vDscEnteroH == null)
        {
    
          openModalMensaje("Formulario Web","Debe ingresar como se entero del hecho, por favor verifique.",this.dialog);
          return false;
    
        }

        

        if(flgAgrupacion)
        {
          dscAgrupacion = this.contenedorDenuncianteC.agrupacionComponent.nombreAgrupacion;
          if(dscAgrupacion == "" || dscAgrupacion == null)
          {
    
            openModalMensaje("Formulario Web","Debe ingresar la descripción de la agrupación, por favor verifique.",this.dialog);
            return false;
    
          }
    
        }
  
        if(flgPersonaNatural || flgAgrupacion)
        {


          if (flgPersonaNatural)
          {
    
            personaNaturalC = this.contenedorDenuncianteC.personaNaturalComponent;
            //formPersonaNatural = personaNaturalC.formPersonaNatural.controls;
            //Revisar: GSOLIS
            if(personaNaturalC.persona != null){
              this.persona = personaNaturalC.persona;
              this.persona.strDpersonaFemidoc = personaNaturalC.persona.strDpersonaFemidoc;
            }else{
              openModalMensaje("Formulario Web","Debe ingresar la fecha de emisión de dni.",this.dialog);
              return false;
            }
          
          }else if (flgAgrupacion)
          {
            personaNaturalC = this.contenedorDenuncianteC.agrupacionComponent.personaNaturalComponent;
    
            //console.log(personaNaturalC);
    
            //formPersonaNatural = personaNaturalC.formPersonaNatural.controls;
            if(personaNaturalC.persona != null){
              this.persona = personaNaturalC.persona;
            }else{
              openModalMensaje("Formulario Web","Debe ingresar la fecha de emisión de dni.",this.dialog);
              return false;
            }
            //console.log(formPersonaNatural);
    
            //dscNomAgrup = this.contenedorDenuncianteC.agrupacionComponent.formAgrupacion.controls.input_nom_agrup.value;
    
            //if(dscNomAgrup == "" || dscNomAgrup == null)
            if(dscAgrupacion == "" || dscAgrupacion == null)
            {
    
              openModalMensaje("Formulario Web","Debe ingresar el nombre de la agrupación, por favor verifique.",this.dialog);
              return false;
    
            }
    
          }


          this.persona = personaNaturalC.persona;
          //
          console.log("===========guardar 6");
    
          if(this.persona.npersonaIdtipdoc == 0 || this.persona.npersonaIdtipdoc == null)
          {
    
            openModalMensaje("Formulario Web","Debe seleccionar el tipo de denunciante, por favor verifique.",this.dialog);
            return false;
    
          }
    
          if(this.persona.cpersonaNrodoc == "" || this.persona.cpersonaNrodoc == null)
          {
    
            openModalMensaje("Formulario Web","Debe ingresar el número de documento, por favor verifique.",this.dialog);
            return false;
    
          }
          


          if(this.persona.cpersonaNombres == "" || this.persona.cpersonaNombres == null)
          {
    
            openModalMensaje("Formulario Web","Debe ingresar el nombre de la persona, por favor verifique.",this.dialog);
            return false;
    
          }
    
          if(this.persona.cpersonaApepaterno == "" || this.persona.cpersonaApepaterno == null)
          {
    
            openModalMensaje("Formulario Web","Debe ingresar el apellido paterno de la persona, por favor verifique.",this.dialog);
            return false;
    
          }
    
          if(this.persona.cpersonaApematerno == "" || this.persona.cpersonaApematerno == null)
          {
    
            openModalMensaje("Formulario Web","Debe ingresar el apellido materno de la persona, por favor verifique.",this.dialog);
            return false;
    
          }
    
          if(this.persona.cpersonaDscdireccion == "" || this.persona.cpersonaDscdireccion == null)
          {
    
            openModalMensaje("Formulario Web","Debe ingresar la dirección de la persona, por favor verifique.",this.dialog);
            return false;
    
          }
    
          if(this.persona.cpersonaDsctelef == "" || this.persona.cpersonaDsctelef == null)
          {
    
            openModalMensaje("Formulario Web","Debe ingresar el teléfono de la persona, por favor verifique.",this.dialog);
            return false;
    
          }
    
          if(this.persona.cpersonaDsccorreo == "" || this.persona.cpersonaDsccorreo == null)
          {
    
            openModalMensaje("Formulario Web","Debe ingresar el correo de la persona, por favor verifique.",this.dialog);
            return false;
    
          }
    
          if(!ValidarEmail(this.persona.cpersonaDsccorreo))
          {
    
            openModalMensaje("Formulario Web","El correo ingresado no es válido, por favor verifique.",this.dialog);
            return false;
    
          }
    
          if(this.persona.cpersonaDscmovil == "" || this.persona.cpersonaDscmovil == null)
          {
    
            openModalMensaje("Formulario Web","Debe ingresar el móvil de la persona, por favor verifique.",this.dialog);
            return false;
    
          }
        }

        if(flgPersonaJuridica)
        {
          let nroDocRuc = this.contenedorDenuncianteC.personaJuridicaComponent.persona.cpersonaNrodoc;
          //persna juridica
          dscValidaSunat = nroDocRuc;//formPersonaJuridica.input_validasunat.value;
          if(dscValidaSunat == "" || dscValidaSunat == null)
          {
    
            openModalMensaje("Formulario Web","Debe ingresar la empresa o agrupación, por favor verifique.",this.dialog);
            return
    
          }
    
          this.persona = this.contenedorDenuncianteC.personaJuridicaComponent.persona;
          flgRepreLegalPJ =  this.contenedorDenuncianteC.personaJuridicaComponent.flgRepreLegal;
    
        }

        flgSolicMP = this.contenedorDenuncianteC.flgSolicMP;
        if(flgSolicMP)
        {
      
           //MP
          flgAceptoMP = this.contenedorMpC.flgAcepto;
          flgConfirmaSolMP = this.contenedorMpC.flgSolicita;
      
          if(!flgAceptoMP)
          {
      
            openModalMensaje("Formulario Web","Debe aceptar lo establecido en medidas de protección, por favor verifique.",this.dialog);
            return false;
      
          }
      

          // if(!flgSolicita)
          // {
      
          //   openModalMensaje("Formulario Web","Debe aceptar lo establecido en medidas de protección, por favor verifique.",this.dialog);
          //   return
      
          // }
      
        }
      }

     return true;
     
}

actualizarFur(vFur : Fur){
  this.persona = new Persona();

  var flgAceptoMP = null;
  var flgConfirmaSolMP = null;
  var formPersonaNatural = null;
  var formContenedorDenunciante = null;
  //var formPersonaJuridica  = null;
  var formAgrupacion  = null;
  var nfurIdtipdenun = null;
  var flgPersonaNatural = null;
  var flgPersonaJuridica = null;
  var flgAgrupacion = null;
  var flgNotificacion = null;
  var flgTrabEntDenun = null;
  var flgSolicMP = null;
  var dscValidaSunat = null;
  var dscAgrupacion = null;
  var cantTotalReg = 0;
  var personaNaturalC : PersonaNaturalComponent = null;
  var vIdMedioNotif = null;
  var dscNomAgrup = null;
  var flgRepreLegalPJ = false;
  var idFur = null;
  
  var vEntidadHechoC = this.denunciaContenedorC.entidadHechoC;


  var vHechoIrregular = this.denunciaContenedorC.hechoIrregularC;

  var flgObraPub = vEntidadHechoC.flgObrasPub;
  var listObraPub = vEntidadHechoC.listObrasPublicas;
  var entidadSelec = vEntidadHechoC.entidad;
  

  var cfurCodent = vEntidadHechoC.cfurCodent;
  var cfurCdepent = vEntidadHechoC.cfurCdepent;
  var cfurCdistent = vEntidadHechoC.cfurCdistent;
  var cfurCprovent = vEntidadHechoC.cfurCprovent;
  var cfurDscrefentidad = vEntidadHechoC.cfurDscrefentidad;
  var cfurDirentidad = vEntidadHechoC.cfurDirentidad;
  var nfurIdtipohecho = vEntidadHechoC.nfurIdtipohecho;
  var cfurDschecho = vHechoIrregular.cfurDschecho;
  var flgPerjuicioEco = ConvertirFlag(vHechoIrregular.perjuicio_economico);
  var flgMontoDesc = ConvertirFlag(vHechoIrregular.flgMontoDesc);
  var flgHechoContOcu = this.denunciaContenedorC.hechoIrregularC.ocurre_hecho;

  var nfurImpmontoperj = this.denunciaContenedorC.hechoIrregularC.nfurImpmontoperj;
  
  var periodoMesIni = vHechoIrregular.select_mes_desde;
  var periodoAnhoIni = vHechoIrregular.select_anio_desde;
  var periodoMesFin = vHechoIrregular.select_mes_hasta;
  var periodoAnhoFin = vHechoIrregular.select_anio_hasta;
  var flgConoceFun = this.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.conoce_funcionario;
  var listDenunciado = this.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.listDenunciado;
  var listEmpresa = this.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.empresaList;


  var flgPruebas = this.denunciaContenedorC.pruebaHechoC.cuenta_pruebas;
  var listEvidencias = [];
  if(flgPruebas){
    listEvidencias = this.denunciaContenedorC.pruebaHechoC.evidenciaC.listEvidencias;
  }

  var flgReqIdent = this.denunciaContenedorC.requireIdentificarse;

  console.log(this.denunciaContenedorC.pruebaHechoC);
  let instanciaC = this.denunciaContenedorC.instanciaC;
  var fchInicio = this.datosGeneralC.fecha_registro;
  var estadoFormulario = this.datosGeneralC.estado_formulario;

  this.fur = new Fur();



  if(flgReqIdent)
  {
    flgPersonaNatural = this.contenedorDenuncianteC.mostrarPersonaNatural;
    flgPersonaJuridica = this.contenedorDenuncianteC.mostrarPersonaJuridica;
    flgAgrupacion = this.contenedorDenuncianteC.mostrarAgrupacion;
    //PESTAÑA DENUNCIANTE
    formContenedorDenunciante = this.contenedorDenuncianteC;

    //PIE CONTENEDOR
    flgNotificacion = this.contenedorDenuncianteC.flgNotificacion;
    flgTrabEntDenun = this.contenedorDenuncianteC.flgTrabEntDenun;
    flgSolicMP = this.contenedorDenuncianteC.flgSolicMP;

    vIdMedioNotif = this.contenedorDenuncianteC.idMedioNotif;

    nfurIdtipdenun = formContenedorDenunciante.denunciante.ndenuncianteIdtipden;

    if(flgPersonaJuridica)
    {

      let nroDocRuc = this.contenedorDenuncianteC.personaJuridicaComponent.persona.cpersonaNrodoc;
    
      dscValidaSunat = nroDocRuc;

    }

    if(flgAgrupacion)
    {

    
      
      //Agrupación
      dscAgrupacion = this.contenedorDenuncianteC.agrupacionComponent.nombreAgrupacion;

    }

    if(flgPersonaNatural || flgAgrupacion)
    {

      console.log("===========guardar 4");

      if (flgPersonaNatural)
      {

        personaNaturalC = this.contenedorDenuncianteC.personaNaturalComponent;
        formPersonaNatural = personaNaturalC.formPersonaNatural.controls;
        //Revisar: GSOLIS
        this.persona.strDpersonaFemidoc = personaNaturalC.persona.strDpersonaFemidoc;
      }else if (flgAgrupacion)
      {
        personaNaturalC = this.contenedorDenuncianteC.agrupacionComponent.personaNaturalComponent;
        formPersonaNatural = personaNaturalC.formPersonaNatural.controls;
      }

      this.persona = personaNaturalC.persona;
    }


    if(flgPersonaJuridica)
    {
      this.persona = this.contenedorDenuncianteC.personaJuridicaComponent.persona;
      flgRepreLegalPJ =  this.contenedorDenuncianteC.personaJuridicaComponent.flgRepreLegal;
    }

    console.log("===========guardar 7");

    vFur.denunciante = new Denunciante();
    vFur.denunciante.persona = new Persona();
    vFur.denunciante.persona = this.persona;

    vFur.denunciante.cdenuncianteFlgmedidaprotec = ConvertirFlag(flgSolicMP);
    vFur.denunciante.cdenuncianteFlgtrabentden = ConvertirFlag(flgTrabEntDenun);
    vFur.denunciante.cdenuncianteFlgnotelec = ConvertirFlag(flgNotificacion);
    

    vFur.denunciante.cdenuncianteDscdirecc = this.persona.cpersonaDscdireccion;
    vFur.denunciante.cdenuncianteCdepartdomic = this.persona.cpersonaCodDepartamento;
    vFur.denunciante.cdenuncianteCprovdomic = this.persona.cpersonaCodProvincia;
    vFur.denunciante.cdenuncianteCdistdomic = this.persona.cpersonaCodDistrito;
    vFur.denunciante.cdenuncianteCreferenciadomic = this.persona.cpersonaDscReferencia;
    vFur.denunciante.cdenuncianteDsccorreo = this.persona.cpersonaDsccorreo;
    vFur.denunciante.cdenuncianteDsctelef = this.persona.cpersonaDsctelef;
    vFur.denunciante.cdenuncianteDscmovil = this.persona.cpersonaDscmovil;
    vFur.denunciante.ndenuncianteIdtipden = nfurIdtipdenun;
    vFur.denunciante.ndenuncianteIdmedionotif = vIdMedioNotif;
    vFur.denunciante.cdenuncianteDscagrup = dscNomAgrup;
    vFur.denunciante.cdenuncianteFlgreprelegal = ConvertirFlag(flgRepreLegalPJ);
    
    //Revisar : GSOLIS
    vFur.denunciante.cdenuncianteDscagrup = dscAgrupacion;
    console.log("===========guardar 8");

    //Notificacion de correo - GSOLIS
    this.contenedorDenuncianteC.email.cbpmmaildetDestinatario = this.persona.cpersonaDsccorreo;
    this.contenedorDenuncianteC.email.cbpmmaildetRemitente = correoEmisor;
    vFur.correoDetalle = this.contenedorDenuncianteC.email;

    //

    //PROVISIONAL PARA PRUEBA
    //var listDenuncianteXFamiliar: DenuncianteXFamiliar[] = [];

    vFur.denunciante.listaDenunciantePorFamiliar = [];

    for (let index = 0; index < 2; index++) 
    {

      vFur.denunciante.listaDenunciantePorFamiliar[index] = new DenuncianteXFamiliar();
      vFur.denunciante.listaDenunciantePorFamiliar[index].cdciantexfamDscapematerf = "prueba";
      vFur.denunciante.listaDenunciantePorFamiliar[index].cdciantexfamDscapepaterf = "prueba1";
      vFur.denunciante.listaDenunciantePorFamiliar[index].cdciantexfamDscnombres = "prueba2";
      vFur.denunciante.listaDenunciantePorFamiliar[index].cdciantexfamNrodocfam = "4460064" + index;
      vFur.denunciante.listaDenunciantePorFamiliar[index].ndciantexfamIdtipodocfam = 1;

    }

  }

  if(flgSolicMP)
  {
     //MP
    flgAceptoMP = this.contenedorMpC.flgAcepto;
    flgConfirmaSolMP = this.contenedorMpC.flgSolicita;
    vFur.denunciante.cdenuncianteFlgconfleyprot = ConvertirFlag(flgConfirmaSolMP);
    vFur.denunciante.cdenuncianteFlgacepmedpyb = ConvertirFlag(flgAceptoMP);
  }

  vFur.cfurCodent = entidadSelec.centCodigo;
  vFur.cfurCdepent = cfurCdepent;
  vFur.cfurCdistent = cfurCdistent;
  vFur.cfurCprovent = cfurCprovent;
  vFur.cfurDscrefentidad = cfurDscrefentidad;
  vFur.cfurDirentidad = cfurDirentidad;
  vFur.nfurIdtipohecho = nfurIdtipohecho;
  vFur.nfurIdcanal = NumCanalFormWeb;

  vFur.cfurFlgdirocuhecho = vEntidadHechoC.getStringDireccionHechos();

  vFur.cfurDschecho = cfurDschecho;

  vFur.cfurFlgexismonto = flgPerjuicioEco;
  vFur.cfurFlgemontodesc = flgMontoDesc;


  vFur.cfurFlghdenconocu = ConvertirFlag(flgHechoContOcu);
  vFur.nfurPeriodoohini = Number(periodoAnhoIni + periodoMesIni);
  vFur.nfurPeriodoohfin = Number(periodoAnhoFin + periodoMesFin);
  vFur.cfurFlgdatfuninv = ConvertirFlag(flgConoceFun);
  vFur.cfurFlgexispruh = ConvertirFlag(flgPruebas);
  vFur.cfurFlgreqidentif = ConvertirFlag(flgReqIdent);
  vFur.nfurImpmontoperj = nfurImpmontoperj;

  let fechaHoraString = this.datosGeneralC.fecha_registro + " " + this.datosGeneralC.hora_inicio;

  console.log(fechaHoraString);
  vFur.strDfurFecregistro = GetDateTime();
  vFur.strDfurFecinicioreg =fechaHoraString; 
  vFur.nfurIdestado = Number(estadoFormulario);
  vFur.nfurIdestadoformulario = Number(estadoFormulario);


  cantTotalReg = listObraPub.length;

  if(cantTotalReg > 0)
  {

    vFur.listaFurPorObraPublica = [];

  }

  for (let index = 0; index < cantTotalReg; index++) 
  {
    vFur.listaFurPorObraPublica[index] = new FurXObraPublica();
    vFur.listaFurPorObraPublica[index] = listObraPub[index];

  }
  
  cantTotalReg = listDenunciado.length;

  if(cantTotalReg > 0)
  {

    vFur.listaDenunciadoPorFicha = [];

  }

  listDenunciado.forEach(denunciado => {
      let denunciadoFicha = new DenunciadoXFicha();
      denunciadoFicha.cdciadoxfichaCodtipo = "FUR";
      denunciadoFicha.denunciado = denunciado;
      vFur.listaDenunciadoPorFicha.push(denunciadoFicha);
  });


  // console.log("=O====================11");
  cantTotalReg = listEmpresa.length;

  if(cantTotalReg > 0)
  {

    vFur.listaFurPorEmpresaInvolucrada = [];

  }

  //this.fur.ListaFurPorEmpresaInvolucrada = listEmpresa;
  for (let index = 0; index < listEmpresa.length; index++) 
  {

    // console.log("=O====================11.0");
    vFur.listaFurPorEmpresaInvolucrada[index] = new FURXEmpresaInvolucrada();

    // console.log("=O====================11.1");
    vFur.listaFurPorEmpresaInvolucrada[index].persona = listEmpresa[index];

    // console.log("=O====================11.2");

  }

  // console.log("=O====================12");

  cantTotalReg = listEvidencias.length;

  if(cantTotalReg > 0)
  {

    vFur.listaDocumentoAdjuntoDet = [];

  }

  //this.fur.listaDocumentoAdjuntoDet = listEvidencias; //PASA A LASERGICHE


  //PROVISIONAL PRUEBA DE INSTANCIAS ///REVISAR

  vFur.listaFichaPorInstancia = [];
  vFur.listaFichaPorInstancia = instanciaC.instanciaList;
 
  /*
  if(instanciaC != null){
    if(instanciaC.listInstancias != null){
      this.fur.listaFichaPorInstancia = instanciaC.listInstancias;
      this.fur.listaFichaPorInstancia.forEach(instancia =>{
          console.log("Fecha instancia: " +instancia.dfichaxinstanFecha);
          console.log("Codigo entidad instancia: " + instancia.entidad.centCodigo);
      });
    }
  }
*/
 
  
/*   if(idFurBase > 0){
    this.fur.nfurIdbase = idFurBase;
    this.fur.cfurCodtipo = "FUR";
   
  } */

/*   if(usuarioSup != null){
    this.fur.cfurIdususuperv = usuarioSup;
  }
  if(usuarioAsig != null){
    this.fur.cfurIdusuasig = usuarioAsig;
  }
  if(this.fur.correoDetalle != null){
    this.fur.correoDetalle.cbpmmailIdcodtip = this.fur.cfurCodtipo;
    this.fur.correoDetalle.nbpmmailIntento = 0;
    if(idFurBase > 0){
      this.fur.correoDetalle.cbpmmailIdcodtip="FUR";
      this.fur.correoDetalle.cbpmmaildetAsunto = "Registro de Formulario Web";
    }else{
      this.fur.correoDetalle.cbpmmaildetAsunto = "Registro de FUR";
    }
    
  
  }

  if(estadoFurEval != null){
    if(estadoFurEval == NumIdEstadoHeEvaluacionSuperv || estadoFurEval == NumIdEstadoHeEvaluacionObservado){
        this.fur.nfurIdestado = estadoFurEval;
    }
  }
   */
  //CUS 04 - 06
  if(this.contenedorMpC != null){
    if(this.contenedorMpC.flgEsFur && this.contenedorMpC.expedienteC.listaExpediente.length >0){
      this.fur.listaFichaExpediente = this.contenedorMpC.expedienteC.listaExpediente;
    }
  }
  return vFur;
}
/*
GSOLIS: SI IDFURBASE NO EXISTE = 0 */
Guardar(idFurBase : number,usuarioSup : string, usuarioAsig : string,estadoFurEval : number){
  
  this.persona = new Persona();

  var flgAceptoMP = null;
  var flgConfirmaSolMP = null;
  var formPersonaNatural = null;
  var formContenedorDenunciante = null;
  //var formPersonaJuridica  = null;
  var formAgrupacion  = null;
  var nfurIdtipdenun = null;
  var vNumIdRangoEdad = null;
  var vCodSexo = null;
  var vFlgServPub = null;
  var vFlgTestigoH =  null;
  var vFlgAccDir = null;
  var vDscEnteroH = null;

  var flgPersonaNatural = null;
  var flgPersonaJuridica = null;
  var flgAgrupacion = null;
  var flgNotificacion = null;
  var flgTrabEntDenun = null;
  var flgSolicMP = null;
  var dscValidaSunat = null;
  var dscAgrupacion = null;
  var cantTotalReg = 0;
  var personaNaturalC : PersonaNaturalComponent = null;
  var vIdMedioNotif = null;
  var dscNomAgrup = null;
  var flgRepreLegalPJ = false;
  var idFur = null;
  
  var vEntidadHechoC = this.denunciaContenedorC.entidadHechoC;

  //var formEntidadHecho = vEntidadHechoC.formEntidadHecho.controls;
  //var formHechoIrregular = this.denunciaContenedorC.hechoIrregularC.formHechoIrregular.controls;

  var vHechoIrregular = this.denunciaContenedorC.hechoIrregularC;

  // var formContenedorDenunciante = this.contenedorDenuncianteC.formContenedorDenunciante.controls;
  

  //var formDatosGenerales = this.datosGeneralC.formDatosGenerales.controls;
  
  //var formEmpresaInvolucrada = this.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.formEmpresaInvolucrada.controls;

  //var formFuncionarioC = this.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.formFuncionario.controls;

  // console.log("=O====================Formularios");
  // console.log(formEntidadHecho);
  // console.log(formHechoIrregular);
  // console.log(formDatosGenerales);
  var flgObraPub = vEntidadHechoC.flgObrasPub;
  var listObraPub = vEntidadHechoC.listObrasPublicas;
  var entidadSelec = vEntidadHechoC.entidad;
  
 /* //Form
  var cfurCodent = formEntidadHecho.select_entidades.value;
  var cfurCdepent = formEntidadHecho.select_departamento.value;
  var cfurCdistent = formEntidadHecho.select_distrito.value;
  var cfurCprovent = formEntidadHecho.select_provincia.value;
  var cfurDscrefentidad = formEntidadHecho.inputReferencia.value;
  var cfurDirentidad = formEntidadHecho.inputDireccion.value;
  var nfurIdtipohecho = formEntidadHecho.select_tipo_hecho.value;
 */
  var cfurCodent = vEntidadHechoC.cfurCodent;
  var cfurCdepent = vEntidadHechoC.cfurCdepent;
  var cfurCdistent = vEntidadHechoC.cfurCdistent;
  var cfurCprovent = vEntidadHechoC.cfurCprovent;
  var cfurDscrefentidad = vEntidadHechoC.cfurDscrefentidad;
  var cfurDirentidad = vEntidadHechoC.cfurDirentidad;
  var nfurIdtipohecho = vEntidadHechoC.nfurIdtipohecho;
  
  
  // console.log("=O====================1");

  //var cfurDschecho = formHechoIrregular.input_descripcion_hecho.value;
  var cfurDschecho = vHechoIrregular.cfurDschecho;
  // console.log("=O====================1.1");

  /*var flgPerjuicioEco = this.denunciaContenedorC.hechoIrregularC.perjuicio_economico;
  var vflgMontoDesc = this.denunciaContenedorC.hechoIrregularC.flgMontoDesc;
  */
  var flgPerjuicioEco = ConvertirFlag(vHechoIrregular.perjuicio_economico);
  var flgMontoDesc = ConvertirFlag(vHechoIrregular.flgMontoDesc);
  

  // console.log("=O====================1.2");

  var flgHechoContOcu = this.denunciaContenedorC.hechoIrregularC.ocurre_hecho;


  // console.log("=O====================1.3");

  //var nfurImpmontoperj = formHechoIrregular.monto.value;
  var nfurImpmontoperj = this.denunciaContenedorC.hechoIrregularC.nfurImpmontoperj;

  // console.log("=O====================2");

  /*var periodoMesIni = formHechoIrregular.select_mes_desde.value;
  var periodoAnhoIni = formHechoIrregular.select_anio_desde.value;
  var periodoMesFin = formHechoIrregular.select_mes_hasta.value;
  var periodoAnhoFin = formHechoIrregular.select_anio_hasta.value;*/
  
  var periodoMesIni = vHechoIrregular.select_mes_desde;
  var periodoAnhoIni = vHechoIrregular.select_anio_desde;
  var periodoMesFin = vHechoIrregular.select_mes_hasta;
  var periodoAnhoFin = vHechoIrregular.select_anio_hasta;

  // console.log("=O====================3");

  var flgConoceFun = this.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.conoce_funcionario;
  var listDenunciado = this.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.listDenunciado;
  var listEmpresa = this.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.empresaList;

  // console.log("============================listEmpresa");
  // console.log(listEmpresa);

  var flgPruebas = this.denunciaContenedorC.pruebaHechoC.cuenta_pruebas;
  var listEvidencias = [];
  if(flgPruebas){
    listEvidencias = this.denunciaContenedorC.pruebaHechoC.evidenciaC.listEvidencias;
  }
 //listFilesEvidencias;//this.denunciaContenedorC.pruebaHechoC.listEvidencias;

  var flgReqIdent = this.denunciaContenedorC.requireIdentificarse;


  //
  console.log(this.denunciaContenedorC.pruebaHechoC);
  let instanciaC = this.denunciaContenedorC.instanciaC;//pruebaHechoC.instanciaC;
  
  

  //General
  var fchInicio = this.datosGeneralC.fecha_registro;//formDatosGenerales.fecha_registro.value;
  var estadoFormulario = this.datosGeneralC.estado_formulario;//formDatosGenerales.estado_formulario.value;

  console.log("===========guardar 1");

  this.fur = new Fur();



  if(flgReqIdent)
  {

    console.log("===========guardar 2");

    flgPersonaNatural = this.contenedorDenuncianteC.mostrarPersonaNatural;
    flgPersonaJuridica = this.contenedorDenuncianteC.mostrarPersonaJuridica;
    flgAgrupacion = this.contenedorDenuncianteC.mostrarAgrupacion;
    //PESTAÑA DENUNCIANTE
    formContenedorDenunciante = this.contenedorDenuncianteC;//formContenedorDenunciante.controls;

    //PIE CONTENEDOR
    flgNotificacion = this.contenedorDenuncianteC.flgNotificacion;
    flgTrabEntDenun = this.contenedorDenuncianteC.flgTrabEntDenun;
    flgSolicMP = this.contenedorDenuncianteC.flgSolicMP;

    vIdMedioNotif = this.contenedorDenuncianteC.idMedioNotif;

    nfurIdtipdenun = formContenedorDenunciante.denunciante.ndenuncianteIdtipden;
    vNumIdRangoEdad = formContenedorDenunciante.denunciante.ndenuncianteIdrangoedad;
    vCodSexo = formContenedorDenunciante.denunciante.cdenuncianteCodsexo;
    vFlgServPub = formContenedorDenunciante.denunciante.cdenuncianteFlgservpub;
    vFlgTestigoH = formContenedorDenunciante.denunciante.cdenuncianteFlgtestigoh;
    vFlgAccDir = formContenedorDenunciante.denunciante.cdenuncianteFlgaccdirinf;
    vDscEnteroH = formContenedorDenunciante.denunciante.cdenuncianteDscenterohecho;

    console.log("===========guardar 3");

    if(flgPersonaJuridica)
    {

      //formPersonaJuridica  = this.contenedorDenuncianteC.PersonaJuridicaComponent.persona.cpersonaNrodoc;
      let nroDocRuc = this.contenedorDenuncianteC.personaJuridicaComponent.persona.cpersonaNrodoc;
       //persna juridica
      dscValidaSunat = nroDocRuc;//formPersonaJuridica.input_validasunat.value;

    }

    if(flgAgrupacion)
    {

      //formAgrupacion  = this.contenedorDenuncianteC.agrupacionComponent.formAgrupacion.controls;
      
      //Agrupación
      dscAgrupacion = this.contenedorDenuncianteC.agrupacionComponent.nombreAgrupacion;

    }

    if(flgPersonaNatural || flgAgrupacion)
    {

      console.log("===========guardar 4");

      if (flgPersonaNatural)
      {

        personaNaturalC = this.contenedorDenuncianteC.personaNaturalComponent;
        formPersonaNatural = personaNaturalC.formPersonaNatural.controls;
        //Revisar: GSOLIS
        this.persona.strDpersonaFemidoc = personaNaturalC.persona.strDpersonaFemidoc;
      }else if (flgAgrupacion)
      {
        personaNaturalC = this.contenedorDenuncianteC.agrupacionComponent.personaNaturalComponent;

        //console.log(personaNaturalC);

        formPersonaNatural = personaNaturalC.formPersonaNatural.controls;

      }

 
      
      //nGModel Persona
      this.persona = personaNaturalC.persona;
      //
      console.log("===========guardar 6");


    }


    if(flgPersonaJuridica)
    {

    
      this.persona = this.contenedorDenuncianteC.personaJuridicaComponent.persona;
      flgRepreLegalPJ =  this.contenedorDenuncianteC.personaJuridicaComponent.flgRepreLegal;

    }

    console.log("===========guardar 7");

    this.fur.denunciante = new Denunciante();
    this.fur.denunciante.persona = new Persona();
    this.fur.denunciante.persona = this.persona;

    this.fur.denunciante.cdenuncianteFlgmedidaprotec = ConvertirFlag(flgSolicMP);
    this.fur.denunciante.cdenuncianteFlgtrabentden = ConvertirFlag(flgTrabEntDenun);
    this.fur.denunciante.cdenuncianteFlgnotelec = ConvertirFlag(flgNotificacion);
    

    this.fur.denunciante.cdenuncianteDscdirecc = this.persona.cpersonaDscdireccion;
    this.fur.denunciante.cdenuncianteCdepartdomic = this.persona.cpersonaCodDepartamento;
    this.fur.denunciante.cdenuncianteCprovdomic = this.persona.cpersonaCodProvincia;
    this.fur.denunciante.cdenuncianteCdistdomic = this.persona.cpersonaCodDistrito;
    this.fur.denunciante.cdenuncianteCreferenciadomic = this.persona.cpersonaDscReferencia;
    this.fur.denunciante.cdenuncianteDsccorreo = this.persona.cpersonaDsccorreo;
    this.fur.denunciante.cdenuncianteDsctelef = this.persona.cpersonaDsctelef;
    this.fur.denunciante.cdenuncianteDscmovil = this.persona.cpersonaDscmovil;
    this.fur.denunciante.ndenuncianteIdtipden = nfurIdtipdenun;
    this.fur.denunciante.ndenuncianteIdmedionotif = vIdMedioNotif;
    this.fur.denunciante.cdenuncianteDscagrup = dscNomAgrup;
    this.fur.denunciante.cdenuncianteFlgreprelegal = ConvertirFlag(flgRepreLegalPJ);
    this.fur.denunciante.ndenuncianteIdrangoedad = vNumIdRangoEdad;

    this.fur.denunciante.cdenuncianteCodsexo = vCodSexo;
    this.fur.denunciante.cdenuncianteFlgservpub = vFlgServPub;
    this.fur.denunciante.cdenuncianteFlgtestigoh = vFlgTestigoH;
    this.fur.denunciante.cdenuncianteFlgaccdirinf = vFlgAccDir;
    this.fur.denunciante.cdenuncianteDscenterohecho = vDscEnteroH;

    //Revisar : GSOLIS
    this.fur.denunciante.cdenuncianteDscagrup = dscAgrupacion;
    console.log("===========guardar 8");

    //Notificacion de correo - GSOLIS
    this.contenedorDenuncianteC.email.cbpmmaildetDestinatario = this.persona.cpersonaDsccorreo;
    this.contenedorDenuncianteC.email.cbpmmaildetRemitente = correoEmisor;
    this.fur.correoDetalle = this.contenedorDenuncianteC.email;

    //

    //PROVISIONAL PARA PRUEBA
    //var listDenuncianteXFamiliar: DenuncianteXFamiliar[] = [];

    this.fur.denunciante.listaDenunciantePorFamiliar = [];

    for (let index = 0; index < 2; index++) 
    {

      this.fur.denunciante.listaDenunciantePorFamiliar[index] = new DenuncianteXFamiliar();
      this.fur.denunciante.listaDenunciantePorFamiliar[index].cdciantexfamDscapematerf = "prueba";
      this.fur.denunciante.listaDenunciantePorFamiliar[index].cdciantexfamDscapepaterf = "prueba1";
      this.fur.denunciante.listaDenunciantePorFamiliar[index].cdciantexfamDscnombres = "prueba2";
      this.fur.denunciante.listaDenunciantePorFamiliar[index].cdciantexfamNrodocfam = "4460064" + index;
      this.fur.denunciante.listaDenunciantePorFamiliar[index].ndciantexfamIdtipodocfam = 1;

    }

  }

  console.log("===========guardar 10");

  if(flgSolicMP)
  {

     //MP
    flgAceptoMP = this.contenedorMpC.flgAcepto;
    flgConfirmaSolMP = this.contenedorMpC.flgSolicita;
/* 
    if(!flgAceptoMP)
    {

      openModalMensaje("Formulario Web","Debe aceptar lo establecido en medidas de protección, por favor verifique.",this.dialog);
      return

    }
 */
    this.fur.denunciante.cdenuncianteFlgconfleyprot = ConvertirFlag(flgConfirmaSolMP);
    this.fur.denunciante.cdenuncianteFlgacepmedpyb = ConvertirFlag(flgAceptoMP);

    // if(!flgSolicita)
    // {

    //   openModalMensaje("Formulario Web","Debe aceptar lo establecido en medidas de protección, por favor verifique.",this.dialog);
    //   return

    // }

  }

  // console.log("=O====================5");

  this.fur.cfurCodent = entidadSelec.centCodigo;
  this.fur.cfurCdepent = cfurCdepent;
  this.fur.cfurCdistent = cfurCdistent;
  this.fur.cfurCprovent = cfurCprovent;
  this.fur.cfurDscrefentidad = cfurDscrefentidad;
  this.fur.cfurDirentidad = cfurDirentidad;
  this.fur.nfurIdtipohecho = nfurIdtipohecho;
  this.fur.nfurIdcanal = NumCanalFormWeb;

  this.fur.cfurFlgdirocuhecho = vEntidadHechoC.getStringDireccionHechos();
  // console.log("=O====================6");
  //Hecho Irregular
  this.fur.cfurDschecho = cfurDschecho;
  //this.fur.cfurFlgexismonto = ConvertirFlag(flgPerjuicioEco);
  //this.fur.cfurFlgemontodesc = ConvertirFlag(vflgMontoDesc);

  this.fur.cfurFlgexismonto = flgPerjuicioEco;
  this.fur.cfurFlgemontodesc = flgMontoDesc;


  this.fur.cfurFlghdenconocu = ConvertirFlag(flgHechoContOcu);
  this.fur.nfurPeriodoohini = Number(periodoAnhoIni + periodoMesIni);
  this.fur.nfurPeriodoohfin = Number(periodoAnhoFin + periodoMesFin);
  this.fur.cfurFlgdatfuninv = ConvertirFlag(flgConoceFun);
  this.fur.cfurFlgexispruh = ConvertirFlag(flgPruebas);
  this.fur.cfurFlgreqidentif = ConvertirFlag(flgReqIdent);
  this.fur.nfurImpmontoperj = nfurImpmontoperj;

  let fechaHoraString = this.datosGeneralC.fecha_registro + " " + this.datosGeneralC.hora_inicio;
  //let fechaHoraString = "26-03-2020 " + this.datosGeneralC.hora_inicio;
  
  console.log(fechaHoraString);
  this.fur.strDfurFecregistro = GetDateTime();
  this.fur.strDfurFecinicioreg =fechaHoraString; //ConvertDateTimeToStrForBD(new Date(fechaHoraString));//GetDateTimeString(fechaHoraString);
  this.fur.nfurIdestado = Number(estadoFormulario);
  this.fur.nfurIdestadoformulario = Number(estadoFormulario);


  cantTotalReg = listObraPub.length;

  if(cantTotalReg > 0)
  {

    this.fur.listaFurPorObraPublica = [];

  }

  for (let index = 0; index < cantTotalReg; index++) 
  {

      this.fur.listaFurPorObraPublica[index] = new FurXObraPublica();
      this.fur.listaFurPorObraPublica[index] = listObraPub[index];
      // this.fur.ListaDenunciadoPorFicha[index].Denunciado = new Denunciado();
      // this.fur.ListaDenunciadoPorFicha[index].Denunciado = listDenunciado[index];

  }
  
  cantTotalReg = listDenunciado.length;

  if(cantTotalReg > 0)
  {

    this.fur.listaDenunciadoPorFicha = [];

  }

  listDenunciado.forEach(denunciado => {
      let denunciadoFicha = new DenunciadoXFicha();
      denunciadoFicha.cdciadoxfichaCodtipo = "FUR";
      denunciadoFicha.denunciado = denunciado;
      this.fur.listaDenunciadoPorFicha.push(denunciadoFicha);
  });


  // console.log("=O====================11");
  cantTotalReg = listEmpresa.length;

  if(cantTotalReg > 0)
  {

    this.fur.listaFurPorEmpresaInvolucrada = [];

  }

  //this.fur.ListaFurPorEmpresaInvolucrada = listEmpresa;
  for (let index = 0; index < listEmpresa.length; index++) 
  {

    // console.log("=O====================11.0");
    this.fur.listaFurPorEmpresaInvolucrada[index] = new FURXEmpresaInvolucrada();

    // console.log("=O====================11.1");
    this.fur.listaFurPorEmpresaInvolucrada[index].persona = listEmpresa[index];

    // console.log("=O====================11.2");

  }

  // console.log("=O====================12");

  cantTotalReg = listEvidencias.length;

  if(cantTotalReg > 0)
  {

    this.fur.listaDocumentoAdjuntoDet = [];

  }

  /* for (let index = 0; index < listEvidencias.length; index++) 
  {

    /* GSOLIS 
    
    this.fur.listaDocumentoAdjuntoDet[index] = new DocumentoDet();
    this.fur.listaDocumentoAdjuntoDet[index].cdocumentoDscNombre = listEvidencias[index].name;
    this.fur.listaDocumentoAdjuntoDet[index].cdocumentoCodTipo = "FUR";
    this.fur.listaDocumentoAdjuntoDet[index].cdocumentoCodSubTipo = "FUR";

  } */
  //this.fur.listaDocumentoAdjuntoDet = listEvidencias; // SE GUARDARA POSTERIORMENTE MEDIANTE LASEFICHE
  //PROVISIONAL PRUEBA DE INSTANCIAS ///REVISAR

  this.fur.listaFichaPorInstancia = [];
  this.fur.listaFichaPorInstancia = instanciaC.instanciaList;
 
  /*
  if(instanciaC != null){
    if(instanciaC.listInstancias != null){
      this.fur.listaFichaPorInstancia = instanciaC.listInstancias;
      this.fur.listaFichaPorInstancia.forEach(instancia =>{
          console.log("Fecha instancia: " +instancia.dfichaxinstanFecha);
          console.log("Codigo entidad instancia: " + instancia.entidad.centCodigo);
      });
    }
  }
*/
 
  /*
  for (let index = 0; index < 2; index++) 
  {

    this.fur.listaFichaPorInstancia[index] = new FichaXInstancia();
    this.fur.listaFichaPorInstancia[index].cfichaxinstanCodent = "008" + index;
    this.fur.listaFichaPorInstancia[index].cfichaxinstanCodtipo = "FUR";
    //this.fur.listaFichaPorInstancia[index].dfichaxinstanFecha = "0083";

  }*/

  //PROVISIONAL ANEXOS
  /* this.fur.listaAnexo = [];

  for (let index = 0; index < 2; index++) 
  {

    this.fur.listaAnexo[index] = new Anexo();
    this.fur.listaAnexo[index].canexoCodtipofichabase = "FUR";
    this.fur.listaAnexo[index].nanexoIdfuranexo = index + 1;
    //this.fur.listaFichaPorInstancia[index].dfichaxinstanFecha = "0083";

  } */

  //PROVISIONAL HOJA EVAL
  /*Hoja de evaluación*/
  //this.fur.hojaEvaluacion = new HojaEvaluacion();
  //this.fur.hojaEvaluacion.nhojaevalIdestado = 1;
  if(idFurBase > 0){
    this.fur.nfurIdbase = idFurBase;
    this.fur.cfurCodtipo = "FUR";
   
  }

  if(usuarioSup != null){
    this.fur.cfurIdususuperv = usuarioSup;
  }
  if(usuarioAsig != null){
    this.fur.cfurIdusuasig = usuarioAsig;
  }
  if(this.fur.correoDetalle != null){
    this.fur.correoDetalle.cbpmmailIdcodtip = this.fur.cfurCodtipo;
    this.fur.correoDetalle.nbpmmailIntento = 0;
    if(idFurBase > 0){
      this.fur.correoDetalle.cbpmmailIdcodtip="FUR";
      this.fur.correoDetalle.cbpmmaildetAsunto = "Registro de Formulario Web";
    }else{
      this.fur.correoDetalle.cbpmmaildetAsunto = "Registro de FUR";
    }
    
  
  }

  if(estadoFurEval != null){
    if(estadoFurEval == NumIdEstadoHeEvaluacionSuperv || estadoFurEval == NumIdEstadoHeEvaluacionObservado){
        this.fur.nfurIdestado = estadoFurEval;
    }
  }
  
  //CUS 04 - 06
  if(this.contenedorMpC != null){
    if(this.contenedorMpC.flgEsFur && this.contenedorMpC.expedienteC.listaExpediente.length >0){
      this.fur.listaFichaExpediente = this.contenedorMpC.expedienteC.listaExpediente;
    }
  }
 
  console.log("=O====================13");
  console.log(this.fur);

  this.furService.RegistrarFur(this.fur).subscribe(
    data => {

      // console.log("===========Resultado");
      // console.log(data);
      // furNew = new Fur();

      // furNew = data;

      // //this.furNew = data;

      // data.fur.nfurId;s

      idFur = data.nfurId;

      if(idFur > 0)
      {

        if(flgPruebas)
        {

          this.denunciaContenedorC.pruebaHechoC.evidenciaC.procesarLaserFiche(idFur).subscribe(data=>{


            this._terminaGuardar(idFur);

                
          //   openModalMensaje("Formulario Web",`Se generó el formulario número ${idFur}, por favor verifique`,this.dialog);
  
          // //this.datosGeneralC.formDatosGenerales.controls.nro_formulario.setValue(idFur);
          //   this.datosGeneralC.nro_formulario = idFur;
          //   this.flgDeshabilitaGuardar = true;
          //   this.guardarEmitter.emit(this.flgDeshabilitaGuardar);
  
  
          });

        }else{

          this._terminaGuardar(idFur);



        }

        
        
        // openModalMensaje("Formulario Web",`Se generó el formulario número ${idFur}, por favor verifique`,this.dialog);

        // //this.datosGeneralC.formDatosGenerales.controls.nro_formulario.setValue(idFur);
        // this.datosGeneralC.nro_formulario = idFur;
        // this.flgDeshabilitaGuardar = true;
        // this.guardarEmitter.emit(this.flgDeshabilitaGuardar);
        /**Generación de oficio */
        //this.datosGeneralC.nro_formulario = idFur;
        this.datosGeneralC.setDatosGenerales(data);


        //this.flgDeshabilitaGuardar = true;
        //this.guardarEmitter.emit(this.flgDeshabilitaGuardar);
        this.oficio.rutaArchivoPlantilla = "C:\\\\archivos\\\\";
        this.oficio.nombreArchivoPlantilla = "OFICIO.DOCX";
        this.oficio.rutaArchivoGenerado = "C:/archivos/";
        this.oficio.nombreArchivoGenerado = "oficioGeneradoCGR.docx";
        this.oficio.asunto = "Formulario web  N° " + idFur;
        this.oficio.cargo = "Ciudadano";
        this.oficio.cargo_EMP_ENC = "ANALISTA";
        this.oficio.contenido = "En ese sentido, en virtud a lo dispuesto en el artículo 8° de la Ley N° 29542 “Ley de Protección al Denunciante en el Ámbito Administrativo y de Colaboración Eficaz en el Ámbito Penal” y en el artículo 10° de su reglamento, aprobado mediante Decreto Supremo N° 038-2011-PCM, los cuales establecen la competencia del (Ministerio del Trabajo y Promoción del Empleo / las Direcciones Regionales de Trabajo y Promoción del Empleo) para realizar inspecciones laborales cuando el trabajador denunciante es objeto de represalias que se materializan en actos de hostildad, independientemente del régimen laboral en el que se encuentre ; se remite a su Despacho, fotocopia de la documentación alcanzada por el mencionado por el mencionado denunciante, en los folios, para las acciones correspondientes y posterior remisión a este Órgano Superiror de Control de la actas o informes conteniendo el resultado de la actuación de inspección, en cumplimiento de lo señalado en el literal e) del artículo 10° del mencinado reglamento.";
        this.oficio.datos_EMISION_CGR = "CGR-123456";
        this.oficio.des_ENTIDAD = "SUNAT";
        this.oficio.direccion_DESTINATARIO ="MZ. C13 LT. 34 Bocanegra-Callao";
        this.oficio.empleado_EMITE ="Congresita";
        this.oficio.iniciales_EMP =  "USIL";
        this.oficio.nombre_ANIO = "DAVC-1993";
        this.oficio.nombre_DESTINATARIO = "Alexander Valderrama";
        this.oficio.pie_PAGINA = "Contraloria General";
        this.oficio. post_FIRMA = "Firmado",
        this.oficio.referencia = "http://192.168.0.1:9000/Hola Mundo";
        this.oficio.sigla_DOC = "ASDQWEZXC-123456";
        this.oficio.ubigeo_DESTINATARIO = "Lima-San Isidro-Navarrete";
        this.oficio.uuoo_DESTINO = "SUNAT";
        
        this.bpmService.generarOficio(this.oficio).subscribe(data =>
          {
              if(data != null){
                console.log(data);
                  
                  openModalMensaje("Formulario Web","Documento generado",this.dialog);
              }
          });

      }else{

        openModalMensaje("Formulario Web","Ocurrió un problema en la actualización del registro",this.dialog);

      }

      // openModalMensaje("Formulario Web","Se actualizó la información correctamente, por favor verifique.",this.dialog);

      // window.location.reload();

      return
      
    });

  // console.log("=O====================14");

  }

  _terminaGuardar(idfurNew:number)
  {
      openModalMensaje("Formulario Web",`Se generó el formulario número ${idfurNew}, por favor verifique`,this.dialog);
  
    //this.datosGeneralC.formDatosGenerales.controls.nro_formulario.setValue(idFur);
      this.datosGeneralC.nro_formulario = idfurNew.toString();
      this.flgDeshabilitaGuardar = true;
      this.guardarEmitter.emit(this.flgDeshabilitaGuardar);
  }


  guardarBorrador(){
    this.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
  }


 

  _deshabilitarCamposFur(){
    this.denunciaContenedorC.entidadHechoC.entidadComponentReadEnable();
    this.denunciaContenedorC.hechoIrregularC.hechoIrregularCReadEnable();
    this.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.funcionarioCReadEnable();
    this.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.empresaCReadEnable();
    this.denunciaContenedorC.pruebaHechoC.pruebaHechoCReadEnable();
    this.denunciaContenedorC.denunciaContenedorCReadEnable();
    this.denunciaContenedorC.instanciaC.instanciaCReadEnable();
    if(this.denunciaContenedorC.pruebaHechoC.cuenta_pruebas){
      this.denunciaContenedorC.pruebaHechoC.evidenciaC.evidenciaCReadEnable();
    }
  
      //Tab denunciante
      setTimeout(()=>{
          if(this.denunciaContenedorC.requireIdentificarse){
            this.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
              //this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
          

          //Tab medida de proteccion
          if(this.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec == "SI"){
            console.log(" :3 : " + this.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec);
           
            setTimeout(()=>{
              this.contenedorMpC.medidaProteccionCReadEnable();
              //this.formularioCmp.contenedorMpC.medidaProteccionCReadEnable();
              
            },0)
          
          }
        }
     

      },0)
      
  }

  _habilitarCamposFur(){
    this.denunciaContenedorC.entidadHechoC.entidadComponentReadDisable();
    this.denunciaContenedorC.hechoIrregularC.hechoIrregularCReadDisable();
    this.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.funcionarioCReadDisable();
    this.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.empresaCReadDisable();
    this.denunciaContenedorC.pruebaHechoC.pruebaHechoCReadDisable();
    this.denunciaContenedorC.denunciaContenedorCReadDisable();
    this.denunciaContenedorC.instanciaC.instanciaCReadDisable();
    //this.formularioCmp.denunciaContenedorC.pruebaHechoC.evidenciaC.evidenciaCReadDisable();
    if(this.denunciaContenedorC.pruebaHechoC.cuenta_pruebas){
      this.denunciaContenedorC.pruebaHechoC.evidenciaC.evidenciaCReadEnable();
    }
      //Tab denunciante
      setTimeout(()=>{
          if(this.denunciaContenedorC.requireIdentificarse){
            this.contenedorDenuncianteC.contenedorDenuncianteReadDisable();
              //this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
          

          //Tab medida de proteccion
           if(this.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec == "SI"){
          
            setTimeout(()=>{
              //this.formularioCmp.contenedorMpC.medidaProteccionCReadEnable();
              this.contenedorMpC.medidaProteccionCReadDisable();
            },0)
          
          }
        }
     

      },0)
  }


  generarCodigoFur(fur : Fur){
    let codigoGenerado : string = "";
    let codigoCorrelativo : string = "";
    let tamanioIdFur : number = 0;
    let stringIdFur : string = "";

    codigoCorrelativo = codigoIncial;
    stringIdFur = String(fur.nfurId);
    tamanioIdFur = stringIdFur.length;
    codigoCorrelativo  = codigoCorrelativo.slice(0,tamanioIdFur);
    codigoCorrelativo = codigoCorrelativo +stringIdFur; 
    codigoGenerado = fur.cfurCodtipo  + "."+anioActualGlobal + "." + codigoCorrelativo + "CGR"; //Verificar si OCI O CGR
    return codigoGenerado;
 }

 actualizarCodigo(codigo : string){
   this.datosGeneralC.nro_formulario = codigo;
 }
}