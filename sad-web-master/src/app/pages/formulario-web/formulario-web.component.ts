import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FurService } from 'src/app/services/fur.service';
import { Fur } from 'src/app/models/Fur';
import { DenunciaContenedorComponent } from '../denuncia/denuncia-contenedor/denuncia-contenedor.component';
import { MatDialog } from '@angular/material/dialog';
import { openModalMensaje, ConvertirFlag, NumCanalFormWeb, ValidarEmail, GetDate, GetDateTime, GetDateTimeString, codigoIncial, anioActualGlobal } from 'src/app/general/variables';
import { Denunciado } from 'src/app/models/Denunciado';
import { DenunciadoXFicha } from 'src/app/models/DenunciadoXFicha';
import { Persona } from 'src/app/models/Persona';
import { DocumentoDet } from 'src/app/models/DocumentoDet';
import { ContenedorDenuncianteComponent } from '../Denunciante/Contenedor/contenedor-denunciante.component';
import { ContenedorMpComponent } from '../MedidasProt/contenedor-mp/contenedor-mp.component';
import { Denunciante } from 'src/app/models/Denunciante';
import { DatosGeneralComponent } from '../datos-general/datos-general.component';
import { FurXObraPublica } from 'src/app/models/FurXObraPublica';
import { FURXEmpresaInvolucrada } from 'src/app/models/FURXEmpresaInvolucrada';
import { DenuncianteXFamiliar } from 'src/app/models/DenuncianteXFamiliar';
import { FichaXInstancia } from 'src/app/models/FichaXInstancia';
import { Anexo } from 'src/app/models/Anexo';
import { HojaEvaluacion } from 'src/app/models/HojaEvaluacion';
import { DatosGeneral } from 'src/app/models/personalizados/DatosGeneral';
import { MformularioWebC } from 'src/app/models/personalizados/MformularioWebC';
import { MdenunciaContenedorC } from 'src/app/models/personalizados/MdenunciaContenedorC';
import { MentidadHechoC } from 'src/app/models/personalizados/mEntidadHechoC';
import { PersonaNaturalComponent } from '../Denunciante/PersonaNatural/persona-natural.component';
import { ContenedorWebComponent } from './contenedor-web/contenedor-web.component';
import { EvaluacionPreliminarService } from 'src/app/services/bpm/evaluacion-preliminar.service';
import { Oficio } from 'src/app/models/BPM/oficio';

@Component({
  selector: 'app-formulario-web',
  templateUrl: './formulario-web.component.html',
  styleUrls: ['./formulario-web.component.css']
})
export class FormularioWebComponent implements OnInit {
  
  @ViewChild(DenunciaContenedorComponent) denunciaContenedorC : DenunciaContenedorComponent;
  @ViewChild(ContenedorDenuncianteComponent) contenedorDenuncianteC : ContenedorDenuncianteComponent;
  @ViewChild(ContenedorMpComponent) contenedorMpC : ContenedorMpComponent;
  @ViewChild(DatosGeneralComponent) datosGeneralC : DatosGeneralComponent;
  @ViewChild(ContenedorWebComponent) contenedorWerb : ContenedorWebComponent;
  //@ViewChild("aTabDenun") campoTipDenun: ElementRef; 
  
  flgGuardarDefinitivo = false;
  tabDenunciante : boolean = false;
  tabProteccion : boolean = false;
  fur:Fur;
  persona: Persona;
  flgDeshabilitaGuardar = false;
  flgGuardado : boolean = false;
  //datosGeneral : DatosGeneral ;//= new DatosGeneral();
  mFormularioWebC : MformularioWebC;
  //furNew : Fur = new Fur();

  constructor(protected furService: FurService,
    public dialog: MatDialog) { 

      //document.getElementById("aTabDenun").;

      // let btn = document.getElementById("aTabDenun").onclick();

      // btn.addEventListener("click", (e:Event) => this.getTrainingName(4));

    }

  // getTrainingName(n:number){
  //     // button click handler
  //  }
  
  ngOnInit() {

    // this.furService.ObtenerFurPorCodigo("134").subscribe(
    //   data => {
    //     console.log("==================FURRRRRRRRR");
    //     console.log(data);
    //   });

    // console.log("========init de form web");

  }

  ConsultarFur(fur: Fur){

    // console.log("===============0FUR");
    // console.log(fur);

    this.mFormularioWebC = new MformularioWebC();

    this.mFormularioWebC.datosGeneral = new DatosGeneral();

    this.mFormularioWebC.datosGeneral.estado_formulario = fur.estado.nestadoId.toString();
    this.mFormularioWebC.datosGeneral.fecha_registro = fur.fch_inicio_reg;
    this.mFormularioWebC.datosGeneral.hora_inicio = fur.hora_inicio_reg;
    this.mFormularioWebC.datosGeneral.nro_formulario = fur.cfurCodigo;

    this.datosGeneralC.ConsultarDatosGenerales(this.mFormularioWebC.datosGeneral);

    this.mFormularioWebC.mDenunciaContenedorC = new MdenunciaContenedorC();
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC = new MentidadHechoC();

    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.flg_deshabilita = true;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.select_entidades = fur.cfurCodent;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.inputDireccion = fur.cfurDirentidad;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.inputReferencia = fur.cfurDscrefentidad;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.select_departamento = fur.cfurCdepent;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.select_provincia = fur.cfurCprovent;
    this.mFormularioWebC.mDenunciaContenedorC.mEntidadHechoC.select_distrito = fur.cfurCdistent;

    console.log("==========this.mFormularioWebC.mDenunciaContenedorC");
    console.log(this.mFormularioWebC.mDenunciaContenedorC);
    console.log(fur);

    this.denunciaContenedorC.ConsultarDenuncia(this.mFormularioWebC.mDenunciaContenedorC);

  
    //this.denunciaContenedorC.
    
    //this.datosGeneralC.datosGeneral.flg_deshabilita = true;

  }

  validarTab(valor){

    //this.campoTipDenun.nativeElement.focus(); 
    //this.getTrainingName(4);
    //document.getElementById("aTabDenun").onclick();

    this.tabDenunciante = valor;

    if(valor)
    {

      setTimeout(() => {

        document.getElementById('aTabDenun').click();
        
      }, 1);

    }
    
  }

  ActivaPanelMP(flgSolicitaMPB){

    this.tabProteccion = flgSolicitaMPB;

    if(flgSolicitaMPB)
    {

      setTimeout(() => {

        document.getElementById('aTabMP').click();
        
      }, 1);  

    }

  }

  guardarBorrador(){

    let valido = true;
    valido = this.contenedorWerb.validarGuardar();
    if(valido){
      this.contenedorWerb._deshabilitarCamposFur();
      this.flgGuardarDefinitivo = true;
    } 
 
    //this.contenedorWerb.Guardar(0);
   /* if(data != null){
      this.flgDeshabilitaGuardar = this.contenedorWerb.flgDeshabilitaGuardar;
    }*/
   
  }

  guardar(){
    this.contenedorWerb.Guardar(0,null,null,null);
    this.flgGuardado = true;

  
  }


  regresar(){
    this.contenedorWerb._habilitarCamposFur();
    this.flgGuardarDefinitivo = false;
  }

  cambioFlgGuardar(valor){
    console.log(valor);
    this.flgDeshabilitaGuardar = valor;
  }
  


}