import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-firma-digital',
  templateUrl: './firma-digital.component.html',
  styleUrls: ['./firma-digital.component.css']
})
export class FirmaDigitalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.location.href ="http://localhost:8092/SAD_WEB/firma.html";
  }

}
