import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
// tslint:disable-next-line: max-line-length
import { openModalMensaje, GlobalVars, CodUnoGestDenuncias, CodUnoSubGerCMP, codAccesoVerificarOk, cookieGlobalVars, CodSNCCGR, CodSNCOCI } from 'src/app/general/variables';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { SeguridadService } from 'src/app/services/externos/seguridad.service';
import { DatosToken } from 'src/app/models/personalizados/seguridad/DatosToken';
import { SeguridadVerificar } from 'src/app/models/personalizados/seguridad/SeguridadVerificar';
import { PersonalCgrService } from 'src/app/services/personal-cgr.service';
import * as CryptoJS from 'crypto-js';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-login-gc',
  templateUrl: './login-gc.component.html',
  styleUrls: ['./login-gc.component.css']
})
export class LoginGcComponent implements OnInit {

  FlgCaptcha =  false;

  tituloMensaje = "Login";
  datosToken:DatosToken;
  seguridadVerificar:SeguridadVerificar;

  formLogin = new FormGroup({
    input_usuario: new FormControl(''),
    input_contrasenha: new FormControl('')
  });

  constructor(
              public dialog: MatDialog,
              public router:Router,
              private route: ActivatedRoute,
              protected seguridadService: SeguridadService,
              protected personalCgrService: PersonalCgrService,
              private cookieService: CookieService
              )
  {

  }

  resolved(captchaResponse: string) {

    //console.log(`Resolved captcha with response: ${captchaResponse}`);

    this.FlgCaptcha = captchaResponse.length > 0 ? true : false;

  }

  ngOnInit()
  {

    // this.validaUsuarioService.generarToken().subscribe(
    //   data => {
    //     console.log("==================data");
    //     console.log(data);
    //   });

  }

  Ingresar()
  {

    var usuario = this.formLogin.controls.input_usuario.value;
    var contrasenha = this.formLogin.controls.input_contrasenha.value;
    var codPersonal = usuario.substring(1);
    var codSuperv;

    if(usuario.length == 0 || contrasenha.length == 0)
    //if(usuario.length == 0 || contrasenha.length == 0 || !this.FlgCaptcha)
    {

      openModalMensaje(this.tituloMensaje,"Debe Ingresar el usuario, contraseña y captcha, por favor verifique",this.dialog);

      return;

    }

    

    GlobalVars.CodUsuario = usuario;
    GlobalVars.CodPersonal = codPersonal;
    GlobalVars.DscContrasenha = contrasenha;
    

    console.log("codPersonal");
    console.log(codPersonal);

    ///////PROVIONAL PARA PRUEBAS SIN TOKEN++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    // this.personalCgrService.ObtenerPersonalCgrPorCodigo(codPersonal).subscribe(data=>{

    //   codSuperv = data.cpPerSupervisor;

    //   GlobalVars.CodPersSuperv = codSuperv;

    //   console.log("============condicion");
    //   console.log(data.cpUnoCodigo);
    //   console.log(CodUnoGestDenuncias);
      
    //   if(data.cpUnoCodigo == CodUnoGestDenuncias || data.cpUnoCodigo == CodUnoSubGerCMP)
    //   {

    //     GlobalVars.FlgSupervisor = (codSuperv == codPersonal) ? true: false;
    //     GlobalVars.FlgGestor = !GlobalVars.FlgSupervisor;

    //   }else{

    //     GlobalVars.FlgOtroUsuario = true;

    //   }

    //   GlobalVars.CodPersSedeDpto = data.sedeCGR.cpSedDpto;
    //   GlobalVars.DscNombreComplPers = data.strDscNombreCompleto;
    //   GlobalVars.CodUNOPers = data.cpUnoCodigo;

    //   console.log("============condicion2");
    //   console.log(GlobalVars.FlgSupervisor);
    //   console.log(GlobalVars.FlgGestor);
    //   console.log(GlobalVars.FlgOtroUsuario);
    //   console.log(codSuperv);
    //   console.log(codPersonal);

    //   cookieGlobalVars(1,this.cookieService);

    //   //return;

    //   //this.router.navigate(['/PanelPrincipal/',usuario], { relativeTo: this.route });
    //   this.router.navigate(['/PanelPrincipal/',CryptoJS.AES.encrypt(usuario,contrasenha).toString()], { relativeTo: this.route });

    // });

    ///////PROVIONAL PARA PRUEBAS SIN TOKEN++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    ////////PROVISIONAL
    // GlobalVars.CodPersSuperv = "12882";
    // GlobalVars.FlgSupervisor = false;
    // GlobalVars.FlgGestor = true;


    // GlobalVars.CodPersSuperv = "12882";
    // GlobalVars.FlgSupervisor = true;
    // GlobalVars.FlgGestor = false;
    // ////////PROVISIONAL

    // GlobalVars.CodPersSedeDpto = "15";////////OJO PROBVISIONAL

    

    // this.seguridadService.generarToken().subscribe(data=>{

    //   console.log(data);

    // });

    // this.seguridadVerificar = new SeguridadVerificar();

    // this.seguridadVerificar.codigoUsuario = usuario;
    // this.seguridadVerificar.clave = contrasenha;
    // //this.seguridadVerificar.token = data.generarToken;
    // this.seguridadVerificar.token = "fw2857yH3IeobJoGmQrrjNkVbRw8KlozJyebSbLg9LM=|beV5cAWMg/uVlpxz8Q1zlrh76+Yy8nNABk03T9C4UGw6zm3se/vIQL2r+QDKQF6n|186f68cbe0bee48031ab85e7b49afbfa|IDLDzyqp9QyDdm11605Pv1iEZsjGCHmOJm3/iAtwwT1NrSEbstiR+1PrdDOnVMju";

    // console.log(this.seguridadVerificar);

    // this.seguridadService.accesoVerificar(this.seguridadVerificar).subscribe(data=>{

    //   console.log(data);

    //   if(data.accesoVerificar == codAccesoVerificarOk)
    
    //   {

    //     this.router.navigate(['/PanelPrincipal',usuario], { relativeTo: this.route });

    //   }else{

    //     openModalMensaje(this.tituloMensaje,"El usuario y contraseña ingresada son incorrectos, por favor verifique",this.dialog);

    //     return;

    //   }

    // });

    //console.log("Acceso1");
    

    //////CON TOKEN++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    this.seguridadService.generarToken().subscribe(data=>{

      //console.log("Acceso2");

      // this.datosToken = new DatosToken();

      // this.datosToken.codigoUsuario = usuario;
      // this.datosToken.clave = contrasenha;
      // this.datosToken.token = data.generarToken;

      // console.log(this.datosToken);

      this.seguridadVerificar = new SeguridadVerificar();

      this.seguridadVerificar.codigoUsuario = usuario;
      this.seguridadVerificar.clave = contrasenha;
      this.seguridadVerificar.token = data.generarToken;
      //this.seguridadVerificar.token = "nhst2sg4pDPM5Ex10icSHG85SV++1PMIAP4y9bIgNwc=|beV5cAWMg/uVlpxz8Q1zlrh76+Yy8nNABk03T9C4UGw6zm3se/vIQL2r+QDKQF6n|c66a75d59389174dfa071fa39acb8bc7|m0mMyioBt744kCmDuW1yA4iSfhe47Fo7kM9lk+Q1aM7yvKwgpJIZAKTj+onTCCIW";

      //console.log(this.seguridadVerificar);

      //console.log("Acceso3");

      setTimeout(() => {

        this.seguridadService.accesoVerificar(this.seguridadVerificar).subscribe(data=>{

         // console.log("Acceso4");

          //console.log(data);
  
          if(data.accesoVerificar == codAccesoVerificarOk)
          {

            this.personalCgrService.ObtenerPersonalCgrPorCodigo(codPersonal).subscribe(data=>{

              console.log("Obtener personal por codigo");
              console.log(data);
              codSuperv = data.cpPerSupervisor;
        
              GlobalVars.CodPersSuperv = codSuperv;
        
              console.log("============condicion");
              console.log(data.cpUnoCodigo);
              console.log(CodUnoGestDenuncias);
              
              if(data.cpUnoCodigo == CodUnoGestDenuncias || data.cpUnoCodigo == CodUnoSubGerCMP)
              {
        
                GlobalVars.FlgSupervisor = (codSuperv == codPersonal) ? true: false;
                GlobalVars.FlgGestor = !GlobalVars.FlgSupervisor;
                GlobalVars.CodSNC = CodSNCCGR;
        
              }else{
        
                GlobalVars.FlgOtroUsuario = true;
                GlobalVars.CodSNC = CodSNCOCI;
        
              }
        
              GlobalVars.CodPersSedeDpto = data.sedeCGR.cpSedDpto;
              GlobalVars.DscNombreComplPers = data.strDscNombreCompleto;
              GlobalVars.CodUNOPers = data.cpUnoCodigo;
              //console.log("Codigo de departamento: " + data.sedeCGR.cpSedCodigo);
              //GlobalVars.CodPersSedeDpto = String(data.sedeCGR.cpSedCodigo);
        
              console.log("============condicion2");
              console.log(GlobalVars.FlgSupervisor);
              console.log(GlobalVars.FlgGestor);
              console.log(GlobalVars.FlgOtroUsuario);
              console.log(codSuperv);
              console.log(codPersonal);
        
              //return;

              cookieGlobalVars(1,this.cookieService);
        
              //Cambio Efectuado Hector 
              //this.router.navigate(['/PanelPrincipal/',CryptoJS.AES.encrypt(usuario,contrasenha).toString()], { relativeTo: this.route });
              this.router.navigate(['/PanelPrincipalServicio/',CryptoJS.AES.encrypt(usuario,contrasenha).toString()], { relativeTo: this.route });
        
            });
            //console.log("Acceso");
  
            //this.router.navigate(['/PanelPrincipal',usuario], { relativeTo: this.route });
  
          }
           else{
  
            openModalMensaje(this.tituloMensaje,"El usuario y contraseña ingresada son incorrectos, por favor verifique",this.dialog);
  
            return;
  
          } 
  
        });
        
      }, 500);

    });

    //////CON TOKEN++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //console.log("Navego1");

    // this.router.navigate(['/PanelPrincipal'],{ queryParams: {user : usuario} });
    

    //console.log("Navego");

    //generar token y pasar al servicio valida usuario
    //PENDIENTE, RLAQUI PASAR DE WS A REST EL SERVCIIO DE SEGURIDAD

  }

}