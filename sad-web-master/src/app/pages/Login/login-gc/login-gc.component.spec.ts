import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginGcComponent } from './login-gc.component';

describe('LoginGcComponent', () => {
  let component: LoginGcComponent;
  let fixture: ComponentFixture<LoginGcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginGcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginGcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
