import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { HechoIrregularService } from 'src/app/services/hecho-irregular.service.';
import { ObraHechoIrregular } from 'src/app/models/ObraHechoIrregular';
import { openModalMensaje, DscTituloPaginator } from 'src/app/general/variables';
import { FormGroup } from '@angular/forms';
import { FurXObraPublicaXHi } from 'src/app/models/FurXObraPublicaXHi';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { NavModalComponent } from '../../General/nav-modal/nav-modal.component';

@Component({
  selector: 'app-modal-irreg-infobras',
  templateUrl: './modal-irreg-infobras.component.html',
  styleUrls: ['./modal-irreg-infobras.component.css']
})
export class ModalIrregInfobrasComponent implements OnInit {

  listHechoIrregular : ObraHechoIrregular[] = [];
  listHechoIrregularSel : ObraHechoIrregular[] = [];
  listaObraXHi: FurXObraPublicaXHi[] = [];

  //
  matTableColumnsDef = ["obraPublicaPorHechoIrregular","acciones"];
  dataSource: MatTableDataSource<ObraHechoIrregular>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(NavModalComponent) navModalC : NavModalComponent;

  constructor(private dialogRef: MatDialogRef<ModalIrregInfobrasComponent>,
    protected hechoIrregularService: HechoIrregularService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

    this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;

    this.navModalC.setDatosNavModal2("Descripción de hecho irregular");

 

    //Datos provisionales
    
    //console.log(this.obraPub.obraPublica);

    console.log("0===============this.listHechoIrregular");
    console.log(this.listHechoIrregular);

    for(let i = 0; i < 5; i++)
    {
      let obraPublicaPorHechoIrregular = new ObraHechoIrregular();

      obraPublicaPorHechoIrregular.num_item = i;
      obraPublicaPorHechoIrregular.flgSeleccionar = false;

      console.log(this.data);
      
      obraPublicaPorHechoIrregular.cobrphiCodinfobras =this.data.obraPub.obraPublica.cobrapublicaCodinfob;
      obraPublicaPorHechoIrregular.cobrphiDsc = "Hecho Irregular " + (i+1);
      obraPublicaPorHechoIrregular.nobrphiId = (i+1);

      this.listHechoIrregular.push(obraPublicaPorHechoIrregular);


    }

    console.log("1===============this.listHechoIrregular");
    console.log(this.listHechoIrregular);

    // if(this.data.editar)
    // {
    //     console.log(this.data);
    //     this.listHechoIrregular.forEach(hecho=>{
    //         this.data.obraPub.listaFurPorObraPubPorHechoIrr.forEach(obraHecho=>{
    //           console.log("id hecho" + hecho.nobrphiId);
    //           console.log(obraHecho.hechoIrregular.flgSeleccionar); 
    //             if(hecho.nobrphiId == obraHecho.hechoIrregular.nobrphiId){
    //                 console.log("flg seleccionar: " + obraHecho.flgSeleccionar);
    //                 hecho.flgSeleccionar = obraHecho.hechoIrregular.flgSeleccionar;
    //             }
    //         });
    //     });
    // }

    //console.log(this.data);
    this.listHechoIrregular.forEach(hecho=>{

        if(this.data.obraPub.listaFurPorObraPubPorHechoIrr != null)
        {

          this.data.obraPub.listaFurPorObraPubPorHechoIrr.forEach(obraHecho=>{

              if(hecho.nobrphiId == obraHecho.obraPublicaPorHechoIrregular.nobrphiId){

                  hecho.flgSeleccionar = obraHecho.obraPublicaPorHechoIrregular.flgSeleccionar;
              }
          });

        }

        
    });

    this._cargarTableMaterial();
      
  }

  formIrregInfobras = new FormGroup({
   
  });

  Aceptar()
  {
    //var indexNew = 0;
    this.listHechoIrregularSel = [];
    //Provisional 
    this.listHechoIrregular.forEach(data =>{

        if(data.flgSeleccionar)
        {
          let obraPublicaHi = new FurXObraPublicaXHi();

          obraPublicaHi.obraPublicaPorHechoIrregular = data;
          obraPublicaHi.nfurxobpxhiIdhi = data.nobrphiId;

          this.listaObraXHi.push(obraPublicaHi);
          
          this.data.obraPub.listaFurPorObraPubPorHechoIrr= this.listaObraXHi;

         // this.listHechoIrregularSel.push(data);
        }

    });

    if(this.data.obraPub.listaFurPorObraPubPorHechoIrr == 0)
    {

      openModalMensaje("Hecho Irregular","Debe seleccionar por lo menos un hecho irregular",this.dialog);

      return;

    }

    
    this.dialogRef.close(this.data.obraPub);

  }

  CheckSeleccionar(hechoIrregular:ObraHechoIrregular, data:any){



    this.listHechoIrregular.forEach(hecho=>{
      if(hecho.nobrphiId == hechoIrregular.nobrphiId){
          
          hecho.flgSeleccionar = data.target.checked;
      }
    });
  }

  cerrarModal(){
    this.dialogRef.close();
  }

  _cargarTableMaterial(){
    this.dataSource = new MatTableDataSource(this.listHechoIrregular);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
