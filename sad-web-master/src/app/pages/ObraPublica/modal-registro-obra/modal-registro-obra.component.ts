import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Departamento } from 'src/app/models/Departamento';
import { Provincia } from 'src/app/models/Provincia';
import { Distrito } from 'src/app/models/Distrito';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { CatalogoService } from 'src/app/services/Catalogo.service';
import { Catalogo } from 'src/app/models/Catalogo';
import { FurXObraPublica } from 'src/app/models/FurXObraPublica';
import { ObraPublica } from 'src/app/models/ObraPublica';
import { InfobrasService } from 'src/app/services/infobras.service';
import { EjecucionInfobra } from 'src/app/models/EjecucionInfobra';
import { ModalIrregInfobrasComponent } from '../modal-irreg-infobras/modal-irreg-infobras.component';
import { iconAgregar, ConvertStrDateToFormatBD, DscTituloPaginator, openModalMensaje } from 'src/app/general/variables';
import { NavModalComponent } from '../../General/nav-modal/nav-modal.component';
import { Entidad } from 'src/app/models/Entidad';
import { EntidadService } from 'src/app/services/entidad/entidad.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { FurXObraPublicaXHi } from 'src/app/models/FurXObraPublicaXHi';

@Component({
  selector: 'app-modal-registro-obra',
  templateUrl: './modal-registro-obra.component.html',
  styleUrls: ['./modal-registro-obra.component.css']
})
export class ModalRegistroObraComponent implements OnInit {

  @ViewChild(NavModalComponent) navModalC : NavModalComponent;

  constructor(private maestraService : MaestrasService,public dialogRef: MatDialogRef<ModalRegistroObraComponent>
    ,@Inject(MAT_DIALOG_DATA) public data: any
    ,protected catalogoService: CatalogoService,
    protected infobrasService: InfobrasService,
    public mdModalIrreg: MatDialog,
    protected entidadService : EntidadService) { }

  departamentos : Departamento[] = [];
  provincias : Provincia[]= [];
  distritos : Distrito[] = [];
  listTipoEstado : Catalogo[] = [];
  //listTipoEjecucion : EjecucionInfobra[] = [];
  listTipoEjecucion : Catalogo[] = [];
  furObraPublica : FurXObraPublica = new FurXObraPublica();
  obraPublica:ObraPublica = new ObraPublica();
  iconoAgregar  = iconAgregar;
  entidades : Entidad[];
  nombreEntidad : string = "";
  strDscTituloModal: string = "Registro Obra";
  matTableColumnsDef = ["Item","hechoIrregular"];
  dataSource: MatTableDataSource<FurXObraPublicaXHi>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // formObra : FormGroup = new FormGroup({
  //   selectTipoEjec : new FormControl(''),
  //   selectDepartamento : new FormControl(''),
  //   nombreObra: new FormControl(''),
  //   selectProvincia : new FormControl(''),
  //   selectDistrito : new FormControl(''),
  //   selectEstadoObra : new FormControl(''),
  //   inputDireccion : new FormControl(''),
  //   inputReferencia : new FormControl(''),
  //   inputCosto : new FormControl('')
  // });

  ngOnInit() {

    this.furObraPublica.listaFurPorObraPubPorHechoIrr = [];

    this.navModalC.setDatosNavModal2("Registrar Obra Pública");

    this.catalogoService.listarCatalogo(1).subscribe(
      data => {
        this.listTipoEstado = data;
                
      });

    this.catalogoService.listarCatalogo(2).subscribe(
      data => {

        this.listTipoEjecucion = data
                
      });

            

    this.maestraService.obtenerDepartamentos().subscribe(data =>{
      this.departamentos = data;
    });

  }

  _cargarTableMaterial(){
    
    this.dataSource = new MatTableDataSource(this.furObraPublica.listaFurPorObraPubPorHechoIrr);
    this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  obtenerProvincias(codDep : string){
    this.maestraService.obtenerProvincias(codDep).subscribe(data =>{
     
        this.provincias = data;
    });  
  }

  obtenerDistritos(codDep : string , codProv : string){

    this.maestraService.obtenerDistritos(codDep,codProv).subscribe(data =>
      {
        //console.log(data);
          this.distritos = data;
      });
  }

  agregarObra(){
      /* this.obraPublica.nobrapublicaIdejec = this.formObra.get('selectTipoEjec').value;
      this.obraPublica.cobrapublicaCdepent  = this.formObra.get('selectDepartamento').value;
     
      this.obraPublica.cobrapublicaDscnombre = this.formObra.get('nombreObra').value;
      this.obraPublica.cobrapublicaCprovent = this.formObra.get('selectProvincia').value;
      this.obraPublica.cobrapublicaCdistent = this.formObra.get('selectDistrito').value;
      this.obraPublica.cobrapublicaEstado = this.formObra.get('selectEstadoObra').value;
      this.obraPublica.cobrapulicaDirec = this.formObra.get('inputDireccion').value;
      this.obraPublica.cobrapublicaRef = this.formObra.get('inputReferencia').value;
      this.obraPublica.nobrapublicaCosto = this.formObra.get('inputCosto').value;
      // */

      var idTipoEjec =  this.obraPublica.nobrapublicaIdejec;
      var impCosto =  this.obraPublica.nobrapublicaCosto;
      var dscNombre =  this.obraPublica.cobrapublicaDscnombre;
      var fchInicio =  this.obraPublica.strDobrapublicaFechaini;
      var fchFin =  this.obraPublica.strDobrapublicaFechafin;
      var codEntidad =  this.obraPublica.cobrapublicaCodent;
      var codDepart = this.obraPublica.cobrapublicaCdepent;
      var codProv = this.obraPublica.cobrapublicaCprovent;
      var dscDireccion = this.obraPublica.cobrapublicaDscdireccion;
      //var dscReferencia = this.obraPublica.cobrapublicaDscreferencia;
      var codDistrito = this.obraPublica.cobrapublicaCdistent;
      var IdEstado = this.obraPublica.nobrapublicaIdestado;
      var vDscMsjErr = null;

      

      if(idTipoEjec == null)
      {

        vDscMsjErr = `el tipo de ejecución`;

      }else if(impCosto == null)
      {

        vDscMsjErr = `el costo`;

      }else if(dscNombre == null)
      {

        vDscMsjErr = `el nombre de la obra`;

      }else if(fchInicio == null)
      {

        vDscMsjErr = `la fecha de inicio`;

      }else if(fchFin == null)
      {

        vDscMsjErr = `la fecha de final`;

      }else if(codEntidad == null)
      {

        vDscMsjErr = `la entidad`;

      }else if(codDepart == null)
      {

        vDscMsjErr = `el departamento`;

      }else if(codProv == null)
      {

        vDscMsjErr = `la provincia`;

      }else if(dscDireccion == null)
      {

        vDscMsjErr = `la dirección`;

      }else if(codDistrito == null)
      {

        vDscMsjErr = `el distrito`;

      }else if(IdEstado == null)
      {

        vDscMsjErr = `el estado`;

      }

      if(vDscMsjErr != null)
      {

        openModalMensaje(this.strDscTituloModal,`Ingrese ${vDscMsjErr}, por favor verifique`,this.mdModalIrreg).afterClosed().subscribe(result=>{

        });
  
        return;

      }

      if(fchInicio > fchFin)
      {

        openModalMensaje(this.strDscTituloModal,`La fecha de inicio es mayor a la fecha final, por favor verifique`,this.mdModalIrreg).afterClosed().subscribe(result=>{

        });

        return;

      }

      

      // if(idTipoEjec == null || impCosto == null || dscNombre == null || fchInicio == null || fchFin == null ||
      //   codEntidad == null || codDepart == null || codProv == null || dscDireccion == null || codDistrito == null || IdEstado == null)
      // {

      //   openModalMensaje(this.strDscTituloModal,`Ingrese todos los datos de la obra pública, por favor verifique`,this.mdModalIrreg).afterClosed().subscribe(result=>{

      //   });

      //   return;

      // }
      

      if(this.furObraPublica.listaFurPorObraPubPorHechoIrr.length == 0)
      {

        openModalMensaje(this.strDscTituloModal,`Ingrese los hechos irregulares, por favor verifique`,this.mdModalIrreg).afterClosed().subscribe(result=>{

        });

        return;

      }

      this.furObraPublica.obraPublica.strDobrapublicaFechaini = ConvertStrDateToFormatBD(this.furObraPublica.obraPublica.strDobrapublicaFechaini);
      this.furObraPublica.obraPublica.strDobrapublicaFechafin = ConvertStrDateToFormatBD(this.furObraPublica.obraPublica.strDobrapublicaFechafin);

      this.furObraPublica.obraPublica.cobrapublicaDscestado = this.listTipoEstado.filter(x => x.ncatalogoId == this.furObraPublica.obraPublica.nobrapublicaIdestado)[0].ccatalogoDsc;

      

      // console.log("==============this.furObraPublica");
      // console.log(this.furObraPublica);

      this.dialogRef.close(this.furObraPublica);
 
  }
  
  abrirModalIrregularidad(){

    this.obraPublica.cobrapublicaCodinfob = "";
    
    this.furObraPublica.obraPublica = this.obraPublica;

    //this.furObraPublica.listaFurPorObraPubPorHechoIrr = [];

    const dialogRef = this.mdModalIrreg.open(ModalIrregInfobrasComponent,{ width: '50%',height:'87%', 
      data: {obraPub : this.furObraPublica}});

    dialogRef.afterClosed().subscribe(result => {

      if(result != null){

        this.furObraPublica = result;
        this._cargarTableMaterial();

      }

    });
  }

  _buscarEntidad(valor){

    let vNombreEntidad = valor.target.value;

    // console.log("===========nombreEntidad");
    // console.log(nombreEntidad);
    
    if(valor.target.value.length >=4){

      this.entidadService.obtenerEntidadesXNombre(vNombreEntidad).subscribe(data =>{

          this.entidades = data;

          // console.log("===========this.entidades");
          // console.log(this.entidades);

      });
    }
  }

  _seleccionarEntidad(event){

    let name = event.target.value;

    // console.log("==============name");
    // console.log(name);

    this._llenarCombosEntidad(name); 
  }

  _llenarCombosEntidad(nombreEntidad :string){

    // console.log("=============nombreEntidad");
    // console.log(nombreEntidad);
    // console.log(this.entidades);

    if(this.entidades != null){

      let list = this.entidades.filter(x => x.centNombre === nombreEntidad)[0];

      if(list != null){    

        // console.log("=============list");
        // console.log(list);

        this.obraPublica.cobrapublicaCodent = list.centCodigo;

        //this._obtenerEntidad(list.centCodigo);

      }
    }
   
  }

  _cambioMonto(strMonto:string){

    this.obraPublica.nobrapublicaCosto = Number(strMonto.replace(/,/g, ''));

    // console.log("========_cambioMonto");
    // console.log(this.obraPublica.nobrapublicaCosto);
    // console.log(strMonto);

  }

  // _obtenerEntidad(codigo : string){
  //   this.entidadService.obtenerEntidadXCodigo(codigo).subscribe(data =>{
  //     if(data != null){
  //       this.obtenerProvincias(data.centDirdep);
  //       this.obtenerDistritos(data.centDirdep,data.centDirpro);
  //       this.cfurCdepent = data.centDirdep;
  //       this.cfurCprovent = data.centDirpro;
  //       this.cfurCdistent = data.centDirdis;
  //       this.cfurDirentidad = data.centDirecc;
  //       this.cfurCodent = data.centCodigo;
  //       this.nombreEntidad = data.centNombre;
  
  //       this.formEntidadHecho.get('select_departamento').setValue(data.centDirdep);
  //       this.formEntidadHecho.get('select_provincia').setValue(data.centDirpro);
  //       this.formEntidadHecho.get('select_distrito').setValue(data.centDirdis);
  //       this.formEntidadHecho.get('inputDireccion').setValue(data.centDirecc);
  //       this.entidad.centCodigo = data.centCodigo;
  //     }
    
  //   });
  // }

    cancelar(){
      this.dialogRef.close();
    }
}
