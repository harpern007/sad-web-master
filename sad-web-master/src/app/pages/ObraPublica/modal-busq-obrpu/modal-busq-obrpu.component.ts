import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Distrito } from 'src/app/models/Distrito';
import { Provincia } from 'src/app/models/Provincia';
import { Departamento } from 'src/app/models/Departamento';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { CatalogoService } from 'src/app/services/Catalogo.service';
import { DistritoService } from 'src/app/services/distrito.service';
import { ProvinciaService } from 'src/app/services/provincia.service';
import { DepartamentoService } from 'src/app/services/departamento.service';
import { Infobras } from 'src/app/models/Infobras';
import { InfobrasService } from 'src/app/services/infobras.service';
import { ModalIrregInfobrasComponent } from '../modal-irreg-infobras/modal-irreg-infobras.component';
import { openModalMensaje, DscTituloPaginator, DscHoraDefault } from 'src/app/general/variables';
import { ObraPublica } from 'src/app/models/ObraPublica';
import { UbigeoProvisionalService } from 'src/app/services/ubigeoProvisional.service';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { FurXObraPublicaXHi } from 'src/app/models/FurXObraPublicaXHi';
import { FurXObraPublica } from 'src/app/models/FurXObraPublica';
import { EntidadService } from 'src/app/services/entidad/entidad.service';
import { Entidad } from 'src/app/models/Entidad';
import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus';
import { InfobraConsulta } from 'src/app/models/personalizados/InfobraConsulta';
import { EjecucionInfobra } from 'src/app/models/EjecucionInfobra';
import { EventoInfobra } from 'src/app/models/EventoInfobra';
import { ExternosService } from 'src/app/services/externos/externos.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { NavModalComponent } from '../../General/nav-modal/nav-modal.component';


@Component({
  selector: 'app-modal-busq-obrpu',
  templateUrl: './modal-busq-obrpu.component.html',
  styleUrls: ['./modal-busq-obrpu.component.css']
})
export class ModalBusqObrpuComponent implements OnInit {
  //Icons
  iconAgregar = faPlus;
  //Errors
  claseError = '';
  flgListInfObras = false;
  flgDeshabilitaBuscar = false;
  
  matTableColumnsDef = ["codigo_infobras","descripcion_obra","costo","flgSeleccionar","Acciones"];
  dataSource: MatTableDataSource<Infobras>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(NavModalComponent) navModalC : NavModalComponent;


  /**GSOLIS
   * 
   */
  select_tipejec: string = "";
    select_departamento: string = "";
    select_provincia: string = "";
    select_distrito: string = "";
    select_entidades : string = ""; //Para las entidades
    input_entidad: string = "";
    input_nombreobra: string = "";
    input_codsnip: string = "";
    input_infobras: string = "";
    select_evento: string="" ;
    select_anho: number;
    select_detevento: string = "";
    select_fase: string = "";
    select_monitoreo: string = "";
    input_montodesde: number;
    input_montohasta:number;

    nombreEntidad : string = "";
   //
  //listTipoEjecucion : Catalogo[] = [];
  listTipoEjecucion : EjecucionInfobra[] = [];
  listEventos : EventoInfobra[] = [];
  listDistrito: Distrito[] = [];
  listProvincia: Provincia[] = [];
  listDepartamento: Departamento[] = [];
  listInfobras : Infobras[] = [];
  listInfobrasSel : Infobras[] = [];
  infobrasFiltro : InfobraConsulta = new InfobraConsulta();//Infobras = new Infobras();
  flgMostrarAvanzada = false;
  tituloMensaje = "Obras Públicas";

  obraPublicaHechoIrregular : FurXObraPublicaXHi;
  
  furObraPublica : FurXObraPublica = new FurXObraPublica();
  listEntidadesObras : Entidad[];
  entidadObra : Entidad = new Entidad();
  obraPublica:ObraPublica = new ObraPublica();
  
  formBusqObraP = new FormGroup({
    select_tipejec: new FormControl(''),
    select_departamento: new FormControl(''),
    select_provincia: new FormControl(''),
    select_distrito: new FormControl(''),
    select_entidades : new FormControl(''), //Para las entidades
    input_entidad: new FormControl(''),
    input_nombreobra: new FormControl(''),
    input_codsnip: new FormControl(''),
    input_infobras: new FormControl(''),
    select_evento: new FormControl(''),
    select_anho: new FormControl(''),
    select_detevento: new FormControl(''),
    select_fase: new FormControl(''),
    select_monitoreo: new FormControl(''),
    input_montodesde: new FormControl(''),
    input_montohasta: new FormControl('')
  });

  constructor( private dialogRef: MatDialogRef<ModalBusqObrpuComponent>,
    public mdModalIrreg: MatDialog,
    public dialog: MatDialog,
    protected catalogoService: CatalogoService,
    protected distritoService: DistritoService,
    protected provinciaService: ProvinciaService,
    protected departamentoService: DepartamentoService,
    protected infobrasService: InfobrasService,
    protected ubigeoProvisionalService: UbigeoProvisionalService,
    private maestraService : MaestrasService,
    private entidadService : EntidadService,
    private externoService : ExternosService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

    

    
    this.navModalC.setDatosNavModal2("Búsqueda General Obras Públicas");

    
    if(this.data.entidad != null && this.data.entidad != ""){
      this.select_entidades = this.data.entidad;
      this.entidadService.obtenerEntidadesXNombre( this.select_entidades).subscribe(data =>{
        this.listEntidadesObras = data;
        this.llenarCombosEntidad(this.data.entidad);
    });
    
    }
   
    this.infobrasService.obtenerEjecucionObras().subscribe(data => this.listTipoEjecucion = data);

    this.infobrasService.obtenerEventos().subscribe(data=>this.listEventos = data);

    this.maestraService.obtenerDepartamentos().subscribe(data=>{
        this.listDepartamento = data;
    });

   
     /* this.ubigeoProvisionalService.obtenerDepartamento().subscribe(
        data => {

          this.listDepartamento = data;
  
         /* this.formBusqObraP.controls.select_provincia.setValue("CELENDIN");
          console.log(data);
          
        });*/

    /*this.ubigeoProvisionalService.obtenerProvincia().subscribe(
      data => {
        this.listProvincia = data;

       // this.formBusqObraP.controls.select_provincia.setValue("CELENDIN");
     
        
      });*/

   /* this.ubigeoProvisionalService.obtenerDistrito().subscribe(
      data => {
        this.listDistrito = data;

        //this.formBusqObraP.controls.select_distrito.setValue("HUASMIN");
  
        
      });*/
 ////////////////PROVISIONALLLLLLLLLLLLLLL

  }

  cambiarDepartamento(idDepartamento: string){
    this.maestraService.obtenerProvincias(idDepartamento).subscribe(data=>{
        this.listProvincia = data;
      
     });
  }

  cambiarProvincia(idProvincia: string,idDepartamento: string)
  {
     this.maestraService.obtenerDistritos(idDepartamento,idProvincia).subscribe(data =>{
         this.listDistrito = data;
     });
  }

  CheckSeleccionar(infobrasSel:Infobras, data:any){
   
    this.listInfobras.forEach(data1 =>
      {
          
          if(data1.codigo_infobras == infobrasSel.codigo_infobras){
            data1.flgSeleccionar = data.target.checked;
         
          }else{
   
            data1.flgSeleccionar = false;
          }
      });
  
  }

  ocultarBusqueda()
  {

    this.flgMostrarAvanzada = false;

  }

  _resetListado()
  {

    this.listInfobras = [];
    this.flgListInfObras = false;

    this._resetMatTable();

  }

  

  Buscar(){

    var departamento = this.select_departamento;//this.formBusqObraP.controls.select_departamento.value;
    var provincia = this.select_provincia;//this.formBusqObraP.controls.select_provincia.value;
    var distrito = this.select_distrito;//this.formBusqObraP.controls.select_distrito.value;
    var vFlgListInfobras = false;

    this._resetListado();

    // this.listInfobras = [];
    // this.flgListInfObras = vFlgListInfobras;

    // this._resetMatTable();

    if( departamento == null || provincia == null || distrito == null ||  departamento.length == 0 || provincia.length == 0 || distrito.length == 0)
    {
     
      openModalMensaje(this.tituloMensaje,"Debe seleccionar el departamento,provincia y distrito, por favor verifique",this.dialog);

      return;

    }else if(this.entidadObra == null || this.entidadObra.centCodigo == null)
    {

      openModalMensaje(this.tituloMensaje,"Debe Ingresar la entidad",this.dialog);

      return;
    }

    this.infobrasFiltro = new InfobraConsulta()

    /* Revisar
    this.infobrasFiltro.estado_de_ejecucion = this.select_tipejec;//this.formBusqObraP.controls.select_tipejec.value;
    this.infobrasFiltro.departamento = departamento;//departamento;
    this.infobrasFiltro.entidad = this.select_entidades;//this.formBusqObraP.controls.input_entidad.value;
    this.infobrasFiltro.provincia = provincia;
    this.infobrasFiltro.distrito = distrito;
    */
   this.infobrasFiltro.cod_departamento = this.select_departamento;
   this.infobrasFiltro.cod_distrito = this.select_distrito;
   this.infobrasFiltro.cod_provincia = this.select_provincia;
   this.infobrasFiltro.entidad = this.nombreEntidad;
   this.infobrasFiltro.estado_de_ejecucion = this.select_tipejec;
   
  if(this.flgMostrarAvanzada)
  {
  
    this.infobrasFiltro.descripcion_obra = this.input_nombreobra;//this.formBusqObraP.controls.input_nombreobra.value;
    this.infobrasFiltro.codigo_snip = this.input_codsnip;//this.formBusqObraP.controls.input_codsnip.value;
    this.infobrasFiltro.codigo_infobras = this.input_infobras;//this.formBusqObraP.controls.input_infobras.value;
    this.infobrasFiltro.evento = this.select_evento;//this.formBusqObraP.controls.select_evento.value;

    // this.infobrasFiltro.Anho = this.select_anho;//this.formBusqObraP.controls.select_anho.value;
    // this.infobrasFiltro.DetalleEvento = this.select_departamento;//this.formBusqObraP.controls.select_detevento.value;
    // this.infobrasFiltro.Fase = this.select_fase;//this.formBusqObraP.controls.select_fase.value;
    // this.infobrasFiltro.MonitoreoCiu = this.select_monitoreo;//this.formBusqObraP.controls.select_monitoreo.value;
    // this.infobrasFiltro.MontoDesde = this.input_montodesde;//this.formBusqObraP.controls.input_montodesde.value;
    // this.infobrasFiltro.MontoHasta = this.input_montohasta;//this.formBusqObraP.controls.input_montohasta.value;
    

  }

  // console.log("=================this.infobrasFiltro");
  // console.log(this.infobrasFiltro);

  this.flgDeshabilitaBuscar = true;

  this.infobrasService.listarInfobras(this.infobrasFiltro).subscribe(
    
    data => {

      this.flgDeshabilitaBuscar = false;
      
      //this.listInfobras = data;
      // console.log("========================data");
      // console.log(data);

      if(data != null){

        if(data.length >0)
        {

          var fechaInicio = "";

          var tamanio = data.length;

          for (let index = 0; index < tamanio; index ++ ) {
  
            this.listInfobras[index] = new Infobras();
  
            fechaInicio = data[index].fecha_de_inicio;
  
            this.listInfobras[index].num_item = index + 1;
            this.listInfobras[index].num_index = index;
            this.listInfobras[index].entidad = data[index].entidad;
            this.listInfobras[index].provincia = data[index].cod_provincia;
            this.listInfobras[index].departamento = data[index].cod_departamento;
            this.listInfobras[index].distrito = data[index].cod_distrito;
            this.listInfobras[index].codigo_snip = data[index].codigo_snip;
            this.listInfobras[index].fecha_de_fin = data[index].fecha_de_fin;
            this.listInfobras[index].fecha_de_inicio = data[index].fecha_de_inicio;
            this.listInfobras[index].catalogo_nivel_1 = data[index].catalogo_nivel_1;
            this.listInfobras[index].codigo_entidad = data[index].codigo_entidad;
            this.listInfobras[index].codigo_infobras = data[index].codigo_infobras;
            this.listInfobras[index].descripcion_obra = data[index].descripcion_obra;
            this.listInfobras[index].estado_de_ejecucion = data[index].estado_de_ejecucion;
            this.listInfobras[index].fecha_registro = data[index].fecha_registro;
            this.listInfobras[index].plazo_de_ejecucion = data[index].plazo_de_ejecucion;
            this.listInfobras[index].costo = data[index].costo;
            
            //this.listInfobras[index].Anho = Number(fechaInicio.substr(0,4));
            this.listInfobras[index].flgSeleccionar = false;
  
          }

          //this.flgListInfObras = true;
          vFlgListInfobras = true;
          
        }else{
          openModalMensaje(this.tituloMensaje,"No se encuentran registros con los parámetros de búsqueda,verifique por favor",this.dialog);
          //this.flgListInfObras = false;
          vFlgListInfobras = false;
        }
      
      }else{
        openModalMensaje(this.tituloMensaje,"No se encuentran registros con los parámetros de búsqueda,verifique por favor",this.dialog);
        //this.flgListInfObras = false;
        vFlgListInfobras = false;
        
      }

      // console.log("============vFlgListInfobras");
      // console.log(vFlgListInfobras);

      // if(vFlgListInfobras)
      // {

      //   // setTimeout(() => {

      //   //   this.dataSource = new MatTableDataSource(this.listInfobras);
      //   //   this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;
      //   //   this.dataSource.paginator = this.paginator;
      //   //   this.dataSource.sort = this.sort;
            
      //   // }, 1);

      // }

      this.flgListInfObras = vFlgListInfobras;

      this._resetMatTable();
      
    });

  }

  _resetMatTable(){

    if(this.flgListInfObras)
    {

      setTimeout(() => {

        this.dataSource = new MatTableDataSource(this.listInfobras);
        this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
          
      }, 1);

    }

    

  }

  BusquedaAvanzada(){

    // this.input_nombreobra = null;//this.formBusqObraP.controls.input_nombreobra.value;
    // this.input_codsnip = null;//this.formBusqObraP.controls.input_codsnip.value;
    // this.input_infobras = null;//this.formBusqObraP.controls.input_infobras.value;
    // this.select_evento = null;//this.formBusqObraP.controls.select_evento.value;

    this.flgMostrarAvanzada = true;

    //console.log("BUSQUEDAD AVANZADA");

  }

  AgregarIrregularidad(codInfobras: string,flgSeleccionar:boolean,obra : Infobras)
  {

    let obraPublica = new ObraPublica();
    var vFchFin = null;
    var vFchIni = null;

    //console.log(obra);
   if(!obra.flgSeleccionar)
    {
      openModalMensaje(this.tituloMensaje,"Primero seleccione la obra pública",this.dialog);
      return;
    }
    
    vFchFin = obra.fecha_de_fin;
    vFchIni = obra.fecha_de_inicio;


    // console.log("=================obra");
    // console.log(obra);

    obraPublica.cobrapublicaCodinfob = obra.codigo_infobras;
    obraPublica.cobrapublicaDscnombre = obra.descripcion_obra;
    obraPublica.nobrapublicaCosto = obra.costo;
    obraPublica.cobrapublicaCdepent = obra.departamento;
    obraPublica.cobrapublicaCdistent = obra.distrito;
    obraPublica.cobrapublicaCprovent = obra.provincia;
    obraPublica.nobrapublicaAnho = obra.Anho;
    obraPublica.cobrapublicaDscestado = obra.estado_de_ejecucion;
    obraPublica.strDobrapublicaFechaini = (vFchIni == null ? "" : obra.fecha_de_inicio + DscHoraDefault);
    obraPublica.strDobrapublicaFechafin = (vFchFin == null ? "" : obra.fecha_de_fin + DscHoraDefault);
    //Servicio Infoobra
    //obraPublica.cobrapublicaEstado = 'En ejecución';
    //obraPublica.dobrapublicaFechaIni = '13-06-2019';
    //obraPublica.dobrapublicaFechaFin = '12-12-2020';
    //obraPublica.nobrapublicaIddeteven = obra.Evento;
    this.furObraPublica.obraPublica = obraPublica;

    /*this.listInfobras.forEach(data=>{
      if(data.codigo_infobras == codInfobras){
          let obraPublica = new ObraPublica();
          obraPublica.cobrapublicaCodinfob = data.codigo_infobras;
          obraPublica.cobrapublicaDscnombre = data.descripcion_obra;
          obraPublica.nobrapublicaCosto = data.costo;
          obraPublica.cobrapublicaCdepent = data.departamento;
          obraPublica.cobrapublicaCdistent = data.distrito;
          obraPublica.cobrapublicaCprovent = data.provincia;
          obraPublica.nobrapublicaAnho = data.Anho;
          obraPublica.nobrapublicaIddeteven = data.Evento;
          console.log(obraPublica);
          this.furObraPublica.obraPublica = obraPublica;
          
      }
    });*/

    const dialogRef = this.mdModalIrreg.open(ModalIrregInfobrasComponent,{ width: '50%',height:'87%', data: {obraPub : this.furObraPublica}});
    
    dialogRef.afterClosed().subscribe(result => {
        if(result != null){
          this.furObraPublica = result;
        }
        
        console.log(this.furObraPublica);
     
    });

    //Falta la homologaci+on de datos con this.obraPublica

  }

  aceptar(){
   
    let flgVacio = 0;
    //if(this.obraPublica.cobrapublicaCodinfob.length == 0 || this.obraPublica.listHechoIrregular.length == 0)

    if(this.furObraPublica.obraPublica == null || this.furObraPublica.listaFurPorObraPubPorHechoIrr == null || this.furObraPublica.listaFurPorObraPubPorHechoIrr.length == 0){
      openModalMensaje(this.tituloMensaje,"Debe seleccionar la obra pública y los hechos irregulares, por favor verifique",this.dialog); 
    }else{
      this.dialogRef.close(this.furObraPublica);
    }

    

   /*this.listInfobras.forEach(obra=>{
        if(obra.flgSeleccionar){
          console.log(obra);
          if(obra.listHechoIrregular.length==0){
            flgVacio = 1;
          }else{
            this.listInfobrasSel.push(obra);
          }
       
        }
       
     });
     if(this.listInfobrasSel.length==0){
      flgVacio = 1;
    }

    if(flgVacio==1){
      openModalMensaje(this.tituloMensaje,"Debe seleccionar la obra pública y los hechos irregulares, por favor verifique",this.dialog); 
     }else{
      
      this.dialogRef.close(this.listInfobrasSel);
     }*/
   
    //this.dialogRef.close(this.obraPublica);
    

    // var numFlgSeleccionar = 0;

    // for (let index = 0; index < this.listInfobras.length; index++) 
    // {

    //   if(this.listInfobras[index].flgSeleccionar)
    //   {

    //     numFlgSeleccionar = 1;

    //   }
      
    // }

    // if(numFlgSeleccionar == 0)
    // {
    // }

  }

  cancelar(){
    this.dialogRef.close();
  }


  buscarEntidad(valor){
    let nombreEntidad = valor.target.value;

    // console.log("============nombreEntidad");
    // console.log(nombreEntidad);
    
    if(nombreEntidad.length >4){

      this.entidadService.obtenerEntidadesXNombre(nombreEntidad).subscribe(data =>{

          this.listEntidadesObras = data;

          // console.log("======this.listEntidadesObras");
          // console.log(this.listEntidadesObras);
          

      });

    }
  }

  seleccionarEntidad(event)
  {
    
    let name = event.target.value;

    this.llenarCombosEntidad(name);

  }

  llenarCombosEntidad(nombreEntidad :string){
  
    
    let list = null;

    if(this.listEntidadesObras == null){return;}

    list = this.listEntidadesObras.filter(x => x.centNombre === nombreEntidad)[0];
    // console.log("ESTA ES LA LISTA");
    // console.log(list);
    if(list != null)
    {
      this.nombreEntidad = list.centNombre;
      this.entidadService.obtenerEntidadXCodigo(list.centCodigo).subscribe(data =>{
   
        //this.obtenerProvincias(data.centDirdep);
        //this.obtenerDistritos(data.centDirdep,data.centDirpro);
        /*this.formEntidadHecho.get('select_departamento').setValue(data.centDirdep);
        this.formEntidadHecho.get('select_provincia').setValue(data.centDirpro);
        this.formEntidadHecho.get('select_distrito').setValue(data.centDirdis);
        this.formEntidadHecho.get('inputDireccion').setValue(data.centDirecc);*/
        if(data != null){
          this.entidadObra = data;
        }
    
        //this.entidad.centCodigo = data.centCodigo;
    });
    }

  }

}
