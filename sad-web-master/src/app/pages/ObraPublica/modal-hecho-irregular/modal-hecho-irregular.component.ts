import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FurXObraPublica } from 'src/app/models/FurXObraPublica';
import { NavModalComponent } from '../../General/nav-modal/nav-modal.component';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DscTituloPaginator } from 'src/app/general/variables';
import { FurXObraPublicaXHi } from 'src/app/models/FurXObraPublicaXHi';

@Component({
  selector: 'app-modal-hecho-irregular',
  templateUrl: './modal-hecho-irregular.component.html',
  styleUrls: ['./modal-hecho-irregular.component.css']
})
export class ModalHechoIrregularComponent implements OnInit {

  matTableColumnsDef = ["Item","Hecho Irregular"];
  obra : FurXObraPublica;

  dataSource: MatTableDataSource<FurXObraPublicaXHi>;

  @ViewChild(NavModalComponent) navModalC : NavModalComponent;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialogRef: MatDialogRef<ModalHechoIrregularComponent>,@Inject(MAT_DIALOG_DATA) public data: any,) { }
  

  ngOnInit() {

    this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;

    this.navModalC.setDatosNavModal2("Descripción de hecho irregular");

    //console.log(this.data);
    this.obra = this.data.obra;

    //this.obra.listaFurPorObraPubPorHechoIrr

    this._cargarTableMaterial();
    //console.log(this.obra);
  //  this.obra.listaFurPorObraPubPorHechoIrr = this.data.listaFurPorObraPubPorHechoIrr;
  }

  cerrarModal(){
    this.dialogRef.close();
  }

  _cargarTableMaterial(){
    this.dataSource = new MatTableDataSource(this.obra.listaFurPorObraPubPorHechoIrr);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
