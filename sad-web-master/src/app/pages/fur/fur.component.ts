import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { EvalinfRegfurComponent } from '../evalinf-regfur/evalinf-regfur.component';
import { FormularioWebComponent } from '../formulario-web/formulario-web.component';
import { FurService } from 'src/app/services/fur.service';

@Component({
  selector: 'app-fur',
  templateUrl: './fur.component.html',
  styleUrls: ['./fur.component.css']
})
export class FurComponent implements OnInit, AfterViewInit {
 

  @ViewChild(EvalinfRegfurComponent) hojaEvaluacionCmp : EvalinfRegfurComponent;

  constructor(private serviceFur : FurService) { }

  ngOnInit() {
    this.hojaEvaluacionCmp.flgMostrarTabFur = true;

    /*this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.entidadHechoC.flgLectura = true;
    this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.hechoIrregularC.flgLectura = true;
    this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.flgLectura = true;*/
    this.serviceFur.ObtenerFurPorId(237).subscribe(data =>{//181 //179
       /* console.log(data);
    this.serviceFur.ObtenerFurPorId(191).subscribe(data =>{//181 //179
        console.log(data);
        this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.entidadHechoC.entidadComponentReadEnable();
        this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.hechoIrregularC.hechoIrregularCReadEnable();
        this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.funcionarioCReadEnable();
        this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.empresaCReadEnable();

       // this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.pruebaHechoC.setPruebaHechoComponent(data);
       // this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.pruebaHechoC.pruebaHechoCReadEnable();
        this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.denunciaContenedorCReadEnable();
      
        if(this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.pruebaHechoC.cuenta_pruebas){
          this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.pruebaHechoC.evidenciaC.evidenciaCReadEnable();
        }
  


        this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.entidadHechoC.setEntidadHecho(data);
        this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.hechoIrregularC.setHechoIrregularComponent(data);
        //this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.setFuncionarioComponent(data);
        this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.setEmpresaComponent(data);
        
        this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.setDenunciaContenedorC(data);
        
        if(this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.pruebaHechoC.cuenta_pruebas){
          this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.pruebaHechoC.evidenciaC.setEvidenciaC(data);
        }
       */
      });
    
    //this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.entidadHechoC.modoLectura(true);
  }

  ngAfterViewInit(): void {

//   this.hojaEvaluacionCmp.formularioCmp.denunciaContenedorC.entidadHechoC.modoLectura(true);
  }

}
