import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { iconoMatAgregar } from 'src/app/general/variables';
import { General } from 'src/app/models/General';

@Component({
  selector: 'app-nav-modal',
  templateUrl: './nav-modal.component.html',
  styleUrls: ['./nav-modal.component.css']
})
export class NavModalComponent implements OnInit {

  strIcono: string = iconoMatAgregar;
  strDscTitulo: string;

  @Output() opCerrarModal = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  _cerrarModal(){

    this.opCerrarModal.emit();

  }

  setDatosNavModal(datosModal:General){

    this.strIcono = datosModal.dscCampo1;
    this.strDscTitulo = datosModal.dscCampo2;

  }

  setDatosNavModal2(dscTitulo: string){

    //this.strIcono = datosModal.dscCampo1;
    this.strDscTitulo = dscTitulo;

  }



}
