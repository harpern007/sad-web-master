import { Component, OnInit, ViewChild } from '@angular/core';
import { Cargo } from 'src/app/models/Cargo';
import { Gestortarea } from 'src/app/models/GestorTarea';
import { Plazo } from 'src/app/models/Plazo';
import { PlazoService } from 'src/app/services/plazo.service';
import { GestorTareaService } from 'src/app/services/gestor-tarea.service';
import { GlobalVars, NumIdCargoGestorCan, MfuncErrorVal, DscTituloPaginator } from 'src/app/general/variables';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { PersonalCgrService } from 'src/app/services/personal-cgr.service';
import { PersonalCgr } from 'src/app/models/PersonalCgr';
import { ErrorValidator } from 'src/app/models/personalizados/ErrorValidator';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-asigna-responsable',
  templateUrl: './asigna-responsable.component.html',
  styleUrls: ['./asigna-responsable.component.css']
})
export class AsignaResponsableComponent implements OnInit {

  strEtiquetaAsign:string = "Asignación";
  strEtiquetaRespAten:string = "Responsable de Atención";
  strIdResponsable:string = "";
  flgSoloLectura = false;
  flgDeshabilitado = true;
  //numIdCargo:number = null;
  strDscIndicacion:string = "";
  listCargo:Cargo[] = [];
  listAsignacion:Gestortarea[]=[];
  numCantTiempo:number = null;
  numIdPlazo:number = null;
  listPlazo:Plazo[] = [];
  listPersonalCgr:PersonalCgr[]=[];
  numIdCargo:number = NumIdCargoGestorCan;
  errorValidator: ErrorValidator[] = [];
  mFuncErrorVal : MfuncErrorVal = new MfuncErrorVal();
  matTableColumnsDef = ["Item","nombre","tarea","fchAsig"];
  dataSource: MatTableDataSource<Gestortarea>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(protected plazoService:PlazoService,
    protected gestorTareaService:GestorTareaService,
    protected maestraService:MaestrasService,
    protected personalCgrService: PersonalCgrService) { }

  ngOnInit() {

    this.plazoService.ListarPlazo().subscribe(data=>{

      this.listPlazo = data;

    });

    this.gestorTareaService.ListarCargaTareaGestorPorSuperv(GlobalVars.CodPersSuperv).subscribe(data=>{

      this.listAsignacion = data;

      console.log("===================this.listAsignacion");
      console.log(this.listAsignacion);

      this._cargarTableMaterial();

    });

    // console.log("============GlobalVars.CodPersSuperv");
    // console.log(GlobalVars.CodPersSuperv);

    this.personalCgrService.ListarPersonalCgrActivoPorSuperv(GlobalVars.CodPersSuperv).subscribe(data=>{

      this.listPersonalCgr = data;

      // console.log("=============this.listPersonalCgr");
      // console.log(this.listPersonalCgr);

    });

    this.maestraService.obtenerCargo().subscribe(data=>{

      this.listCargo = data;

    });


  }

  _cargarTableMaterial(){
    
    setTimeout(() => {

      this.dataSource = new MatTableDataSource(this.listAsignacion);

      this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      
    }, 1);
    
  }

  ValidarDatos()
  {

    var flgError = false;

    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.strIdResponsable.length > 0 ? false:true), "Ingrese el responsable",this.errorValidator,true);
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.strDscIndicacion.length > 0 ? false:true), "Ingrese la indicación",this.errorValidator,false);
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.numCantTiempo != null && this.numIdPlazo != null  ? false:true), "Ingrese el Plazo para evaluación",this.errorValidator,false);

    flgError = this.mFuncErrorVal.RevisarErrorValidator(this.errorValidator);

    // console.log("==============flgError");
    // console.log(flgError);

    return flgError;

  }

}
