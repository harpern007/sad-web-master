import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignaResponsableComponent } from './asigna-responsable.component';

describe('AsignaResponsableComponent', () => {
  let component: AsignaResponsableComponent;
  let fixture: ComponentFixture<AsignaResponsableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignaResponsableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignaResponsableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
