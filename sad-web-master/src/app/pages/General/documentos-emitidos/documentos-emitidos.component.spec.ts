import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosEmitidosComponent } from './documentos-emitidos.component';

describe('DocumentosEmitidosComponent', () => {
  let component: DocumentosEmitidosComponent;
  let fixture: ComponentFixture<DocumentosEmitidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentosEmitidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosEmitidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
