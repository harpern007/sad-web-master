import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalMensaje } from 'src/app/models/ModalMensaje';

@Component({
  selector: 'app-modal-mensaje',
  templateUrl: './modal-mensaje.component.html',
  styleUrls: ['./modal-mensaje.component.css']
})
export class ModalMensajeComponent implements OnInit {

  flgBtnRegresar:boolean = false;

  constructor( public dialogRef: MatDialogRef<ModalMensajeComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: ModalMensaje) { }

  ngOnInit() 
  {

    this.flgBtnRegresar = this.data.flgRegresar;

  }

  _regresar(){

    this.dialogRef.close(true);

  }

  _aceptar(){

    this.dialogRef.close(false);

  }

 
}
