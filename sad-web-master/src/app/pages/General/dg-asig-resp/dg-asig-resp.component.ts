import { Component, OnInit } from '@angular/core';
import { TipoAtencionService } from 'src/app/services/tipo-atencion.service';
import { TipoAtencion } from 'src/app/models/TipoAtencion';
import { Canal } from 'src/app/models/Canal';
import { CanalService } from 'src/app/services/canal.service';

@Component({
  selector: 'app-dg-asig-resp',
  templateUrl: './dg-asig-resp.component.html',
  styleUrls: ['./dg-asig-resp.component.css']
})
export class DgAsigRespComponent implements OnInit {

  flgSoloLectura = false;
  listTipoAtencion:TipoAtencion[] = [];
  listCanal:Canal[] = [];
  numIdCanal:number = null;
  numIdTipAtencion:number = null;

  constructor(protected tipoAtencionService:TipoAtencionService,
    protected canalService:CanalService) { }

  ngOnInit() {

    this.tipoAtencionService.listarTipoAtencion().subscribe(data=>{

      this.listTipoAtencion = data;

    });

    this.canalService.listarCanal().subscribe(data=>{

      this.listCanal = data;

    });

  }

}
