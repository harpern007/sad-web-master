import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DgAsigRespComponent } from './dg-asig-resp.component';

describe('DgAsigRespComponent', () => {
  let component: DgAsigRespComponent;
  let fixture: ComponentFixture<DgAsigRespComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DgAsigRespComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DgAsigRespComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
