import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocAGenerarComponent } from './doc-a-generar.component';

describe('DocAGenerarComponent', () => {
  let component: DocAGenerarComponent;
  let fixture: ComponentFixture<DocAGenerarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocAGenerarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocAGenerarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
