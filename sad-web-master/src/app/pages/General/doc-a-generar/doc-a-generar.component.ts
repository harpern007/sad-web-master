import { Component, OnInit } from '@angular/core';
import { Oficio } from 'src/app/models/BPM/oficio';
import { EvaluacionPreliminarService } from 'src/app/services/bpm/evaluacion-preliminar.service';
import { MatDialog } from '@angular/material';
import { openModalMensaje } from 'src/app/general/variables';

@Component({
  selector: 'app-doc-a-generar',
  templateUrl: './doc-a-generar.component.html',
  styleUrls: ['./doc-a-generar.component.css']
})
export class DocAGenerarComponent implements OnInit {

  flgOficio:boolean = false;
  flgMemo:boolean = false;
  flgCarta:boolean = false;

  oficio : Oficio = new Oficio();
  constructor(protected bpmService : EvaluacionPreliminarService, public dialog: MatDialog,) { }

  ngOnInit() {
  }

  _checkOficio(event:any)
  {

    this.flgOficio = event.target.checked;

  }

  _checkMemo(event:any)
  {

    this.flgMemo = event.target.checked;

  }

  _checkCarta(event:any)
  {

    this.flgCarta = event.target.checked;

  }

  _GenerarDocumento()
  {
    //PENDIENTE LO DE GENERACIÓN DE OFICIOS
    //Generación de ofiios
    this._generarOficio();
  }

  _GuardarDocumento()
  {
    //PENDIENTE LO DE GENERACIÓN DE OFICIOS
  }

  _generarOficio(){  
        let numeroOficio = '000000010';
        this.oficio.rutaArchivoPlantilla = "C:\\\\archivos\\\\";
        this.oficio.nombreArchivoPlantilla = "OFICIO.DOCX";
        this.oficio.rutaArchivoGenerado = "C:/archivos/";
        this.oficio.nombreArchivoGenerado = "oficioGeneradoCGR.docx";
        this.oficio.asunto = "Oficio  N° " + numeroOficio;
        this.oficio.cargo = "Ciudadano";
        this.oficio.cargo_EMP_ENC = "ANALISTA";
        this.oficio.contenido = "En ese sentido, en virtud a lo dispuesto en el artículo 8° de la Ley N° 29542 “Ley de Protección al Denunciante en el Ámbito Administrativo y de Colaboración Eficaz en el Ámbito Penal” y en el artículo 10° de su reglamento, aprobado mediante Decreto Supremo N° 038-2011-PCM, los cuales establecen la competencia del (Ministerio del Trabajo y Promoción del Empleo / las Direcciones Regionales de Trabajo y Promoción del Empleo) para realizar inspecciones laborales cuando el trabajador denunciante es objeto de represalias que se materializan en actos de hostildad, independientemente del régimen laboral en el que se encuentre ; se remite a su Despacho, fotocopia de la documentación alcanzada por el mencionado por el mencionado denunciante, en los folios, para las acciones correspondientes y posterior remisión a este Órgano Superiror de Control de la actas o informes conteniendo el resultado de la actuación de inspección, en cumplimiento de lo señalado en el literal e) del artículo 10° del mencinado reglamento.";
        this.oficio.datos_EMISION_CGR = "CGR-123456";
        this.oficio.des_ENTIDAD = "SUNAT";
        this.oficio.direccion_DESTINATARIO ="MZ. C13 LT. 34 Bocanegra-Callao";
        this.oficio.empleado_EMITE ="Congresita";
        this.oficio.iniciales_EMP =  "USIL";
        this.oficio.nombre_ANIO = "DAVC-1993";
        this.oficio.nombre_DESTINATARIO = "Alexander Valderrama";
        this.oficio.pie_PAGINA = "Contraloria General";
        this.oficio. post_FIRMA = "Firmado",
        this.oficio.referencia = "http://192.168.0.1:9000/Hola Mundo";
        this.oficio.sigla_DOC = "ASDQWEZXC-123456";
        this.oficio.ubigeo_DESTINATARIO = "Lima-San Isidro-Navarrete";
        this.oficio.uuoo_DESTINO = "SUNAT";
                this.bpmService.generarOficio(this.oficio).subscribe(data =>
          {
              if(data != null){
                
                openModalMensaje("Hoja evaluación","Se generó el oficio N° 00000201",this.dialog);

              }
          });
  }
}
