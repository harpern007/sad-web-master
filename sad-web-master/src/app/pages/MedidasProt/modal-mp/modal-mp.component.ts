import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ModalGeneral } from 'src/app/models/ModalGeneral';
import { ModalNotificacionComponent } from '../../Denunciante/ModalNotificacion/modal-notificacion.component';
import { FormGroup, FormControl } from '@angular/forms';
import { NavModalComponent } from '../../General/nav-modal/nav-modal.component';

@Component({
  selector: 'app-modal-mp',
  templateUrl: './modal-mp.component.html',
  styleUrls: ['./modal-mp.component.css']
})
export class ModalMpComponent implements OnInit {

  // formModalMP = new FormGroup({
  //   //nro_formulario : new FormControl('')
  // });

  @ViewChild(NavModalComponent) navModalC : NavModalComponent;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ModalGeneral,
  public dialogRef: MatDialogRef<ModalNotificacionComponent>
  ) { }

  ngOnInit() {

    this.navModalC.setDatosNavModal2("Instruccciones");

    // console.log("=======================Modal general")
    // console.log(this.data);

  }

  _cancelar(){

    this.dialogRef.close();

  }

  // onNoClick(): void {
  //   this.dialogRef.close();
  // }

}
