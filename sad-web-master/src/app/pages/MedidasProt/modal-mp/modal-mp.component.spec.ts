import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalMpComponent } from './modal-mp.component';

describe('ModalMpComponent', () => {
  let component: ModalMpComponent;
  let fixture: ComponentFixture<ModalMpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalMpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
