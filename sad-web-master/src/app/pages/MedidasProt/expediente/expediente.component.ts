import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Anho} from 'src/app/models/Anho';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { ExpedienteSGD } from 'src/app/models/ExpedienteSGD';
import { FiltroExpediente } from 'src/app/models/personalizados/FiltroExpediente';
import { ExpedienteService } from 'src/app/services/expediente.service';
import { FichaExpediente } from 'src/app/models/FichaExpediente';
import { Fur } from 'src/app/models/Fur';
import { iconAgregar, iconEliminar, DscTituloPaginator } from 'src/app/general/variables';



@Component({
  selector: 'app-expediente',
  templateUrl: './expediente.component.html',
  styleUrls: ['./expediente.component.css']
})
export class ExpedienteComponent implements OnInit {
  matTableColumnsDef = ["Expediente","Fecha recepción","Asunto","Estado","Entidad","Acciones"];
  listAnios : Anho[] = [];
  dataSourceSGD: MatTableDataSource<ExpedienteSGD>;
  dataSourceExpSelec: MatTableDataSource<ExpedienteSGD>;
  select_anio_desde : string;
  numeroExpediente : string;
  filtroExpediente : FiltroExpediente = new FiltroExpediente();
  listaExpedienteResultado : Fur[] = [];
  listaExpedienteResultadoSGD : ExpedienteSGD[] = [];
  listaExpediente : FichaExpediente[] = [];
  listaExpedienteSelec : ExpedienteSGD[] = [];
  
  fichaExpediente : FichaExpediente = new FichaExpediente();
  flgEsSGD : boolean = true;
  flgEsOCI : boolean = false;
  flgAgregado : boolean = false;
  flgLectura : boolean = false;
    //Icono
    iconoAgregar = iconAgregar;
    iconoEliminar = iconEliminar;
    //
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor( private maestraService : MaestrasService , private expedienteService : ExpedienteService) { }

  ngOnInit() {
    this.maestraService.obtenerAnho().subscribe(data => this.listAnios = data);
    //this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;
  }

  buscarExpediente(){
    this.filtroExpediente.anio_expediente = this.select_anio_desde;
    this.filtroExpediente.nu_expediente = this.numeroExpediente;
    //Expedientes del SGD
    this.expedienteService.listarExpedientes(this.filtroExpediente).subscribe(data =>
      {
        if(data != null){
          this.listaExpedienteResultadoSGD = data;
          this._cargarTableMaterialSGD();
        }
      });

      //Expediente en SAD (PARA OCI)
      if(this.flgEsOCI){
        this.expedienteService.listarExpedientesSAD(this.filtroExpediente).subscribe(data=>{
            if(data != null){
    
            }
        });
      }
  }

  _cargarTableMaterialSGD(){


    setTimeout(() => {

      this.dataSourceSGD = new MatTableDataSource(this.listaExpedienteResultadoSGD);

      if(this.paginator != null){this.paginator._intl.itemsPerPageLabel = DscTituloPaginator}; //this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;

      this.dataSourceSGD.paginator = this.paginator;
      this.dataSourceSGD.sort = this.sort;
      
    }, 1);


    // this.dataSourceSGD = new MatTableDataSource(this.listaExpedienteResultadoSGD);
    // this.dataSourceSGD.paginator = this.paginator;
    // this.dataSourceSGD.sort = this.sort;
  }

  _cargarTableMaterialExpSelec(){

    setTimeout(() => {

      this.dataSourceExpSelec = new MatTableDataSource(this.listaExpedienteSelec);

      if(this.paginator != null){this.paginator._intl.itemsPerPageLabel = DscTituloPaginator}; //this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;

      this.dataSourceExpSelec.paginator = this.paginator;
      this.dataSourceExpSelec.sort = this.sort;
      
    }, 1);

    // this.dataSourceExpSelec = new MatTableDataSource(this.listaExpedienteSelec);
    // this.dataSourceSGD.paginator = this.paginator;
    // this.dataSourceSGD.sort = this.sort;
  }

  _agregarExpSGD(expedienteSGD : ExpedienteSGD){
      this.fichaExpediente.cfichaexpedienteNumExp = expedienteSGD.nu_expediente;
      this.fichaExpediente.nfichaexpedienteAnio = this.select_anio_desde;
      this.fichaExpediente.nfichaexpedienteTipExp = 'SGD';
      this.fichaExpediente.cfichaexpedienteCodtipo = 'FUR';
      this.flgAgregado = true;
      this.listaExpedienteSelec.push(expedienteSGD);
      this.listaExpediente.push(this.fichaExpediente);
      this._cargarTableMaterialExpSelec();
      this._cargarTableMaterialSGD();
  }

  _eliminarExpSGD(expedienteSGD : ExpedienteSGD){
    this.listaExpedienteSelec.splice(this.listaExpedienteSelec.indexOf(expedienteSGD),1);
    this._cargarTableMaterialExpSelec();
    this.flgAgregado = false;
    this.listaExpediente.splice(0,this.listaExpediente.length);
    this.fichaExpediente = new FichaExpediente();
  }
}
