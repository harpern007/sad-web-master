import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ParametroService } from 'src/app/services/parametro.service';
import { Parametro } from 'src/app/models/Parametro';
import { FormGroup, FormControl } from '@angular/forms';
import { ModalMpComponent } from '../modal-mp/modal-mp.component';
import { Fur } from 'src/app/models/Fur';
import { ExpedienteComponent } from '../expediente/expediente.component';

@Component({
  selector: 'app-contenedor-mp',
  templateUrl: './contenedor-mp.component.html',
  styleUrls: ['./contenedor-mp.component.css']
})
export class ContenedorMpComponent implements OnInit {
  @ViewChild(ExpedienteComponent) expedienteC : ExpedienteComponent;
  pTitulo: Parametro = new Parametro();
  pTitulo2: Parametro = new Parametro();
  pCuerpo: Parametro = new Parametro();
  flgAcepto = false;
  flgSolicita = false;
  flgLectura : boolean = false;
  flgEsFur : boolean = false;
 

  constructor(public dialog: MatDialog,
    protected parametroService: ParametroService,
    ) { }

  ngOnInit() {

    this.parametroService.obtenerParametro(1).subscribe(
      data => {
        this.pTitulo = data;
        console.log("=======================0Obtener Parametro");
        console.log(data);
        //this.formHechos.controls.select_canal.setValue(5);
      }
    );

    this.parametroService.obtenerParametro(2).subscribe(
      data => {
        this.pTitulo2 = data;
        //console.log(data);
        //this.formHechos.controls.select_canal.setValue(5);
      }
    );

    this.parametroService.obtenerParametro(3).subscribe(
      data => {
        this.pCuerpo = data;
        //console.log(data);
        //this.formHechos.controls.select_canal.setValue(5);
      }
    );

  }

  AbrirInstrucciones(){

    const dialogRef = this.dialog.open(ModalMpComponent,{ width: '70%',
    data: this.pTitulo}); //height: '70%'
    
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });

  }

  CambioSolicita(flgEvent){

    this.flgSolicita = flgEvent.checked;

  }

  medidaProteccionCReadEnable(){
    console.log("Medidas de protección lectura true ");
      this.flgLectura = true;
  }

  medidaProteccionCReadDisable(){
    this.flgLectura = false;
  }

  setMedidaProteccionC(fur : Fur){
    if(fur.cfurCodtipo == 'FUR'){
      this.flgEsFur = true;
   } 
    this.flgAcepto = fur.denunciante.cdenuncianteFlgconfleyprot == "SI" ? true : false;
    this.flgSolicita = fur.denunciante.cdenuncianteFlgacepmedpyb == "SI" ? true : false;
    console.log("Flag es fur: " + this.flgEsFur);
  }

  // SolicitaSI(){

  //   this.flgSolicita = true;

  // }

  // SolicitaNO(){

  //   this.flgSolicita = false;

  // }

  CambioAcepto(flgEvent){

    this.flgAcepto = flgEvent.checked;

  }

  // AceptoSI(){

  //   this.flgAcepto = true;

  // }

  // AceptoNO(){

  //   this.flgAcepto = false;

  // }

}
