import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorMpComponent } from './contenedor-mp.component';

describe('ContenedorMpComponent', () => {
  let component: ContenedorMpComponent;
  let fixture: ComponentFixture<ContenedorMpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContenedorMpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContenedorMpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
