import { Component, OnInit, ViewChild } from '@angular/core';
import { DgHojaEvalComponent } from '../../datos-general/dg-hoja-eval/dg-hoja-eval.component';
import { AntecedentesFurComponent } from '../antecedentes-fur/antecedentes-fur.component';
import { EvidenciasComponent } from '../../denuncia/evidencias/evidencias.component';
import { ContenedorWebComponent } from '../../formulario-web/contenedor-web/contenedor-web.component';
import { ResAnalisis } from 'src/app/models/ResAnalisis';
import { HojaEvaluacion } from 'src/app/models/HojaEvaluacion';
import { ErrorValidator } from 'src/app/models/personalizados/ErrorValidator';
import { MfuncErrorVal, GetDateTime, NumIdEstadoAtendido, openModalMensaje, GlobalVars } from 'src/app/general/variables';
import { Fur } from 'src/app/models/Fur';
import { FurService } from 'src/app/services/fur.service';
import { ResAnalisisService } from 'src/app/services/res-analisis.service';
import { HojaEvaluacionService } from 'src/app/services/hoja-evaluacion.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-evaluacion-fud',
  templateUrl: './evaluacion-fud.component.html',
  styleUrls: ['./evaluacion-fud.component.css']
})
export class EvaluacionFudComponent implements OnInit {

   //PROPIEDADES EN VISTA
   codResAnalisis:string = "";
   dscResConclus:string = "";
   dscResRecomen:string = "";
   flgMostrarTabFur = false;
   
   //PROPIEDADES EN VISTA
 
   idFur:number;
 
   @ViewChild(DgHojaEvalComponent) dgHojaEvalC : DgHojaEvalComponent;
   @ViewChild(AntecedentesFurComponent) antecedentesFurC : AntecedentesFurComponent;
   @ViewChild(EvidenciasComponent) evidenciasC : EvidenciasComponent;
   
   @ViewChild(ContenedorWebComponent) formularioCmp : ContenedorWebComponent;
   //@ViewChildren(FormularioWebComponent) formularioCmp : FormularioWebComponent;
   
   listResAnalisis: ResAnalisis[] = [];
   hojaEvaluacion : HojaEvaluacion = new HojaEvaluacion();
   errorValidator: ErrorValidator[] = [];
   mFuncErrorVal : MfuncErrorVal = new MfuncErrorVal();
   fur : Fur = new Fur();
   idFUrBaseV : number ;
 
 
   /*
   CUS06
   */
   flgSegundaEval : boolean = false;
   furEvaluacion : Fur = new Fur();
   flgExisteAntecedente : boolean = false;
   flgAntecedenteInterno : boolean = false;
   flgInformacionComplementaria : boolean = false;
   flgDenunciaInterna : boolean = false;
   /*
   */
 
   constructor(protected furService: FurService,
     protected resAnalisisService: ResAnalisisService,
     protected hojaEvaluacionService: HojaEvaluacionService,
     public router:Router,
     private route: ActivatedRoute,
     protected serviceFur : FurService,
     public dialog: MatDialog) 
    { 
 
     
     }
 
   ngOnInit() {
 
     this.idFur = this.route.snapshot.params.idFur;
     
      
     setTimeout(() => {
 
       //console.log("============recuperar13");
 
       this.dgHojaEvalC.DatosDefault();
       
       /**
        * GSOLIS
        */
  
         //this._obtenerDatosFur(this.idFur);
         
       /*
       * FIN GSOLIS
        */
    
 
       this.antecedentesFurC.RecuperarAntecedentes(this.idFur); // temporal
 
      this.resAnalisisService.ListarResultadoAnalisis().subscribe(data=>{
   
         this.listResAnalisis = data;
         //Espera que los datos se llenen para deshabilitar los campos
         this._deshabilitarCamposFur();
       });
       // this.formularioCmp.denunciaContenedorC.denunciaContenedorCReadEnable();
      
     }, 5);
     
     
     // console.log("===============this.errorValidator");
     // console.log(this.errorValidator);
 
   }
 
   
   cambiarFlgExistente(elemento){
     console.log(elemento);
     let valor = (elemento as HTMLInputElement);
     console.log(elemento.toElement.value);
     //this.flgExisteAntecedente = valor;
     console.log(this.flgExisteAntecedente);
   }
   _Guardar()
   {
 
     //console.log("==========antes de guardar");
       // console.log(this.codResAnalisis);
 
     var fur = this.antecedentesFurC.listFormWeb[0];
     var vIdHoja;
 
     // console.log("=============this.dscResConclus");
     // console.log(this.dscResConclus);
     // console.log("=============this.dscResRecomen");
     // console.log(this.dscResRecomen);
 
     this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.codResAnalisis.length > 0 ? false:true), "Ingrese el resultado del análisis",this.errorValidator,true);
     this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.dscResConclus.length > 0 ? false:true), "Ingrese la conclusión",this.errorValidator,false);
     this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.dscResRecomen.length > 0 ? false:true), "Ingrese la recomendación",this.errorValidator,false);
     //  console.log("==========antes de guardar");
     //  console.log(this.mFuncErrorVal);
     //console.log(this.errorValidator);
 
     //if(this.errorValidator.filter(x => x.flgInvalid).length > 0)
     if(this.mFuncErrorVal.RevisarErrorValidator(this.errorValidator)){return;}
 
     //console.log("a grabar");
 
     this.hojaEvaluacion.strDhojaevalFregini= this.dgHojaEvalC.fchHrInicioReg;
     this.hojaEvaluacion.strDhojaevalFregistro = GetDateTime();
     this.hojaEvaluacion.nhojaevalIdfur = fur.nfurId;
     this.hojaEvaluacion.nhojaevalIdresanalisis = Number(this.codResAnalisis);
     this.hojaEvaluacion.chojaevalDscconclusion1 = this.dscResConclus;
     this.hojaEvaluacion.chojaevalDscconclusion2 = this.dscResRecomen;
 
     this.hojaEvaluacion.listaDocumentoDetalle = this.evidenciasC.listEvidencias;
 
     fur.nfurIdestado = NumIdEstadoAtendido;
     //this.hojaEvaluacion.nhojaevalIdresanalisis = this.formHojaEvaluacion.controls.select_resanali.;
 
 
     console.log("==============this.hojaEvaluacion");
     console.log(this.hojaEvaluacion);
     if(this.codResAnalisis == "1"){
      // this.formularioCmp.Guardar(this.idFur);
     }
     
     this.hojaEvaluacionService.RegistrarHojaEvaluacion(this.hojaEvaluacion).subscribe(data=>{
 
       vIdHoja = data.nhojaevalId;
 
       if(vIdHoja > 0)
       {
 
         this.furService.ActualizarFur(fur).subscribe(data=>{
 
           
           if(data.nfurId > 0)
           {
             //
             openModalMensaje("Hoja Evaluación",`Se registró correctamente la hoja de evaluación N° ${vIdHoja}, por favor verifique`,
             this.dialog).afterClosed().subscribe(result=>{
               this._GuardarFur();
               setTimeout(()=>{
                this._Retornar();
                },5);
     
             });
 
           }else
           {
             openModalMensaje("Asignar Información","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{
 
               this._Retornar();
     
             });
     
             //flgGuardado = false;
     
             return;
           }
 
           
 
         });
 
       }
 
     });
 
   }
 
   
     _obtenerDatosFur(idFur : number){
       this.idFUrBaseV = idFur;
       this.serviceFur.ObtenerFurPorId(idFur).subscribe(data =>{//181 //179
         //
         this.furEvaluacion = data;
         console.log(data);
         //Primera validación para ver si el dato que viene es un FUR(CUS04 O CUS06)
         if(this.furEvaluacion.cfurCodtipo == 'FUR'){
           this.formularioCmp.denunciaContenedorC.flgEsFur = true;
           this.flgSegundaEval = true;
         }
         //Cuando se ejecuta el obtenerDatosFur es porque se va registrar un FUR. CUS04
         this.furEvaluacion.cfurCodtipo = 'FUR';
      
         this.formularioCmp.denunciaContenedorC.entidadHechoC.setEntidadHecho(this.furEvaluacion);
         this.formularioCmp.denunciaContenedorC.hechoIrregularC.setHechoIrregularComponent(this.furEvaluacion);
         this.formularioCmp.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.setFuncionarioComponent(this.furEvaluacion);
         this.formularioCmp.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.setEmpresaComponent(this.furEvaluacion);
         this.formularioCmp.denunciaContenedorC.pruebaHechoC.setPruebaHechoComponent(this.furEvaluacion);
         this.formularioCmp.denunciaContenedorC.setDenunciaContenedorC(this.furEvaluacion);
         this.formularioCmp.denunciaContenedorC.instanciaC.setInstanciaC(this.furEvaluacion);
         
           //Tab denunciante
           setTimeout(()=>{
               if(this.formularioCmp.denunciaContenedorC.requireIdentificarse){

                 this.formularioCmp.contenedorDenuncianteC.setContenedorDenuncianteC(this.furEvaluacion);
                   //this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
         
 
               //Tab medida de proteccion
               if(this.formularioCmp.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec == "SI"){
               
                 setTimeout(()=>{
                   //this.formularioCmp.contenedorMpC.medidaProteccionCReadEnable();
                   //Se cambia el valor ya que ahora es fur y además se muestra la sección expedientes
                   
                  // this.fur.cfurCodtipo = 'FUR';

                   this.formularioCmp.contenedorMpC.setMedidaProteccionC(this.furEvaluacion);
                   
                 },0)
               
               }
             }
          
 
           },0)
           
 
           
       });
     
     }
 
   _Retornar()
   {
 
     this.router.navigate(['/PanelPrincipal/',GlobalVars.CodUsuario], { relativeTo: this.route });
 
   }
 
   _Cancelar(){
 
     this._Retornar();
 
   }
     _GuardarFur(){
        let idFur;
       // this.fur = this.formularioCmp.llenarFur(this.fur);
       // this.fur.nfurIdbase = this.idFUrBaseV;
       // console.log(this.fur);
       // this.furService.RegistrarFur(this.fur).subscribe(
       //   data => {
   
       //     // console.log("===========Resultado");
       //     // console.log(data);
       //     // furNew = new Fur();
   
       //     // furNew = data;
   
       //     // //this.furNew = data;
   
       //     // data.fur.nfurId;
   
       //     idFur = data.nfurId;
   
       //     if(idFur > 0)
       //     {
             
       //       openModalMensaje("Formulario Web",`Se generó el formulario número ${idFur}, por favor verifique`,this.dialog);
   
            
       //     }else{
   
       //       openModalMensaje("Formulario Web","Ocurrió un problema en la actualización del registro",this.dialog);
   
       //     }
   
       //     // openModalMensaje("Formulario Web","Se actualizó la información correctamente, por favor verifique.",this.dialog);
   
       //     // window.location.reload();
   
       //     return
           
       //   });
       this.formularioCmp.Guardar(this.idFur,null,null,null);
     }
 
 
     cambioResultado(codigoResultado){
       console.log("Valor del primer rdb: "+ this.flgExisteAntecedente);
       console.log(codigoResultado);
       if(codigoResultado == 1){
         this._habilitarCamposFur();
        
         this._obtenerDatosFur(this.idFur);
       }else{
         this._deshabilitarCamposFur();
         this.furEvaluacion.cfurCodtipo = 'REG';
       }
       
     }
 
     _deshabilitarCamposFur(){
       this.formularioCmp.denunciaContenedorC.entidadHechoC.entidadComponentReadEnable();
       this.formularioCmp.denunciaContenedorC.hechoIrregularC.hechoIrregularCReadEnable();
       this.formularioCmp.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.funcionarioCReadEnable();
       this.formularioCmp.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.empresaCReadEnable();
       this.formularioCmp.denunciaContenedorC.pruebaHechoC.pruebaHechoCReadEnable();
       this.formularioCmp.denunciaContenedorC.denunciaContenedorCReadEnable();
       this.formularioCmp.denunciaContenedorC.instanciaC.instanciaCReadEnable();
       if(this.formularioCmp.denunciaContenedorC.pruebaHechoC.cuenta_pruebas){
         this.formularioCmp.denunciaContenedorC.pruebaHechoC.evidenciaC.evidenciaCReadEnable();
       }
     
         //Tab denunciante
         setTimeout(()=>{
             if(this.formularioCmp.denunciaContenedorC.requireIdentificarse){
               this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
                 //this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
             
 
             //Tab medida de proteccion
             if(this.formularioCmp.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec == "SI"){
               console.log(" :3 : " + this.formularioCmp.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec);
              
               setTimeout(()=>{
                 this.formularioCmp.contenedorMpC.medidaProteccionCReadEnable();
                 //this.formularioCmp.contenedorMpC.medidaProteccionCReadEnable();
                 
               },0)
             
             }
           }
        
 
         },0)
         
     }
 
     _habilitarCamposFur(){
       this.formularioCmp.denunciaContenedorC.entidadHechoC.entidadComponentReadDisable();
       this.formularioCmp.denunciaContenedorC.hechoIrregularC.hechoIrregularCReadDisable();
       this.formularioCmp.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.funcionarioCReadDisable();
       this.formularioCmp.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.empresaCReadDisable();
       this.formularioCmp.denunciaContenedorC.pruebaHechoC.pruebaHechoCReadDisable();
       this.formularioCmp.denunciaContenedorC.denunciaContenedorCReadDisable();
       this.formularioCmp.denunciaContenedorC.instanciaC.instanciaCReadDisable();
       //this.formularioCmp.denunciaContenedorC.pruebaHechoC.evidenciaC.evidenciaCReadDisable();
       if(this.formularioCmp.denunciaContenedorC.pruebaHechoC.cuenta_pruebas){
         this.formularioCmp.denunciaContenedorC.pruebaHechoC.evidenciaC.evidenciaCReadEnable();
       }
         //Tab denunciante
         setTimeout(()=>{
             if(this.formularioCmp.denunciaContenedorC.requireIdentificarse){
               this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadDisable();
                 //this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
             
 
             //Tab medida de proteccion
              if(this.formularioCmp.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec == "SI"){
             
               setTimeout(()=>{
                 //this.formularioCmp.contenedorMpC.medidaProteccionCReadEnable();
                 this.formularioCmp.contenedorMpC.medidaProteccionCReadDisable();
               },0)
             
             }
           }
        
 
         },0)
     }
 }
 