import { Component, OnInit, ViewChild, ViewChildren, Output, EventEmitter } from '@angular/core';
import { DgHojaEvalComponent } from '../datos-general/dg-hoja-eval/dg-hoja-eval.component';
import { FurService } from 'src/app/services/fur.service';
import { AntecedentesFurComponent } from './antecedentes-fur/antecedentes-fur.component';
import { ResAnalisisService } from 'src/app/services/res-analisis.service';
import { ResAnalisis } from 'src/app/models/ResAnalisis';
import { HojaEvaluacion } from 'src/app/models/HojaEvaluacion';
import { HojaEvaluacionService } from 'src/app/services/hoja-evaluacion.service';
import { EvidenciasComponent } from '../denuncia/evidencias/evidencias.component';
import { ErrorValidator } from 'src/app/models/personalizados/ErrorValidator';
//import { MfuncErrorVal} from 'src/app/general/variables';
import { ContenedorWebComponent } from '../formulario-web/contenedor-web/contenedor-web.component';
//import { MfuncErrorVal, GlobalVars, openModalMensaje, NumIdEstadoAtendido, GetDateTime} from 'src/app/general/variables';
//import { ActivatedRoute, Router } from '@angular/router';
import { Fur } from 'src/app/models/Fur';
import { MfuncErrorVal, GlobalVars, openModalMensaje, NumIdEstadoAtendido, GetDateTimeString, GetDateTime, NumIdEstadoHeSuperv, NumIdEstadoHeEvaluac, CodTipoFichaHe, NumIdEstadoHeEvaluacionSuperv, NumIdEstadoFudPendiente, NumIdEstadoHeEvaluacionObservado, iconEliminar, NumResultadoAnalisDenuncia, ipPrueba, DscTituloPaginator} from 'src/app/general/variables';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { PersonalCgrService } from 'src/app/services/personal-cgr.service';
import { PersonalCgr } from 'src/app/models/PersonalCgr';
import { Fud } from 'src/app/models/Fud';
import { FudService } from 'src/app/services/fud.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ObservacionDetalle } from 'src/app/models/observacionDetalle';
import { EvaluacionPreliminar } from 'src/app/models/BPM/evaluacionPreliminar';
import { EvaluacionPreliminarService } from 'src/app/services/bpm/evaluacion-preliminar.service';

@Component({
  selector: 'app-evalinf-regfur',
  templateUrl: './evalinf-regfur.component.html',
  styleUrls: ['./evalinf-regfur.component.css']
})
export class EvalinfRegfurComponent implements OnInit {

  //PROPIEDADES EN VISTA
  numIdResAnalisis:number = null;
  dscResConclus:string = "";
  dscResRecomen:string = "";
  flgMostrarTabFur = false;
  
  strCodSupervDer:string = "";
  strDscComentarios:string = "";
  flgDeshabilitado = true;
  //PROPIEDADES EN VISTA

  idFur:number;
  listSupervisor:PersonalCgr[] = [];

  @ViewChild(DgHojaEvalComponent) dgHojaEvalC : DgHojaEvalComponent;
  @ViewChild(AntecedentesFurComponent) antecedentesFurC : AntecedentesFurComponent;
  @ViewChild(EvidenciasComponent) evidenciasC : EvidenciasComponent;
  
  @ViewChild(ContenedorWebComponent) formularioCmp : ContenedorWebComponent;

  @Output() opRetornaEvaluar = new EventEmitter();
  //@ViewChildren(FormularioWebComponent) formularioCmp : FormularioWebComponent;
  
  listResAnalisis: ResAnalisis[] = [];
  hojaEvaluacion : HojaEvaluacion = new HojaEvaluacion();
  errorValidator: ErrorValidator[] = [];
  mFuncErrorVal : MfuncErrorVal = new MfuncErrorVal();
  fur : Fur = new Fur();
  idFUrBaseV : number ;


  /*
  CUS06
  */
  flgSegundaEval : boolean = false;
  furEvaluacion : Fur = new Fur();
  flgExisteAntecedente : string = "";
  flgTipoAntecedente : string = "";
  flgInformacionComplementaria : string = "";
  flgTipoInformacion:string = "";
  flgTipoDenuncia : string = "";
  fud : Fud = new Fud();
  flgEsFudEval : boolean = false;
  
  /*
  */
  /**CUS 07 */
  flgLecturaFud : boolean = false;
  observacion : string = "";
  hojaEvaluacionSegundaEval : HojaEvaluacion = new HojaEvaluacion();
  matTableColumnsDef = ["observacion","estado","acciones"];
  dataSource: MatTableDataSource<ObservacionDetalle>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  listaObservaciones : ObservacionDetalle[] = [];
  estadoFurEvaluacion : number;
    //Icons
    iconoEliminar = iconEliminar;
  /* */
  constructor(protected furService: FurService,
    protected resAnalisisService: ResAnalisisService,
    protected hojaEvaluacionService: HojaEvaluacionService,
    public router:Router,
    private route: ActivatedRoute,
    protected serviceFur : FurService,
    public dialog: MatDialog,
    protected personalCgrService : PersonalCgrService,
    protected fudService : FudService,
    protected evaluacionPreliminarService : EvaluacionPreliminarService) 

   { 

    
    }

  ngOnInit() {

    //this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;

    // this.idFur = this.route.snapshot.params.idFur;
    
     
    // setTimeout(() => {

    //   //console.log("============recuperar13");

    //   this.dgHojaEvalC.DatosDefault();
      
    //   this._obtenerDatosFur(this.idFur); // TAMBIEN RECUPERA DATOS DE HOJA EVAL
      

    //   this.antecedentesFurC.RecuperarAntecedentes(this.idFur); // temporal

    //   this.resAnalisisService.ListarResultadoAnalisis().subscribe(data=>{
  
    //     this.listResAnalisis = data;
    //     //Espera que los datos se llenen para deshabilitar los campos
    //     this._deshabilitarCamposFur();
    //   });
      
      
      
    // }, 5);

    // this.personalCgrService.ListarSupervisorCgrActivo().subscribe(data=>{
  
    //   this.listSupervisor = data;
    //   //Espera que los datos se llenen para deshabilitar los campos
      
    // });

  }

  mostrarRegistro(idFur:number){

    this.idFur = idFur;//this.route.snapshot.params.idFur;
         
    setTimeout(() => {

      //console.log("============recuperar13");

      this.dgHojaEvalC.DatosDefault();
      
      this._obtenerDatosFur(this.idFur); // TAMBIEN RECUPERA DATOS DE HOJA EVAL
      

      this.antecedentesFurC.RecuperarAntecedentes(this.idFur); // temporal

      this.resAnalisisService.ListarResultadoAnalisis().subscribe(data=>{
  
        this.listResAnalisis = data;
        //Espera que los datos se llenen para deshabilitar los campos
        this._deshabilitarCamposFur();
      });
      
      
      
    }, 5);

    this.personalCgrService.ListarSupervisorCgrActivo().subscribe(data=>{
  
      this.listSupervisor = data;
      //Espera que los datos se llenen para deshabilitar los campos
      
    });

  }

  _setDatosHe(boolFlgSuperv:boolean){

    // console.log("=========this.numIdResAnalisis");
    // console.log(this.numIdResAnalisis);

    var furAntec = this.antecedentesFurC.listFormWeb[0];

    this.hojaEvaluacion.strDhojaevalFregini= this.dgHojaEvalC.fchHrInicioReg;
    this.hojaEvaluacion.nhojaevalIdfur = furAntec.nfurId;
    this.hojaEvaluacion.nhojaevalIdresanalisis = this.numIdResAnalisis;
    this.hojaEvaluacion.chojaevalDscconclusion1 = this.dscResConclus;
    this.hojaEvaluacion.chojaevalDscconclusion2 = this.dscResRecomen;
    //this.hojaEvaluacion.listaDocumentoDetalle = this.evidenciasC.listEvidencias;
    this.hojaEvaluacion.chojaevalCodsnc = GlobalVars.CodSNC;
    this.hojaEvaluacion.chojaevalComentarios = this.strDscComentarios;
    this.hojaEvaluacion.chojaevalCodsupervder = this.strCodSupervDer;
    this.hojaEvaluacion.chojaevalCodusureg = GlobalVars.CodPersonal;

    // console.log("================0this.strDscComentarios");
    // console.log(this.strDscComentarios);
    
    if(boolFlgSuperv)
    {

      this.hojaEvaluacion.strDhojaevalFregistro = GetDateTime();
      this.hojaEvaluacion.nhojaevalIdestado = NumIdEstadoHeSuperv;
      furAntec.nfurIdestado = NumIdEstadoAtendido;
      
    }else{

      this.hojaEvaluacion.nhojaevalIdestado = NumIdEstadoHeEvaluac;
    }
    /**Cus06 */
    this.hojaEvaluacion.chojaevalFlgexisantec = this.flgExisteAntecedente;
    this.hojaEvaluacion.chojaevalFlgtipoantec = this.flgTipoAntecedente;
    this.hojaEvaluacion.chojaevalTipinform = this.flgTipoInformacion;
    this.hojaEvaluacion.chojaevalTipodenun = this.flgTipoDenuncia;
    this.hojaEvaluacion.chojaevalFlginfcompl = this.flgInformacionComplementaria;

    return furAntec;

  }

  _Guardar()
  {

    
    var vIdHoja;
    //var furAntec = this._setDatosHe(false);

    this._setDatosHe(false);

    // this.hojaEvaluacion.strDhojaevalFregini= this.dgHojaEvalC.fchHrInicioReg;
    // //this.hojaEvaluacion.strDhojaevalFregistro = GetDateTime();
    // this.hojaEvaluacion.nhojaevalIdfur = this.antecedentesFurC.listFormWeb[0].nfurId;
    // this.hojaEvaluacion.nhojaevalIdresanalisis = this.numIdResAnalisis;
    // this.hojaEvaluacion.chojaevalDscconclusion1 = this.dscResConclus;
    // this.hojaEvaluacion.chojaevalDscconclusion2 = this.dscResRecomen;

    // this.hojaEvaluacion.listaDocumentoAdjuntoDet = this.evidenciasC.listEvidencias;

    // //fur.nfurIdestado = NumIdEstadoAtendido;

    console.log("==============this.hojaEvaluacion");
    console.log(this.hojaEvaluacion);

    if(this.hojaEvaluacion.nhojaevalId > 0)
    {

      this.hojaEvaluacionService.ActualizarHojaEvaluacion(this.hojaEvaluacion).subscribe(data=>{

        vIdHoja = data.nhojaevalId;
  
        if(vIdHoja > 0)
        {

          this.evidenciasC.procesarLaserFiche(vIdHoja).subscribe(data=>{

                
            openModalMensaje("Hoja Evaluación",`Se registró correctamente la hoja de evaluación N° ${vIdHoja}, por favor verifique`,
              this.dialog).afterClosed().subscribe(result=>{
                //this._GuardarFur();
                this._Retornar();
      
              });


          });
  
          // openModalMensaje("Hoja Evaluación",`Se registró correctamente la hoja de evaluación N° ${vIdHoja}, por favor verifique`,
          //     this.dialog).afterClosed().subscribe(result=>{
          //       //this._GuardarFur();
          //       this._Retornar();
      
          //     });
  
        }else{
  
          openModalMensaje("Asignar Información","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{
  
            this._Retornar();
  
          });
  
          //flgGuardado = false;
  
          return;
  
        }
  
      });

    }else{

      this.hojaEvaluacionService.RegistrarHojaEvaluacion(this.hojaEvaluacion).subscribe(data=>{

        vIdHoja = data.nhojaevalId;

        console.log("============vIdHoja");
        console.log(vIdHoja);

  
        if(vIdHoja > 0)
        {

          this.evidenciasC.procesarLaserFiche(vIdHoja).subscribe(data=>{

                
            openModalMensaje("Hoja Evaluación",`Se registró correctamente la hoja de evaluación N° ${vIdHoja}, por favor verifique`,
            this.dialog).afterClosed().subscribe(result=>{
              //this._GuardarFur();
              this._Retornar();
    
            });


          });
  
          // openModalMensaje("Hoja Evaluación",`Se registró correctamente la hoja de evaluación N° ${vIdHoja}, por favor verifique`,
          //     this.dialog).afterClosed().subscribe(result=>{
          //       //this._GuardarFur();
          //       this._Retornar();
      
          //     });
  
        }else{
  
          openModalMensaje("Asignar Información","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{
  
            this._Retornar();
  
          });
  
          //flgGuardado = false;
  
          return;
  
        }
  
      });
      
    }
        
    

  }

  // _supervisorCus3()
  // {

  //   //var fur = this.antecedentesFurC.listFormWeb[0];
  //   var furAntec;
  //   var vIdHoja;

  //   this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.numIdResAnalisis != null ? false:true), "Ingrese el resultado del análisis",this.errorValidator,true);
  //   this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.dscResConclus.length > 0 ? false:true), "Ingrese la conclusión",this.errorValidator,false);
  //   this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.dscResRecomen.length > 0 ? false:true), "Ingrese la recomendación",this.errorValidator,false);
  //   this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.strCodSupervDer.length > 0 ? false:true), "Ingrese al supervisor",this.errorValidator,false);
  //   this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.strDscComentarios.length > 0 ? false:true), "Ingrese los comentarios",this.errorValidator,false);
    
  //   //  console.log("==========antes de guardar");
  //   //  console.log(this.mFuncErrorVal);
  //   //console.log(this.errorValidator);

  //   //if(this.errorValidator.filter(x => x.flgInvalid).length > 0)
  //   if(this.mFuncErrorVal.RevisarErrorValidator(this.errorValidator)){return;}

  //   //console.log("a grabar");

  //   furAntec = this._setDatosHe(true);

  //   // this.hojaEvaluacion.strDhojaevalFregini = this.dgHojaEvalC.fchHrInicioReg;
  //   // this.hojaEvaluacion.strDhojaevalFregistro = GetDateTime();
  //   // this.hojaEvaluacion.nhojaevalIdfur = fur.nfurId;
  //   // this.hojaEvaluacion.nhojaevalIdresanalisis = this.numIdResAnalisis;
  //   // this.hojaEvaluacion.chojaevalDscconclusion1 = this.dscResConclus;
  //   // this.hojaEvaluacion.chojaevalDscconclusion2 = this.dscResRecomen;

  //   // this.hojaEvaluacion.listaDocumentoAdjuntoDet = this.evidenciasC.listEvidencias;

  //   // fur.nfurIdestado = NumIdEstadoAtendido;
    


  //   console.log("==============this.hojaEvaluacion");
  //   console.log(this.hojaEvaluacion);
  //   // if(this.codResAnalisis == "1"){
  //   //  // this.formularioCmp.Guardar(this.idFur);
  //   // }
    
  //   this.hojaEvaluacionService.RegistrarHojaEvaluacion(this.hojaEvaluacion).subscribe(data=>{

  //     vIdHoja = data.nhojaevalId;

  //     if(vIdHoja > 0)
  //     {

  //       this.furService.ActualizarFur(furAntec).subscribe(data=>{

          
  //         if(data.nfurId > 0)
  //         {
  //           //
  //           openModalMensaje("Hoja Evaluación",`Se registró correctamente la hoja de evaluación N° ${vIdHoja}, por favor verifique`,
  //           this.dialog).afterClosed().subscribe(result=>{
  //             //this._GuardarFur();
  //             this._Retornar();
    
  //           });

  //         }else
  //         {
  //           openModalMensaje("Asignar Información","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{

  //             this._Retornar();
    
  //           });
    
  //           //flgGuardado = false;
    
  //           return;
  //         }

          

  //       });

  //     }

  //   });

  // }

  _supervisor()
  {

    var furAntec;
    var vIdHoja;

    console.log("==============valiacion");

    console.log(this.dscResConclus);
    console.log(this.dscResRecomen);
    console.log(this.strCodSupervDer);
    console.log(this.strDscComentarios);

    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.numIdResAnalisis != null ? false:true), "Ingrese el resultado del análisis",this.errorValidator,true);
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.dscResConclus.length > 0 ? false:true), "Ingrese la conclusión",this.errorValidator,false);
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.dscResRecomen.length > 0 ? false:true), "Ingrese la recomendación",this.errorValidator,false);
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.strCodSupervDer.length > 0 ? false:true), "Ingrese al supervisor",this.errorValidator,false);
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.strDscComentarios.length > 0 ? false:true), "Ingrese los comentarios",this.errorValidator,false);

    console.log("==============valiacion11111");
    
    //  console.log("==========antes de guardar");
    //  console.log(this.mFuncErrorVal);
    //console.log(this.errorValidator);

    //if(this.errorValidator.filter(x => x.flgInvalid).length > 0)
    if(this.mFuncErrorVal.RevisarErrorValidator(this.errorValidator)){return;}

    //console.log("a grabar");
    furAntec = this._setDatosHe(true);
   /*  this.hojaEvaluacion.strDhojaevalFregini = this.dgHojaEvalC.fchHrInicioReg;
    this.hojaEvaluacion.strDhojaevalFregistro = GetDateTime();
    this.hojaEvaluacion.nhojaevalIdfur = fur.nfurId;
    this.hojaEvaluacion.nhojaevalIdresanalisis = this.numIdResAnalisis;
    this.hojaEvaluacion.chojaevalDscconclusion1 = this.dscResConclus;
    this.hojaEvaluacion.chojaevalDscconclusion2 = this.dscResRecomen;

    this.hojaEvaluacion.listaDocumentoDetalle = this.evidenciasC.listEvidencias;

    fur.nfurIdestado = NumIdEstadoAtendido; */
    //this.hojaEvaluacion.nhojaevalIdresanalisis = this.formHojaEvaluacion.controls.select_resanali.;


    console.log("==============this.hojaEvaluacion");
    console.log(this.hojaEvaluacion);
    // if(this.codResAnalisis == "1"){
    //  // this.formularioCmp.Guardar(this.idFur);
    // }
    console.log("Segunda evaluación: " + this.flgSegundaEval);
    if(this.flgSegundaEval){
        if(this.hojaEvaluacion.nhojaevalId != 0){
          this.furEvaluacion.hojaEvaluacion = this.hojaEvaluacion;
          /*this.llenarDatosFUD(this.furEvaluacion);
          this.fudService.RegistrarFud(this.fud).subscribe(data =>{
              console.log(data);
          });*/
          console.log("Rgistro de hoja de evluación II");
         console.log(this.hojaEvaluacion);
         console.log(this.furEvaluacion);

         if(this.estadoFurEvaluacion == NumIdEstadoHeEvaluacionObservado){
          this._devolverObservacionASupervisor();
         }else{
           if(this.estadoFurEvaluacion != NumIdEstadoHeEvaluacionSuperv){
            this.estadoFurEvaluacion = NumIdEstadoHeEvaluacionSuperv;
            this._enviarFurASupervisor();
           }
         }


        
         

 
        }
    }else{

      this.hojaEvaluacionService.RegistrarHojaEvaluacion(this.hojaEvaluacion).subscribe(data=>{
  
        vIdHoja = data.nhojaevalId;
  
        if(vIdHoja > 0)
        {
  
          this.furService.ActualizarFur(furAntec).subscribe(data=>{
  
            
            if(data.nfurId > 0)
            {
              //
              openModalMensaje("Hoja Evaluación",`Se registró correctamente la hoja de evaluación N° ${vIdHoja}, por favor verifique`,
              this.dialog).afterClosed().subscribe(result=>{
              
                this._GuardarFur(this.idFur,null,null,null);
                setTimeout(()=>{
                this._Retornar();
                },5);
              });
  
            }else
            {
              openModalMensaje("Asignar Información","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{
  
                this._Retornar();
      
              });
      
              //flgGuardado = false;
      
              return;
            }
  
            
  
          });
  
        }
  
      });

    }

  }

  _devolverObservacionASupervisor(){
    this.furEvaluacion = this.formularioCmp.actualizarFur(this.furEvaluacion);
    this.hojaEvaluacionSegundaEval.listaObservacionDetalle.forEach(observacion =>{
        observacion.cobservacionFlgactivo = 'NO';
    });
    setTimeout(()=>{
      this.furEvaluacion.nfurIdestado = NumIdEstadoHeEvaluacionSuperv;
      this.furService.ActualizarFur(this.furEvaluacion).subscribe(data=>{
        console.log("ACTUALIZAR FUR");
        console.log(data);
          this.hojaEvaluacionService.ActualizarHojaEvaluacion(this.hojaEvaluacionSegundaEval).subscribe(data1=>{
            console.log("ACTUALIZAR HOJA EVAL");
              console.log(data1);
          });
      });
    
    },5);
   
    //console.log(this.furEvaluacion);
    
    console.log(this.furEvaluacion);

    
  }
  _enviarFurASupervisor() {
    let furAntec;
    furAntec = this._setDatosHe(true);
    this.hojaEvaluacionService.RegistrarHojaEvaluacion(this.hojaEvaluacion).subscribe(data=>{
      let vIdHoja : number;
      vIdHoja = data.nhojaevalId;

      if(vIdHoja > 0)
      {

        this.furService.ActualizarFur(furAntec).subscribe(data=>{

          
          if(data.nfurId > 0)
          {
            //
            openModalMensaje("Hoja Evaluación",`Se registró correctamente la hoja de evaluación N° ${vIdHoja}, por favor verifique`,
            this.dialog).afterClosed().subscribe(result=>{
            
              this._GuardarFur(this.furEvaluacion.nfurId,this.strCodSupervDer,GlobalVars.CodPersonal,this.estadoFurEvaluacion);
              setTimeout(()=>{
              this._Retornar();
              },5);
            });

          }else
          {
            openModalMensaje("Asignar Información","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{

              this._Retornar();
    
            });
    
            //flgGuardado = false;
    
            return;
          }

          

        });

      }

    }); 
  }

  
    _obtenerDatosFur(idFur : number){
      console.log("ID FUR: " + idFur);
      var listEvidenciaHe = null;
      var idHe = null;

      this.idFUrBaseV = idFur;

      this.serviceFur.ObtenerFurPorId(idFur).subscribe(data =>{//181 //179
        //Llenar los datos del fur CUS 06 / 07
        this.furEvaluacion = data;
        if(this.furEvaluacion.cfurCodtipo == 'FUR'){
          
          this.flgSegundaEval = true;
          this.formularioCmp.flgEsFud = true;
          
          if(this.furEvaluacion.nfurIdestado == NumIdEstadoHeEvaluacionSuperv){
              this.flgEsFudEval = true;
              this._llenarDatosHojaEval(this.furEvaluacion.nfurIdbase);
              this._deshabilitarCamposHojaEval();
          }

          if(this.furEvaluacion.nfurIdestado == NumIdEstadoHeEvaluacionObservado){
            this._llenarDatosHojaEval(this.furEvaluacion.nfurIdbase);
          }

        }else{
          this.flgSegundaEval = false;
          this.formularioCmp.flgEsFud = false;
        }
        //

        console.log("================data.hojaEvaluacion");
        console.log(data.hojaEvaluacion);


        if(data.hojaEvaluacion != null)
        {

          idHe = data.hojaEvaluacion.nhojaevalId;
          listEvidenciaHe = data.hojaEvaluacion.listaDocumentoDetalle;

          this.hojaEvaluacion = data.hojaEvaluacion;

          //this.dgHojaEvalC.fchHrInicioReg = data.hojaEvaluacion.strDhojaevalFregini;
          this.numIdResAnalisis = data.hojaEvaluacion.nhojaevalIdresanalisis;
          this.dscResConclus = data.hojaEvaluacion.chojaevalDscconclusion1;
          this.dscResRecomen = data.hojaEvaluacion.chojaevalDscconclusion2;
          this.strDscComentarios = data.hojaEvaluacion.chojaevalComentarios;
          this.strCodSupervDer = this.hojaEvaluacion.chojaevalCodsupervder;

          if(listEvidenciaHe != null){this.evidenciasC.setEvidenciaGeneral(listEvidenciaHe);}

          this.dgHojaEvalC.setDatos(data.hojaEvaluacion);

        }

        this.evidenciasC.setFichaDocumento(CodTipoFichaHe, CodTipoFichaHe,idHe);

        this.formularioCmp.denunciaContenedorC.entidadHechoC.setEntidadHecho(data);
        this.formularioCmp.denunciaContenedorC.hechoIrregularC.setHechoIrregularComponent(data);
        this.formularioCmp.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.setFuncionarioComponent(data);
        this.formularioCmp.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.setEmpresaComponent(data);
        this.formularioCmp.denunciaContenedorC.pruebaHechoC.setPruebaHechoComponent(data);
        this.formularioCmp.denunciaContenedorC.setDenunciaContenedorC(data);
        this.formularioCmp.denunciaContenedorC.instanciaC.setInstanciaC(data);
        
        if(!this.formularioCmp.flgEsFud){ //CUS 05 - 06 . 07 Si es un fud solo se debe visualizar la pestaña denuncia
          //Tab denunciante
          setTimeout(()=>{
              if(this.formularioCmp.denunciaContenedorC.requireIdentificarse){
                this.formularioCmp.contenedorDenuncianteC.setContenedorDenuncianteC(data);
                  //this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
              

              //Tab medida de proteccion
              if(this.formularioCmp.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec == "SI"){
              
                setTimeout(()=>{
                  //this.formularioCmp.contenedorMpC.medidaProteccionCReadEnable();
                  //Se cambia el valor ya que ahora es fur y además se muestra la sección expedientes
                  
                  this.fur.cfurCodtipo = 'FUR';
                  this.formularioCmp.contenedorMpC.setMedidaProteccionC(data);
                  if(this.fur.cfurCodtipo == 'FUR'){
                      this.formularioCmp.contenedorMpC.flgEsFur = true;
                  }
                },0)
              
              }
            }
         

          },0)

        }
        
        let codigoGenerado : string = "";
        codigoGenerado = this.formularioCmp.generarCodigoFur(this.furEvaluacion); 
        this.formularioCmp.actualizarCodigo(codigoGenerado);
      });
    
    }

  _Retornar()
  {

    this.opRetornaEvaluar.emit();

    //this.router.navigate(['/PanelPrincipal/',GlobalVars.CodUsuario], { relativeTo: this.route });

  }

  _Cancelar(){

    this._Retornar();

  }
    _GuardarFur(vIdFur : number,vUsupervisor : string, vUsuAsignado, estadoFurEval : number){
       let idFur;
      // this.fur = this.formularioCmp.llenarFur(this.fur);
      // this.fur.nfurIdbase = this.idFUrBaseV;
      // console.log(this.fur);
      // this.furService.RegistrarFur(this.fur).subscribe(
      //   data => {
  
      //     // console.log("===========Resultado");
      //     // console.log(data);
      //     // furNew = new Fur();
  
      //     // furNew = data;
  
      //     // //this.furNew = data;
  
      //     // data.fur.nfurId;
  
      //     idFur = data.nfurId;
  
      //     if(idFur > 0)
      //     {
            
      //       openModalMensaje("Formulario Web",`Se generó el formulario número ${idFur}, por favor verifique`,this.dialog);
  
           
      //     }else{
  
      //       openModalMensaje("Formulario Web","Ocurrió un problema en la actualización del registro",this.dialog);
  
      //     }
  
      //     // openModalMensaje("Formulario Web","Se actualizó la información correctamente, por favor verifique.",this.dialog);
  
      //     // window.location.reload();
  
      //     return
          
      //   });
      this.formularioCmp.Guardar(vIdFur,vUsupervisor,vUsuAsignado, estadoFurEval);
    }


    cambioResultado(codigoResultado)
    {

      //console.log(codigoResultado);

      if(codigoResultado == 1){

        this._habilitarCamposFur();

      }else{

        this._deshabilitarCamposFur();

      }

      console.log("==============this.listResAnalisis");
      console.log(this.listResAnalisis);
      console.log(codigoResultado);

      var itemResAnalisis = this.listResAnalisis.filter(x => x.nresanalisisId == codigoResultado)[0];

      this.dscResConclus = itemResAnalisis.cresanalisisDscconc1;
      this.dscResRecomen = itemResAnalisis.cresanalisisDscconc2;
      
    }

    _deshabilitarCamposFur(){

      //console.log("==============_deshabilitarCamposFur");
      
      this.formularioCmp.denunciaContenedorC.entidadHechoC.entidadComponentReadEnable();
      this.formularioCmp.denunciaContenedorC.hechoIrregularC.hechoIrregularCReadEnable();
      this.formularioCmp.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.funcionarioCReadEnable();
      this.formularioCmp.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.empresaCReadEnable();
      this.formularioCmp.denunciaContenedorC.pruebaHechoC.pruebaHechoCReadEnable();
      this.formularioCmp.denunciaContenedorC.denunciaContenedorCReadEnable();
      this.formularioCmp.denunciaContenedorC.instanciaC.instanciaCReadEnable();
      if(this.formularioCmp.denunciaContenedorC.pruebaHechoC.cuenta_pruebas){
        this.formularioCmp.denunciaContenedorC.pruebaHechoC.evidenciaC.evidenciaCReadEnable();
      }
    
        //Tab denunciante
        setTimeout(()=>{
            if(this.formularioCmp.denunciaContenedorC.requireIdentificarse){
              this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
                //this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
            

            //Tab medida de proteccion
            if(this.formularioCmp.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec == "SI"){
              console.log(" :3 : " + this.formularioCmp.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec);
             
              setTimeout(()=>{
                this.formularioCmp.contenedorMpC.medidaProteccionCReadEnable();
                //this.formularioCmp.contenedorMpC.medidaProteccionCReadEnable();
                
              },0)
            
            }
          }
       

        },0)
        
    }

    _habilitarCamposFur(){
      this.formularioCmp.denunciaContenedorC.entidadHechoC.entidadComponentReadDisable();
      this.formularioCmp.denunciaContenedorC.hechoIrregularC.hechoIrregularCReadDisable();
      this.formularioCmp.denunciaContenedorC.hechoIrregularC.funcionarioInvolucradoC.funcionarioCReadDisable();
      this.formularioCmp.denunciaContenedorC.hechoIrregularC.empresaInvolucradaC.empresaCReadDisable();
      this.formularioCmp.denunciaContenedorC.pruebaHechoC.pruebaHechoCReadDisable();
      this.formularioCmp.denunciaContenedorC.denunciaContenedorCReadDisable();
      this.formularioCmp.denunciaContenedorC.instanciaC.instanciaCReadDisable();
      //this.formularioCmp.denunciaContenedorC.pruebaHechoC.evidenciaC.evidenciaCReadDisable();
      if(this.formularioCmp.denunciaContenedorC.pruebaHechoC.cuenta_pruebas){
        this.formularioCmp.denunciaContenedorC.pruebaHechoC.evidenciaC.evidenciaCReadEnable();
      }
      //CUS 05 - 07.
      if(!this.formularioCmp.flgEsFud){
        //Tab denunciante
        setTimeout(()=>{
            if(this.formularioCmp.denunciaContenedorC.requireIdentificarse){
              this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadDisable();
                //this.formularioCmp.contenedorDenuncianteC.contenedorDenuncianteReadEnable();
            

            //Tab medida de proteccion
             if(this.formularioCmp.contenedorDenuncianteC.denunciante.cdenuncianteFlgmedidaprotec == "SI"){
            
              setTimeout(()=>{
                //this.formularioCmp.contenedorMpC.medidaProteccionCReadEnable();
                this.formularioCmp.contenedorMpC.medidaProteccionCReadDisable();
              },0)
            
            }
          }
       

        },0)

      }else{

      }
    }


    _concluir(){
      alert('Concluir.');
    }

    /*CUS06 */


    asignarFlgExistente(evento){
      
      this.flgExisteAntecedente = evento.toElement.value;
      console.log("Flg existente: " + this.flgExisteAntecedente);
    }

    asignarFlgTipoAntecedente(evento){
      this.flgTipoAntecedente = evento.toElement.value;
      console.log("Flg antecedente: " + this.flgTipoInformacion);
    }

    asignarFlgInformComplementaria(evento){
      this.flgInformacionComplementaria = evento.toElement.value;
      console.log("Flg información complementaria: " + this.flgInformacionComplementaria);
    }

    asignarTipoInformacion(evento){
      this.flgTipoInformacion = evento.toElement.value;
      console.log("flg tipo información: " + this.flgTipoInformacion);
    }

    asignarTipoDenuncia(evento){
      this.flgTipoDenuncia = evento.toElement.value;
    }


    llenarDatosFUD(furEvaluacion : Fur){

      this.fud.nfudIdfur = furEvaluacion.nfurId;
     
      this.fud.cfudCodtipo = 'FUD';
      this.fud.strDfudFecregistro = GetDateTime();
      this.fud.nfudIdestado = NumIdEstadoFudPendiente ;
      


    }
    /*CUS 07 */

    _llenarDatosHojaEval(idFurAsignHojaEval : number){
      this.serviceFur.ObtenerFurPorId(idFurAsignHojaEval).subscribe(data =>{
        console.log("Hoja de evaluación");  
        console.log(data);
        this.hojaEvaluacionSegundaEval = data.hojaEvaluacion;
        this.estadoFurEvaluacion = this.furEvaluacion.nfurIdestado;
        if(this.hojaEvaluacionSegundaEval != null){
          this._llenarDatosVistaHojaEval(this.hojaEvaluacionSegundaEval);
          
        }
      });
    }
  _deshabilitarCamposHojaEval() {
    this.flgLecturaFud = true;
    //throw new Error("Method not implemented.");
  }
  _llenarDatosVistaHojaEval(hojaEval : HojaEvaluacion) {
     if(hojaEval.chojaevalFlgexisantec != null){
       this.flgExisteAntecedente = hojaEval.chojaevalFlgexisantec;
     }
     if(hojaEval.chojaevalFlgtipoantec != null){
        this.flgTipoAntecedente = hojaEval.chojaevalFlgtipoantec;
     }

     if(hojaEval.chojaevalTipodenun != null){
        this.flgTipoDenuncia = hojaEval.chojaevalTipodenun;
     }

     if(hojaEval.chojaevalFlginfcompl != null){
        this.flgInformacionComplementaria = hojaEval.chojaevalFlginfcompl;
     }

     if(hojaEval.chojaevalTipinform != null){
        this.flgTipoInformacion = hojaEval.chojaevalTipinform;
     }

      this.numIdResAnalisis = hojaEval.nhojaevalIdresanalisis;
      this.dscResConclus = hojaEval.chojaevalDscconclusion1;
      this.dscResRecomen = hojaEval.chojaevalDscconclusion2;
      
      this.strDscComentarios = hojaEval.chojaevalComentarios;
      this.strCodSupervDer = hojaEval.chojaevalCodsupervder;
      
      if(this.hojaEvaluacionSegundaEval.listaObservacionDetalle.length > 0){
        this.listaObservaciones = this.hojaEvaluacionSegundaEval.listaObservacionDetalle;
        this._cargarTableMaterial();
      }
      this._validacionesFud();
    //throw new Error("Method not implemented.");
  }

  _agregarComentario(){
      if(this.observacion != null && this.observacion.length >0){

        let observacion = new ObservacionDetalle();
        observacion.cobservacionCodtipo = 'HEV';
        observacion.cobservacionDsc = this.observacion;
        observacion.nobservacionIdtipo = this.hojaEvaluacionSegundaEval.nhojaevalId;
        this.listaObservaciones.push(observacion);
        this._cargarTableMaterial();
        this.observacion = "";
      }
  }

  _cargarTableMaterial(){
    setTimeout(() => {

      this.dataSource = new MatTableDataSource(this.listaObservaciones);
      this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      
    }, 1);
    
  }

  _eliminar(observacion){
    this.listaObservaciones.splice(this.listaObservaciones.indexOf(observacion),1);
    this._cargarTableMaterial();
  }

  _aprobar(){
    this.llenarDatosFUD(this.furEvaluacion);
    this.furEvaluacion.nfurIdestado = NumIdEstadoAtendido;

    this.furService.ActualizarFur(this.furEvaluacion).subscribe(data =>{
        
      openModalMensaje("Hoja Evaluación",`Se actualizó la hoja de evaluación del fud N° ${this.furEvaluacion.nfurIdbase}`,this.dialog).afterClosed().subscribe(result=>{
       
        //this._Retornar();
      });
    });

    this.fudService.RegistrarFud(this.fud).subscribe(data =>{
      let idFud;
      if(data.nfudId != null){
        idFud =  data.nfudId;

          if(data != null){
            let evaluacion : EvaluacionPreliminar = new EvaluacionPreliminar();
            //NO SEGMENTA EN LA BPM POR DEPART
            //FALTA INTEGRACIÓN FIRMA
            //NUEVOS CAMBIOS NO CONTEMPLADOS DE VERONICA
            
            
            evaluacion.bpmIdFud = data.nfudId;
            evaluacion.ipservidor = ipPrueba;
            this.evaluacionPreliminarService.inciarProcesoEvaluacionPreliminar(evaluacion).subscribe(dataEval =>{
                console.log(dataEval);
                if(data != null){
                  openModalMensaje("Evaluación FUR",`Se registró correctamente la hoja de evaluación N° ${idFud}, por favor verifique`,this.dialog).afterClosed().subscribe(result=>{
                  this._Retornar();
                  });
                }
              
            });
          }
          //this._GuardarFur();
          //this._Retornar();

      }else{
        openModalMensaje("Evaluación FUR","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{
  
         // this._Retornar();

        });
      }
    }); 
  }

  _observar(){
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.numIdResAnalisis != null ? false:true), "Ingrese el resultado del análisis",this.errorValidator,true);
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.dscResConclus.length > 0 ? false:true), "Ingrese la conclusión",this.errorValidator,false);
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.dscResRecomen.length > 0 ? false:true), "Ingrese la recomendación",this.errorValidator,false);
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.strCodSupervDer.length > 0 ? false:true), "Ingrese al supervisor",this.errorValidator,false);
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.strDscComentarios.length > 0 ? false:true), "Ingrese los comentarios",this.errorValidator,false);
    
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.listaObservaciones.length > 0 ? false:true), "Ingrese por lo menos una observación",this.errorValidator,false);
    
    this.hojaEvaluacionSegundaEval.listaObservacionDetalle = this.listaObservaciones;
      console.log(this.hojaEvaluacionSegundaEval);
      console.log(this.listaObservaciones);
       this.furEvaluacion.nfurIdestado = NumIdEstadoHeEvaluacionObservado;
      
     /*  this.hojaEvaluacionService.registrarObservaciones(this.listaObservaciones).subscribe(data=>{
        console.log(data);
      }); */

       if(this.listaObservaciones.length > 0){
        this.hojaEvaluacionSegundaEval.listaObservacionDetalle = this.listaObservaciones;
        this.hojaEvaluacionService.ActualizarHojaEvaluacion(this.hojaEvaluacionSegundaEval).subscribe(data =>{
            if (data != null){
              this.furService.ActualizarFur(this.furEvaluacion).subscribe(data =>{
        
                openModalMensaje("Hoja Evaluación",`Se observó la hoja de evaluación del fud N° ${this.furEvaluacion.nfurIdbase}`,this.dialog).afterClosed().subscribe(result=>{
                 
                  //this._Retornar();
                });
              }); 
            }
        });
      }else{
       // this.mFuncErrorVal.RevisarErrorValidator(this.errorValidator);
      }
     

  }

  _validacionesFud(){
    console.log("Validación FUD");
    console.log( this.estadoFurEvaluacion);
    console.log(this.furEvaluacion.nfurIdestado);
     if(this.numIdResAnalisis == NumResultadoAnalisDenuncia && this.furEvaluacion.nfurIdestado != NumIdEstadoHeEvaluacionSuperv){
      console.log("Validación FUD d");
      this._habilitarCamposFur();
     }else{
       console.log("Validación FUD i ");
      this._deshabilitarCamposFur();
       
     }
  }

}
