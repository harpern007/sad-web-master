import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAntecedenteComponent } from './modal-antecedente.component';

describe('ModalAntecedenteComponent', () => {
  let component: ModalAntecedenteComponent;
  let fixture: ComponentFixture<ModalAntecedenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAntecedenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAntecedenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
