import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormularioWebComponent } from '../../formulario-web/formulario-web.component';
import { Fur } from 'src/app/models/Fur';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-antecedente',
  templateUrl: './modal-antecedente.component.html',
  styleUrls: ['./modal-antecedente.component.css']
})
export class ModalAntecedenteComponent implements OnInit {

  @ViewChild(FormularioWebComponent) formularioWebComponent : FormularioWebComponent;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Fur) { }

  ngOnInit()
  {

    console.log("============this.data");
    console.log(this.data);

    setTimeout(() => {

      this.formularioWebComponent.ConsultarFur(this.data);
      
    }, 5);

    

  }

  // MostrarDatosFur(fur:Fur){

    

  // }

}
