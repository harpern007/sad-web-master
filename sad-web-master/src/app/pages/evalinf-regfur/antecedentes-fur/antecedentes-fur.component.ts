import { Component, OnInit } from '@angular/core';
import { FurService } from 'src/app/services/fur.service';
import { Fur } from 'src/app/models/Fur';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-antecedentes-fur',
  templateUrl: './antecedentes-fur.component.html',
  styleUrls: ['./antecedentes-fur.component.css']
})
export class AntecedentesFurComponent implements OnInit {

  listFormWeb: Fur[];

  constructor(protected furService: FurService,
    public dialog: MatDialog) {}

  ngOnInit() {

  }

  RecuperarAntecedentes(idFur:number)
  {

    this.listFormWeb = [];

    //console.log("============recuperar");

    this.furService.ObtenerFurPorId(idFur).subscribe(data=>{

     

      this.listFormWeb[0] = new Fur();

      // this.listFormWeb[0].cfurCodigo = data.cfurCodigo;
      // this.listFormWeb[0].canal = data.canal;
      // this.listFormWeb[0].estado = data.estado;
      // this.listFormWeb[0].strDfurFecinicioreg = data.strDfurFecinicioreg;
      // this.listFormWeb[0].cfurCodent = data.cfurCodent

      this.listFormWeb[0] = data;
      
    });

  }

  _VerAntecedente(){

    // const dialogRef = this.dialog.open(ModalAntecedenteComponent,{ width: '75%',height:'75%',
    // data : this.listFormWeb[0]});
    
    // dialogRef.afterClosed().subscribe(result => {


    // });

  }

}