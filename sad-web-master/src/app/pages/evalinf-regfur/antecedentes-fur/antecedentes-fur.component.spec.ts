import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AntecedentesFurComponent } from './antecedentes-fur.component';

describe('AntecedentesFurComponent', () => {
  let component: AntecedentesFurComponent;
  let fixture: ComponentFixture<AntecedentesFurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AntecedentesFurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AntecedentesFurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
