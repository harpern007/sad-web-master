import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { MfuncErrorVal, GetDateHour, ValidarEmail, GetDateTime, openModalMensaje, ConvertStrDateToFormatBD, ConvertDateTimeToStr, RecupPartesFchInicio, GlobalVars, GetDateTimeString, ConvertDateTimeToStr2 } from 'src/app/general/variables';
import { ErrorValidator } from 'src/app/models/personalizados/ErrorValidator';
import { Canal } from 'src/app/models/Canal';
import { CanalService } from 'src/app/services/canal.service';
import { Fur } from 'src/app/models/Fur';
import { EvidenciasComponent } from '../denuncia/evidencias/evidencias.component';
import { Observable } from 'rxjs';
import { FurService } from 'src/app/services/fur.service';
import { MatDialog } from '@angular/material/dialog';
import { MatDatepickerInputEvent } from '@angular/material';
// import { CompileTemplateMetadata } from '@angular/compiler';
// import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-registro-documento',
  templateUrl: './registro-documento.component.html',
  styleUrls: ['./registro-documento.component.css']
})
export class RegistroDocumentoComponent implements OnInit {

  //trIdExpediente:string = "";
  strHoraReg:string  = GetDateHour()[1];
  strFechaRegistro:string = GetDateHour()[0];
  strDscEstado:string = "Registrado";
  flgDeshabilitado = true;
  flgSoloLectura = false;
  mFuncErrorVal : MfuncErrorVal = new MfuncErrorVal();
  errorValidator: ErrorValidator[] = [];
  listCanal: Canal[] = [];
  strNroRegistro:string;
  numIdCanal:number = 4;
  strDscAsunto:string = "";
  strFchRecep:string = "";
  dateFchRecep:Date = null;
  //strDscRegOtr:string = "";//CFUR_DSCREGOTR //strDscRemitente:string = "";
  strDscRemitente:string = "";
  strDscCorreo:string = "";
  strDscSumilla:string="";
  strDscDetPrensa:string="";
  strDscNomProg:string="";
  strFurCodigo :string = null;
  //dateFchRecep:Date = null;
  fur:Fur;
  flgRepPrensa = false;
  flgCorreoElec = true;
  flgCarta = false;
  flgRegistrar = true;

  //Datos a Guardar
  dscRegotr:string;
  dscRegotr2:string;
  dscRegotr3:string;
  //public dateControl = new FormControl(new Date(2021,9,4,5,6,7));

  @ViewChild(EvidenciasComponent) evidenciasC : EvidenciasComponent;
  @Output() opGuardadoOk = new EventEmitter();

  constructor(protected canalService:CanalService,
    protected furService:FurService,
    public dialog: MatDialog) { }

  ngOnInit() {

    this.canalService.listarCanalDocumento().subscribe(data=>{

      this.listCanal = data;

    });

    setTimeout(() => {

      this.evidenciasC.setEvidenciaTitulo("Lista de archivos Adjuntos");
      
    }, 1);

    

    //this._CambioTipDocumento(this.numIdCanal);

  }

  _CambioTipDocumento(numIdCanal:number)
  {

    //if(event == null){return;}

    // console.log("=============event");
    // console.log(event);

    //var numIdCanal = idCanal//event.target.value;

    // console.log("=============numIdCanal");
    // console.log(numIdCanal);

    this.flgRepPrensa = false;
    this.flgCorreoElec = false;
    this.flgCarta = false;

    switch (Number(numIdCanal)) {
      case 4:

        this.flgCorreoElec = true;
        
        break;

      case 6:

        this.flgCarta = true;
        
        break;

      case 7:

        this.flgRepPrensa = true;
        
        break;
    
      default:
        break;
    }

    this.ValidarDatos();

    // console.log("=============this.flgRepPrensa");
    // console.log(this.flgRepPrensa);
    // console.log(this.flgCorreoElec);
    // console.log(this.flgCarta);

  }

  dateChanged(event: any) {

    if (event.value) {

      // var date = new Date(event.value.toString());

       console.log("=================event.value.toString()");
       console.log(event.value.toString());

      // console.log("=================event.value");
      // console.log(event.value);

      // console.log("=================date.getMonth()");
      // console.log(GetDateTimeString(event.value.toString()));
      

      //this.strFchRecep = GetDateTimeString(event.value.toString());
      this.strFchRecep = event.value.toString();

      this.ValidarDatos();

    }
   
    
  }

  ValidarDatos()
  {

    var FlgError = false;
    //var flgErrorCampo = false;
    //var flgValDatGen = false;
    var dscCorreo = "";
    var dscRemitente = "";
    var dscSumilla = "";
    var dscDetPrensa = "";
    var dscNomProg = "";
    //var dscMensaje = "";

    //console.log("V1");
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValIndex((this.numIdCanal != null ? false:true), "Ingrese el tipo de documento",this.errorValidator,0);

    this.errorValidator = this.mFuncErrorVal.AgregarErrorValIndex((this.strDscAsunto.length > 0 ? false:true), "Ingrese el asunto",this.errorValidator,1);

    if(this.flgCorreoElec || this.flgCarta)
    {

      dscRemitente = this.strDscRemitente;

      this.errorValidator = this.mFuncErrorVal.AgregarErrorValIndex((dscRemitente.length > 0 ? false:true), "Ingrese el remitente",this.errorValidator,2);

      this.dscRegotr = dscRemitente;

    }

    if(this.flgCorreoElec)
    {

      dscCorreo = this.strDscCorreo;

      this.errorValidator = this.mFuncErrorVal.AgregarErrorValIndex((dscCorreo.length > 0 && ValidarEmail(dscCorreo) ? false : true), "Ingrese el correo",this.errorValidator,3);

      this.dscRegotr2 = dscCorreo;

    }

    if(this.flgRepPrensa)
    {

      dscSumilla = this.strDscSumilla;
      dscDetPrensa = this.strDscDetPrensa;
      dscNomProg = this.strDscNomProg;

      this.errorValidator = this.mFuncErrorVal.AgregarErrorValIndex((dscSumilla.length > 0 ? false : true), "Ingrese la sumilla",this.errorValidator,4);

      this.errorValidator = this.mFuncErrorVal.AgregarErrorValIndex((dscDetPrensa.length > 0 ? false : true), "Ingrese el detalle de prensa",this.errorValidator,5);

      this.errorValidator = this.mFuncErrorVal.AgregarErrorValIndex((dscNomProg.length > 0 ? false : true), "Ingrese el nombre del programa",this.errorValidator,6);

      this.dscRegotr = dscSumilla;
      this.dscRegotr2 = dscDetPrensa;
      this.dscRegotr3 = dscNomProg;

    }

    console.log("=================this.strFchRecep");
    console.log(this.strFchRecep);

    this.errorValidator = this.mFuncErrorVal.AgregarErrorValIndex((this.strFchRecep.length > 0 ? false : true), "Ingrese la fecha de recepción",this.errorValidator,7);

    if(this.mFuncErrorVal.RevisarErrorValidator(this.errorValidator))
    {

      FlgError = true

    }

    return FlgError;
    
  }

  GuardarDatos()
  {

    //var numDoc = null;
    var idFur = null;

    this.fur = new Fur();

    this.fur.nfurId = Number(this.strNroRegistro);

    this.fur.strDfurFecregistro = GetDateTime();
    this.fur.strDfurFecinicioreg = this.strFechaRegistro + " " + this.strHoraReg;
    this.fur.nfurIdcanal = this.numIdCanal;

    this.fur.cfurDscregotr = this.dscRegotr;
    this.fur.cfurDscregotr2 = this.dscRegotr2;
    this.fur.cfurDscregotr3 = this.dscRegotr3;

    this.fur.cfurIdususuperv = GlobalVars.CodPersSuperv;
    this.fur.cfurIdusureg = GlobalVars.CodPersonal;

    this.fur.cfurCodigo = this.strFurCodigo;

    //this.fur.dfurFchrecep = this.dateFchRecep;

    //this.fur.strDfurFchrecep = ConvertStrDateToFormatBD(this.strFchRecep);
    this.fur.strDfurFchrecep = GetDateTimeString(this.strFchRecep);

    this.fur.cfurDschecho = this.strDscAsunto;
    
    // this.fur.listaDocumentoAdjuntoDet = [];

    console.log("============this.evidenciasC.listEvidencias");
    console.log(this.evidenciasC.listEvidencias);

    return new Observable((observer) => {

        console.log("==========this.fur");

        console.log(this.fur);

        console.log("==========INGRESA SUSCRIBVEE LASER FICHE");
        //console.log(data);

        // this.evidenciasC.listEvidencias.forEach(element => {

        //   this.fur.listaDocumentoAdjuntoDet.push(element);
          
        // });
  
        if(!this.flgRegistrar)
        {
  
          console.log("==========ACTUALIZAR");
  
          this.furService.ActualizarFur(this.fur).subscribe(data=>{
  
            idFur = data.nfurId;
        
            if(idFur > 0)
            {

              this.evidenciasC.procesarLaserFiche(idFur).subscribe(data=>{

                
                openModalMensaje("Documentos",`Se actualizó el Documento Nro. ${idFur}, por favor verifique`,this.dialog).afterClosed().subscribe(result=>{
  
                    this.opGuardadoOk.emit();
    
                  });


              });

              // this.evidenciasC.procesarLaserFiche(idFur).subscribe( {

              //   complete(){

              //     openModalMensaje("Documentos",`Se actualizó el Documento Nro. ${idFur}, por favor verifique`,this.dialog).afterClosed().subscribe(result=>{
  
              //       this.opGuardadoOk.emit();
    
              //     });


              //   }

              // });
              
            }else
            {
      
              openModalMensaje("Expediente","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{
  
              });
      
              return;
      
            }
        
          });
  
        }else{
  
          console.log("==========REGISTRAR");
  
          this.furService.RegistrarFur(this.fur).subscribe(data=>{
  
            idFur = data.nfurId;
    
            if(idFur > 0)
            {

              this.evidenciasC.procesarLaserFiche(idFur).subscribe(data=>{

                
                openModalMensaje("Documentos",`Se generó el Documento Nro. ${idFur}, por favor verifique`,this.dialog).afterClosed().subscribe(result=>{
    
                  this.strNroRegistro = idFur;
                  this.opGuardadoOk.emit();
      
                });


              });

           
              
            }else{
    
              openModalMensaje("Expediente","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{
    
                //observer.error();
    
              });
    
              //return;
    
            }
    
          });
  
        }
  
      return () => {
        // Detach the event handler from the target
        
      };
    });

  }

  _mensajeOk(idFur:number){

    openModalMensaje("Documentos",`Se generó el Documento Nro. ${idFur}, por favor verifique`,this.dialog).afterClosed().subscribe(result=>{
    
      this.strNroRegistro = idFur.toString();
      this.opGuardadoOk.emit();

    });

  }

  soloLectura(flgParam:boolean)
  {

    this.evidenciasC.flgSoloLectura = flgParam;
    this.flgSoloLectura = flgParam;

  }

  SetDatos(data:Fur)
  {

    console.log("============data RD");
    console.log(data);

    var numIdCanal = data.nfurIdcanal;


    this.strNroRegistro = data.nfurId.toString();
    this.strHoraReg = RecupPartesFchInicio(data.strDfurFecinicioreg,1);
    this.strFechaRegistro = RecupPartesFchInicio(data.strDfurFecinicioreg,2);
    this.numIdCanal = numIdCanal;
   

    this.strDscAsunto = data.cfurDschecho;

    if(this.strDscRemitente != null){this.strDscRemitente = data.cfurDscregotr;}
    if(this.strDscCorreo != null){this.strDscCorreo = data.cfurDscregotr2;}

    if(this.strDscSumilla != null){this.strDscSumilla = data.cfurDscregotr;}
    if(this.strDscDetPrensa != null){this.strDscDetPrensa = data.cfurDscregotr2;}
    if(this.strDscNomProg != null){this.strDscNomProg = data.cfurDscregotr3;}

    //this.dateFchRecep = data.dfurFchrecep;

    this.strFchRecep = ConvertDateTimeToStr2(data.dfurFchrecep);

    //this.evidenciasC.listEvidencias = data.listaDocumentoAdjuntoDet;

    // console.log("==============this.evidenciasC.listEvidencias");
    // console.log(this.evidenciasC.listEvidencias);

    this.strFurCodigo = data.cfurCodigo;

    this.flgRegistrar = false;

    this._CambioTipDocumento(numIdCanal);

    this.evidenciasC.setEvidenciaC(data);

  }



}
