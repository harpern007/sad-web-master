import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroDocumentoComponent } from './registro-documento.component';

describe('RegistroDocumentoComponent', () => {
  let component: RegistroDocumentoComponent;
  let fixture: ComponentFixture<RegistroDocumentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroDocumentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroDocumentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
