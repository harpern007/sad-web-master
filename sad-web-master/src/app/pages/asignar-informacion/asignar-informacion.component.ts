import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { PersonalCgrService } from 'src/app/services/personal-cgr.service';
import { NumIdCargoGestorCan,  openModalMensaje, NumIdEstadoAsignado, NumIdCanalExp, NumCanalFormWeb, RecupPartesFchInicio } from 'src/app/general/variables';
import { AsignaResponsableComponent } from '../General/asigna-responsable/asigna-responsable.component';
import { FurService } from 'src/app/services/fur.service';
import { Fur } from 'src/app/models/Fur';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
//import { EvidenciasComponent } from '../denuncia/evidencias/evidencias.component';
import { BodyDatosExpedienteComponent } from '../Dashboard/body-datos-expediente/body-datos-expediente.component';
import { RegistroDocumentoComponent } from '../registro-documento/registro-documento.component';
import { DatosRegistroComponent } from './datos-registro/datos-registro.component';
import { DgAsigRespComponent } from '../General/dg-asig-resp/dg-asig-resp.component';
import { ContenedorWebComponent } from '../formulario-web/contenedor-web/contenedor-web.component';
//import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-asignar-informacion',
  templateUrl: './asignar-informacion.component.html',
  styleUrls: ['./asignar-informacion.component.css']
})

export class AsignarInformacionComponent implements OnInit {

  //listPersonalCgr:PersonalCgr[] = [];
  //listCargo:Cargo[] = [];
  numIdCargo:number = NumIdCargoGestorCan;
  @ViewChild(AsignaResponsableComponent) asignaResponsableC : AsignaResponsableComponent;
  //@ViewChild(EvidenciasComponent) evidenciasC : EvidenciasComponent;
  @ViewChild(BodyDatosExpedienteComponent) bodyDatosExpedienteC : BodyDatosExpedienteComponent;
  @ViewChild(RegistroDocumentoComponent) registroDocumentoC : RegistroDocumentoComponent;
  @ViewChild(DatosRegistroComponent) datosRegistroC : DatosRegistroComponent;
  @ViewChild(DgAsigRespComponent) dgAsigRespC : DgAsigRespComponent;
  @ViewChild(ContenedorWebComponent) contenedorWebC : ContenedorWebComponent;
  
  @Output() opRetornaAsignar = new EventEmitter();

  idFurParam:number = null;
  fur:Fur = new Fur();
  flgMostrarExp= false;
  flgMostrarOtr = false;
  flgMostrarFormWeb = false;
  flgMostrarInfor = false;

  constructor(protected maestraService:MaestrasService,
    protected personalCgrService: PersonalCgrService,
    protected furService:FurService,
    public dialog: MatDialog,
    public router:Router,
    private route: ActivatedRoute
    // ,
    // private cookieService: CookieService
    ) { }

  ngOnInit() {

      
  }

  mostrarRegistro(idFur:number){

    //var idFur = this.route.snapshot.params.idFur;
    var idCanalRecup;
    var idFurStr;

    this.idFurParam = idFur;

    this.dgAsigRespC.flgSoloLectura = true;

    console.log("==========idFur");
    console.log(idFur);

    console.log("============recuperar1");

    //setTimeout(() => {

      this.furService.ObtenerFurPorId(idFur).subscribe(data=>{

        console.log("============recuperar1");
        console.log(data); 
  
        //this.evidenciasC.flgSoloLectura = true;
        //this.evidenciasC.evidenciaCReadEnable();
        //this.evidenciasC.formEvid.controls.evidenciaFile.disable();
  
        //this.evidenciasC.listEvidencias = data.listaDocumentoAdjuntoDet;
        idCanalRecup = data.nfurIdcanal;
        idFurStr = data.nfurId.toString();
        
        
        this.dgAsigRespC.numIdTipAtencion = data.nfurIdtipaten;
        this.dgAsigRespC.numIdCanal = idCanalRecup;
  
        // console.log("=======new1");
        // console.log(this.fur);
  
        if(idCanalRecup == NumCanalFormWeb)
        {
  
          
          this.flgMostrarFormWeb = true;
  
          setTimeout(() => {

            
            this.datosRegistroC.strEtiquetaReg = "N° Formulario Web";

            setTimeout(() => {

              this.contenedorWebC.verFormularioWeb(data);
              
            }, 0);

            
           
            
          }, 0);

          

  
        }else
        {
  
          this.flgMostrarInfor = true;// POR PFOBLEMAS EN LA VISUALIZACIÓN
          
          this.datosRegistroC.strEtiquetaReg = "N° Documento";
  
          if(idCanalRecup == NumIdCanalExp)
          {
  
            this.flgMostrarExp = true;
  
            
  
            // console.log("=======new3");
            // console.log(this.fur);
  
            setTimeout(() => {
  
  
              this.bodyDatosExpedienteC.soloLectura(true);
              
              this.bodyDatosExpedienteC.SetDatos(data);
  
              
            }, 1);
    
          }else{
    
            this.flgMostrarOtr = true;
  
            
            setTimeout(() => {
  
              
              this.registroDocumentoC.soloLectura(true);
  
              this.registroDocumentoC.SetDatos(data);
  
              
            }, 1);
    
          }

          

        }

        this.datosRegistroC.numIdEstado = data.nfurIdestado;
        this.datosRegistroC.numIdPrioridad = data.nfurIdprioridad;
        this.datosRegistroC.strNroRegistro = idFurStr;
        this.datosRegistroC.strFchRegistro = RecupPartesFchInicio(data.strDfurFecinicioreg,2);
        this.datosRegistroC.strHrInicio = RecupPartesFchInicio(data.strDfurFecinicioreg,1);
        this.datosRegistroC.strHrFin = RecupPartesFchInicio(data.strDfurFecregistro,1);

        this.fur = data;
  
      });    
      
    //}, 2000);
  }

  _Asignar(){

    var furId;

    if(this.asignaResponsableC.ValidarDatos())
    {

      return;

    }

    //return;

    this.fur.cfurIdusuasig = this.asignaResponsableC.strIdResponsable;
    this.fur.cfurDscindica = this.asignaResponsableC.strDscIndicacion;
    this.fur.nfurNumplazoasig = this.asignaResponsableC.numCantTiempo;
    this.fur.nfurIdplazoasig = this.asignaResponsableC.numIdPlazo;
    this.fur.nfurIdestado = NumIdEstadoAsignado;

    console.log("============Asignar FUR");
    console.log(this.fur);
    
    this.furService.ActualizarFur(this.fur).subscribe(data=>{

      furId = data.nfurId;

      
      if(data.nfurId > 0)
      {
        
        openModalMensaje("Asignar Información",`Se asignó correctamente el registro N° ${furId}, por favor verifique`,this.dialog).afterClosed().subscribe(result=>{

          this._RetornarSeguimiento();

        });
        
      }else{

        openModalMensaje("Asignar Información","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{

          this._RetornarSeguimiento();

        });

        //flgGuardado = false;

        return;

      }

      //this.opGuardado.emit(flgGuardado);

      //return

    });

  }

  _RetornarSeguimiento(){

   
    this.opRetornaAsignar.emit();

    // console.log("==========GlobalVars.CodUsuario");

    // console.log(GlobalVars.CodUsuario);

    // this.router.navigate(['/PanelPrincipal/',GlobalVars.CodUsuario], { relativeTo: this.route });

  }

  _Cancelar()
  {

    // openModalMensaje("Asignar Información",`¿Usted está seguro de salir de la Asignación de FUR?`,this.dialog).afterClosed().subscribe(result=>{

    //   this._RetornarSeguimiento();

    // });

    this._RetornarSeguimiento();

  }

}
