import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarInformacionComponent } from './asignar-informacion.component';

describe('AsignarInformacionComponent', () => {
  let component: AsignarInformacionComponent;
  let fixture: ComponentFixture<AsignarInformacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarInformacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarInformacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
