import { Component, OnInit } from '@angular/core';
import { MfuncErrorVal, CodTipoFichaFUR } from 'src/app/general/variables';
import { ErrorValidator } from 'src/app/models/personalizados/ErrorValidator';
import { Prioridad } from 'src/app/models/Prioridad';
import { Estado } from 'src/app/models/Estado';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { PrioridadService } from 'src/app/services/prioridad.service';

@Component({
  selector: 'app-datos-registro',
  templateUrl: './datos-registro.component.html',
  styleUrls: ['./datos-registro.component.css']
})
export class DatosRegistroComponent implements OnInit {

  strEtiquetaReg:string = "";
  strNroRegistro:string = "";
  strHrInicio:string = "";
  strFchRegistro:string = "";
  strHrFin:string = "";
  numIdEstado:number = null;
  numIdPrioridad:number = null;

  listPrioridad:Prioridad[] = [];
  listEstados:Estado[] = [];
  flgDeshabilitado = true;
  
  constructor(protected maestrasService:MaestrasService,
              protected prioridadService:PrioridadService) { }

  ngOnInit() {

    this.maestrasService.obtenerEstado().subscribe(data=>{

      this.listEstados = data;

    });

    this.prioridadService.ListarPrioridadPorFicha(CodTipoFichaFUR).subscribe(data=>{

      this.listPrioridad = data;

    });

  }

}
