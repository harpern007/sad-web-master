import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyDatosExpedienteComponent } from './body-datos-expediente.component';

describe('BodyDatosExpedienteComponent', () => {
  let component: BodyDatosExpedienteComponent;
  let fixture: ComponentFixture<BodyDatosExpedienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyDatosExpedienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyDatosExpedienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
