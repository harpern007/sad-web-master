import { Component, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';
import { ExpedienteDet } from 'src/app/models/ExpedienteDet';
import { UniOrganica } from 'src/app/models/UniOrganica';
import { UniOrganicaService } from 'src/app/services/uni-organica.service';
import { TipoDocExp } from 'src/app/models/TipoDocExp';
import { TipoDocExpService } from 'src/app/services/tipo-doc-exp.service';
import { Anho } from 'src/app/models/Anho';
import { AnhoService } from 'src/app/services/Anho.service';
import { Prioridad } from 'src/app/models/Prioridad';
import { PrioridadService } from 'src/app/services/prioridad.service';
import { SedeCgr } from 'src/app/models/SedeCgr';
import { SedeCgrService } from 'src/app/services/sede-cgr.service';
import { PersonalCgr } from 'src/app/models/PersonalCgr';
import { PersonalCgrService } from 'src/app/services/personal-cgr.service';
import { MfuncErrorVal, openModalMensaje, TipoNroExpdiente, GetAnho, NumIdCanalExp, CodTipoExpDetREF, CodTipoExpDetDES, CodTipoFichaEXP, GlobalVars,  RecupPartesFchInicio,  ConvertDateTimeToStr, ConvertStrDateToFormatBD, RecupPartesDocExp, CodUnoGestDenuncias, CodUnoSubGerCMP } from 'src/app/general/variables';
import { ErrorValidator } from 'src/app/models/personalizados/ErrorValidator';
import { FurService } from 'src/app/services/fur.service';
import { Fur } from 'src/app/models/Fur';
import { MatDialog } from '@angular/material/dialog';
import { BodyDatosGeneralComponent } from '../body-datos-general/body-datos-general.component';
import { Observable } from 'rxjs/internal/Observable';
//import { DatePipe } from '@angular/common';
//import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-body-datos-expediente',
  templateUrl: './body-datos-expediente.component.html',
  styleUrls: ['./body-datos-expediente.component.css']
})
export class BodyDatosExpedienteComponent implements OnInit {

  strCodUno:string = "";
  strDscFirmado:string = "";
  strDscElaborado:string = "";
  codPersFirmado:string = "";
  codPersElaborado:string = "";
  numIdTipoDoc:number = null;
  strNroDoc1:string = "";
  strNroDoc2:string = GetAnho().toString();
  strNroDoc3:string = TipoNroExpdiente;
  strDscAsunto:string = "";
  listaExpRef:ExpedienteDet[] = [];
  listaExpDest:ExpedienteDet[] = [];
  listUniOrganica:UniOrganica[] = [];
  listTipoDocExp:TipoDocExp[] = [];
  listAnho:Anho[]=[];
  listPrioridad:Prioridad[]=[];
  listSedeCgr:SedeCgr[]=[];
  listPersonalCgr:PersonalCgr[]=[];
    
  flgDeshabilitado = true;//siempre true
  flgSoloLectura = false;
  flgRegistrar = true;
  
  fur:Fur;

  @Output() opGuardadoOk = new EventEmitter();

  @ViewChild(BodyDatosGeneralComponent) bodyDatosGeneralC : BodyDatosGeneralComponent;

  errorValidator: ErrorValidator[] = [];
  mFuncErrorVal : MfuncErrorVal = new MfuncErrorVal();

  constructor(protected uniOrganicaService:UniOrganicaService,
    protected tipoDocExpService:TipoDocExpService,
    protected anhoService:AnhoService,
    protected prioridadService:PrioridadService,
    protected sedeCgrService:SedeCgrService,
    protected personalCgrService:PersonalCgrService,
    protected furService:FurService,
    public dialog: MatDialog) { }

  ngOnInit() {

    this.uniOrganicaService.ListarUnidadOrganicaActiva().subscribe(data=>{

      this.listUniOrganica = data;

    });

    this.tipoDocExpService.ListarTipoDocumentoExpediente().subscribe(data=>{

      this.listTipoDocExp = data;

    });

    this.anhoService.listarAnho().subscribe(data=>{

      this.listAnho = data;

    });

    this.prioridadService.ListarPrioridadPorFicha(CodTipoFichaEXP).subscribe(data=>{

      this.listPrioridad = data;

    });

    this.sedeCgrService.listarSedeCgr().subscribe(data=>{

      this.listSedeCgr = data;

    });

    this.personalCgrService.ListarPersonalCgrActivo().subscribe(data=>{

      this.listPersonalCgr = data.filter(x => x.cpUnoCodigo == CodUnoGestDenuncias || x.cpUnoCodigo == CodUnoSubGerCMP);

      //(errorValidator.filter(x => x.flgInvalid).length > 0 ? true : false)

      //console.log(data);

    });

  }

  

  _EliminarDest(index:number){

    this.listaExpDest.splice(index,1);

    this.ValidarDatos();

  }

  ValidarDatos()
  {

    var FlgError = false;
    var flgValDatGen = false;

    //console.log("V1");

    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.strCodUno.length > 0 ? false:true), "Ingrese la Unidad Orgánica",this.errorValidator,true);
    
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.numIdTipoDoc != null ? false:true), "Ingrese el tipo de documento",this.errorValidator,false);
    //console.log("V3");
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.strNroDoc1.length > 0 ? false:true), "Ingrese el número de documento",this.errorValidator,false);
    //console.log("V4");
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this.strDscAsunto.length > 0 ? false:true), "Ingrese el asunto",this.errorValidator,false);
    //console.log("V5");
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this._ValidaGrillaRef(this.listaExpRef)), "Ingrese todos los datos de la lista de Referencia",this.errorValidator,false);
    //console.log("V6");
    this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((this._ValidaGrillaDest(this.listaExpDest)), "Ingrese todos los datos de la lista de Destino",this.errorValidator,false);
    //console.log("V7");

    flgValDatGen = this.bodyDatosGeneralC.ValidarDatos();

    if(this.mFuncErrorVal.RevisarErrorValidator(this.errorValidator) || flgValDatGen)
    {

      FlgError = true

    }

    return FlgError;
    
  }

  // _recuperaDscPersonal(codPersona:string)
  // {

  //   var dscPersonal = "";

  //   this.personalCgrService.ObtenerPersonalCgrPorCodigo(codPersona).subscribe(data=>{

  //     // console.log("=========this.strDscElaborado");
  //     // console.log(this.strDscElaborado);

  //     dscPersonal = "";

  //     if(data != null)
  //     {

  //       dscPersonal = data.cpPerApPater + " " + data.cpPerApMater  + ", " + data.cpPerNombre;

  //     }

  //     this.strDscElaborado = dscPersonal

  //   });

  // }

  _CambioUO(){

    var uoSeleccion = this.listUniOrganica.filter(x => x.cpUnoCodigo == this.strCodUno)[0];
    var dscPersonal = "";

    // console.log("=========uoSeleccion");
    // console.log(uoSeleccion);
    // console.log(this.listUniOrganica);
    // console.log(this.strCodUno);

    //if(uoSeleccion == null){return;}

    this.personalCgrService.ObtenerPersonalCgrPorCodigo(uoSeleccion.cpUnoCodresp).subscribe(data=>{

      console.log("=========this.strDscElaborado");
      console.log(data);

      dscPersonal = "";

      if(data != null)
      {

        dscPersonal = data.cpPerApPater + " " + data.cpPerApMater  + ", " + data.cpPerNombre;

      }

      this.strDscElaborado = dscPersonal

    });

    this.personalCgrService.ObtenerPersonalCgrPorCodigo(uoSeleccion.cpUnoJefe).subscribe(data=>{

      dscPersonal = "";

      if(data != null)
      {

        dscPersonal = data.cpPerApPater + " " + data.cpPerApMater  + ", " + data.cpPerNombre;

      }

      this.strDscFirmado = dscPersonal;

    });

    this.codPersFirmado = uoSeleccion.cpUnoJefe;
    this.codPersElaborado = uoSeleccion.cpUnoCodresp;

    this.ValidarDatos();

  }

  // _cambioDocumento(){

  //   this.ValidarDatos();

  // }
  // _cambioNroDoc(){

  //   this.ValidarDatos();

  // }

  // _cambioDscAsunto(){

  //   this.ValidarDatos();

  // }

  // _cambioAnhoRef(){


  // }

  // GuardarDatos()
  // {

  //   return new Observable(this._GuardarDatos);

  // }

  GuardarDatos()
  {

    var numDoc = null;
    var idFur = null;
    
    this.fur = new Fur();

    this.fur = this.bodyDatosGeneralC.RecuperarDatos(this.fur);

    numDoc = this.strNroDoc1 + "-" + this.strNroDoc2 + "-" + this.strNroDoc3;

    this.fur.cfurCoduno = this.strCodUno;
    this.fur.nfurIdtipodocexp = Number(this.numIdTipoDoc);
    this.fur.cfurCodigo = numDoc;
    this.fur.cfurNroexpediente = numDoc;
    this.fur.cfurDschecho = this.strDscAsunto;
    this.fur.nfurIdcanal = NumIdCanalExp;
    this.fur.cfurIdusureg = GlobalVars.CodPersonal;
    this.fur.cfurIdususuperv = GlobalVars.CodPersSuperv;
    this.fur.cfurCodperscgr1 = this.codPersFirmado;
    this.fur.cfurCodperscgr2 = this.codPersElaborado;
    //CFUR_CODPERSCGR1
    
    this.fur.listaExpedienteDet = [];

    this.listaExpRef.forEach(element => {

      //2020-01-23

      element.strDexpdetFecha = ConvertStrDateToFormatBD(element.strDexpdetFecha);//element.fch_registro;

      //Provisional por que no se tiene el servicio del combo
      element.nexpdetIdtipdoc = Number(element.nexpdetIdtipdoc);
      element.nexpdetIdprioridad = Number(element.nexpdetIdprioridad);


      this.fur.listaExpedienteDet.push(element);
      
    });

    this.listaExpDest.forEach(element => {

      //element.strDexpdetFecha = element.fch_registro;
       //Provisional por que no se tiene el servicio del combo
       //element.nexpdetIdtipodoc = Number(element.nexpdetIdtipodoc);
       element.nexpdetIdprioridad = Number(element.nexpdetIdprioridad);

      this.fur.listaExpedienteDet.push(element);
      
    });


    // for (let index = 0; index < array.length; index++) {

    //   const element = array[index];
      
    // }
   
    //this.fur.listaExpedienteDet = 

    return new Observable((observer) => {

      console.log("==================this.fur");

      console.log(this.fur);

      if(!this.flgRegistrar)
      {

        console.log("==================ACTUALIZAR");



        this.furService.ActualizarFur(this.fur).subscribe(data=>{

          idFur = data.nfurId;

      
          if(idFur > 0)
          {
            
            openModalMensaje("Expediente",`Se actualizó el Expediente Nro. ${idFur}, por favor verifique`,this.dialog).afterClosed().subscribe(result=>{

              this.bodyDatosGeneralC.strIdExpediente = idFur;
    
              this.opGuardadoOk.emit();
              
  
            });
            
          }else{
    
            openModalMensaje("Expediente","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{

              //observer.error();
  
              observer.complete();
  
              //return flgProcesado;
  
            });
    
            //flgGuardado = false;
    
            return;
    
          }
    
          //this.opGuardado.emit(flgGuardado);
    
          //return
    
        });

      }else{

        console.log("==================REGISTRAR");

        this.furService.RegistrarFur(this.fur).subscribe(data=>{

          idFur = data.nfurId;
  
          if(idFur > 0)
          {
  
            //flgProcesado = true;
            
            openModalMensaje("Expediente",`Se generó el Expediente Nro. ${idFur}, por favor verifique`,this.dialog).afterClosed().subscribe(result=>{
  
              this.bodyDatosGeneralC.strIdExpediente = idFur;
  
              //return flgProcesado;
              //observer.complete();
  
              //console.log("=====emitir");
  
              this.opGuardadoOk.emit();
              
  
            });
            
          }else{
  
            openModalMensaje("Expediente","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{
  
              //observer.error();
  
              observer.complete();
  
              //return flgProcesado;
  
            });
  
            //flgGuardado = false;
  
            //return;
  
          }
  
          //this.opGuardado.emit(flgGuardado);
  
          //return
  
        });

      }
      
      //var flgProcesado = false;
  
      return () => {

        // console.log("============Procesado");

        // flgProcesado;
        // Detach the event handler from the target
        
      };
    });

  }

  SetDatos(data:Fur)
  {

    // console.log("========FURRRRR");
    // console.log(data);

    var listaExpediente = data.listaExpedienteDet;
    //var furAux:Fur = new Fur();

    //furAux.cfurNroexpediente = data.cfurNroexpediente;//para fragmentar el num_doc_exp
    //furAux.strDfurFecinicioreg = data.strDfurFecinicioreg;
    //furAux.strDfurFecregistro = data.strDfurFecregistro;

    
    this.strCodUno = data.cfurCoduno;
    this.numIdTipoDoc = data.nfurIdtipodocexp;
    this.strNroDoc1 = RecupPartesDocExp(data.cfurNroexpediente,1);
    this.strNroDoc2 = RecupPartesDocExp(data.cfurNroexpediente,2);
    this.strNroDoc3 = RecupPartesDocExp(data.cfurNroexpediente,3);
    this.strDscAsunto = data.cfurDschecho;

    data.listaExpedienteDet.forEach(element => {

      element.strDexpdetFecha = ConvertDateTimeToStr(element.dexpdetFecha);
      
    });


    if(listaExpediente != null)
    {

      this.listaExpRef = data.listaExpedienteDet.filter(x=>x.cexpdetCodtipo == CodTipoExpDetREF);
      this.listaExpDest = data.listaExpedienteDet.filter(x=>x.cexpdetCodtipo == CodTipoExpDetDES);

      //console.log(ConvertDateTimeToStr(this.listaExpRef[0].dexpdetFecha));

      //this.listaExpRef[0].dexpdetFecha = ConvertDateTimeToStr(this.listaExpRef[0].dexpdetFecha);

      // var date = new Date();
      // console.log(this.datePipe.transform(date,"yyyy-MM-dd"));

      // this.listaExpRef[0].dexpdetFecha = this.datePipe.transform(date,"yyyy-MM-dd");

      //this.listaExpRef[0].strDexpdetFecha = ConvertDateTimeToStr(this.listaExpRef[0].dexpdetFecha);

      

    }

    this.bodyDatosGeneralC.strIdExpediente = data.nfurId.toString();
    this.bodyDatosGeneralC.strFechaRegistro = RecupPartesFchInicio(data.strDfurFecinicioreg,2);
    this.bodyDatosGeneralC.strHoraReg = RecupPartesFchInicio(data.strDfurFecinicioreg,1);
    this.bodyDatosGeneralC.numCantTiempo = data.nfurNumplazo;
    this.bodyDatosGeneralC.numIdPlazo = data.nfurIdplazo;

    this.flgRegistrar = false;

    setTimeout(() => {

      this._CambioUO();
      
    }, 500);

    

  }

//   _GuardarDatos(observer)
//   {

// //    return new Observable()

//     var numDoc = null;
//     var idFur = null;
//     //var flgGuardado = true;

//     this.fur = new Fur();

//     // if(this.ValidarDatos() || this.bodyDatosGeneralC.ValidarDatos())
//     // {

//     //   observer.error();

//     //   return;
//     // }

//     // setTimeout(() => {

//     //   this.fur = this.bodyDatosGeneralC.RecuperarDatos(this.fur);
      
//     // }, 1);
   
//     console.log(this.bodyDatosGeneralC);
    
//     this.bodyDatosGeneralC.RecuperarDatos(this.fur).subscribe((data:Fur)=>{

//       //this.fur = data;

//       this.fur = data;

//     });
    

//     numDoc = this.strNroDoc1 + "-" + this.strNroDoc2 + "-" + this.strNroDoc3;

//     this.fur.cfurCoduno = this.strCodUno;
//     this.fur.nfurIdtipodocexp = this.numIdTipoDoc;
//     //this.fur.cfurCodigo = numDoc;
//     this.fur.cfurNroexpediente = numDoc;
//     this.fur.cfurDschecho = this.strDscAsunto;

//     this.furService.ActualizarFur(this.fur).subscribe(data=>{

//       idFur = data.nfurId;

//       if(idFur > 0)
//       {
        
//         openModalMensaje("Expediente",`Se generó el Expediente Nro. ${idFur}, por favor verifique`,this.dialog).afterClosed().subscribe(result=>{

//           this.bodyDatosGeneralC.strIdExpediente = idFur;
//           observer.complete();

//         });
        
//       }else{

//         openModalMensaje("Expediente","Ocurrió un problema en la actualización del registro",this.dialog).afterClosed().subscribe(result=>{

//           observer.error();

//         });

//         //flgGuardado = false;

//         return;

//       }

//       //this.opGuardado.emit(flgGuardado);

//       //return

//     });

//   }

  _ValidaGrillaRef(listaExpedienteDet:ExpedienteDet[])
  {

    var FlgError = false;

    //  console.log("=================listaExpedienteDet");
    //  console.log(listaExpedienteDet);
    //  console.log(listaExpedienteDet.filter(x => x.cexpdetCodanho.length == 0 || 
    //   x.nexpdetIdtipdoc == null || 
    //   x.cexpdetNrodoc.length == 0 || 
    //   x.cexpdetNroexp.length == 0 || 
    //   x.strDexpdetFecha == null || 
    //   x.cexpdetDscindica.length == 0 ||
    //   x.nexpdetIdprioridad == null
    //   ));

    if(listaExpedienteDet.filter(x => x.cexpdetCodanho == null || 
      x.nexpdetIdtipdoc == null || 
      x.cexpdetNrodoc == null || 
      x.cexpdetNroexp == null || 
      x.strDexpdetFecha == null || 
      x.cexpdetDscindica == null ||
      x.nexpdetIdprioridad == null
      ).length > 0 || listaExpedienteDet.length == 0) 
    {

      FlgError = true;

    }

    return FlgError;

  }

  _ValidaGrillaDest(listaExpedienteDet:ExpedienteDet[])
  {

    var FlgError = false;

    if(listaExpedienteDet.filter(x => x.cexpdetCoduno == null || 
      x.cexpdetCodlocal == null || 
      x.cexpdetCodpersonal == null || 
      x.cexpdetCodresp == null || 
      x.cexpdetDscindica == null ||
      x.nexpdetIdprioridad == null       
      ).length > 0 || listaExpedienteDet.length == 0) 
    {

      FlgError = true;

    }

    return FlgError;

  }

  _AgregarRef(){

    var numUltReg = this.listaExpRef.length;

    this.listaExpRef[numUltReg] = new ExpedienteDet();
    this.listaExpRef[numUltReg].cexpdetCodtipo = CodTipoExpDetREF;
    this.listaExpRef[numUltReg].cexpdetCodanho = null;
    this.listaExpRef[numUltReg].nexpdetIdtipdoc = null;
    this.listaExpRef[numUltReg].cexpdetNrodoc = null;
    this.listaExpRef[numUltReg].cexpdetNroexp = null;
    this.listaExpRef[numUltReg].strDexpdetFecha = null;
    this.listaExpRef[numUltReg].cexpdetDscindica = null;
    this.listaExpRef[numUltReg].nexpdetIdprioridad = null;
    
  }

  _EliminarRef(index:number){

    this.listaExpRef.splice(index,1);

    this.ValidarDatos();

  }

  _AgregarDest(){

    var numUltReg = this.listaExpDest.length;

    this.listaExpDest[numUltReg] = new ExpedienteDet();
    this.listaExpDest[numUltReg].cexpdetCodtipo = CodTipoExpDetDES;
    this.listaExpDest[numUltReg].cexpdetCoduno = null;
    this.listaExpDest[numUltReg].cexpdetCodlocal = null;
    this.listaExpDest[numUltReg].cexpdetCodpersonal = null;
    this.listaExpDest[numUltReg].cexpdetCodresp = null;
    this.listaExpDest[numUltReg].cexpdetDscindica = null;
    this.listaExpDest[numUltReg].nexpdetIdprioridad = null;

  }

  soloLectura(flgParam:boolean){
    this.bodyDatosGeneralC.flgSoloLectura = flgParam;
    this.flgSoloLectura = flgParam;
  }

  

}
