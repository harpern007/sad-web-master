import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyOpcionesComponent } from './body-opciones.component';

describe('BodyOpcionesComponent', () => {
  let component: BodyOpcionesComponent;
  let fixture: ComponentFixture<BodyOpcionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyOpcionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyOpcionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
