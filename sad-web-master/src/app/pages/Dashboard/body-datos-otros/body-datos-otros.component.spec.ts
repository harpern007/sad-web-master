import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyDatosOtrosComponent } from './body-datos-otros.component';

describe('BodyDatosOtrosComponent', () => {
  let component: BodyDatosOtrosComponent;
  let fixture: ComponentFixture<BodyDatosOtrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyDatosOtrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyDatosOtrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
