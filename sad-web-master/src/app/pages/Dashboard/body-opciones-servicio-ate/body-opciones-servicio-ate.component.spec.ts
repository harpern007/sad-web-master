import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyOpcionesServicioAteComponent } from './body-opciones-servicio-ate.component';

describe('BodyOpcionesServicioAteComponent', () => {
  let component: BodyOpcionesServicioAteComponent;
  let fixture: ComponentFixture<BodyOpcionesServicioAteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyOpcionesServicioAteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyOpcionesServicioAteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
