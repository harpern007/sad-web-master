import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FurSeguimientoService } from 'src/app/services/fur-seguimiento..service';
import { FurCabSeguimiento } from 'src/app/models/FurCabSeguimiento';
import { GlobalVars, NumIdEstadoRegistrado, NumIdEstadoAsignado, NumIdCanalExp, NumCanalFormWeb, NumIdCanalCorreo, NumIdCanalCarta, NumIdCanalReporte } from 'src/app/general/variables';
import { FurSeguimientoFiltro } from 'src/app/models/FurSeguimientoFiltro';


@Component({
  selector: 'app-body-opciones-servicio-ate',
  templateUrl: './body-opciones-servicio-ate.component.html',
  styleUrls: ['./body-opciones-servicio-ate.component.css']
})

export class BodyOpcionesServicioAteComponent implements OnInit {

    //@Input() ipCodDep: string;
  //strCodDep:string;
  furCabSeguimiento : FurCabSeguimiento = new FurCabSeguimiento();
  @Output() opOpcionSeleccionada = new EventEmitter();
  
  idCaso:number = 0;

  //CUS05
  furSeguimientoFUR : FurSeguimientoFiltro = new FurSeguimientoFiltro();

  constructor(protected furSeguimientoService: FurSeguimientoService) { }

  ngOnInit() {

    // this.furSeguimientoService.ObtenerCantidadFurDashboard(this.ipCodDep).subscribe(
    //   data => {

    //     this.furCabSeguimiento = data;

    //   });

  }

  // AbrirExpediente(idCaso:number)
  // {

    

  // }

  _cambiarStyleOpcion(strIdDiv:string,flgReset:boolean)
  {

    var strFontWeight = (flgReset ? "":"bold");
    var strFontSize = (flgReset ? "":"large");
    var strBackgroundColor = (flgReset ? "":"beige");

    document.getElementById(strIdDiv).style.fontWeight = strFontWeight;
    document.getElementById(strIdDiv).style.fontSize = strFontSize;
    document.getElementById(strIdDiv).style.backgroundColor = strBackgroundColor;
  }

  

  BuscarDocumento(idCaso:number)
  {

    // var idDiv = "divOpcion";
    
    // this.idCaso = idCaso;

    this.opOpcionSeleccionada.emit(idCaso);

    this.styleBuscarDocumento(idCaso);

    // for (let index = 1; index <= 4; index++) {

    //   this._cambiarStyleOpcion(idDiv + index,true);
    // }

    // this._cambiarStyleOpcion(idDiv + idCaso.toString(),false);

    
  }

  styleBuscarDocumento(idCaso:number)
  {

    var idDiv = "divOpcion";
    
    this.idCaso = idCaso;

    //this.opOpcionSeleccionada.emit(idCaso);

    for (let index = 1; index <= 4; index++) {

      this._cambiarStyleOpcion(idDiv + index,true);
    }

    this._cambiarStyleOpcion(idDiv + idCaso.toString(),false);

    
  }

  ConsultarFurSeguimiento()
  {

    this._ComputarCantidadOpc(NumIdCanalExp);
    this._ComputarCantidadOpc(NumCanalFormWeb);
    this._ComputarCantidadOpc(NumIdCanalCorreo);
    this._ComputarCantidadOpc(NumIdCanalCarta);
    this._ComputarCantidadOpc(NumIdCanalReporte);

    console.log("Este es el seguimiento fur de FUR");
    console.log(GlobalVars.CodPersSedeDpto);
       //CUS05
    this.furSeguimientoFUR.strFechaIni = "01/01/2019";
    this.furSeguimientoFUR.strFechaFin = "01/01/2100";

    this.furSeguimientoFUR.cfurCdepent =  GlobalVars.CodPersSedeDpto;
    this.furSeguimientoFUR.cfurCodtipo = 'FUR';
    this.furSeguimientoFUR.nfurIdcanal = null;
    console.log(this.furSeguimientoFUR);
    console.log("Codigo de departamento");
    console.log( GlobalVars.CodPersSedeDpto);
    this.furSeguimientoService.obtenerCantidadFurDashboard(GlobalVars.CodPersSedeDpto).subscribe(data=>{
      console.log(data);
      if(data != null){
        this.furCabSeguimiento.cantFur = data;
      }
    })
    // var idEstadoFiltro = null;
    // var furSeguimientoFiltro:FurSeguimientoFiltro = new FurSeguimientoFiltro();

    // console.log("===========strCodDep");
    // console.log(strCodDep);

    // furSeguimientoFiltro.nfurIdcanal = NumIdCanalExp;

    // if(GlobalVars.FlgSupervisor)
    // {

    //   idEstadoFiltro = NumIdEstadoRegistrado;
    //   furSeguimientoFiltro.cfurIdususuperv = GlobalVars.CodPersSuperv;

    // }else if(GlobalVars.FlgGestor)
    // {

    //   idEstadoFiltro = NumIdEstadoAsignado;
    //   furSeguimientoFiltro.cfurIdusuasig = GlobalVars.CodPersonal;

    // }

    // furSeguimientoFiltro.nfurIdestado = idEstadoFiltro;

    // this.furSeguimientoService.ListarGCSeguimiento(furSeguimientoFiltro).subscribe(
    //   data => {
    //     this.furCabSeguimiento.cantExpediente = data.length;
    //   });

    // this.furSeguimientoService.ObtenerCantidadFurDashboard(strCodDep).subscribe(
    //   data => {

    //     console.log("===========data");
    //     console.log(data);

    //     this.furCabSeguimiento< = data;

    //   });

  }

  _ComputarCantidadOpc(idCanal:number)
  {

    var idEstadoFiltro = null;
    var numCantidad = 0 ;
    var furSeguimientoFiltro:FurSeguimientoFiltro = new FurSeguimientoFiltro();
    var flgFormWeb = (idCanal == NumCanalFormWeb ? true : false);
    var flgExpediente = (idCanal == NumIdCanalExp ? true : false);
    var flgOtros = (idCanal == NumIdCanalCorreo || idCanal == NumIdCanalCarta || idCanal == NumIdCanalReporte ? true : false);
    
    furSeguimientoFiltro.nfurIdcanal = idCanal;
    furSeguimientoFiltro.strFechaIni = "01/01/2019";
    furSeguimientoFiltro.strFechaFin = "01/01/2100";

    this.furCabSeguimiento.cantOtros = 0;

    if(GlobalVars.FlgSupervisor)
    {

      idEstadoFiltro = NumIdEstadoRegistrado;

      if(flgFormWeb)
      {

        furSeguimientoFiltro.cfurCdepent = GlobalVars.CodPersSedeDpto;

      }else{

        furSeguimientoFiltro.cfurIdususuperv = GlobalVars.CodPersSuperv;

      }

    }else if(GlobalVars.FlgGestor)
    {

      idEstadoFiltro = NumIdEstadoAsignado;
      furSeguimientoFiltro.cfurIdusuasig = GlobalVars.CodPersonal;

    }

    furSeguimientoFiltro.nfurIdestado = idEstadoFiltro;
 
    
    console.log("=============furSeguimientoFiltro");
    console.log(furSeguimientoFiltro);

    this.furSeguimientoService.ListarGCSeguimiento(furSeguimientoFiltro).subscribe(
      data => {
          console.log(data);
        if(data != null)
        {

          numCantidad = data.length;

        }
       

        if(flgExpediente)
        {

          this.furCabSeguimiento.cantExpediente = numCantidad;

        }else if(flgFormWeb)
        {

          this.furCabSeguimiento.cantFormWeb = numCantidad;

        }else if(flgOtros)
        {

          this.furCabSeguimiento.cantOtros = this.furCabSeguimiento.cantOtros + numCantidad;

        }
        
          
      });

      
  }

  Orientar_Atender_Solicitud(){
    this.styleBuscarDocumento(1);    
    this.opOpcionSeleccionada.emit(1);
    this.styleBuscarDocumento(1);
  }

  Revisar_Oficio(){
    this.styleBuscarDocumento(2);
    this.opOpcionSeleccionada.emit(2);
    this.styleBuscarDocumento(2);
  }

  Aprobar_Oficio(){
    this.styleBuscarDocumento(3);
    this.opOpcionSeleccionada.emit(3);
    this.styleBuscarDocumento(3);
  }

  Generar_Reporte(){
    this.styleBuscarDocumento(4);
    this.opOpcionSeleccionada.emit(4);
    this.styleBuscarDocumento(4);
  }

}
