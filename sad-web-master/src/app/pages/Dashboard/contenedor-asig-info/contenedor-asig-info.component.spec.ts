import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorAsigInfoComponent } from './contenedor-asig-info.component';

describe('ContenedorAsigInfoComponent', () => {
  let component: ContenedorAsigInfoComponent;
  let fixture: ComponentFixture<ContenedorAsigInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContenedorAsigInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContenedorAsigInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
