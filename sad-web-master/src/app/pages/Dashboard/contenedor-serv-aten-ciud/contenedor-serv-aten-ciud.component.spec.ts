import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorServAtenCiudComponent } from './contenedor-serv-aten-ciud.component';

describe('ContenedorServAtenCiudComponent', () => {
  let component: ContenedorServAtenCiudComponent;
  let fixture: ComponentFixture<ContenedorServAtenCiudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContenedorServAtenCiudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContenedorServAtenCiudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
