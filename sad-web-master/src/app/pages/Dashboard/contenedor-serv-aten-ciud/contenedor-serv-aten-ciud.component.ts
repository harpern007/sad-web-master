import { Component, OnInit, ViewChild } from '@angular/core';
//import { BodyOpcionesComponent } from '../body-opciones/body-opciones.component';
import { BodyOpcionesServicioAteComponent } from '../body-opciones-servicio-ate/body-opciones-servicio-ate.component';
import { BodyBusquedaComponent } from '../body-busqueda/body-busqueda.component';
import { BodyRegistroComponent } from '../body-registro/body-registro.component';
import { GlobalVars } from 'src/app/general/variables';
import { AsignarInformacionComponent } from '../../asignar-informacion/asignar-informacion.component';
import { EvalinfRegfurComponent } from '../../evalinf-regfur/evalinf-regfur.component';


@Component({
  selector: 'app-contenedor-serv-aten-ciud',
  templateUrl: './contenedor-serv-aten-ciud.component.html',
  styleUrls: ['./contenedor-serv-aten-ciud.component.css']
})
export class ContenedorServAtenCiudComponent implements OnInit {

  //strCodDepUsu:string = "15";//Provisioal hasta definir desde donde se consumira los adatos del usuario, servicios seguridad CGR o BD SAD
  flgOrientarAtender = true;
  flgMostrarOpciones = true;
  flgRevisarOficio = false;
  flgAprobarOficio = false;
  flgGenerarReporte = false;
  idOpcionSeleccionado:number = 1;

  @ViewChild(BodyOpcionesServicioAteComponent) bodyOpcionesC : BodyOpcionesServicioAteComponent;
  @ViewChild(BodyBusquedaComponent) bodyBusquedaC : BodyBusquedaComponent;
  @ViewChild(BodyRegistroComponent) bodyRegistroC : BodyRegistroComponent;
  @ViewChild(AsignarInformacionComponent) asignarInformacionC : AsignarInformacionComponent;
  @ViewChild(EvalinfRegfurComponent) evalinfRegfurC : EvalinfRegfurComponent;

  constructor() { }

  ngOnInit() {

    //var codDep = GlobalVars.CodPersSedeDpto;

    //GlobalVars.CodDep = codDep;

    setTimeout(() => {

      this.bodyOpcionesC.ConsultarFurSeguimiento();
      this.bodyOpcionesC.BuscarDocumento(this.idOpcionSeleccionado);

      //this.bodyBusquedaC.MostrarEtiquetas(this.idOpcionSeleccionado,true);
      
    }, 1);

    //OJO PARA IDENTIFICAR AL SUPERVISOR

    //this.bodyBusquedaC.flgSoloLectura = !GlobalVars.FlgSupervisor

  }


  OpcionSeleccionada(idOpcion)
  {

    this.idOpcionSeleccionado = idOpcion;

    if(this.bodyBusquedaC != null)
    {

      this.bodyBusquedaC.MostrarEtiquetas(this.idOpcionSeleccionado,true);
      this.bodyOpcionesC.styleBuscarDocumento(this.idOpcionSeleccionado);
      //this.bodyOpcionesC.BuscarDocumento(this.idOpcionSeleccionado);

    }

  }

  NuevoSeleccionado(idFur:number)//click a boton nuevo desde body busqueda
  {

    this._mostrarRegistro(idFur,false);


  }

  _mostrarRegistro(idFur:number,flgSoloLectura:boolean)
  {
    // console.log("=============idFur");
    // console.log(idFur);

    //this.flgMostrarRegistro = true; // true
    this.flgMostrarOpciones = false;

    //console.log("=============flgSeleccion1");

    setTimeout(() => {

      this.bodyRegistroC.MostrarRegistro(this.idOpcionSeleccionado,idFur,flgSoloLectura);

      
      
    }, 1);
  }

  verSeleccionado(idFur:number)//click a boton nuevo desde body busqueda
  {

    this._mostrarRegistro(idFur,true);


  }

  asignarSeleccionado(idFur:number)//click a boton nuevo desde body busqueda
  {

    //this.flgMostrarAsignar = true;
    this.flgMostrarOpciones = false;

    setTimeout(() => {

      this.asignarInformacionC.mostrarRegistro(idFur);

    }, 1);

    

    //this._mostrarRegistro(idFur,true);


  }

  evaluarSeleccionado(idFur:number)//click a boton nuevo desde body busqueda
  {

    //this.flgMostrarEvaluar = true;
    this.flgMostrarOpciones = false;

    setTimeout(() => {

      this.evalinfRegfurC.mostrarRegistro(idFur);

    }, 1);

    

    //this._mostrarRegistro(idFur,true);


  }

  retornaEvaluar() // Click a guardar o cancelar del body registro
  {

    //this.flgMostrarEvaluar = false;//false
    this.flgMostrarOpciones = true;

    this._resetPanelPrincipal();


  }

  _resetPanelPrincipal()
  {
    setTimeout(() => {

      this.OpcionSeleccionada(this.idOpcionSeleccionado);
      this.bodyOpcionesC.ConsultarFurSeguimiento();
      
    }, 1);
  }

  retornaAsignar() // Click a guardar o cancelar del body registro
  {

    //this.flgMostrarAsignar = false;//false
    this.flgMostrarOpciones = true;

    this._resetPanelPrincipal();

    // setTimeout(() => {

    //   this.OpcionSeleccionada(this.idOpcionSeleccionado);
    //   this.bodyOpcionesC.ConsultarFurSeguimiento();
      
    // }, 1);

  }

  RetornaRegistro(flgRetorno) // Click a guardar o cancelar del body registro
  {

    //this.flgMostrarRegistro = flgRetorno;//false
    this.flgMostrarOpciones = !flgRetorno;

    this._resetPanelPrincipal();

    // setTimeout(() => {

    //   this.OpcionSeleccionada(this.idOpcionSeleccionado);
    //   this.bodyOpcionesC.ConsultarFurSeguimiento();
      
    // }, 1);

  }

  fnOrientarAtender(){

    this.flgOrientarAtender = true;
    this.flgMostrarOpciones = true;

    this._resetPanelPrincipal();

  }
}
