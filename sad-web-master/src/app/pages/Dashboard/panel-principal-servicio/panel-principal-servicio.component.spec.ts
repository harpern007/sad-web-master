import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelPrincipalServicioComponent } from './panel-principal-servicio.component';

describe('PanelPrincipalServicioComponent', () => {
  let component: PanelPrincipalServicioComponent;
  let fixture: ComponentFixture<PanelPrincipalServicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelPrincipalServicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelPrincipalServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
