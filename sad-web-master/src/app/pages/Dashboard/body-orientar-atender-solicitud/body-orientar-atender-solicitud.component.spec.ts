import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyOrientarAtenderSolicitudComponent } from './body-orientar-atender-solicitud.component';

describe('BodyOrientarAtenderSolicitudComponent', () => {
  let component: BodyOrientarAtenderSolicitudComponent;
  let fixture: ComponentFixture<BodyOrientarAtenderSolicitudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyOrientarAtenderSolicitudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyOrientarAtenderSolicitudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
