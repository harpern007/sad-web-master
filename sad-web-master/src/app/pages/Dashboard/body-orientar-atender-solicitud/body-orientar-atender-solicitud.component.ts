import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { BodyDatosExpedienteComponent } from '../body-datos-expediente/body-datos-expediente.component';
import { RegistroDocumentoComponent } from '../../registro-documento/registro-documento.component';
import { FurService } from 'src/app/services/fur.service';
import { EstadoService } from 'src/app/services/estado.service';
import { CanalService } from 'src/app/services/canal.service';
import { PersonaReniecService } from 'src/app/services/persona-reniec.service';
import { Fur } from 'src/app/models/Fur';
import { ContenedorWebComponent } from '../../formulario-web/contenedor-web/contenedor-web.component';
import { Router, ActivatedRoute } from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Canal } from '../../../models/Canal';
import { Estado } from '../../../models/Estado';
import { PersonaReniec } from '../../../models/PersonaReniec';

@Component({
  selector: 'app-body-orientar-atender-solicitud',
  templateUrl: './body-orientar-atender-solicitud.component.html',
  styleUrls: ['./body-orientar-atender-solicitud.component.css']
})
export class BodyOrientarAtenderSolicitudComponent implements OnInit {

  strDNI:string = ""
  strNroOrientacion:string = "";
  strFechaRegistro:string = "";
  strHoraInicio:string = "";
  strHoraFin:string = "";

  numIdCanal:number = null;
  numIdEstado:number = null;
  objPersonaReniec:PersonaReniec;

  listCanal:Canal[] = [];
  listEstado:Estado[] = [];

  flgMostrarRegExp = false;
  flgMostrarRegOtr = false;
  flgSoloLectura = false;
  flgMostrarFormWeb = false;

  @ViewChild(BodyDatosExpedienteComponent) bodyDatosExpedienteC : BodyDatosExpedienteComponent;
  @ViewChild(RegistroDocumentoComponent) registroDocumentoC : RegistroDocumentoComponent;
  @ViewChild(ContenedorWebComponent) contenedorWebC : ContenedorWebComponent;

  @Output() opRetornaRegistro = new EventEmitter();
  @Output() opOrientaAtiende = new EventEmitter();

  matTableColumnslistaAtencionDef = ["item","tipoOrientacion","fchRegistro","estado","entidad","nroOficio","codFUR"];
  matTableColumnslistaDenunciaDef = ["item","tipoOrientacion","fchRegistro","estado","entidad","nroOficio",
  "anexCargo","fchRecepcion"];
  matTableColumnsdenunciasInvoDef = ["item","descripcion","fchRegistro","estado","entidad"];
  matTableColumnsdocumEmitidosDef = ["item","nroDocumento","nroProyecto","tipoDocumento","tipoDestinatario","fchEmision",
  "medComunicacion","fchNotificacion","cargo"];
  
  dataSource: MatTableDataSource<Fur>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(protected furService:FurService,
    public router:Router,
    private route: ActivatedRoute,
    protected canalService:CanalService,
    protected estadoService:EstadoService,
    protected personaReniecService:PersonaReniecService) { }

  ngOnInit() {

    this.canalService.listarCanalServicioAtencion().subscribe(data=>{

      this.listCanal = data;
      console.log(this.listCanal);

    });

    this.estadoService.listarEstadoPorFicha('SAC').subscribe(data=>{

      this.listEstado = data;
      console.log(this.listEstado);

    });

  }

  _Registrar()
  {

    if(this.flgMostrarRegExp)
    {

      if(!this.bodyDatosExpedienteC.ValidarDatos())
      {

        // this.bodyDatosExpedienteC.GuardarDatos().subscribe({

        //   //complete() { this.EmitirDato(); }
        //   complete() { 
        //     console.log("===========data"); 
        //     console.log(this); 
        //   }
  
        // });

        this.bodyDatosExpedienteC.GuardarDatos().subscribe({
          complete(){}
        });
       

      }

      // (new Observable(this.bodyDatosExpedienteC.GuardarDatos)).subscribe({

      //   complete() { this.EmitirDato(); }//para retornar a la interfaz anterior

      //   }
        
      // );

    }else if(this.flgMostrarRegOtr)
    {

      if(!this.registroDocumentoC.ValidarDatos())
      {

        // this.registroDocumentoC.GuardarDatos().subscribe({

        //   complete() { this.EmitirDato(); }
  
        // });

        this.registroDocumentoC.GuardarDatos().subscribe({
          complete(){}
        });

      }


    }

    //this.EmitirDato();

  }

  _GuardadoExpediente()
  {

    //console.log("=====emitir1");

    this.EmitirDato();

  }

  _GuardadoDocumento()
  {

    //console.log("=====emitir1");

    this.EmitirDato();

  }

  _Cancelar()
  {

    this.EmitirDato();

  }

  EmitirDato()
  {

    this.opRetornaRegistro.emit(false);

  }

  MostrarRegistro(idOpcion:number,idFur:number,flgSoloLectura:boolean){

    console.log("=================idOpcion");
    console.log(idOpcion);

    console.log("=============idFur");
    console.log(idFur);

    this.flgSoloLectura = flgSoloLectura;

    // var listaExpediente;
    // var furAux:Fur = new Fur();
    // var idFurStr;

    this.flgMostrarRegExp = (idOpcion == 1?true:false);
    this.flgMostrarRegOtr = (idOpcion == 3?true:false);
    this.flgMostrarFormWeb = (idOpcion == 2?true:false);

    if(idFur == 0) {return;}

    this.furService.ObtenerFurPorId(idFur).subscribe(data=>{

      // listaExpediente = data.listaExpedienteDet;
      // idFurStr = data.nfurId.toString();

      // furAux.cfurNroexpediente = data.cfurNroexpediente;//para fragmentar el num_doc_exp
      // furAux.strDfurFecinicioreg = data.strDfurFecinicioreg;
      // furAux.strDfurFecregistro = data.strDfurFecregistro;

      if(this.flgMostrarRegExp)
      {

        this.bodyDatosExpedienteC.SetDatos(data);
        this.bodyDatosExpedienteC.soloLectura(flgSoloLectura);
        

        // this.bodyDatosExpedienteC.strCodUno = data.cfurCoduno;
        // this.bodyDatosExpedienteC.numIdTipoDoc = data.nfurIdtipodocexp;
        // this.bodyDatosExpedienteC.strNroDoc1 = furAux.num_doc_exp;
        // this.bodyDatosExpedienteC.strNroDoc2 = furAux.num_doc_exp2;
        // this.bodyDatosExpedienteC.strNroDoc3 = furAux.num_doc_exp3;
        // this.bodyDatosExpedienteC.strDscAsunto = data.cfurDschecho;

        // if(listaExpediente != null)
        // {

        //   this.bodyDatosExpedienteC.listaExpRef = data.listaExpedienteDet.filter(x=>x.cexpdetCodtipo == CodTipoExpDetREF);
        //   this.bodyDatosExpedienteC.listaExpDest = data.listaExpedienteDet.filter(x=>x.cexpdetCodtipo == CodTipoExpDetDES);

        // }

        // this.bodyDatosExpedienteC.bodyDatosGeneralC.strIdExpediente = idFurStr;
        // this.bodyDatosExpedienteC.bodyDatosGeneralC.strFechaRegistro = furAux.fch_inicio_reg;
        // this.bodyDatosExpedienteC.bodyDatosGeneralC.strHoraReg = furAux.hora_inicio_reg;
        // this.bodyDatosExpedienteC.bodyDatosGeneralC.numCantTiempo = data.nfurNumplazo;
        // this.bodyDatosExpedienteC.bodyDatosGeneralC.numIdPlazo = data.nfurIdplazo;

      }else if(this.flgMostrarRegOtr)
      {

        this.registroDocumentoC.SetDatos(data);
        this.registroDocumentoC.soloLectura(flgSoloLectura);

      }else if(this.flgMostrarFormWeb)
      {

        setTimeout(() => {

          this.contenedorWebC.verFormularioWeb(data);
          
        }, 0);
        
  
      }

    });

  }

}
