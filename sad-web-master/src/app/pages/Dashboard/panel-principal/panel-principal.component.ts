import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalVars, cookieGlobalVars } from 'src/app/general/variables';
//import * as CryptoJS from 'crypto-js';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-panel-principal',
  templateUrl: './panel-principal.component.html',
  styleUrls: ['./panel-principal.component.css']
})
export class PanelPrincipalComponent implements OnInit {

  mostrarOri = false;
  mostrarSeg = true;
  //Cambio de Valor mostrarSeg
  //mostrarSeg = false;
  usuario: string;
  flagMenu : boolean = true;

  //mostrar Menu Servicio Atencion Al Ciudadano
  //Variable Agregada Al : 06/04/2020 ---//
  mostrarAte = false;
  //-------------------------------------//
  constructor(private route: ActivatedRoute,
    public router:Router,
    private cookieService: CookieService
    ) { }

  ngOnInit() {

    //this.usuario = this.route.snapshot.params.usuario;
    

    cookieGlobalVars(2,this.cookieService);
    
    //this.usuario = CryptoJS.AES.decrypt(this.route.snapshot.params.usuario, GlobalVars.DscContrasenha).toString(CryptoJS.enc.Utf8);

    this.usuario = GlobalVars.DscNombreComplPers;
    

    console.log("==========Globalvars");
    console.log(GlobalVars.CodUsuario);

    //console.log(this.usuario);

    // this.route.queryParams
    //   .subscribe(params => {
    //     // Defaults to 0 if no query param provided.
        
    //     this.usuario = params['usuario'];

    //     console.log("=================this.usuario");

    //     console.log(this.usuario);

    //     console.log(params);
    //   });

  }

  MostrarOrientacion(){

    this.mostrarOri = true;

  }

  MostrarSeguimiento(){

    this.mostrarSeg = true;

    //this.router.navigate(['/BodyOpciones/']);

  }

  //Funcion Agregada Al : 06/04/2020 ---//

  MostrarAtencion(){

    this.mostrarAte = true;

  }
  //------------06-04-2020----------------------//

  EvaluarInformacion(){
    
  }

  openNav() {
    if(this.flagMenu){
     document.getElementById("mySidenav").style.width = "250px";
     this.flagMenu = false;
   }else{
     document.getElementById("mySidenav").style.width = "0px";
     this.flagMenu = true;
   }
   
   //document.getElementById("main").style.marginLeft = "350px";
   }

}
