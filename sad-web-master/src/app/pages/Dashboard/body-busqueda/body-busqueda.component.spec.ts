import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyBusquedaComponent } from './body-busqueda.component';

describe('BodyBusquedaComponent', () => {
  let component: BodyBusquedaComponent;
  let fixture: ComponentFixture<BodyBusquedaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyBusquedaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyBusquedaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
