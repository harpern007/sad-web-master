import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { AnhoService } from 'src/app/services/Anho.service';
import { Anho } from 'src/app/models/Anho';
import { FurSeguimientoService } from 'src/app/services/fur-seguimiento..service';
import { FurSeguimientoFiltro } from 'src/app/models/FurSeguimientoFiltro';
import { GetAnho, GlobalVars, NumIdEstadoRegistrado, NumCanalFormWeb, NumIdEstadoAsignado,  NumIdCanalExp, iconPanelContraer, NumIdCanalCarta, NumIdCanalCorreo, NumIdCanalReporte, NumIdEstadoHeEvaluacionSuperv, NumIdEstadoHeEvaluacionObservado, DscTituloPaginator } from 'src/app/general/variables';
import { Fur } from 'src/app/models/Fur';
import { Router, ActivatedRoute } from '@angular/router';
import { CanalService } from 'src/app/services/canal.service';
import { Canal } from 'src/app/models/Canal';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { Estado } from 'src/app/models/Estado';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-body-busqueda',
  templateUrl: './body-busqueda.component.html',
  styleUrls: ['./body-busqueda.component.css']
})
export class BodyBusquedaComponent implements OnInit {

  //general : General;
  strEtiquetaBusq:string;
  strEtiquetaResult:string;
  strEtiquetaRadioNoPen:string;
  strEtiquetaRadioPen:string;
  listAnho:Anho[] = [];
  idCanal:number = 3;
  flgMostrarResultado = false;
  //codDep:string;
  listFurSeguimiento : Fur[] = [];
  flgMostrarNuevo = false;
  strAnho:string = GetAnho().toString();
  strParametro:string;
  flgRadio = true;//false = pendiente
  @Output() opNuevoSeleccionado  = new EventEmitter();
  @Output() opVerSeleccionado  = new EventEmitter();
  @Output() opAsignarSeleccionado  = new EventEmitter();
  @Output() opEvaluarSeleccionado  = new EventEmitter();
  
  furSeguimientoFiltro:FurSeguimientoFiltro;
  idOpcionSelec:number;
  listCanal:Canal[] = [];
  listEstado:Estado[] = [];
  numIdTipoDocCan:number = 0;
  //flgDeshabilitarAsig = false;
  //flgDeshabilitarEdit = false;
  flgBuscarSoloReg = false;
  flgMostrarEditar = false;
  flgMostrarAsig = false;
  flgMostrarAten = false;
  flgMostrarVer = false;
  flgMostrarTipoDoc = false;
  flgContraerPanel = false;
  strIconPanel = "";
  /**Cus 06 */
  flgMostrarEval = false;
  /**/
  numIdEstado:number = (GlobalVars.FlgGestor ? NumIdEstadoAsignado : NumIdEstadoRegistrado);


  
  displayedColumns:string[] = [];
  // displayedColumns: string[] = [];
  
  strDscColBtnAsign = "btnAsignar";
  strDscColBtnEdit = "btnEditar";
  strDscColBtnVer = "btnVer";
  strDscColBtnAten = "btnAtender";
  strDscColBtnEval = "btnEvaluar"

  matTableColumnsDef = ["item","codigo", "expediente" ,"fchRecepcion","fchRegistro","estado","dscAsunto","dscEntidad"];
  dataSource: MatTableDataSource<Fur>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

 
  //flgMostrarSoloReg = false;
//  flgSupervisor = GlobalVars.FlgSupervisor;


  // formBusqueda = new FormGroup({
  //   input_param : new FormControl(''),
  //   select_anho : new FormControl(''),

  // });
  
  constructor(protected anhoService: AnhoService,
    protected furSeguimientoService:FurSeguimientoService,
    public router:Router,
    private route: ActivatedRoute,
    protected canalService:CanalService,
    protected maestraService:MaestrasService
    ) { 

      this.dataSource = new MatTableDataSource();

      //this.displayedColumns = this.displayedColumnsDef;

     
    }

  ngOnInit() {

    this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;

   
    this.flgMostrarResultado = false;

    //this.BuscarAsigSI();

    this.anhoService.listarAnho().subscribe(
      data => {
        this.listAnho = data;
      });

    this.canalService.listarCanalDocumento().subscribe(data=>{

      this.listCanal = data;

    });

    this.maestraService.listarEstadoPorFicha("FUR").subscribe(data=>{

      this.listEstado = data;

    });

    //this.iconPanel = iconPanelContraer(this.flgContraerPanel);
    this._panelFiltro();

    //iconPanelContraer(this.flgContraerPanel);
  
  }

  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  //   this.dataSource.sort = this.sort;
  // }

  _panelFiltro(){

    this.flgContraerPanel = !this.flgContraerPanel;

    this.strIconPanel = iconPanelContraer(this.flgContraerPanel);

    // console.log("=================this.strIconPanel");
    // console.log(this.strIconPanel);

    //return iconPanelContraer(this.flgContraerPanel);

  }

  

  _checkSoloReg(event:any)
  {

    // console.log("==========event.target.checked");
    // console.log(event.target.checked);

    this.flgBuscarSoloReg = event.target.checked;

    //this.MostrarEtiquetas(this.idOpcionSelec,false);
    this._resetBusqueda();

  }

  _cambioAnho()
  {

    this._resetBusqueda();
    
  }

  _cambioTipoDoc()
  {

    this._resetBusqueda();

  }

  _cambioEstado()
  {

    this._resetBusqueda();

  }

  _resetBusqueda()
  {

    this.MostrarEtiquetas(this.idOpcionSelec,false);

  }

  _aplicaColorGrilla(index:number){

    // console.log("========================index");
    // console.log(index);

    return (index%2) ? true : false;

  }

  // _CambioSoloReg()
  // {

  // }

  MostrarEtiquetas(idOpcion:number,flgOpcionSelec:boolean){

    var dscEtiqueta = "";
    var dscEtiqueta2 = "";
    var vFlgMostrarEdit = false;
    var vFlgMostrarNuevo = false;
    var vflgFiltroReg = this._recupFlgFiltroEstReg();
    var vflgFiltroAsig = this._recupFlgFiltroEstAsig();
    
    //var vFlgMostrarAten = false;

    this.flgMostrarTipoDoc = false;

    //this.flgBuscarSoloReg = (flgOpcionSelec ? false : this.flgBuscarSoloReg);//SI ESTA FUNCION SE EJECUTA DESDE EL BODYOPCIONES

    this.idOpcionSelec = idOpcion;
    
    //this.codDep = codDep;//this.general.dscCampo1;
    //this.flgMostrarSoloReg = false;
    //this.flgMostrarNuevo = false;
    //this.flgRadio = true;//siempre muestra pendientes
    this.listFurSeguimiento = [];
    this.dataSource = new MatTableDataSource();
    //this.flgDeshabilitarAsig = (GlobalVars.FlgOtroUsuario ? true : !GlobalVars.FlgSupervisor);//APLICA PARA SUPERV
    //this.flgMostrarAsig = (GlobalVars.FlgOtroUsuario || !this.flgRadio ? false : GlobalVars.FlgSupervisor);//APLICA PARA SUPERV

    //this.flgMostrarAsig = (GlobalVars.FlgOtroUsuario || vflgFiltroAsig ? false : GlobalVars.FlgSupervisor);//APLICA PARA SUPERV
    this.flgMostrarAsig = (vflgFiltroReg && GlobalVars.FlgSupervisor ? true : false);//APLICA PARA SUPERV

    //vFlgMostrarEdit = (GlobalVars.FlgOtroUsuario || !this.flgBuscarSoloReg ? false : (GlobalVars.FlgGestor || GlobalVars.FlgSupervisor ? true : false));//;//APLICA PARA GESTOR
    //vFlgMostrarEdit = (GlobalVars.FlgOtroUsuario ? false : (GlobalVars.FlgGestor || GlobalVars.FlgSupervisor ? true : false));//;//APLICA PARA GESTOR
    vFlgMostrarEdit = vflgFiltroReg;

    //this.flgMostrarAten = (this.flgBuscarSoloReg ? false : GlobalVars.FlgGestor);
    //this.flgMostrarAten = GlobalVars.FlgGestor;
    //this.flgMostrarAten = (vflgFiltroReg ? false : GlobalVars.FlgGestor); // NO APLICA ATENCION CUANDO EL FILTRO DE ESTADO ES REG
    this.flgMostrarAten = (vflgFiltroAsig && GlobalVars.FlgGestor ? true : false); // NO APLICA ATENCION CUANDO EL FILTRO DE ESTADO ES REG

    // console.log("===========GlobalVars.FlgOtroUsuario");
    // console.log(GlobalVars.FlgOtroUsuario);
    // console.log("============GlobalVars.FlgGestor");
    // console.log(GlobalVars.FlgGestor);
    // console.log("============this.flgBuscarSoloReg");
    // console.log(this.flgBuscarSoloReg);

    // console.log("============GlobalVarsFlgSupervisor");
    // console.log(GlobalVars.FlgSupervisor);

    dscEtiqueta = (GlobalVars.FlgGestor ? "Atendidos":"Asignados");
    dscEtiqueta2 = (GlobalVars.FlgGestor ? "Atención":"Asignación");

    switch (idOpcion) {

      case 1:

        this.strEtiquetaBusq = "Expediente";
        this.strEtiquetaResult = "Expedientes";

        this.idCanal = NumIdCanalExp;
        vFlgMostrarNuevo = true;
        //this.flgMostrarNuevo = true;
        //this.flgMostrarSoloReg = true;
                
        break;

      case 2:

        this.strEtiquetaBusq = "Formulario Web";
        this.strEtiquetaResult = "Formularios Web";
        // this.strEtiquetaRadioNoPen = "Formularios Web Asignados";
        // this.strEtiquetaRadioPen = "Formularios Web Pendientes de Asignación";
        this.idCanal = NumCanalFormWeb;
        vFlgMostrarEdit = false;
        //this.flgMostrarSoloReg = false;
      
        break;

      case 3:

        this.strEtiquetaBusq = "Documento";
        this.strEtiquetaResult = "Documentos";
        // this.strEtiquetaRadioNoPen = "Documentos Asignados";
        // this.strEtiquetaRadioPen = "Documentos Pendientes de Asignación";
        this.idCanal = 0;
        //this.flgMostrarNuevo = true;
        vFlgMostrarNuevo = true;
        this.flgMostrarTipoDoc = true
      
        break;

      case 4:

        this.strEtiquetaBusq = "FUR";
        this.strEtiquetaResult = "FUR's";
        // this.strEtiquetaRadioNoPen = "FUR's Asignados";
        // this.strEtiquetaRadioPen = "FUR's Pendientes de Asignación";
        this.idCanal = 99;
        //this.idCanal = null;
        vFlgMostrarEdit = false;
      
        break;
  
      default:
        break;
    }

    this.flgMostrarEditar = vFlgMostrarEdit;

    this.flgMostrarNuevo = (GlobalVars.FlgOtroUsuario ? false : vFlgMostrarNuevo);
    this.flgMostrarVer = GlobalVars.FlgOtroUsuario;

    this.strEtiquetaRadioNoPen = `${this.strEtiquetaResult} ${dscEtiqueta}`;
    this.strEtiquetaRadioPen = `${this.strEtiquetaResult} Pendientes de ${dscEtiqueta2}`;

    
    console.log("?=??????===========ANTESSSSSSSS00000000000000");
    console.log(this.matTableColumnsDef);

    this.displayedColumns = [];

    this.matTableColumnsDef.forEach(element => {

      this.displayedColumns.push(element);
      
    });    

    if(this.flgMostrarAsig){this.displayedColumns.push(this.strDscColBtnAsign);}
    if(this.flgMostrarEditar){this.displayedColumns.push(this.strDscColBtnEdit);}
    if(this.flgMostrarAten){this.displayedColumns.push(this.strDscColBtnAten);}
    if(this.flgMostrarVer){this.displayedColumns.push(this.strDscColBtnVer);}
   
    console.log("?=??????===========ANTESSSSSSSS");
    console.log(this.displayedColumns);

    // this.displayedColumns.push("btnAsignar");

    // console.log("?=??????===========DESPUES");
    // console.log(this.displayedColumns);
    // console.log(this.matTableColumnsDef);

  }

  _recupFlgFiltroEstReg()
  {
    return (this.numIdEstado == NumIdEstadoRegistrado ? true : false);
  }

  _recupFlgFiltroEstAsig()
  {
    return (this.numIdEstado == NumIdEstadoAsignado ? true : false);
  }

  _cambioParametro(){

    this.Buscar();

  }

  _datetime(){
    //document.getElementById("datetimepicker1"). datetimepicker();
    
  }

  Buscar(){

    console.log("==========Buscar");

    this.listFurSeguimiento = [];

    var vParamAnho = this.strAnho;
    var vParamParametro = (this.strParametro == "" ? null : this.strParametro);
    //var idEstadoRadio = null;

    var vFchInicial = "01/01/" + vParamAnho;
    var vFchFinal = "31/12/" + vParamAnho;
    var vIdCanal = (this.idCanal == 0 ? this.numIdTipoDocCan : this.idCanal);
    var vflgFiltroAsignado = this._recupFlgFiltroEstAsig();
    var vflgFiltroReg = this._recupFlgFiltroEstReg();
    var vListFurSeguimiento : Fur[] = [];
    
    this.furSeguimientoFiltro = new FurSeguimientoFiltro();
    
    this.furSeguimientoFiltro.cfurCodigo = vParamParametro;
    
    this.furSeguimientoFiltro.strFechaIni = vFchInicial;
    this.furSeguimientoFiltro.strFechaFin = vFchFinal;
    this.furSeguimientoFiltro.nfurIdestado = this.numIdEstado;

    /** */

       /*CUS05*/
       let furSeguimientoFiltroFur = new FurSeguimientoFiltro();
       furSeguimientoFiltroFur.cfurCodigo = vParamParametro;
       furSeguimientoFiltroFur.strFechaIni = vFchInicial;
       furSeguimientoFiltroFur.strFechaFin = vFchFinal;
       furSeguimientoFiltroFur.nfurIdestado = this.numIdEstado;
       if(vIdCanal == 99){
         furSeguimientoFiltroFur.nfurIdcanal = null;
         furSeguimientoFiltroFur.cfurCdepent = GlobalVars.CodPersSedeDpto;
         furSeguimientoFiltroFur.cfurCodtipo = "FUR";
         if(this.numIdEstado == NumIdEstadoHeEvaluacionSuperv || this.numIdEstado == NumIdEstadoHeEvaluacionObservado){
          this.displayedColumns.push(this.strDscColBtnEval);
          
          //if(this.flgMostrarEval){this.displayedColumns.push(this.strDscColBtnEval);}
         }
       }
   

    // if(this.flgBuscarSoloReg)
    // {

    //   idEstadoRadio = NumIdEstadoRegistrado;

    // }else
    // {

    //   console.log("===========this.flgRadio");
    //   console.log(this.flgRadio);

    //   if(GlobalVars.FlgSupervisor)
    //   {

    //     idEstadoRadio = (this.flgRadio ? NumIdEstadoRegistrado : NumIdEstadoAsignado);

    //   }else if(GlobalVars.FlgGestor)
    //   {

    //     idEstadoRadio = (this.flgRadio ? NumIdEstadoAsignado : NumIdEstadoAtendido);

    //   }
     
    // }

    
    if(this.idCanal == NumCanalFormWeb)
    {

      // this.flgRadio true es pendientes

      this.furSeguimientoFiltro.cfurCdepent = (GlobalVars.FlgSupervisor ? GlobalVars.CodPersSedeDpto : null);
      // this.furSeguimientoFiltro.nfurIdestado = idEstadoRadio;
      
      this.furSeguimientoFiltro.cfurIdusuasig = (GlobalVars.FlgGestor ? GlobalVars.CodPersonal : null); // para CUS03
      //this.furSeguimientoFiltro.flgAsignado = (this.flgRadio ? null : !this.flgRadio);

    }else{

      // this.furSeguimientoFiltro.cfurIdususuperv = (!this.flgBuscarSoloReg && GlobalVars.FlgSupervisor ? GlobalVars.CodPersSuperv : null);// para CUS03
      // this.furSeguimientoFiltro.cfurIdusuasig = (!this.flgBuscarSoloReg && GlobalVars.FlgGestor ? GlobalVars.CodPersonal : null); // para CUS03
      // this.furSeguimientoFiltro.cfurIdusureg = (this.flgBuscarSoloReg ? GlobalVars.CodPersonal : null); //para cus02


      this.furSeguimientoFiltro.cfurIdususuperv = (GlobalVars.FlgSupervisor ? GlobalVars.CodPersSuperv : null);
      this.furSeguimientoFiltro.cfurIdusuasig = (GlobalVars.FlgGestor && vflgFiltroAsignado ? GlobalVars.CodPersonal : null); // para CUS03
      this.furSeguimientoFiltro.cfurIdusureg = (GlobalVars.FlgGestor && vflgFiltroReg ? GlobalVars.CodPersonal : null); //para cus02

      //this.furSeguimientoFiltro.nfurIdestado = idEstadoRadio;

      //this.furSeguimientoFiltro.nfurIdestado = (this.flgBuscarSoloReg || this.flgRadio ? NumIdEstadoRegistrado : null);//Para cus02

    }

    //this.furSeguimientoFiltro.flgAsignado = (this.flgBuscarSoloReg ? null : !this.flgRadio); // para cus03
    //this.furSeguimientoFiltro. FALTA USUARIO
    //this.furSeguimientoFiltro.cfurCdepent = (GlobalVars.FlgSupervisor  ? GlobalVars.CodPersSedeDpto:null);
    //this.furSeguimientoFiltro.cfurCdepent = GlobalVars.CodPersSedeDpto;
    

    
    console.log("===========this.furSeguimientoFiltro")
    console.log(this.furSeguimientoFiltro);
    console.log(vIdCanal);

    if(vIdCanal == 0) // PARA OTROS TODOS
    {

     
      this.furSeguimientoFiltro.nfurIdcanal = NumIdCanalCarta;

      // console.log("===========this.furSeguimientoFiltro1111111111")
      // console.log(this.furSeguimientoFiltro);

      this.furSeguimientoService.ListarGCSeguimiento(this.furSeguimientoFiltro).subscribe(
        data => {

          vListFurSeguimiento = data;

          this.furSeguimientoFiltro.nfurIdcanal = NumIdCanalCorreo;

          // console.log("===========this.furSeguimientoFiltro22222222222")
          // console.log(this.furSeguimientoFiltro);

          this.furSeguimientoService.ListarGCSeguimiento(this.furSeguimientoFiltro).subscribe(
            data => {

              data.forEach(element => {

                vListFurSeguimiento.push(element);
                
              });
    
              this.furSeguimientoFiltro.nfurIdcanal = NumIdCanalReporte;

              // console.log("===========this.furSeguimientoFiltro33333333")
              // console.log(this.furSeguimientoFiltro);

              this.furSeguimientoService.ListarGCSeguimiento(this.furSeguimientoFiltro).subscribe(
                data => {
        
                  data.forEach(element => {

                    vListFurSeguimiento.push(element);
                    
                  });

                  //this.listFurSeguimiento = vListFurSeguimiento;

                  this._recuperaListado(vListFurSeguimiento);
    
                  
                });
              
            });

        });

       
    }
    else{
      if(vIdCanal == 99){

        console.log("===========furSeguimientoFiltroFur")
        console.log(furSeguimientoFiltroFur);

        this.furSeguimientoService.obtenerFurDashboard(furSeguimientoFiltroFur).subscribe(
          data =>{
            this.listFurSeguimiento = data;
    
            this.dataSource = new MatTableDataSource(data);
    
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          });
      }else{
        this.furSeguimientoFiltro.nfurIdcanal = vIdCanal;
  
        this.furSeguimientoService.ListarGCSeguimiento(this.furSeguimientoFiltro).subscribe(
          data => {
  
            // data.forEach((element,index) => {
  
            //   element.flgDeshabilitaEditar = (element.cfurIdusureg == GlobalVars.CodPersonal && element.nfurIdestado == NumIdEstadoRegistrado ? false:true);
            //   element.numItem = index + 1;
              
            // });
  
            //this.listFurSeguimiento = data;
  
            this._recuperaListado(data);
  
            // this.dataSource = new MatTableDataSource(data);
  
            // this.dataSource.paginator = this.paginator;
            // this.dataSource.sort = this.sort;
  
  
          });

          this.furSeguimientoFiltro.nfurIdcanal = vIdCanal;
          console.log("VID CANAL : " + vIdCanal);
          console.log(furSeguimientoFiltroFur);
          if(vIdCanal != 99){
            this.furSeguimientoService.ListarGCSeguimiento(this.furSeguimientoFiltro).subscribe(
              data => {
        
                this.listFurSeguimiento = data;
        
                this.dataSource = new MatTableDataSource(data);
        
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
        
        
              });
          }
          /*else{
            this.furSeguimientoService.obtenerCantidadFurDashboard(furSeguimientoFiltroFur).subscribe(
              data =>{
                this.listFurSeguimiento = data;
        
                this.dataSource = new MatTableDataSource(data);
        
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
              });
          }*/
        
      }
    }
}

  _recuperaListado(listFur:Fur[]){

    listFur.forEach((element,index) => {

      element.flgDeshabilitaEditar = (element.cfurIdusureg == GlobalVars.CodPersonal && element.nfurIdestado == NumIdEstadoRegistrado ? false:true);
      element.numItem = index + 1;
      
    });

    // console.log("===============listFur");
    // console.log(listFur);

    this.dataSource = new MatTableDataSource(listFur);

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  

  _cambioRadio(flgEvent){

    this.flgRadio = flgEvent.checked;

    this._resetBusqueda();
    
  }

  _EmitirSeleccionado(idFur:number)
  {
    this.opNuevoSeleccionado.emit(idFur);
  }

  _Editar(idFur:number)
  {
    
    this._EmitirSeleccionado(idFur);

  }

  _Ver(idFur:number)
  {
    
    this.opVerSeleccionado.emit(idFur);

  }

  Nuevo(){

    //console.log("=============Nuevo");
    this._EmitirSeleccionado(0);

    //this.opNuevoSeleccionado.emit(true);

  }

  _Asignar(idFur:number){

    this.opAsignarSeleccionado.emit(idFur);

    //this.router.navigate(['/AsignarInformacion/',idFur], { relativeTo: this.route });

  }

  _Atender(idFur:number){

    this.opEvaluarSeleccionado.emit(idFur);

    //this.router.navigate(['/EvaluarInformacion/',idFur], { relativeTo: this.route });
  }

}