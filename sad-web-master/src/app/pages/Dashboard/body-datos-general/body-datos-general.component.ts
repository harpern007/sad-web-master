import { Component, OnInit } from '@angular/core';
import { PlazoService } from 'src/app/services/plazo.service';
import { Plazo } from 'src/app/models/Plazo';
import { GetDateHour, MfuncErrorVal, GetDateTime } from 'src/app/general/variables';
import { ErrorValidator } from 'src/app/models/personalizados/ErrorValidator';
import { Fur } from 'src/app/models/Fur';

@Component({
  selector: 'app-body-datos-general',
  templateUrl: './body-datos-general.component.html',
  styleUrls: ['./body-datos-general.component.css']
})
export class BodyDatosGeneralComponent implements OnInit {

  strIdExpediente:string = "";
  strHoraReg:string = GetDateHour()[1];
  numCantTiempo:number = null;
  strFechaRegistro:string = GetDateHour()[0];
  
  strDscEstado:string = "Registrado";
  numIdPlazo:number = null;
  listPlazo:Plazo[] = [];
  mFuncErrorVal : MfuncErrorVal = new MfuncErrorVal();
  errorValidator: ErrorValidator[] = [];
  flgDeshabilitado = true;
  flgSoloLectura = false;

  constructor(protected plazoService:PlazoService) { }

  ngOnInit() {

    // var dateHour = GetDateHour();

    // this.strFechaRegistro = dateHour[0];
    // this.strHoraReg = dateHour[1];

    this.plazoService.ListarPlazo().subscribe(data=>{

      this.listPlazo = data;

    });

  }

  ValidarDatos()
  {

    var FlgError = false;
    var flgRegCantTiempo = (this.numCantTiempo != null ? true : false);
    var flgRegPlazo = (this.numIdPlazo != null ? true : false);

    // console.log("==========general");
    // console.log(this.strCantTiempo);
    // console.log(this.strIdPlazo);

    if(flgRegCantTiempo || flgRegPlazo)
    {


      this.errorValidator = this.mFuncErrorVal.AgregarErrorValidator((flgRegCantTiempo  && flgRegPlazo  ? false:true), "Ingrese el plazo de atención",this.errorValidator,true);

      FlgError = this.mFuncErrorVal.RevisarErrorValidator(this.errorValidator)


    }
       
    
    
    return FlgError;
    
  }

  // RecuperarDatos()
  // {

  //   return new Observable(this._RecuperarDatos);

  // }

  // RecuperarDatos(fur:Fur)
  // {

  //   return new Observable((observer) => {

  //     fur.strDfurFecregistr = GetDateTime();
  //     fur.strDfurFecinicioreg = this.strFechaRegistro + " " + this.strHoraReg;
  //     fur.nfurNumplazo = this.numCantTiempo;
  //     fur.nfurIdplazo = this.numIdPlazo;
  
  //     return () => {
  //       // Detach the event handler from the target
  //       fur;
  //     };
  //   });

  // }

  RecuperarDatos(fur:Fur)
  {

    var vStrIdExp = this.strIdExpediente;

    fur.strDfurFecregistro = GetDateTime();
    fur.strDfurFecinicioreg = this.strFechaRegistro + " " + this.strHoraReg;
    fur.nfurNumplazo = this.numCantTiempo;
    fur.nfurIdplazo = this.numIdPlazo;

    fur.nfurId = (vStrIdExp.length == 0 ? null: Number(this.strIdExpediente));
      

    return fur;

  }

}
