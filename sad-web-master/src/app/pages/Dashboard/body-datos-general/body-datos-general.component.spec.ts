import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyDatosGeneralComponent } from './body-datos-general.component';

describe('BodyDatosGeneralComponent', () => {
  let component: BodyDatosGeneralComponent;
  let fixture: ComponentFixture<BodyDatosGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyDatosGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyDatosGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
