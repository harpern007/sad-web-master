import { Component, OnInit } from '@angular/core';
import { GetDateHour, NumIdEstadoHePendiente, GlobalVars, RecupPartesFchInicio } from 'src/app/general/variables';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { Estado } from 'src/app/models/Estado';
import { UniOrganicaService } from 'src/app/services/uni-organica.service';
import { PersonalCgrService } from 'src/app/services/personal-cgr.service';
import { HojaEvaluacion } from 'src/app/models/HojaEvaluacion';

@Component({
  selector: 'app-dg-hoja-eval',
  templateUrl: './dg-hoja-eval.component.html',
  styleUrls: ['./dg-hoja-eval.component.css']
})
export class DgHojaEvalComponent implements OnInit {

  fchRegistro:string = "";
  hrInicio:string = "";
  fchHrInicioReg:string = "";
  nroHojaEval:string = "";
  nroExpediente:string = "";
  hrFin:string = "";
  numIdEstado:number = null;
  listEstado:Estado[] = [];
  //listUniOrganica: UniOrganica[] = [];
  strDscARA: string = "";
  strDscUO:string = "";
  // get fchHrInicioReg(): string {
  //   return this.fchRegistro + " " + this.hrInicio;
  // }

  //this.fchRegistro,this.hrInicio,this.fchHrInicioReg

  // formDatosGenerales = new FormGroup({
  //   nro_hojaEval :  new FormControl(''),
  //   hora_inicio :  new FormControl(''),
  //   fecha_registro :  new FormControl(''),
  //   hora_fin :   new FormControl(''),
  //   num_expediente: new FormControl('')
 
  // });

  constructor(protected maestrasService: MaestrasService,
    protected uniOrganicaService: UniOrganicaService,
    protected personalCgrService: PersonalCgrService,
    ) { }

  ngOnInit() {

    this.maestrasService.listarEstadoPorFicha("HEV").subscribe(data =>{

      this.listEstado = data;

      console.log(data);
    
    });

  }

  DatosDefault(){

    var fechaHora = GetDateHour();

    // this.formDatosGenerales.controls.fecha_registro.setValue(fechaHora[0]);
    // this.formDatosGenerales.controls.hora_inicio.setValue(fechaHora[1]);
    this.fchRegistro = fechaHora[0];
    this.hrInicio = fechaHora[2];//formato hora y minuto
    this.fchHrInicioReg = fechaHora[0] + " " + fechaHora[1]; // PARA GUARDAR EN LA BD
    this.numIdEstado = NumIdEstadoHePendiente;

    this.uniOrganicaService.ListarUnidadOrganicaActiva().subscribe(data =>{

      this.strDscUO = GlobalVars.CodUNOPers + " - " + data.filter(x=>x.cpUnoCodigo == GlobalVars.CodUNOPers)[0].cpUnoDescripcion;
    
    });

    this.strDscARA = GlobalVars.CodPersonal + "-" + GlobalVars.DscNombreComplPers;

    console.log("===================this.numIdEstado");

    console.log(this.numIdEstado);
    console.log(this.hrInicio);

  }

  setDatos(hojaEvaluacion: HojaEvaluacion)
  {

    console.log("=================hojaEvaluacion")
    console.log(hojaEvaluacion);

    var strFchRegIni = hojaEvaluacion.strDhojaevalFregini;

    

    console.log(strFchRegIni);
    console.log(RecupPartesFchInicio(strFchRegIni,3));

    this.fchRegistro = RecupPartesFchInicio(strFchRegIni,2);
    this.hrInicio = RecupPartesFchInicio(strFchRegIni,3);
    this.fchHrInicioReg = strFchRegIni;
    this.nroHojaEval = hojaEvaluacion.chojaevalCodformatocgr;
    this.nroExpediente = hojaEvaluacion.nhojaevalIdfur.toString();
    this.numIdEstado = hojaEvaluacion.nhojaevalIdestado;

  }

  // GetDatos()
  // {

  //   return [this.fchRegistro,this.hrInicio,this.fchHrInicioReg];

  // }

}
