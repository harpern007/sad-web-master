import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DgHojaEvalComponent } from './dg-hoja-eval.component';

describe('DgHojaEvalComponent', () => {
  let component: DgHojaEvalComponent;
  let fixture: ComponentFixture<DgHojaEvalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DgHojaEvalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DgHojaEvalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
