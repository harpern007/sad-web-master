import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { Estado } from 'src/app/models/Estado';
import { GetDate, NumIdEstadoRegistrado, RecupPartesFchInicio } from 'src/app/general/variables';
import { DatosGeneral } from 'src/app/models/personalizados/DatosGeneral';
import { Fur } from 'src/app/models/Fur';

@Component({
  selector: 'app-datos-general',
  templateUrl: './datos-general.component.html',
  styleUrls: ['./datos-general.component.css']
})
export class DatosGeneralComponent implements OnInit {

  listEstados : Estado[] = [];
  
  nro_formulario : string;
  hora_inicio : string;
  estado_formulario : number;
  fecha_registro : string;
  hora_inicioView : string;
  estado_formularioView : number;
  fecha_registroView : string;
 
  constructor(private maestraService : MaestrasService) {

   }

  ngOnInit() {

    let fechaString = GetDate();

    let fecha = new Date();
    
    //let dia = fecha.getDay();
    //let fechaString =  fecha.getFullYear() +"-"+ Number(fecha.getMonth()+1)+  "-" + ((dia<10) ? ("0"+dia) :  dia); 
    
    //this.formDatosGenerales.get('fecha_registro').setValue(fechaString);
    this.fecha_registro = fechaString;
    this.maestraService.obtenerEstado().subscribe(data =>{
        this.listEstados = data;
        this.estado_formulario = NumIdEstadoRegistrado;
        // this.formDatosGenerales.get('estado_formulario').setValue(this.listEstados[0].nestadoId);
      });
 
    //this.formDatosGenerales.get('hora_inicio').setValue(fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds());
    this.hora_inicio = fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
  }

  ConsultarDatosGenerales(datosGeneral : DatosGeneral)
  {

    this.estado_formulario = Number(datosGeneral.estado_formulario);
    this.fecha_registro = datosGeneral.fecha_registro;
    this.hora_inicio = datosGeneral.hora_inicio;
    this.nro_formulario = datosGeneral.nro_formulario;

    this._replicaDatosView();

  }

  setDatosGenerales(data:Fur)
  {

    var vFechaReg = RecupPartesFchInicio(data.strDfurFecregistro,2);
    var vHoraInicio = RecupPartesFchInicio(data.strDfurFecinicioreg,1) ;
    var vIdEstado = data.nfurIdestado;

    //this.nro_formulario = data.nfurId.toString();
    this.nro_formulario = data.cfurCodigo;

    this.estado_formulario = vIdEstado;
    this.fecha_registro = vFechaReg ;
    this.hora_inicio = vHoraInicio;

    this._replicaDatosView();
    
    // this.estado_formularioView = vIdEstado;
    // this.fecha_registroView = vFechaReg;
    // this.hora_inicioView = vHoraInicio;

  }

  _replicaDatosView(){

    this.estado_formularioView = this.estado_formulario;
    this.fecha_registroView = this.fecha_registro;
    this.hora_inicioView = this.hora_inicio;

  }

}
