import { Component, OnInit, ViewChild } from '@angular/core';
import { EntidadService } from 'src/app/services/entidad/entidad.service';
import { FichaXInstancia } from 'src/app/models/FichaXInstancia';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalInstanciaComponent } from './modal-instancia/modal-instancia.component';
import { iconEliminar, iconAgregar, iconEditar, DscTituloPaginator } from 'src/app/general/variables';
import { Fur } from 'src/app/models/Fur';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-instancia',
  templateUrl: './instancia.component.html',
  styleUrls: ['./instancia.component.css']
})
export class InstanciaComponent implements OnInit {
  
  dataSource: MatTableDataSource<FichaXInstancia>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  matTableColumnsDef = ["Entidad","NúmeroDoc","Fecha","Estado","Edtar","Eliminar"];

  instanciaList : FichaXInstancia[] =[];
  otra_instancia: false;
  iconAgregar = iconAgregar;
  iconoEliminar = iconEliminar;
  iconoEditar = iconEditar;
  flgLectura : boolean = false;
  constructor(private entidadService : EntidadService,public dialog: MatDialog) { }

  ngOnInit() {

    

  }

  cambioOtraInstancia(valor){
    this.otra_instancia = valor;
  }

  abrirModalInstancia(instanciaList : FichaXInstancia[] , instanciaModificar : FichaXInstancia, modificar : boolean){
    this.abrirModal(instanciaList, instanciaModificar , modificar);
  }

  abrirModal( instanciaList : FichaXInstancia[] , instanciaModificar : FichaXInstancia, modificar : boolean){
    const configuracionGlobal = new MatDialogConfig();
    configuracionGlobal.disableClose = true;
    configuracionGlobal.autoFocus = true;
    configuracionGlobal.data = {
      listaInstancia: instanciaList,
      instancia : instanciaModificar,
      modificar : modificar
    }
    configuracionGlobal.width = "60%";
    const  modal=this.dialog.open(ModalInstanciaComponent, configuracionGlobal);
    
        modal.afterClosed().subscribe( data=>{
          
           if(data != null){
            this._cargarTableMaterial();
           }
       
         }
         
      );  
  }

  eliminar(instancia){
    this.instanciaList.splice(this.instanciaList.indexOf(instancia),1);
   this._cargarTableMaterial();
  }

  editar(instanciaList : FichaXInstancia[] , instanciaModificar : FichaXInstancia, modificar : boolean){
    this.abrirModal(instanciaList,instanciaModificar,modificar);
  }

  setInstanciaC(fur : Fur){
    this.instanciaList = fur.listaFichaPorInstancia;
    this._cargarTableMaterial();
  }

  instanciaCReadEnable(){
    // console.log("===================this.flgLectura");
    // console.log(this.flgLectura);
    this.flgLectura = true;
  }
  
  instanciaCReadDisable(){
    this.flgLectura = false;
  }

  _cargarTableMaterial(){


    setTimeout(() => {

      console.log("=====================this.instanciaList");
      console.log(this.instanciaList);

      this.dataSource = new MatTableDataSource(this.instanciaList);

      if(this.paginator != null){this.paginator._intl.itemsPerPageLabel = DscTituloPaginator}; //this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      
    }, 1);

    
  }
}
