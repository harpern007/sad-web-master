import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FichaXInstancia } from 'src/app/models/FichaXInstancia';
import { EntidadService } from 'src/app/services/entidad/entidad.service';
import { Entidad } from 'src/app/models/Entidad';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { ModalEmpresaComponent } from '../../empresa/modal-involucrada/modal-empresa/modal-empresa.component';
import { CatalogoService } from 'src/app/services/Catalogo.service';
import { Catalogo } from 'src/app/models/Catalogo';
import { openModalMensaje, ConvertDateTimeToStrForBD, anioActualGlobal, stringFechaMaxima } from 'src/app/general/variables';
import { NavModalComponent } from 'src/app/pages/General/nav-modal/nav-modal.component';

@Component({
  selector: 'app-modal-instancia',
  templateUrl: './modal-instancia.component.html',
  styleUrls: ['./modal-instancia.component.css']
})
export class ModalInstanciaComponent implements OnInit {

  instancia :FichaXInstancia  = new FichaXInstancia();
  listEntidadesObras : Entidad[];
  listEstado : Catalogo[] = [];
  nombreEntidad : string = "";
  fechaMaxima : string = stringFechaMaxima;

  @ViewChild(NavModalComponent) navModalC : NavModalComponent;

  constructor(private entidadService : EntidadService,
              public dialogRef: MatDialogRef<ModalEmpresaComponent>,@Inject(MAT_DIALOG_DATA) 
              public data: any, private dialog : MatDialog,
              private catalogoService : CatalogoService) { }

  

  ngOnInit() {

    this.navModalC.setDatosNavModal2("Instancia");

   this.catalogoService.listarCatalogo(21).subscribe(data =>{
    this.listEstado = data;
   });

   if(this.data.modificar)
   {
      this.instancia = this.data.instancia;
      this.data.listaInstancia.splice(this.data.listaInstancia.indexOf(this.instancia),1);
   }

  }

  obtenerEstado(idCatalogo : number){
    console.log(idCatalogo);
      this.listEstado.forEach(cat =>{
      
          if(cat.ncatalogoId == idCatalogo){
            this.instancia.estado = cat;
           
          }
      });
  }
  buscarEntidad(valor){
    console.log(valor);
    let nombreEntidad = valor.target.value;
    
    if(valor.target.value.length >3){
      this.entidadService.obtenerEntidadesXNombre(nombreEntidad).subscribe(data =>{
          this.listEntidadesObras = data;

          console.log("==============this.listEntidadesObras");
          console.log(this.listEntidadesObras);

      });
    }
  }

  seleccionarEntidad(event){
    let name = event.target.value;
    this.llenarCombosEntidad(name); 
  }

  _asignarCodTipoEnt(evento){
    this.instancia.cfichaxinstanCodtipoinst = evento.toElement.value;

    console.log(this.instancia.cfichaxinstanCodtipoinst);
    
  }

  obtenerEntidad(codigo : string){
    this.entidadService.obtenerEntidadXCodigo(codigo).subscribe(data =>{
      if(data != null){
        this.instancia.entidad.centNombre = data.centNombre;
        this.instancia.entidad.centDirdep = data.centDirdep;
        this.instancia.entidad.centDirpro = data.centDirpro;
        this.instancia.entidad.centDirdis = data.centDirdis;
        this.instancia.entidad.centDirecc = data.centDirecc;
        this.instancia.entidad.centCodigo = data.centCodigo;
      }
     
     });
  }

 llenarCombosEntidad(nombreEntidad :string){
  
    
    let list = this.listEntidadesObras.filter(x => x.centNombre === nombreEntidad)[0];
    
    if(list != null){
      this.nombreEntidad = list.centNombre;
      this.entidadService.obtenerEntidadXCodigo(list.centCodigo).subscribe(data =>{
        this.instancia.entidad = data;
      });
    }

  }

  agregarInstancia(){

      
      let valido : boolean = true;
      this.instancia.strDfichaxinstanFechaprest = ConvertDateTimeToStrForBD(this.instancia.dfichaxinstanFchPresent);
      this.obtenerEstado(this.instancia.estado.ncatalogoId);
      
      if(this.instancia.entidad != null && this.instancia.estado.ncatalogoId != null 
        && this.instancia.cfichaxinstanMotivo.length >0 && this.instancia.strDfichaxinstanFechaprest != null){
          this.data.listaInstancia.forEach(instanciaV => {
            if(instanciaV.entidad.centCodigo == this.instancia.entidad.centCodigo
              && instanciaV.estado.ncatalogoId == this.instancia.estado.ncatalogoId){
                valido = false;
            }
        });
       
        if(valido){
          this.instancia.cfichaxinstanCodtipo = "FUR";
          this.instancia.cfichaxinstanCodent = this.instancia.entidad.centCodigo;
          this.instancia.nfichaxinstanIdestado = this.instancia.estado.ncatalogoId;
          console.log(this.instancia.dfichaxinstanFchPresent);
          let fechaValidar = new Date(this.instancia.dfichaxinstanFchPresent);
          console.log(fechaValidar);
          if(fechaValidar.getFullYear() > anioActualGlobal){
            openModalMensaje('Instancia','El año no puede ser mayor al año actual.',this.dialog);
          }else{
            this.data.listaInstancia.push(this.instancia);
            this.dialogRef.close(this.data);
          }
         
        }else{
          openModalMensaje('Instancia','Ya se encuentra registrada esta instancia.',this.dialog);
        }
      }else{
        openModalMensaje('Instancia','Debe completar todos los campos antes de continuar.',this.dialog);
      }
  }

  cancelar(){
    this.dialogRef.close();
  }

}
