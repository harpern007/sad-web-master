import { Component, OnInit, Output, Inject, ViewChild } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { openModalMensaje } from 'src/app/general/variables';
import { NavModalComponent } from 'src/app/pages/General/nav-modal/nav-modal.component';

@Component({
  selector: 'app-modal-monto',
  templateUrl: './modal-monto.component.html',
  styleUrls: ['./modal-monto.component.css']
})
export class ModalMontoComponent implements OnInit {

  strMontoView : string = null;
  strMontoFinal : string = null;
  tituloMensaje = "Monto de perjuicio";
  mensaje : string = "Debe llenar por lo menos un campo";
  cbk_monto  : boolean = false;
  flg_monto_desconocido : string;
  flgMontoDisable : boolean = false;
  /*formModalMonto = new FormGroup({
    input_monto : new FormControl(''),
    cbk_monto : new FormControl(false)
  });*/

  @ViewChild(NavModalComponent) navModalC : NavModalComponent;
  


  constructor( public dialogRef: MatDialogRef<ModalMontoComponent>, private dialog : MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

    var vImpMonto = this.data.monto;

    this.navModalC.setDatosNavModal2("Monto Asociado");

    if(this.data.editar){

      // console.log("==========this.data");
      // console.log(this.data);

      this.strMontoView = vImpMonto;
      this.strMontoFinal = vImpMonto;
      this.cbk_monto = this.data.cbkMonto;
      
      this.validarCampoMonto();
    }
    
  }

  guardarMonto(){

      console.log("=============this.strMontoFinal");
      console.log(this.strMontoFinal);
  
      if( (this.strMontoFinal != null && this.strMontoFinal.length > 0 && this.cbk_monto == false) || this.cbk_monto == true) 
      {

        this.dialogRef.close({monto:this.strMontoFinal , cbk_monto : this.cbk_monto});

      }else
      {
        openModalMensaje(this.tituloMensaje,this.mensaje,this.dialog);
      }
  }

  _cambioMonto(data:string)
  {

    // console.log("=============data");
    // console.log(data);
    // console.log(this.monto);

    this.strMontoFinal = data;

    //this.monto = parseFloat(this.monto).toFixed(2);


  }

  cancelar(){
    this.dialogRef.close();
  }

  cambioMontoDesconocido(cbkMontoDesconocido){
        //    this.formModalMonto.get('cbk_monto').setValue(cbkMontoDesconocido.target.checked);
        this.cbk_monto = cbkMontoDesconocido.target.checked;    
        this.validarCampoMonto();
  }

  validarCampoMonto(){
    //if(this.formModalMonto.get('cbk_monto').value){
    if(this.cbk_monto){
     // this.formModalMonto.get('input_monto').disable();
     // this.formModalMonto.get('input_monto').setValue('');
      this.strMontoView = null;
      this._cambioMonto(null);
      this.flgMontoDisable = true;
    }else{
      this.flgMontoDisable = false;
//      this.formModalMonto.get('input_monto').enable();   
    }
  }

}
