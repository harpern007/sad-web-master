import { Component, OnInit, ViewChild } from '@angular/core';
import { Persona } from 'src/app/models/Persona';
import { ExternosService } from 'src/app/services/externos/externos.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalEmpresaComponent } from '../modal-involucrada/modal-empresa/modal-empresa.component';
import { openModalMensaje, iconEliminar, DscTituloPaginator} from 'src/app/general/variables';
import { Fur } from 'src/app/models/Fur';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';


@Component({
  selector: 'app-empresa-involucrada',
  templateUrl: './empresa-involucrada.component.html',
  styleUrls: ['./empresa-involucrada.component.css']
})
export class EmpresaInvolucradaComponent implements OnInit {
  //Icono
  iconoEliminar = iconEliminar;
  //

  dataSource: MatTableDataSource<Persona>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  flgLectura : boolean = false;
  matTableColumnsDef = ["ruc","razonSocial","direccion","representante","acciones"];
  empresaList: Persona[] = []; 
  cbk_ruc  : boolean = true;
  cbk_empresa : string = "RUC";
  inputRuc :string;
  inputRazonSocial ; string;
  empresa_registrada :boolean  = false;
  flgRucDisabled = false;
  flgRazonSocialDisabled = true;
  flgDeshabilitaValidar = false;
  //dscTextoValidar = DscEtiquetaBtnVal;
  
 /* formEmpresaInvolucrada = new FormGroup({
    input_ruc: new FormControl(''),
    input_razon_social : new FormControl(''),
    flg_empresa_registrada : new FormControl(''),
    cbk_empresa : new FormControl('RUC'),
    empresa_registrada  : new FormControl(false)
    
  });*/
  constructor(private externoService : ExternosService,public dialog: MatDialog) { }

  ngOnInit() {

    

  }

  obtenerEmpresa(){

      console.log("========Obtener Empresa");

      
      
      
      let isRuc = this.cbk_empresa;//this.formEmpresaInvolucrada.get('cbk_empresa').value;
      let valorInput = this.inputRazonSocial;
      let resultado ;
      let persona = new Persona();
   
      if(isRuc == 'RAZON_SOCIAL' ){ 
        ///Refactorizar codigo
          if(valorInput != ''){//this.formEmpresaInvolucrada.get('input_razon_social').value
            
          //this.dscTextoValidar = DscEtiquetaBtnVal;
          this.flgDeshabilitaValidar = true;
          valorInput = //this.formEmpresaInvolucrada.get('input_razon_social').value;
              this.externoService.obtenerEmpresaXRazonSocial(valorInput).subscribe(data=>{

                resultado  = JSON.parse(JSON.stringify(data));
               // if(resultado.listaRetorno != null){ //Validación del servicio anterior
                if(resultado.ruc != null){
                  //persona = this.cargarDatosPersona(resultado.listaRetorno[0]);
                  persona = this.cargarDatosPersona(resultado);
                  this.abrirModal(persona,true,this.empresaList);
                   
                }else{
                  openModalMensaje('No se encontró datos de la razón social','Debe ingresar una razón social valida',this.dialog);
                }
                /*this.dialog.open(ModalEmpresaComponent,{width:'60%',data:{cpersonaNroDoc:persona.cpersonaNroDoc,
                  cpersonaRazonsocial:persona.cpersonaRazonsocial,flgValida:true,listaEmpresa: this.empresaList}});*/

                  //this.dscTextoValidar = DscEtiquetaBtnVal2;

                  this.flgDeshabilitaValidar = false;

               });
            
            
           
          }else{
            //this.flgDeshabilitaValidar = false;
            openModalMensaje('Empresa Involucrada','Debe ingresar una razón social',this.dialog);
          }
          
      }else{

        if(this.inputRuc != ''){ //this.formEmpresaInvolucrada.get('input_ruc').value

          if(this.inputRuc.length < 11){ //this.formEmpresaInvolucrada.get('input_ruc').value.length

            //this.flgDeshabilitaValidar = false;

            openModalMensaje('Empresa Involucrada','Debe ingresar un ruc valido',this.dialog);

          }else{

            valorInput = this.inputRuc;//this.formEmpresaInvolucrada.get('input_ruc').value;

            //this.dscTextoValidar = DscEtiquetaBtnVal;

            this.flgDeshabilitaValidar = true;

            this.externoService.obtenerEmpresaXRUC(valorInput).subscribe(data=>{
                resultado = JSON.parse(JSON.stringify(data));
                //if(resultado.listaRetorno != null){ //El servicio anterior validaba esto ahora siempre devuelve null
                if(resultado.ruc != null){
                 // persona = this.cargarDatosPersona(resultado.listaRetorno[0]);
                 persona = this.cargarDatosPersona(resultado); 
                 this.abrirModal(persona,true,this.empresaList);
                }else{
                  openModalMensaje('No se encontró datos del RUC','Debe ingresar un ruc valido',this.dialog);
                }

                this.flgDeshabilitaValidar = false;

                //this.dscTextoValidar = DscEtiquetaBtnVal2;
              
                /*this.dialog.open(ModalEmpresaComponent,{width:'60%',data:{cpersonaNroDoc:persona.cpersonaNroDoc,
                  cpersonaRazonsocial:persona.cpersonaRazonsocial,flgValida:true,listaEmpresa: this.empresaList}});*/
            });

          }
        
        }else{
          //this.flgDeshabilitaValidar = false;
          openModalMensaje('Empresa Involucrada','Debe ingresar un ruc',this.dialog);
          
        }
       
      }
    
  }

  cargarDatosPersona(resultado){
    console.log(resultado);
     let persona = new Persona();
     persona.cpersonaNrodoc = resultado.ruc;
     persona.cpersonaRazonsocial = resultado.razon_Social;
     //persona.cpersonaRazonsocial = resultado.nombre;

      //Faltaría agregar campo dirección y representante legal
     persona.cpersonaDscreprelegal = resultado.representante;
      persona.cpersonaDscdireccion = resultado.domicilio_Legal; 
     return persona;
  }

  abrirModal(personaArg:Persona,flgValidaArg : boolean, listaEmpresaArg : Persona[])
  {

    const configuracionGlobal = new MatDialogConfig();

    configuracionGlobal.disableClose = true;
    configuracionGlobal.autoFocus = true;

    configuracionGlobal.data = {
      cpersonaNrodoc:personaArg.cpersonaNrodoc,
      cpersonaRazonsocial:personaArg.cpersonaRazonsocial,
      cpersonaDscdireccion : personaArg.cpersonaDscdireccion,
      cpersonaDscreprelegal :personaArg.cpersonaDscreprelegal,
      flgValida:flgValidaArg,
      listaEmpresa: listaEmpresaArg
    }

    configuracionGlobal.width = "60%";

    const  modal=this.dialog.open(ModalEmpresaComponent, configuracionGlobal);

        modal.afterClosed().subscribe( data=>{
          
          if(data != null)
          {
            this.flgDeshabilitaValidar = false;
            
            this.inputRazonSocial = "";
            this.inputRuc = "";
            this._cargarTableMaterial();
            //this.formEmpresaInvolucrada.get('empresa_registrada').setValue(false);
           
          }

          this.empresa_registrada = false;
       
        }
         
      );  

  }


  cambiarEmpresaRegistada(evento){

    //console.log(evento);

    if(evento.target.checked)
    {

      let persona  = new Persona();

      this.abrirModal(persona,false,this.empresaList);

    }
  
  }

  eliminar(empresa){
    this.empresaList.splice(this.empresaList.indexOf(empresa),1);
    this._cargarTableMaterial();
  }

  rucSeleccionado(evento){

    this.inputRazonSocial = "";
    this.flgRucDisabled = false;
    this.flgRazonSocialDisabled = true;
    console.log(this.cbk_empresa);
    //this.formEmpresaInvolucrada.get('input_razon_social').setValue('');
  }

  razonSeleccionado(){
    this.inputRuc = "";
    this.flgRazonSocialDisabled = false;
    this.flgRucDisabled = true;
    //this.formEmpresaInvolucrada.get('input_ruc').setValue('');  
  }


  setEmpresaComponent(fur : Fur){

    if(fur.listaFurPorEmpresaInvolucrada != null){

      fur.listaFurPorEmpresaInvolucrada.forEach(empresa =>{
        this.empresaList.push(empresa.persona);
      });

      // console.log("=========================fur.listaFurPorEmpresaInvolucrada");
      // console.log(fur.listaFurPorEmpresaInvolucrada);

      this._cargarTableMaterial();
    }
    
    
  }

  empresaCReadEnable(){
    this.flgLectura = true;
    this.flgRazonSocialDisabled = true;
    this.flgRucDisabled = true;
  }

  empresaCReadDisable(){
    this.flgLectura = false;
    this.flgRazonSocialDisabled = false;
    this.flgRucDisabled = false;
  }

  _cargarTableMaterial(){
    /**
    setTimeout(() => {

      this.dataSource = new MatTableDataSource(this.empresaList);
      this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      
    }, 1);*/
    
    
  }
}
