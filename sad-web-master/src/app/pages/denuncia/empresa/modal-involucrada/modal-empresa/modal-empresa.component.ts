import { Component, OnInit, Inject, Input, Output, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Persona } from 'src/app/models/Persona';
import { openModalMensaje, openModalMensajePrg } from 'src/app/general/variables';
import { Departamento } from 'src/app/models/Departamento';
import { Provincia } from 'src/app/models/Provincia';
import { Distrito } from 'src/app/models/Distrito';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { NavModalComponent } from 'src/app/pages/General/nav-modal/nav-modal.component';

@Component({
  selector: 'app-modal-empresa',
  templateUrl: './modal-empresa.component.html',
  styleUrls: ['./modal-empresa.component.css']
})
export class ModalEmpresaComponent implements OnInit {
  formModalEmpresaValidada = new FormGroup({
    inputRuc : new FormControl(''),
    inputRazonSocial : new FormControl(''),
    inputDireccion : new FormControl(''),
    inputRepresentante : new FormControl('')
  });
  
  empresa = new Persona();
  flgRazonSDisabled : boolean = false;
  flgNroDocDisabled : boolean = false;
  flgDireccionDisabled : boolean = false;
  flgRepresentanteDisabled : boolean = false;
  flgValida : boolean = true;

 //
 cfurCdepent : string = "";
 cfurCprovent : string = "";
 cfurCdistent : string = "";
 departamentos: Departamento[] = [];
 provincias: Provincia[] = [];
 distritos: Distrito[] = [];

 @ViewChild(NavModalComponent) navModalC : NavModalComponent;
  
  constructor(public dialogRef: MatDialogRef<ModalEmpresaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private dialog : MatDialog, private maestraService : MaestrasService) { }

  ngOnInit() {

    this.navModalC.setDatosNavModal2("Empresa Validada");

    if(this.cfurCdepent != null){
      let dep = this.data.departamento;
      let prov = this.data.provincia;
      let distr = this.data.distrito;
      
      this.maestraService.obtenerDepartamentos().subscribe(departamentos =>{
        this.departamentos = departamentos;
        this.cfurCdepent = this.data.departamento;
          this.maestraService.obtenerProvincias(dep).subscribe(provincia =>{
            this.provincias = provincia;
            this.cfurCprovent = this.data.provincia;
            this.maestraService.obtenerDistritos(dep,prov).subscribe(distrito =>
              { 
                  this.distritos = distrito;
                  this.cfurCdistent = distr;
              });
        }); 
      });

    }
    this.flgValida = this.data.flgValida;
    this.habilitarCampos();
    console.log("Data del modal");
    console.log(this.data);
    this.empresa.cpersonaNrodoc = this.data.cpersonaNrodoc;
    this.empresa.cpersonaRazonsocial = this.data.cpersonaRazonsocial;
   this.empresa.cpersonaDscdireccion = this.data.cpersonaDscdireccion;
   this.empresa.cpersonaDscreprelegal = this.data.cpersonaDscreprelegal;
  }

  habilitarCampos(){
    if(this.flgValida){
      this.flgNroDocDisabled = true;
      this.flgRazonSDisabled = true;
      this.flgDireccionDisabled = true;
      this.flgRepresentanteDisabled = true;
      /*
      this.formModalEmpresaValidada.get('inputRuc').disable();
      this.formModalEmpresaValidada.get('inputRazonSocial').disable();
      this.formModalEmpresaValidada.get('inputDireccion').disable();
      this.formModalEmpresaValidada.get('inputRepresentante').disable();
      */
      
    }else{
      this.flgNroDocDisabled = false;
      this.flgRazonSDisabled = false;
      this.flgDireccionDisabled = false;
      this.flgRepresentanteDisabled = false;
      /*
      this.formModalEmpresaValidada.get('inputRuc').enable();
      this.formModalEmpresaValidada.get('inputRazonSocial').enable();
      this.formModalEmpresaValidada.get('inputDireccion').enable();
      this.formModalEmpresaValidada.get('inputRepresentante').enable();
      */
    }

  }

  agregarEmpresa()
  {
     // let personaGuardar = new Persona();

      let flgNoRepetido = true;
      let numeroDoc = this.empresa.cpersonaNrodoc;//this.formModalEmpresaValidada.get('inputRuc').value;
      let razonSocial = this.empresa.cpersonaRazonsocial;//this.formModalEmpresaValidada.get('inputRazonSocial').value;
 
      if(numeroDoc != null && razonSocial != null)
      {

        this.data.listaEmpresa.forEach(empresa => {
       
          if(this.flgValida && empresa.cpersonaNrodoc == numeroDoc){
            flgNoRepetido = false;
          }else{
              if(!this.flgValida && empresa.cpersonaRazonsocial == razonSocial || empresa.cpersonaNrodoc == numeroDoc){
                flgNoRepetido = false;
              }
          }
         });

        if(flgNoRepetido)
        {
          /*
          personaGuardar.cpersonaNrodoc = numeroDoc;//this.formModalEmpresaValidada.get('inputRuc').value;
          personaGuardar.cpersonaRazonsocial = razonSocial; //this.formModalEmpresaValidada.get('inputRazonSocial').value;
          personaGuardar.cpersonaDscdireccion = this.formModalEmpresaValidada.get('inputDireccion').value;
          personaGuardar.cpersonaDscreprelegal = this.formModalEmpresaValidada.get('inputRepresentante').value;
          */
          //this.data.listaEmpresa.push(personaGuardar);

          openModalMensajePrg("Empresas Involucradas",`¿Usted está seguro que desea agregar la empresa?`,this.dialog).subscribe(result=>{

            console.log("===========0result");
            console.log(result);

            if(result != true)
            {

              this.data.listaEmpresa.push(this.empresa);
              this.dialogRef.close(this.data);

            }

          });
          
        }else{
          openModalMensaje('Empresas Involucradas','La empresa seleccionada ya se encuentra agregada',this.dialog);
        }
          
      }else{
        openModalMensaje('Empresas Involucradas','Existen campos vacíos',this.dialog);
      }
      
    
      
      
      

     
    }


    cerrar(){
      this.dialogRef.close();
    }
}
