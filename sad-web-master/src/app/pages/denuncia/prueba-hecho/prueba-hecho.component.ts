import { Component, OnInit, ViewChild } from '@angular/core';
import { DocumentoDet } from 'src/app/models/DocumentoDet';
import { FormGroup, FormControl } from '@angular/forms';
import { Entidad } from 'src/app/models/Entidad';
import { EntidadService } from 'src/app/services/entidad/entidad.service';
import { InstanciaComponent } from '../instancia/instancia.component';
import { Fur } from 'src/app/models/Fur';
import { EvidenciasComponent } from '../evidencias/evidencias.component';

@Component({
  selector: 'app-prueba-hecho',
  templateUrl: './prueba-hecho.component.html',
  styleUrls: ['./prueba-hecho.component.css']
})
export class PruebaHechoComponent implements OnInit {

  @ViewChild(InstanciaComponent) instanciaC : InstanciaComponent;
  @ViewChild(EvidenciasComponent) evidenciaC : EvidenciasComponent;
  fileToUpload: File = null;
  listFilesEvidencias : File[] = [];
  otra_instancia: false;
  cuenta_pruebas = false;
  require_identificarse : false;


  flgLectura : boolean = false;
 // listEvidencias : DocumentoDet[] = [];
  formEvidencias = new  FormGroup({
    cuentaPruebas: new FormControl(false),
    evidenciaFile : new FormControl()
  });
  constructor(private entidadService : EntidadService) { }

  ngOnInit() {
  }



  cambioCuentaPruebas(event){
    console.log(event);
    this.cuenta_pruebas = event.checked;
    this.formEvidencias.get('cuentaPruebas').setValue(this.cuenta_pruebas);
  }

  cambioOtraInstancia(event){
      this.otra_instancia = event.checked;
  }

  pruebaHechoCReadEnable(){

    this.flgLectura = true;

  }

  pruebaHechoCReadDisable(){
    this.flgLectura = false;
  }
  setPruebaHechoComponent(fur : Fur)
  {
    //console.log("Prueba hechos: " + fur.cfurFlgpruebahechos);
    this.cuenta_pruebas = fur.cfurFlgexispruh == "SI"?  true : false;
    //console.log("Cuenta Pruebas" + this.cuenta_pruebas);

    setTimeout(()=>{

      if(this.cuenta_pruebas){

        //this.evidenciaC.evidenciaCReadEnable();

        if(this.flgLectura)
        {

          this.evidenciaC.evidenciaCReadEnable();

        }else{

          this.evidenciaC.evidenciaCReadDisable();

        }

        this.evidenciaC.setEvidenciaC(fur);

      }

    },0)
   
  }
}
