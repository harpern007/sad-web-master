import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Departamento } from 'src/app/models/Departamento';
import { Provincia } from 'src/app/models/Provincia';
import { Distrito } from 'src/app/models/Distrito';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { Entidad } from 'src/app/models/Entidad';
import { openModalMensaje } from 'src/app/general/variables';
import { NavModalComponent } from 'src/app/pages/General/nav-modal/nav-modal.component';

@Component({
  selector: 'app-modal-entidad',
  templateUrl: './modal-entidad.component.html',
  styleUrls: ['./modal-entidad.component.css']
})
export class ModalEntidadComponent implements OnInit {

  departamentos : Departamento[] = [];
  provincias : Provincia[]= [];
  distritos : Distrito[] = [];
  formEntidadModal = new FormGroup({
    selectDistritoModal : new FormControl('',Validators.required),
    selectDepartamentoModal : new FormControl('',Validators.required),
    selectProvinciaModal : new FormControl('',Validators.required),
    inputDireccionModal : new FormControl(''),
    inputReferenciaModal : new FormControl('')
  });

  @ViewChild(NavModalComponent) navModalC : NavModalComponent;

  constructor(public dialogRef: MatDialogRef<ModalEntidadComponent>,
    private maestraService : MaestrasService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog) { }
  
 


  ngOnInit() {

    this.navModalC.setDatosNavModal2("Nueva Dirección de Entidad");
      
    this.maestraService.obtenerDepartamentos().subscribe(data =>{
        this.departamentos = data;
    });
  }

    obtenerProvincias(codDep : string){
    this.maestraService.obtenerProvincias(codDep).subscribe(data =>{
     
        this.provincias = data;
    });  
  }

    obtenerDistritos(codDep : string , codProv : string){
  
    this.maestraService.obtenerDistritos(codDep,codProv).subscribe(data =>
      {
          console.log(data);
          this.distritos = data;
      });
  }

  
  agregarEntidad(){
    if(this.formEntidadModal.valid){
      let entidadGuardar = new Entidad();
      this.obtenerProvincias(this.formEntidadModal.get('selectProvinciaModal').value);
      this.obtenerDistritos(this.formEntidadModal.get('selectDepartamentoModal').value,this.formEntidadModal.get('selectProvinciaModal').value);
      entidadGuardar.centDirecc = this.formEntidadModal.get('inputDireccionModal').value;
      entidadGuardar.centDirRef = this.formEntidadModal.get('inputReferenciaModal').value;
      entidadGuardar.centDirdep = this.formEntidadModal.get('selectDepartamentoModal').value;
      entidadGuardar.centDirpro = this.formEntidadModal.get('selectProvinciaModal').value;
      entidadGuardar.centDirdis = this.formEntidadModal.get('selectDistritoModal').value;
     
      this.dialogRef.close(entidadGuardar);
    }else{
      openModalMensaje("Dirección Entidad","Aún no ha completado los campos obligatorios",this.dialog);
    }
  
  }

  cancelar(){
    this.dialogRef.close();
  }

}
