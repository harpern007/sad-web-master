import { Component, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, DoCheck, Input, ɵConsole, ViewChild } from '@angular/core';
import { Departamento } from 'src/app/models/Departamento';
import { Provincia } from 'src/app/models/Provincia';
import { Distrito } from 'src/app/models/Distrito';
import { FormGroup, FormControl } from '@angular/forms';
import { EntidadService } from 'src/app/services/entidad/entidad.service';
import { Entidad } from 'src/app/models/Entidad';
import { TipoHecho } from 'src/app/models/TipoHecho';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalEntidadComponent } from './modal-entidad/modal-entidad.component';
import { ModalBusqObrpuComponent } from '../../ObraPublica/modal-busq-obrpu/modal-busq-obrpu.component';
import { ModalContratacionComponent } from '../contratacion/modal-contratacion/modal-contratacion.component';
import { FurXObraPublica } from 'src/app/models/FurXObraPublica';
import { ModalRegistroObraComponent } from '../../ObraPublica/modal-registro-obra/modal-registro-obra.component';
import { openModalMensaje, iconEditar, constobraPublica, constcontrataciones, DscTituloPaginator } from 'src/app/general/variables';
import { ModalHechoIrregularComponent } from '../../ObraPublica/modal-hecho-irregular/modal-hecho-irregular.component';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus'
import { faTrash } from '@fortawesome/free-solid-svg-icons/faTrash';
import { MentidadHechoC } from 'src/app/models/personalizados/mEntidadHechoC';
import { Fur } from 'src/app/models/Fur';
import { faEye } from '@fortawesome/free-solid-svg-icons/faEye';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ModalIrregInfobrasComponent } from '../../ObraPublica/modal-irreg-infobras/modal-irreg-infobras.component';

@Component({
  selector: 'app-entidad-hecho',
  templateUrl: './entidad-hecho.component.html',
  styleUrls: ['./entidad-hecho.component.css']
})
export class EntidadHechoComponent implements OnInit{
  //
  flgLectura = false;

  //Icons
  iconAgregar = faPlus;
  iconEliminar = faTrash;
  iconVer = faEye;
  iconEditar = iconEditar;
  //
  cObraPublica = constobraPublica;
  cContratacion = constcontrataciones;
  //Atributos sección entidad hecho
  cfurCdepent:string;
  cfurCprovent:string;
  cfurCdistent:string;
  cfurCodent:string;
  cfurDirentidad:string;
  cfurDscrefentidad:string;
  nfurIdtipohecho:number;
  direccionHechos: boolean = true;

  nombreModal : string = "Atención";
  mensaje : string = "Debe llenar la entidad para completar la acción. ";
  nombreEntidad : string = "";
  encontroObra : boolean = false;
  departamentos: Departamento[] = [];
  provincias: Provincia[] = [];
  distritos: Distrito[] = [];
  entidades : Entidad[];
  tipoHechos : TipoHecho[] = [];
  entidad : Entidad = new Entidad();
  codigoEntidadSeleccionada : Number = 0;
  flgObrasPub = false;
 
  // entidad : Entidad= new Entidad();
  //codigoEntidadSeleccionada : Number = 0;
  
  listObrasPublicas : FurXObraPublica[] = [];
  
  formEntidadHecho = new FormGroup({
    input_ruc : new FormControl(''),
    inputDireccion : new FormControl(''),
    select_departamento : new FormControl(''),
    select_provincia : new FormControl(''),
    select_distrito : new FormControl(''),
    select_entidades : new FormControl(''),
    inputReferencia : new FormControl(''),
    select_tipo_hecho : new FormControl()
  });

  //Obras
  
  matTableColumnsDef = ["nombreObra","estadoObra","costo","fechaInicio","fechaFin","Irregularidades","editar","eliminar"];
  dataSourceObras: MatTableDataSource<FurXObraPublica>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  //@ViewChild(MatPaginator) paginator: MatPaginator;

  @Output()
  tipoHechoOut = new EventEmitter();

  constructor(private entidadService : EntidadService,private maestraService : MaestrasService,public dialog : MatDialog,
    public mdModalIrreg: MatDialog) { }

  ngOnInit() {

      
    
      //this.direccionHechos = true;

      this.maestraService.obtenerDepartamentos().subscribe(data =>{
        this.departamentos = data;
      });

      this.maestraService.obtenerTipoHecho().subscribe(data=>{
          this.tipoHechos = data;
      });
  }


/*
  get  flgLectura() : boolean{
    return this._flgLectura;
  }

  set flgLectura(valor : boolean){
    this._flgLectura = valor;
   
    if(this._flgLectura){
      this.modoLectura(this._flgLectura);  
    }
  } 
  */

  ConsultarEntidadHecho(mEntidadHechoC:MentidadHechoC)
  {
    // console.log("===========mEntidadHechoC");
    // console.log(mEntidadHechoC);
    this.formEntidadHecho.controls.select_entidades.setValue(mEntidadHechoC.select_entidades);
    this.formEntidadHecho.controls.select_departamento.setValue(mEntidadHechoC.select_departamento);
    this.formEntidadHecho.controls.select_provincia.setValue(mEntidadHechoC.select_provincia);
    this.formEntidadHecho.controls.select_distrito.setValue(mEntidadHechoC.select_distrito);
    this.formEntidadHecho.controls.inputDireccion.setValue(mEntidadHechoC.inputDireccion);
    this.formEntidadHecho.controls.inputReferencia.setValue(mEntidadHechoC.inputReferencia);
    
    // get('select_departamento').setValue(data.centDirdep);
    // this.formEntidadHecho.get('select_provincia').setValue(data.centDirpro);
    // this.formEntidadHecho.get('select_distrito').setValue(data.centDirdis);
    // this.formEntidadHecho.get('inputDireccion').setValue(data.centDirecc); 
  }

  buscarEntidad(valor){

    
    let nombreEntidad = valor.target.value;
    
    if(valor.target.value.length >=4){
      this.entidadService.obtenerEntidadesXNombre(nombreEntidad).subscribe(data =>{
          this.entidades = data;
      });
    }
  }

  seleccionarEntidad(event){
    let name = event.target.value;
    this.llenarCombosEntidad(name); 
  }

  obtenerEntidad(codigo : string){
    this.entidadService.obtenerEntidadXCodigo(codigo).subscribe(data =>{
      if(data != null){
        //this.cfurCdepent.;
        this.obtenerProvincias(data.centDirdep);
        this.obtenerDistritos(data.centDirdep,data.centDirpro);
        this.cfurCdepent = data.centDirdep;
        this.cfurCprovent = data.centDirpro;
        this.cfurCdistent = data.centDirdis;
        this.cfurDirentidad = data.centDirecc;
        this.cfurCodent = data.centCodigo;
        this.nombreEntidad = data.centNombre;
  
        this.formEntidadHecho.get('select_departamento').setValue(data.centDirdep);
        this.formEntidadHecho.get('select_provincia').setValue(data.centDirpro);
        this.formEntidadHecho.get('select_distrito').setValue(data.centDirdis);
        this.formEntidadHecho.get('inputDireccion').setValue(data.centDirecc);
        this.entidad.centCodigo = data.centCodigo;
      }
    
    });
  }

  llenarCombosEntidad(nombreEntidad :string){
    if(this.entidades != null){
      let list = this.entidades.filter(x => x.centNombre === nombreEntidad)[0];
      if(list != null){    
        this.obtenerEntidad(list.centCodigo);
      }
    }
   
  }

  enviarHecho(){
    let tipHechoEnvio;
    //let tipoHechoSelec = this.formEntidadHecho.get('select_tipo_hecho').value;
    let tipoHechoSelec = this.nfurIdtipohecho;

    this.flgObrasPub = (tipoHechoSelec == 1) ? true:false;

    this.tipoHechos.forEach(tipHecho => {

      if(tipHecho.ntipohechoId == tipoHechoSelec){
          tipHechoEnvio = tipHecho; 
          
          this.emitirHecho(tipHechoEnvio);
        }
    });
  }

  emitirHecho(tipoHechoEnvio){
    this.tipoHechoOut.emit(tipoHechoEnvio);
  }

  cambioDireccion(cbkDireccion){
    this.direccionHechos = cbkDireccion.checked;    

    console.log("============this.direccionHechos");
    console.log(this.direccionHechos);

     // if(this.formEntidadHecho.get('select_entidades').value != null && this.formEntidadHecho.get('select_entidades').value != ""){
     console.log(this.cfurCodent); 

     if(this.cfurCodent!= null && this.cfurCodent != "" && this.nombreEntidad != null){

        if(this.direccionHechos){
          //this.llenarCombosEntidad(this.formEntidadHecho.get('select_entidades').value);
          this.llenarCombosEntidad(this.nombreEntidad);
          this.cfurDscrefentidad = "";
        }else{
          this.abrirModal();
        }
      }else{
        if(!this.direccionHechos){
          openModalMensaje(this.nombreModal,this.mensaje,this.dialog);
          this.dialog.afterAllClosed.subscribe(data=>{
            this.direccionHechos = true;
          });
        }    
      }
  }

 

  abrirModal(){
    const configuracionGlobal = new MatDialogConfig();
    configuracionGlobal.disableClose = false;
    configuracionGlobal.autoFocus = true;
    configuracionGlobal.data = {
      //cpersonaNroDoc:personaArg.cpersonaNroDoc,
      //cpersonaRazonsocial:personaArg.cpersonaRazonsocial,flgValida:flgValidaArg,listaEmpresa: listaEmpresaArg
    }
    configuracionGlobal.width = "60%";
    const  modal=this.dialog.open(ModalEntidadComponent, configuracionGlobal);

        modal.afterClosed().subscribe(
          data =>{

            console.log("================data");
            console.log(data);

            if(data != null){
           
              this.obtenerProvincias(data.centDirdep);
              this.obtenerDistritos(data.centDirdep,data.centDirpro);
              this.llenarFormulario(data);
            }else{

              this.direccionHechos = true;

            }
          });  
  }


  abrirModalIrregularidades(obra){
    const configuracionGlobal = new MatDialogConfig();
    configuracionGlobal.disableClose = false;
    configuracionGlobal.autoFocus = true;
    configuracionGlobal.data = {
      obra : obra
    }
    configuracionGlobal.width = "60%";
    this.dialog.open(ModalHechoIrregularComponent, configuracionGlobal);   
  }

  llenarFormulario(entidadArg: Entidad){
      console.log(entidadArg);
      this.cfurDirentidad = entidadArg.centDirecc;
      this.cfurDscrefentidad = entidadArg.centDirRef;
      this.cfurCdepent = entidadArg.centDirdep;
      this.cfurCprovent = entidadArg.centDirpro;
      this.cfurCdistent = entidadArg.centDirdis;
      

      this.formEntidadHecho.get('inputDireccion').setValue(entidadArg.centDirecc);
      this.formEntidadHecho.get('inputReferencia').setValue(entidadArg.centDirRef);
      this.formEntidadHecho.get('select_departamento').setValue(entidadArg.centDirdep);
      this.formEntidadHecho.get('select_provincia').setValue(entidadArg.centDirpro);
      this.formEntidadHecho.get('select_distrito').setValue(entidadArg.centDirpro);
  }

  obtenerProvincias(codDep : string){
    this.maestraService.obtenerProvincias(codDep).subscribe(data =>{
        this.provincias = data;
    });  
  }

  obtenerDistritos(codDep : string , codProv : string){
    this.maestraService.obtenerDistritos(codDep,codProv).subscribe(data =>
      { 
          this.distritos = data;
      });
  }


  abrirModalContratacion(){
    const configuracionGlobal = new MatDialogConfig();
    configuracionGlobal.disableClose = false;
    configuracionGlobal.autoFocus = true;
    configuracionGlobal.data = {
      
      //cpersonaNroDoc:personaArg.cpersonaNroDoc,
      //cpersonaRazonsocial:personaArg.cpersonaRazonsocial,flgValida:flgValidaArg,listaEmpresa: listaEmpresaArg
    }
    configuracionGlobal.width = "60%";
    const  modal=this.dialog.open(ModalContratacionComponent, configuracionGlobal);

        modal.afterClosed().subscribe(
          data =>{
            console.log(data);
          
          } );  
  }

   abrirModalObras(){
    const configuracionGlobal = new MatDialogConfig();
    configuracionGlobal.disableClose = true;
    configuracionGlobal.autoFocus = true;
    configuracionGlobal.data = {
      entidad : this.nombreEntidad
      //cpersonaNroDoc:personaArg.cpersonaNroDoc,
      //cpersonaRazonsocial:personaArg.cpersonaRazonsocial,flgValida:flgValidaArg,listaEmpresa: listaEmpresaArg
    }

    configuracionGlobal.width = "90%";
    
    const  modal=this.dialog.open(ModalBusqObrpuComponent, configuracionGlobal);

        modal.afterClosed().subscribe(
          data =>{
            
            if(data != null){
              this.listObrasPublicas.push(data);
              this._cargarTableMaterialObra();
             /* this.obtenerProvincias(data.centDirdep);
              this.obtenerDistritos(data.centDirdep,data.centDirpro);
              this.llenarFormulario(data);*/
            }
          
          } );  

          
  }

  cambiarEncontroObra(evento){
    this.encontroObra = evento.checked;
  }

  abrirModalRegistroObra(){
    const configuracionGlobal = new MatDialogConfig();
    configuracionGlobal.disableClose = true;
    configuracionGlobal.autoFocus = true;
    configuracionGlobal.width = "60%";
    configuracionGlobal.height = "75%";

    configuracionGlobal.data = {
      //listDenunciado:this.listDenunciado
    }

    const  modal=this.dialog.open(ModalRegistroObraComponent, configuracionGlobal);
        modal.afterClosed().subscribe(
         data =>{
            if(data != null){
              // console.log(data);
              // console.log(this.listObrasPublicas);
              this.listObrasPublicas.push(data);
              //console.log(this.listObrasPublicas);
              this._cargarTableMaterialObra();
            }
         }
      );  
  }
  
  editarHechoIrregular(obra : FurXObraPublica){
    const dialogRef = this.mdModalIrreg.open(ModalIrregInfobrasComponent,{ width: '50%',height:'87%', data: {obraPub : obra,editar:true}});
    
    dialogRef.afterClosed().subscribe(result => {
        if(result != null){
          obra = result;
        }
 
     
    });
  }
  eliminarObras(obra){
    this.listObrasPublicas.splice(this.listObrasPublicas.indexOf(obra),1);
  }
  

  verHechoIrregular(obra){
      this.abrirModalIrregularidades(obra);
  }



  modoLectura(valor : boolean){
    if(valor){
      this.formEntidadHecho.controls.input_ruc.disable();
      this.formEntidadHecho.controls.select_departamento.disable();
      this.formEntidadHecho.controls.select_provincia.disable();
      this.formEntidadHecho.controls.select_distrito.disable();
      this.formEntidadHecho.controls.select_entidades.disable();
      this.formEntidadHecho.controls.inputReferencia.disable();
      this.formEntidadHecho.controls.flg_direccion_hechos.disable();
      this.formEntidadHecho.controls.select_tipo_hecho.disable();
    }

  }

  setEntidadHecho(fur : Fur)
  {
    this.nfurIdtipohecho = fur.nfurIdtipohecho;
    this.cfurDscrefentidad = fur.cfurDscrefentidad;
    this.obtenerEntidad(fur.cfurCodent);
    this.direccionHechos = (fur.cfurFlgdirocuhecho == "SI") ? true : false;
    //this.direccionHechos = (fur.cfurFlgdirocuhecho == "SI") ? false : true;

    this.maestraService.obtenerTipoHecho().subscribe(data=>{
      this.tipoHechos = data;
      this.enviarHecho();
    });
    

    this.formEntidadHecho.controls.select_tipo_hecho.setValue( fur.nfurIdtipohecho);
    this.formEntidadHecho.controls.inputReferencia.setValue(fur.cfurDscrefentidad);

    this.listObrasPublicas = fur.listaFurPorObraPublica;

    this._cargarTableMaterialObra();

  }

  getStringDireccionHechos(){
     return  this.direccionHechos ? "SI" : "NO";
  }

  entidadComponentReadEnable(){
      this.flgLectura = true;
  }

  entidadComponentReadDisable(){
    this.flgLectura = false;
  }

  _cargarTableMaterialObra(){
    
    setTimeout(() => {

      this.dataSourceObras = new MatTableDataSource(this.listObrasPublicas);

      if(this.paginator != null){this.paginator._intl.itemsPerPageLabel = DscTituloPaginator};

      this.dataSourceObras.paginator = this.paginator;
      this.dataSourceObras.sort = this.sort;
      
    }, 10);
    
  }
}
