import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { EntidadService } from 'src/app/services/entidad/entidad.service';
import { Entidad } from 'src/app/models/Entidad';
import { TipoHecho } from 'src/app/models/TipoHecho';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalMontoComponent } from '../modal/modal-monto/modal-monto.component';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { Anho } from 'src/app/models/Anho';
import { Mes } from 'src/app/models/mes';
import { FuncionarioInvolucradoComponent } from '../Funcionario/funcionario-involucrado/funcionario-involucrado.component';
import { EmpresaInvolucradaComponent } from '../empresa/empresa-involucrada/empresa-involucrada.component';
import { openModalMensaje, getBooleanFlag } from 'src/app/general/variables';
import {faEdit} from '@fortawesome/free-solid-svg-icons/faEdit'
import { Fur } from 'src/app/models/Fur';
@Component({
  selector: 'app-hecho-irregular',
  templateUrl: './hecho-irregular.component.html',
  styleUrls: ['./hecho-irregular.component.css']
})
export class HechoIrregularComponent implements OnInit {

  @ViewChild(FuncionarioInvolucradoComponent) funcionarioInvolucradoC : FuncionarioInvolucradoComponent;
  @ViewChild(EmpresaInvolucradaComponent) empresaInvolucradaC : EmpresaInvolucradaComponent;

  //Modo lectura
  //private _flgLectura = false;
  flgLectura : boolean = false;
  //Icon editar
  iconEditar = faEdit;
  //Campos Hecho irregular - FUR
  cfurDschecho : string;
  //cfurFlgexismonto : string;
  nfurIdtipohecho : number;
  descripcionTipoHecho : string;
  nfurImpmontoperj:number;

  flgMontoDesc = false;
  //Periodo
  select_anio_desde  : string;
  select_anio_hasta : string;
  select_mes_hasta : string;
  select_mes_desde : string;
  
  entidades: Entidad[] = [];
  list_tipo_hecho : TipoHecho[] = [];
  perjuicio_economico : boolean = false ;
  ocurre_hecho : boolean = false;


 
  //monto :number;
  


  listAnios : Anho[] = [];
  listMeses : Mes[]= [];

  formHechoIrregular = new FormGroup({
    select_entidades : new FormControl(''),
    select_entidades_inv : new FormControl(''),
    tipo_hecho : new FormControl(''),
    input_descripcion_hecho : new FormControl(''),
    flg_perjuicio_economico : new FormControl(''),
    flg_ocurre_hecho : new FormControl(''),
    select_anio_desde  : new FormControl(''),
    select_mes_desde : new FormControl(''),
    select_anio_hasta : new FormControl(''),
    select_mes_hasta : new FormControl(''),
    monto : new FormControl(''),
    cbkMontoDesconocido : new FormControl('')
  });
  constructor(protected entidadService: EntidadService,public dialog: MatDialog, private maestraService : MaestrasService) { }

  get input_descripcion_hecho(){
    return this.formHechoIrregular.get('input_descripcion_hecho');
  }
  ngOnInit() {
   
   // this.validarOcurreHecho();
    this.maestraService.obtenerAnho().subscribe(data => this.listAnios = data);
    this.maestraService.obtenerMes().subscribe(data =>  this.listMeses = data);
  }

  cambioExisteMonto(cbkMonto){
   
    this.perjuicio_economico = cbkMonto.checked;
    if(cbkMonto.checked){
      this.abrirModal(false);
    }else{
     // this.formHechoIrregular.get('monto').setValue(0);
     this.nfurImpmontoperj = 0;
    }
  }


  editarMonto(){
   
    //this.perjuicio_economico = cbkMonto.checked;
    //if(cbkMonto.checked){
    
      this.abrirModal(true);
      
    //}
  }


  abrirModal(editar: boolean){
    const configuracionGlobal = new MatDialogConfig();
    configuracionGlobal.disableClose = true;
    configuracionGlobal.autoFocus = true;
   // configuracionGlobal.data = {editar:editar,monto : this.formHechoIrregular.get('monto').value,cbkMonto :  this.formHechoIrregular.get('cbkMontoDesconocido').value};
    configuracionGlobal.data = {editar:editar,monto :this.nfurImpmontoperj,
                            cbkMonto :  this.flgMontoDesc};
    configuracionGlobal.width = "40%";
    const  modal=this.dialog.open(ModalMontoComponent, configuracionGlobal);

        modal.afterClosed().subscribe(
          data => 
          {   if(data!= null){
                console.log(data);
                //this.formHechoIrregular.get('monto').setValue( data.get('input_monto').value);
                this.nfurImpmontoperj =  data.monto;

                console.log("===============this.nfurImpmontoperj");
                console.log(this.nfurImpmontoperj);

                //this.formHechoIrregular.get('cbkMontoDesconocido').setValue(data.get('cbk_monto').value);  
                this.flgMontoDesc = data.cbk_monto;
                  //console.log( data.get('input_monto').value);
                   //this.formHechoIrregular.get('monto').setValue( data.get('input_monto').value);
          
              }else{
                this.perjuicio_economico = !this.perjuicio_economico;
              }
            
          }
      );  
  }


  cambioOcurreHecho(cbkOcurrioHecho){
    this.ocurre_hecho=cbkOcurrioHecho.checked 
    //this.validarOcurreHecho();
  }

  _cambioMesIni(){

    this.select_anio_desde = null;
    this.select_anio_hasta = null;
    this.select_mes_hasta = null;
   
    if(this._validarPeriodo())
    {

      setTimeout(() => {

        this.select_mes_desde = null;
        
      }, 1);

    }

    

  }

  _cambioAnhoIni(){

    this.select_anio_hasta = null;
    this.select_mes_hasta = null;
   

    if(this._validarPeriodo())
    {

      setTimeout(() => {

        this.select_anio_desde = null;
        
      }, 1);


    }

    

  }

  _cambioMesFin(){

    this.select_anio_hasta = null;
   
    if(this._validarPeriodo())
    {

      setTimeout(() => {

        this.select_mes_hasta = null;
        
      }, 1);

      

    }

    
    
  }

  _cambioAnhoFin(){
   
    if(this._validarPeriodo())
    {

      setTimeout(() => {

        this.select_anio_hasta = null;
        
      }, 1);
      

    }

    console.log("==============this.select_anio_hasta");
    console.log(this.select_anio_hasta);
    
  }

  _validarPeriodo(){
    
    let desdeAnio = Number.parseInt(this.select_anio_desde);
    let hastaAnio = Number.parseInt(this.select_anio_hasta);
    let desdeMes = Number.parseInt(this.select_mes_desde);
    let hastaMes = Number.parseInt(this.select_mes_hasta);
    var flgError = false;

    if( desdeAnio> hastaAnio){

      openModalMensaje("Periodo","El año del campo Hasta no puede ser menor que el mes del campo Desde",this.dialog);

      flgError = true;
      
      //this.select_anio_hasta=null;
    }else{
      if(desdeAnio ==  hastaAnio){
          if( desdeMes> hastaMes){
            openModalMensaje("Periodo","El mes del campo Hasta no puede ser menor que el mes del campo Desde",this.dialog);
            flgError = true;
            //this.formHechoIrregular.get('select_mes_desde').setValue(null);
          }
      }
    }

    return flgError;

  }

  validarOcurreHecho(){
    if(this.ocurre_hecho){
      this.formHechoIrregular.get('select_mes_desde').enable();
      this.formHechoIrregular.get('select_anio_desde').enable();
      this.formHechoIrregular.get('select_mes_hasta').enable();
      this.formHechoIrregular.get('select_anio_hasta').enable();
    }else{
      this.formHechoIrregular.get('select_mes_desde').disable();
      this.formHechoIrregular.get('select_anio_desde').disable();
      this.formHechoIrregular.get('select_mes_hasta').disable();
      this.formHechoIrregular.get('select_anio_hasta').disable();
    }
  }
  
  /*
  get  flgLectura() : boolean{
    return this._flgLectura;
  }

  set flgLectura(valor : boolean){
    this._flgLectura = valor;
   
    if(this._flgLectura){
      this.modoLectura(this._flgLectura);  
    }
  } 
*/

  getStringPerjuicioEconomico() : string {
    return this.perjuicio_economico ? "SI" : "NO";
  }


  getStringFlgMontoDesconocido() : string{
    return this.flgMontoDesc ? "SI" : "NO";
  }


  setHechoIrregularComponent(fur : Fur){
    this.cfurDschecho = fur.cfurDschecho;
    this.perjuicio_economico =getBooleanFlag(fur.cfurFlgexismonto );
    if(this.perjuicio_economico){
      this.nfurImpmontoperj = fur.nfurImpmontoperj;
      this.flgMontoDesc =  getBooleanFlag(fur.cfurFlgemontodesc);
    }
    this.ocurre_hecho = getBooleanFlag(fur.cfurFlghdenconocu);
    this.select_anio_desde = String(fur.nfurPeriodoohini).substring(0,4);
    this.select_mes_desde = String(fur.nfurPeriodoohini).substring(4,6);
    this.select_anio_hasta = String(fur.nfurPeriodoohfin).substring(0,4);
    this.select_mes_hasta =  String(fur.nfurPeriodoohfin).substring(4,6);;
    
  }

  hechoIrregularCReadEnable(){
    this.flgLectura = true;
  }

  hechoIrregularCReadDisable(){
    this.flgLectura = false;
  }
  
  modoLectura(valor : boolean){
    if(valor){
        //Formulario
        this.formHechoIrregular.controls.select_entidades_inv.disable();
        this.formHechoIrregular.controls.tipo_hecho.disable();
        this.formHechoIrregular.controls.input_descripcion_hecho.disable();
        this.formHechoIrregular.controls.flg_ocurre_hecho.disable();
        this.formHechoIrregular.controls.select_anio_desde.disable();
        this.formHechoIrregular.controls.select_mes_desde.disable();
        this.formHechoIrregular.controls.select_anio_hasta.disable();
        this.formHechoIrregular.controls.select_mes_hasta.disable();
        this.formHechoIrregular.controls.monto.disable();
        this.formHechoIrregular.controls.cbkMontoDesconocido.disable();
        this.formHechoIrregular.controls.flg_perjuicio_economico.disable();
        //

    }
  }
}
