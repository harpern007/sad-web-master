import { Component, OnInit, ViewChild } from '@angular/core';
import { DocumentoDet } from 'src/app/models/DocumentoDet';
import { iconEliminar, GetDateTime, CodTipoFichaFUR, CodTipoFichaHe, GlobalVars, CodSubTipoFichaFUR, saveContent, iconVer, DscTituloPaginator, openModalMensaje, listTypesUpload } from 'src/app/general/variables';
import { Fur } from 'src/app/models/Fur';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { LaserFicheService } from 'src/app/services/laser-fiche.service';
import { LaserFiche } from 'src/app/models/LaserFiche';
import { Observable } from 'rxjs';
import { DocumentoDetService } from 'src/app/services/documento-det.service';
import { ActualizarDet } from 'src/app/models/DocumentoCab';

@Component({
  selector: 'app-evidencias',
  templateUrl: './evidencias.component.html',
  styleUrls: ['./evidencias.component.css']
})
export class EvidenciasComponent implements OnInit {

  dataSource: MatTableDataSource<DocumentoDet>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  matTableColumnsDef = ["Nombre evidencia","FchHrreg","estado","eliminar","ver"];

  iconoEliminar = iconEliminar;
  iconoVer = iconVer;
  dscExtensiones = listTypesUpload.join();
  
  //flgLectura : boolean = false;

  fileToUpload: File = null;
  listFilesEvidencias : File[] = [];
  otra_instancia: false;
  cuenta_pruebas = false;
  require_identificarse : false;
  flgSoloLectura = false;
  evidenciaFile:string = "";
  strCodTipoFicha = CodTipoFichaFUR;
  strCodSubTipoFicha = CodSubTipoFichaFUR;
  strDscTitulo :string = "Lista de Pruebas de hechos";
  numIdTipoFicha = null;

  listEvidencias : DocumentoDet[] = [];
  // formEvid = new  FormGroup({
  //   evidenciaFile : new FormControl()
  // });
  constructor(protected laserFicheService: LaserFicheService,
    protected documentoDetService: DocumentoDetService,
    public dialog: MatDialog
    ) { }

  ngOnInit() {

    this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;

  }

  handleFileInput(files: FileList, event:any) {

    let documento = new DocumentoDet();
    var reader  = new FileReader();

    this.fileToUpload = files.item(0);

    // console.log("===============this.fileToUpload.type");
    // console.log(this.fileToUpload.type);
    // console.log(this.fileToUpload);

    if(!listTypesUpload.includes(this.fileToUpload.type))
    {

      openModalMensaje("Evidencias","El archivo seleccionado no es un documento , por favor verifique",this.dialog);

      return;

    }


    if(this.fileToUpload.size/1024/1024 > 5)
    {

      //openModalMensaje

      openModalMensaje("Evidencias","El tamaño del archivo ha superado los 5MB, por favor verifique",this.dialog);

      return;


    }

    reader.onload = (function(theFile) {
      return function(e) {

        // console.log("==========e.target.result");
        // console.log(e.target.result);
        // Render thumbnail.
        documento.fileBase64 = e.target.result;
        
        
      };
    })(this.fileToUpload);

    //reader.readAsBinaryString(this.fileToUpload);
    reader.readAsDataURL(this.fileToUpload);
    

    // console.log("=============reader.result");
    // console.log(reader.result);

    documento.cdocumentoDscnombre = files.item(0).name;
    //  console.log("================files");
    //  console.log(files.item(0));
    // // console.log(this.fileToUpload);
    //  console.log(event.target);
    documento.strDdocumentoFchreg = GetDateTime();
    documento.cdocumentoCodtipo = this.strCodTipoFicha;
    documento.cdocumentoCodsubtipo = this.strCodSubTipoFicha;
    documento.ndocumentoIdtipd = this.numIdTipoFicha;

    // console.log("======documento");
    // console.log(documento);
    // console.log(this.listEvidencias);

    this.listEvidencias.push(documento);
    //console.log("===============1");
    this.listFilesEvidencias.push(this.fileToUpload);
    //console.log("===============2");
    this.evidenciaFile = "";
    this._cargarTableMaterial();
    //console.log(this.listEvidencias);

  }

  _verArchivo(index:number){

    // console.log("=============0_descargarArchivo");
    // console.log(index);
    //console.log(this.listEvidencias[index].fileBase64);
    //console.log("=============0_descargarArchivo");}

    var vBase64 = this.listEvidencias[index].fileBase64;
    var vFileName = this.listEvidencias[index].cdocumentoDscnombre;

    vBase64 = vBase64.substring(vBase64.indexOf(",") + 1);

    // console.log(vBase64);

    saveContent(vBase64, vFileName);

    //window.open("data:application/octet-stream;base64," + vBase64);
    //window.open(`data:application/octet-stream;filename=${vFileName};base64, ${vBase64}`);
    
    //window.open("data:application/pdf," + encodeURI(vBase64)); 

    //window.open(`data:application/pdf;base64, ${vBase64}`, '_self');

  }

  eliminar(ev){
  
 
    this.listEvidencias.splice(this.listEvidencias.indexOf(ev),1);
  
    this.listFilesEvidencias.forEach(fileV => {
     
        if(ev.cdocumentoDscNombre == fileV.name){
          this.listFilesEvidencias.splice(this.listFilesEvidencias.indexOf(fileV),1);
        }
    });
    this._cargarTableMaterial();
    //this.listEvidencias.splice(this.listEvidencias.indexOf(ev),1);
  }
  
  setEvidenciaC(fur : Fur){
    //console.log(fur.listaDocumentoAdjuntoDet);
    this.listEvidencias = fur.listaDocumentoAdjuntoDet;
    this._cargarTableMaterial();
    //console.log(  this.listEvidencias);
    //console.log(  this.listEvidencias);
  }

  setFichaDocumento(codTipoFicha:string, codSubtipoFicha:string, idTipoFicha:number){

    this.strCodTipoFicha = codTipoFicha;
    this.strCodSubTipoFicha = codSubtipoFicha;
    this.numIdTipoFicha = idTipoFicha;

  }


  setEvidenciaGeneral(listaDocumentoDet : DocumentoDet[]){
    
    this.listEvidencias = listaDocumentoDet;
    
  }
  
  evidenciaCReadEnable(){
    this.flgSoloLectura = true;
    console.log(this.listEvidencias);
  }

  evidenciaCReadDisable(){
    this.flgSoloLectura = false;
    console.log(this.listEvidencias);
  }

  _cargarTableMaterial(){
    this.dataSource = new MatTableDataSource(this.listEvidencias);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  procesarLaserFiche(idTipoDoc:number)
  {

    const laserficheObservable = new Observable(observer => {
     
      var newName = "";
      var laserFiche:LaserFiche = null;

      var actualizarDet : ActualizarDet = new  ActualizarDet();

      actualizarDet.codTipo = this.strCodTipoFicha;
      actualizarDet.codSubtipo = this.strCodSubTipoFicha;
      actualizarDet.idTipoDoc = idTipoDoc;

      this.listEvidencias.forEach((element,index) => {

        if(element.cdocumentoDscurl == null)
        {

          laserFiche = new LaserFiche();

          newName = element.cdocumentoDscnombre.substring(0,element.cdocumentoDscnombre.indexOf(".")) + "-" + index.toString() + "-" + idTipoDoc.toString() + element.cdocumentoDscnombre.substring(element.cdocumentoDscnombre.indexOf("."));
  
          //laserFiche.filename = element.cdocumentoDscnombre;
          laserFiche.usuario = GlobalVars.CodUsuario;
          laserFiche.fileBase64 = element.fileBase64;
          laserFiche.filename = newName;

          element.ndocumentoIdtipd = idTipoDoc;
          element.cdocumentoDscnombre = newName;

          console.log("==============laserFiche");
          console.log(laserFiche);
          
          this.laserFicheService.UploadLaserFiche(laserFiche).subscribe(
            data => {

              element.cdocumentoDscurl = data.url_documento;
              
            });

        }
  
      });

      console.log("=============actualizarDet.listaDocumentoDet");
      console.log(actualizarDet);

      actualizarDet.listaDocumentoDetalle = this.listEvidencias;

      this.documentoDetService.RegistrarListaEvidencia(actualizarDet).subscribe(
        data => {

          //this.listEvidencias = data;

          observer.next(data);
          
        });



    });

    

    return laserficheObservable;

    //var laserFiche:LaserFiche = null;
    //var backSlash = "\";

    // return new Observable((observer) => {

      

    //   console.log("==============this.listEvidencias");
    //   console.log(this.listEvidencias);

    //   observer.complete();

    //   return () => {


    //     this.listEvidencias
    //     // Detach the event handler from the target
        
    //   };
    // });

    
  }

  setEvidenciaTitulo(param:string){

    this.strDscTitulo = param;

  }
  



}
