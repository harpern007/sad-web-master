import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { HechoIrregularComponent } from '../hecho-irregular/hecho-irregular.component';
import { EntidadHechoComponent } from '../entidad-hecho/entidad-hecho.component';
import { PruebaHechoComponent } from '../prueba-hecho/prueba-hecho.component';
import { Fur } from 'src/app/models/Fur';
import { MdenunciaContenedorC } from 'src/app/models/personalizados/MdenunciaContenedorC';
import { InstanciaComponent } from '../instancia/instancia.component';

@Component({
  selector: 'app-denuncia-contenedor',
  templateUrl: './denuncia-contenedor.component.html',
  styleUrls: ['./denuncia-contenedor.component.css']
})
export class DenunciaContenedorComponent implements OnInit {

  @ViewChild(HechoIrregularComponent) hechoIrregularC : HechoIrregularComponent;
  @ViewChild(EntidadHechoComponent) entidadHechoC : EntidadHechoComponent;
  @ViewChild(PruebaHechoComponent) pruebaHechoC : PruebaHechoComponent;
  @ViewChild(InstanciaComponent) instanciaC : InstanciaComponent;
  
  requireIdentificarse : boolean =  false;
  @Output()
  requiereIdentiEmitter = new EventEmitter();

 require_identificarse : boolean = false;
 flgLectura : boolean = false;
 //CUS04
 flgEsFur : boolean = false;
  constructor() { }

  ngOnInit() {
  
      
  }

  obtenerDatosHijo(tipoHecho){
    
    //this.hechoIrregularC.formHechoIrregular.get('tipo_hecho').setValue(tipoHecho.ctipohechoDescripcion);
    console.log(tipoHecho);
    this.hechoIrregularC.descripcionTipoHecho=tipoHecho.ctipohechoDescripcion;
    this.hechoIrregularC.cfurDschecho = null;
    this.hechoIrregularC.nfurIdtipohecho = tipoHecho.ntipohechoId;
  }

  emitirCambio(cbkRequiereIden){
    console.log("activar tab: " + cbkRequiereIden );

    this.requireIdentificarse = cbkRequiereIden;
    // this.requiereIdentiEmitter.emit(this.requireIdentificarse);
    this.requiereIdentiEmitter.emit(this.requireIdentificarse);
  }

  ConsultarDenuncia(mDenunciaContenedorC:MdenunciaContenedorC)
  {

    console.log("================mDenunciaContenedorC");
    console.log(mDenunciaContenedorC);

    this.entidadHechoC.ConsultarEntidadHecho(mDenunciaContenedorC.mEntidadHechoC);


  }

  denunciaContenedorCReadEnable(){
    this.flgLectura = true;
  }

  denunciaContenedorCReadDisable(){
    this.flgLectura = false;
  }

  setDenunciaContenedorC(fur : Fur){
    this.requireIdentificarse = fur.cfurFlgreqidentif == "SI" ? true : false; 
    console.log("Requiere identificarse: " + this.requireIdentificarse);
    this.emitirCambio(this.requireIdentificarse);
  }

  


}
