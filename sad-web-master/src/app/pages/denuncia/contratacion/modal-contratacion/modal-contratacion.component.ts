import { Component, OnInit } from '@angular/core';
import { Catalogo } from 'src/app/models/Catalogo';
import { Distrito } from 'src/app/models/Distrito';
import { Provincia } from 'src/app/models/Provincia';
import { Departamento } from 'src/app/models/Departamento';
import { Infobras } from 'src/app/models/Infobras';
import { ObraPublica } from 'src/app/models/ObraPublica';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-modal-contratacion',
  templateUrl: './modal-contratacion.component.html',
  styleUrls: ['./modal-contratacion.component.css']
})
export class ModalContratacionComponent implements OnInit {
  listTipoEjecucion : Catalogo[] = [];
  listDistrito: Distrito[] = [];
  listProvincia: Provincia[] = [];
  listDepartamento: Departamento[] = [];
  listInfobras : Infobras[] = [];
  listInfobrasSel : Infobras[] = [];
  infobrasFiltro : Infobras = new Infobras();
  flgMostrarAvanzada = false;
  tituloMensaje = "Obras Públicas";
 
  obraPublica:ObraPublica = new ObraPublica();
 


  formBusqObraP = new FormGroup({
    select_tipejec: new FormControl(''),
    select_departamento: new FormControl(''),
    select_provincia: new FormControl(''),
    select_distrito: new FormControl(''),
    input_entidad: new FormControl(''),
    input_nombreobra: new FormControl(''),
    input_codsnip: new FormControl(''),
    input_infobras: new FormControl(''),
    select_evento: new FormControl(''),
    select_anho: new FormControl(''),
    select_detevento: new FormControl(''),
    select_fase: new FormControl(''),
    select_monitoreo: new FormControl(''),
    input_montodesde: new FormControl(''),
    input_montohasta: new FormControl('')
  });
  constructor() { }

  ngOnInit() {
  }

}
