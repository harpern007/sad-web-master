import { Component, OnInit, ViewChild } from '@angular/core';
import { Denunciado } from 'src/app/models/Denunciado';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';
import { Cargo } from 'src/app/models/Cargo';
import { ExternosService } from 'src/app/services/externos/externos.service';
import { ModalFuncionarioComponent } from '../modal-funcionario/modal-funcionario/modal-funcionario.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { TipoDocumento } from 'src/app/models/TipoDocumento';
import { iconEliminar, iconAgregar, DscTituloPaginator, iconEditar } from 'src/app/general/variables';
import { Fur } from 'src/app/models/Fur';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { cloneDeep } from 'lodash';

@Component({
  selector: 'app-funcionario-involucrado',
  templateUrl: './funcionario-involucrado.component.html',
  styleUrls: ['./funcionario-involucrado.component.css']
})
export class FuncionarioInvolucradoComponent implements OnInit {
  //
  //private _flgLectura = false;
  flgLectura : boolean = false;

  //Icons
  iconoEliminar = iconEliminar;
  iconAgregar = iconAgregar;
  iconEditar = iconEditar;
  //
  matTableColumnsDef = ["numeroDoc","nombres","apellidoP","apellidoM","cargoDsc","eliminar","editar"];
  dataSource: MatTableDataSource<Denunciado>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  conoce_funcionario : boolean =  false;      
  //
  
  listTipoDoc : TipoDocumento[]= [];
  listCargo : Cargo[] = [];
  permanece_entidad : boolean = false;
  listDenunciado : Denunciado[] = [];
  listDenCopy : Denunciado[] = [];

  constructor(private maestra :MaestrasService, private externoService : ExternosService,public dialog: MatDialog) {
    
   }

   
  ngOnInit() {

    
     
  }

  cambiarConoceFuncionario(cbkFuncionario){

    this.conoce_funcionario = cbkFuncionario.checked 

  }

  _editar(index:number){

    console.log("======_editar");

    console.log(this.listDenunciado);

    // this.listDenunciado[index]

    // setDatosModalFunc
    this.abrirModal(index);


  }

  abrirModalFuncionario(){

    this.abrirModal(null);

  }

  abrirModal(numIndexEdit : number)
  {

    // console.log("================this.listDenunciado0000000000000000000000");
    // console.log(this.listDenunciado)

    //let vListDenunciado: Denunciado[] = [];
    
    const configuracionGlobal = new MatDialogConfig();
    const vListDenunciadoOrig = cloneDeep(this.listDenunciado);

    configuracionGlobal.disableClose = true;
    configuracionGlobal.autoFocus = true;
    
    configuracionGlobal.width = "60%";

    configuracionGlobal.data = {
      listModalDenunciado : this.listDenunciado,
      numIndexEdit : numIndexEdit
    }
    const  modal=this.dialog.open(ModalFuncionarioComponent, configuracionGlobal);

        modal.afterClosed().subscribe(
          
         data =>{

          console.log("=========dataAfterClosed");
          console.log(data);

          console.log(this.listDenunciado);

          // console.log("================this.listDenCopy222222222222222222222222");
          // console.log(this.listDenCopy);

          // console.log("================myClonedArray222222222222222222222222");
          // console.log(myClonedArray);

          //console.log(data.listDenunciado);

          if(data != null)
          {

            //console.log("=========data11111111111");

            if(data.listModalDenunciado.length >0){

              //console.log("=========data1333333");
          
              this.listDenunciado = data.listModalDenunciado;
              
            }

          }else{ // REGRESO SIN EDITAR O AGREGAR

            console.log("==================vListDenunciadoOrig");
            console.log(vListDenunciadoOrig);

            this.listDenunciado = vListDenunciadoOrig;


          }

          setTimeout(() => {

            this._cargarTableMaterial();
            
          }, 1);


          //console.log(data.listDenunciado.length);

          
    
          //this.conoce_funcionario = data.cbkConoce;
          
         }
      );  
  }


  eliminar(funcionario){
    this.listDenunciado.splice(this.listDenunciado.indexOf(funcionario),1);
    this._cargarTableMaterial();
  }
  
  modoLectura(valor : boolean){
    if(valor){
      
    }
  }

  setFuncionarioComponent(fur : Fur){
     if(fur.listaDenunciadoPorFicha != null){
      fur.listaDenunciadoPorFicha.forEach(denunciadoFicha =>{
        this.listDenunciado.push(denunciadoFicha.denunciado);
      });
      this._cargarTableMaterial();
    } 
      
  }

  funcionarioCReadEnable(){
    this.flgLectura = true; 
  }

  funcionarioCReadDisable(){
    this.flgLectura = false;
  }

  _cargarTableMaterial(){

    /**setTimeout(() => {
      //JCMF
      this.dataSource = new MatTableDataSource(this.listDenunciado);
      this.paginator._intl.itemsPerPageLabel = DscTituloPaginator;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      
    }, 1);*/
    
    
  }
}
