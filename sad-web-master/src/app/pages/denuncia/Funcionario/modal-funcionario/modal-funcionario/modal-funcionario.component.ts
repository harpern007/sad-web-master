import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidatorFn, Validator } from '@angular/forms';
import { ExternosService } from 'src/app/services/externos/externos.service';
import { MaestrasService } from 'src/app/services/maestra/maestras.service';

import { Cargo } from 'src/app/models/Cargo';
import { Denunciado } from 'src/app/models/Denunciado';
import { Persona } from 'src/app/models/Persona';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { TipoDocumento } from 'src/app/models/TipoDocumento';
import { ConvertirFlag, PERSONA_EXTERNA, PERSONA_MAESTRA, GetDate, iconError, getBooleanFlag } from 'src/app/general/variables';
import { openModalMensaje } from 'src/app/general/variables';
import { PersonaService } from 'src/app/services/persona.service';
import { NavModalComponent } from 'src/app/pages/General/nav-modal/nav-modal.component';


@Component({
  selector: 'app-modal-funcionario',
  templateUrl: './modal-funcionario.component.html',
  styleUrls: ['./modal-funcionario.component.css']
})

export class ModalFuncionarioComponent implements OnInit {

  //Icons
  iconoError = iconError;

  //
  tipoDocumento : number ;
  personaBusqueda = new Persona();
  denunciado = new Denunciado();

  flgNroDocDisabled : boolean;
  flgTipoDocDisabled : boolean;
  flgApellidoPDisabled : boolean;
  flgApellidoMDisabled : boolean;
  flgNombresDisabled : boolean;
  flgCargoDisabled : boolean;
  
  //
  pApellidoError = "Ingrese apellido paterno , el campo no acepeta números";
  sApellidoError = "Ingrese apellido materno, el campo no acepta números";
  nombresError = "Ingrese sus nombres completos, el campo no acepta números";
  cargoError = "Seleccione un cargo";
  tipoDocumentoError = "Seleccione un tipo de docuento";

  soloLetras = "^[a-zA-Z ]+$"; //\s
  conoce_funcionario : boolean =  false;
  listTipoDoc : TipoDocumento[]= [];
  listCargo : Cargo[] = [];
  flgPermaneceEntidad : boolean = false;
  flgDeshabilitaValidar = false;
  flgModalEditar = false;
 
  @ViewChild(NavModalComponent) navModalC : NavModalComponent;

  
  formFuncionario = new FormGroup({
  
    flg_conoce_funcionario : new FormControl(''),
    input_sApellido : new FormControl('',[Validators.required,Validators.maxLength(32),Validators.pattern(this.soloLetras)]),
    
    input_pApellido : new FormControl('',[Validators.required,Validators.maxLength(32),Validators.pattern(this.soloLetras)]),
    input_nombresDenun : new FormControl('',[Validators.required,Validators.maxLength(32),Validators.pattern(this.soloLetras)]),
    select_tipoDoc : new FormControl(null,[Validators.required]),
    select_cargo : new FormControl('',[Validators.required]),
    input_nroDoc : new FormControl(''),
    flg_permanece_entidad : new FormControl('')
  });

  
  constructor(private maestra :MaestrasService, private externoService : ExternosService,
    public dialogRef: MatDialogRef<ModalFuncionarioComponent>, private personaService : PersonaService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog : MatDialog) { 

  }

  get input_pApellido() {
    return this.formFuncionario.controls.input_pApellido;
  }
  
  get input_sApellido(){
    return this.formFuncionario.get('input_sApellido');
  }

  get flg_conoce_funcionario(){
    return this.formFuncionario.get('flg_conoce_funcionario');
  }

  
  get input_nombresDenun()  {
  
    return this.formFuncionario.get('input_nombresDenun');
  }
  
  get select_tipoDoc()  {
    return this.formFuncionario.get('select_tipoDoc');
  }
  
  get select_cargo()  {
    return this.formFuncionario.get('select_cargo');
  }
  get input_nroDoc()  {
    return this.formFuncionario.get('input_nroDoc');
  }
  
  get flg_permanece_entidad()  {
  
    return this.formFuncionario.get('flg_permanece_entidad');
  }

  ngOnInit() {

    var vFlgModalEditar = false;
    var vDenunciado = new Denunciado();

    this.navModalC.setDatosNavModal2("Funcionario Involucrado");


    this.maestra.obtenerTipoDocumento().subscribe(data =>{
      this.listTipoDoc = data;
     
    });

    this.maestra.obtenerCargo().subscribe(data =>{
      this.listCargo = data;
     
    })

    //this.cambioTipoDocumento();

    // setTimeout(() => {

     
      
    // }, 1);

    if(this.data.numIndexEdit != null)
    {

      vDenunciado = this.data.listModalDenunciado[this.data.numIndexEdit];

      

      this._setDatosModalFunc(vDenunciado);

      vFlgModalEditar = true;

    }

    this.flgModalEditar = vFlgModalEditar;

  }

  cambiarConoceFuncionario(cbkFuncionario){
    this.conoce_funcionario=cbkFuncionario.checked 
    this.cambioTipoDocumento();
  }

  agregarDenunciantes(){

    console.log(this.personaBusqueda);

    let valido = true;
    let repetido = true;

    //let denunciado = new Denunciado();
    let tipoDoc = this.tipoDocumento;//this.formFuncionario.get('select_tipoDoc').value;
    let cargoV = new Cargo();
    var idCargo = this.denunciado.cargo.ncargoId;//this.formFuncionario.get('select_cargo').value;
    var nroDoc = this.denunciado.cdenunciadoNrodoc;//this.formFuncionario.get('input_nroDoc').value;
    var nombresDenun = this.denunciado.cdenunciadoNombres;//this.formFuncionario.get('input_nombresDenun').value;
    var pApellidoDenun = this.denunciado.cdenunciadoApepaterno;//this.formFuncionario.get('input_pApellido').value;
    var mApellidoDenun = this.formFuncionario.get('input_sApellido').value;
    var vNumIndex = this.denunciado.numIndex;

    // if(this.personaBusqueda != null)
    // {

    //   this.personaBusqueda.npersonaIdtipdoc = tipoDoc;

    // }

    

    //this.formFuncionario.controls.input_pApellido.invalid()
    
   /* denunciado.cdenunciadoApepaterno = pApellidoDenun;
    denunciado.cdenunciadoApematerno = mApellidoDenun;
    denunciado.cdenunciadoNombres = nombresDenun;
    denunciado.ndenunciadoIdtipdoc = tipoDoc;
    denunciado.tipoDocumento = new TipoDocumento();*/

    this.denunciado.tipoDocumento.ntipdocumentoId = tipoDoc;

    console.log(this.denunciado);

    if(tipoDoc == 1  )
    {

      if(nroDoc != null)
      {

        if(nroDoc.length > 0) //
        {
          this.personaBusqueda.cpersonaNombres =  nombresDenun;
          this.personaBusqueda.cpersonaApepaterno = pApellidoDenun;
          this.personaBusqueda.cpersonaApematerno = mApellidoDenun;      
          this.personaBusqueda.cpersonaNrodoc = nroDoc;
          //this.denunciado.ndenunciadoIdtipdoc = this.personaBusqueda.npersonaIdtipdoc;
          this.denunciado.ndenunciadoIdtipdoc = tipoDoc;
         // denunciado.cdenunciadoNrodoc = this.personaBusqueda.cpersonaNrodoc;
        }

      }
      else{
        
        valido = false;
      } 
    }else{
      this.personaBusqueda = null;
    }

    this.denunciado.persona =  this.personaBusqueda;

    cargoV = this.obtenerCargo(idCargo);
    this.denunciado.cargo = cargoV;
    this.denunciado.cdenunciadoFlgpermentidad = ConvertirFlag(this.flgPermaneceEntidad);
    this.denunciado.ndenunciadoIdcargo = idCargo;
    
    console.log(cargoV);
    
    if(this.data.listModalDenunciado.length > 0)
    {
     // if(this.formFuncionario.get('select_tipoDoc').value == 1){
      if(this.tipoDocumento == 1){

        this.data.listModalDenunciado.forEach((den,index) => {

          if(this._validaEditarIndex(index)){return;};

          // if(this.flgModalEditar && index == this.data.numIndexEdit)
          // {

          //   return;

          // }

          if(den.ndenunciadoIdtipdoc == 1)
          {

            if(den.persona.cpersonaNrodoc == this.denunciado.persona.cpersonaNrodoc)
            {
              
              repetido = false;
            
            }

          }
          
        });

      }else{

        this.data.listModalDenunciado.forEach((den,index) => {

          if(this._validaEditarIndex(index)){return;};

          // console.log("==============den");
          // console.log(den);

          // console.log("==============this.denunciado");
          // console.log(this.denunciado);

          if(den.cdenunciadoNombres.toUpperCase().trim() == this.denunciado.cdenunciadoNombres.toUpperCase().trim() && 
            den.cdenunciadoApepaterno.toUpperCase().trim() == this.denunciado.cdenunciadoApepaterno.toUpperCase().trim() &&
            den.cdenunciadoApematerno.toUpperCase().trim() == this.denunciado.cdenunciadoApematerno.toUpperCase().trim()){
              repetido = false;
             
          }

        });

      }

      if(this.denunciado.cargo == null || this.denunciado.cargo.ncargoId == null)
      {
          valido = false;
      }
    }


      
    //Se debe validar de la otra forma (falta implementar) if(this.formFuncionario.valid){

      //console.log("=============listadoffinal1");

    if(valido && repetido){

      //console.log("=============listadoffina2");

      if(vNumIndex != null) // EDITAR
      {

        this.data.listModalDenunciado.splice(vNumIndex,1);

      }

      this.data.listModalDenunciado.push(this.denunciado);

      this.data.listModalDenunciado.forEach((element,index) => {

        element.numIndex = index;
        
      });

      this.dialogRef.close({listModalDenunciado:this.data.listModalDenunciado,cbkConoce:false});
      //console.log("=============listadoffina4");
    }else{

      if(!valido){

        openModalMensaje('Funcionario','Existen campos vacios',this.dialog);
      }

      if(!repetido){
        openModalMensaje('Funcionario','El funcionario ya se encuentra registrado',this.dialog);
      }
    

    }

    /*}else{

      openModalMensaje('Funcionario','Existen campos vacios',this.dialog);

    }*/
      
   
}

_validaEditarIndex(indexBucle:number){

  var vFlgError = false;

  if(this.flgModalEditar && indexBucle == this.data.numIndexEdit)
  {

    vFlgError = true;

  }

  return vFlgError;

}

CambioCheckPerm(flgEvent){
  this.flgPermaneceEntidad = flgEvent.checked;
}

cancelar(){
  //this.dialogRef.close({listDenunciado:this.data.listDenunciado,cbkConoce:false});
  this.dialogRef.close();
}

obtenerCargo(idCargo : number){
    let cargo = new Cargo();  
    this.listCargo.forEach(item =>{
        if(idCargo == item.ncargoId){
            cargo = item;
        }
    });
    return cargo;
}
  
llenarPersonaBusqueda(dataPersona,tipoDocumento,tipoServicio){

  this.personaBusqueda.npersonaIdtipdoc = tipoDocumento;
  
  if(tipoServicio == 1){
    this.personaBusqueda.cpersonaNrodoc = dataPersona.dcDocument;
    this.denunciado.cdenunciadoNombres = dataPersona.dcFirstName;
    this.denunciado.cdenunciadoApepaterno = dataPersona.dcLastName1;
    this.denunciado.cdenunciadoApematerno = dataPersona.dcLastName2;

  }else{

    this.personaBusqueda.cpersonaNrodoc = dataPersona.cpersonaNrodoc;
    this.personaBusqueda.npersonaId = dataPersona.npersonaId;
    this.denunciado.cdenunciadoNombres = dataPersona.cpersonaNombres;
    this.denunciado.cdenunciadoApepaterno = dataPersona.cdenunciadoApepaterno;
    this.denunciado.cdenunciadoApematerno = dataPersona.cdenunciadoApepaterno;
    
    }

  }

  obtenerPersonaXNumeroDocumento(){

    const dni =  this.denunciado.cdenunciadoNrodoc;//this.formFuncionario.get('input_nroDoc').value;
    const tipoDoc =  String(this.tipoDocumento);//this.formFuncionario.get('select_tipoDoc').value;

    if(dni != null){

      if(dni.length == 8){


        this.flgDeshabilitaValidar = true;
        this.personaService.obtenerPersona(tipoDoc,dni).subscribe(personaMa =>{
          let resultadoMa = JSON.parse(JSON.stringify(personaMa));
          console.log(personaMa);
            if(resultadoMa == null){
                this.externoService.obtenerPersonaXDniReniec(dni).subscribe(data=>{
                  let resultado = JSON.parse(JSON.stringify(data));
                  this.llenarPersonaBusqueda(resultado,tipoDoc,PERSONA_EXTERNA);
                  this.flgDeshabilitaValidar = false;
              });
            }else{
              if(resultadoMa.cdenunciadoNombres == null || resultadoMa.cdenunciadoApepaterno == null || resultadoMa.cdenunciadoApematerno){
                this.externoService.obtenerPersonaXDniReniec(dni).subscribe(data=>{
                  let resultado = JSON.parse(JSON.stringify(data));
                  this.llenarPersonaBusqueda(resultado,tipoDoc,PERSONA_EXTERNA);
                  this.flgDeshabilitaValidar = false;
                });
              }else{
                this.llenarPersonaBusqueda(resultadoMa,tipoDoc,PERSONA_MAESTRA);
                this.flgDeshabilitaValidar = false;
              }
            }
        });
      }else{
        openModalMensaje("Funcionario","Debe ingresar un DNI valido",this.dialog);

      }
    }
  
 
  }

  _setDatosModalFunc(denunciado:Denunciado)
  {

    this.denunciado = new Denunciado();

    //this.denunciado.flgEditar = flgEditar;

    console.log("=============0denunciado");
    console.log(denunciado);

    this.tipoDocumento = denunciado.tipoDocumento.ntipdocumentoId;

    this.cambioTipoDocumento();

    this.denunciado = denunciado;

    this.flgPermaneceEntidad = getBooleanFlag(denunciado.cdenunciadoFlgpermentidad);

  }

  cambioTipoDocumento(){

    console.log("===============00this.tipoDocumento");
    console.log(this.tipoDocumento);
  
    //if(this.formFuncionario.get('select_tipoDoc').value == 1){
    if(this.tipoDocumento == 1){

      this.flgApellidoPDisabled = true;
      this.flgApellidoMDisabled = true;
      this.flgNombresDisabled = true;
      this.flgNroDocDisabled = false;
    
    }else{

      this.flgApellidoPDisabled = false;
      this.flgApellidoMDisabled = false;
      this.flgNombresDisabled = false;
      this.flgNroDocDisabled = true;
      //this.flgCargoDisabled = true;
      
      this.denunciado.cdenunciadoNrodoc = "";
      this.denunciado.cdenunciadoApepaterno = "";
      this.denunciado.cdenunciadoApematerno = "";
      this.denunciado.cdenunciadoNombres = "";
      this.denunciado.cargo.ncargoId = null;
      

    }
  }

  
}
