import { FormControl } from '@angular/forms';

export interface ValidationResult {
  [key: string]: boolean;
}

export class NumberValidator {

  public static isInteger(control: FormControl): ValidationResult {
     // check if string has a dot
      const hasDot: boolean = control.value.indexOf('.') >= 0 ? true : false;
     // convert string to number
      // tslint:disable-next-line: variable-name
      let number: number = Math.floor(control.value);
     // get result of isInteger()
      const integer: boolean = Number.isInteger(number);
     // validate conditions
      const valid: boolean = !hasDot && integer && number > 0;
      console.log('isInteger > valid', hasDot, number, valid);
      if (!valid) {
          return { isInteger: true };
      }
      return null;
  }}
